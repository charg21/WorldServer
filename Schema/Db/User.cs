[Table( "user", "유저" )]
public class User
{
    [ Index( "PRIMARY" ) ]
    [ Field( "id" ) ]
    [ Comment( "식별자" ) ]
    public ulong Id;

    [ Index( "account" ) ]
    [ Field( "account", "varchar(20)" ) ]
    [ Comment( "계정" ) ]
    public string Account;

    [ Field( "password" ) ]
    public string Password;

    [ Field( "pc_id" ) ]
    public ulong PcId;
}
