rem 패킷 폴더 삭제 

set PacketDir = ..\Source\Protocol\Packet
set EnumDir = ..\Source\Protocol\Enum

rmdir /s /q ..\Source\Protocol\Packet
rmdir /s /q ..\Source\Protocol\Enum
rmdir /s /q ..\Source\Protocol\PacketDispatcher

rem 패킷 디렉토리 생성
mkdir ..\Source\Protocol\Packet
mkdir ..\Source\Protocol\Enum
mkdir ..\Source\Protocol\PacketDispatcher

rem 복사
xcopy /y ..\Tool\PacketGenerator\Protocol\cpp\Packet ..\Source\Protocol\Packet
xcopy /y ..\Tool\PacketGenerator\Protocol\cpp\Enum ..\Source\Protocol\Enum
xcopy /y ..\Tool\PacketGenerator\Protocol\cpp\PacketDispatcher ..\Source\Protocol\PacketDispatcher
rem xcopy /D 

rem 클라이언트용 작업
rem rmdir /s /q C:\dev\Unity\SeamlessServer2Client\Client\Assets\Scripts\Protocol\Packet
rem rmdir /s /q C:\dev\Unity\SeamlessServer2Client\Client2\Assets\Scripts\Protocol\Packet

rem mkdir C:\dev\Unity\SeamlessServer2Client\Client2\Assets\Scripts\Protocol\Packet
rem mkdir C:\dev\Unity\SeamlessServer2Client\Client2\Assets\Scripts\Protocol\Packet

rem xcopy /y  C:\dev\SeamlessServer2\Tool\PacketGenerator\Protocol\cs\Packet C:\dev\Unity\SeamlessServer2Client\Client2\Assets\Scripts\Protocol\Packet
rem xcopy /y  C:\dev\SeamlessServer2\Tool\PacketGenerator\Protocol\cs\Packet C:\dev\Unity\SeamlessServer2Client\Client\Assets\Scripts\Protocol\Packet
