rem Protocol generator
COPY F:\Dev\trunk\Binary\PacketGenerator\bin\Release\net8.0\ProtocolGenerator.exe  ProtocolGenerator
COPY F:\Dev\trunk\Binary\PacketGenerator\bin\Release\net8.0\ProtocolGenerator.dll  ProtocolGenerator
COPY F:\Dev\trunk\Binary\PacketGenerator\bin\Release\net8.0\CodeGenerator.dll      ProtocolGenerator


rem Orm generator
COPY F:\Dev\trunk\Binary\DbSyncableGenerator\bin\Release\net8.0\OrmGenerator.exe  OrmGenerator
COPY F:\Dev\trunk\Binary\DbSyncableGenerator\bin\Release\net8.0\OrmGenerator.dll  OrmGenerator
COPY F:\Dev\trunk\Binary\DbSyncableGenerator\bin\Release\net8.0\CodeGenerator.dll OrmGenerator