# SeamlessServer

Actor Model 컨셉 Seamless MMOPRG 서버

# Library

External Lib
C/C++ -> 일반 -> 추가 포함 디렉토리
External\rapidjson

# Directory

Executable
```Text
실행 파일
```

Schema
```Text
Packet 및 Db Orm 객체 스키마
```

Source

```Text
1. Mala.Core: 프로젝트 Core 라이브러리

2. WorldServer: 게임 월드( 이동, 시야처리, 전투 )로직 서버

```

# ORM
```C++

// DB 커넥션을 획득한다.
auto* dbConnection = dbConnectoinPool.Get();

{

    /*
    SELECT * 
    FROM SCHEMA.user
    WHERE user.id = 1488809284554719246;
    */
    UserDbModel userDbModel;
    userDbModel.BindAllColumns();
    userDbModel.SetKeyId( 1488809284554719246 );
    userDbModel.Select( dbConnection );
}


/*

User, Pc, Item 테이블 PcId로 LEFT JOIN SELECT 처리

SELECT t1.id, t1.pc_id, t1.account, t1.password, t1.mail, t2.id, t2.user_id, t2.name, t2.hp, t2.x, t2.y, t2.z, t3.owner_id, t3.root_owner_id, t3.id, t3.design_id, t3.count, t3.locked 
FROM test.User AS t1 
LEFT JOIN test.Pc AS t2 ON t1.pc_id = t2.id 
LEFT JOIN test.Item AS t3 ON t1.pc_id = t3.owner_id 
WHERE t1.account LIKE "Bot%" 
LIMIT 10

*/
{
    LeftJoin< UserDbModel, PcDbModel, ItemDbModel > leftJoin;
    leftJoin._connection = dbConnection;
    leftJoin.BindAllColumn();
    leftJoin.On().Equal< UserDbModel::PcId, PcDbModel::Id >()
            .On().Equal< UserDbModel::PcId, ItemDbModel::OwnerId >()
            .Where().Like< UserDbModel::Account >( L"Bot%" )
            .Limit( 10 );
    leftJoin.Select( dbConnection );

    /// LEFT JOIN SELECT 결과를 통해, 프라이머리키 기반으로 중복객체를 제외한 ORM객체 목록을 얻어옴 
    auto [ userList, pcList, itemList ] = leftJoin.ToList< UserDbModel::PRIMARY, PcDbModel::PRIMARY, ItemDbModel::PRIMARY >();

    std::cout << userList.size() << std::endl;
    std::cout << pcList.size() << std::endl;
    std::cout << itemList.size() << std::endl;
}

```