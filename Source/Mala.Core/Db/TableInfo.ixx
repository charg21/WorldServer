export module Mala.Db.Table;

#include "../MalaMacro.h"

import <array>;
import <string>;
import <string_view>;
import <vector>;
import <functional>;

import Mala.Db.DbTypes;
import Mala.Container.String;

NAMESPACE_BEGIN( Mala::Db )

class ColumnInfo;
class IndexInfo;
class IDbModelCache;

struct TableInfo
{
    using ColumnInfoList = std::vector< ColumnInfo* >;
    using IndexList      = std::vector< IndexInfo* >;

    size_t         _columnCapacity{};
    std::string    _name;
    std::wstring   _nameW;
    ColumnInfoList _columnInfos;
    IndexList      _indexs;
    size_t         _allFieldBitMask{};
    IDbModelCache* _dbModelCache{};

    const ColumnInfoList& GetColumnInfoList() { return _columnInfos; }
    const ColumnInfoList& GetColumnInfoList() const { return _columnInfos; }
    const IndexList& GetIndexList() { return _indexs; }
    const IndexList& GetIndexList() const { return _indexs; }
    IDbModelCache* GetDbModelCache() { return _dbModelCache; }
    IDbModelCache* GetDbModelCache() const { return _dbModelCache; }

    constexpr static size_t pkIndex{};
};

NAMESPACE_END( Mala::Db )
