import <iostream>;

import Mala.Core.Defer;
import Mala.Core.CoreTLS;
import Mala.Memory;
import Mala.Db.DbConnection;
import Mala.Db.DbModelCache;
import Mala.Db.TxContext;
import Mala.Db.TxExecutor;
import Mala.Db.Table;
import Mala.Threading.JobExecutor;

using namespace Mala::Db;

TxContext::TxContext( SourceLocationRef where )
: _from{ where }
{
}

void TxContext::Execute()
{
    switch( _txState )
    {
    case ETxContextState::Created:
        // TODO: 로그
        break;

    case TxState::WaitingToExecute:
        Execute_LogicJob();
        break;

    case TxState::Executing:
        Execute_DbJob();
        break;

    case TxState::ExecutingDb:
        Execute_Sync();
        break;

    case TxState::ExecutingSync:
        Execute_Completed();
        break;

    case ETxContextState::Completed:
    case ETxContextState::Canceled:
        break;

    case TxState::Failed:
        Execute_Failed();
        break;
    }
}

void TxContext::Cancel()
{
    _txState = TxState::Canceled;
}

void TxContext::ChangeState( TxState newState )
{
    _txState = newState;
}

void TxContext::Execute_LogicJob()
{
    if ( _txState != TxState::WaitingToExecute )
    {
        // TODO 로그
        return;
    }

    _txState = ETxContextState::Executing;

    auto self = shared_from_this();
	for ( const auto& job : _readyJobList )
		if ( !job( self ) )
			return;

    ExecuteDbJob( [ tx = std::move( self ) ]()
    {
        tx->Execute();
    } );
}

void TxContext::Execute_DbJob()
{
    if ( _txState != TxState::Executing )
    {
        // TODO 로그
        return;
    }

    _txState = ETxContextState::ExecutingDb;
    if ( !LDbConnection )
    {
        LDbConnection = GetConnection();
        LDbConnection->SetForeignKeyCheck( false );
    }

    _connection = LDbConnection;
    _connection->BeginTransaction();

    bool result = [ this ]()
    {
        bool result = true;
        while ( !_dbJobList.empty() )
        {
			auto dbJob = std::move( _dbJobList.front() );
            _dbJobList.pop_front();

            result = dbJob( _connection );
            if ( !result )
                return false;
        }

        // Pre DbJob ( In DbThread )
        size_t affectedRowCount = 0;
        for ( auto& [ queryType, model ] : _modelList )
        {
            switch ( queryType )
            {
            case EQueryType::Update:
                result = model->Update( _connection );
                if ( !result )
                    return false;

                continue;
            case EQueryType::Insert:
                result = model->Insert( _connection );
                if ( !result )
                    return false;

                continue;
            case EQueryType::Delete:
                affectedRowCount += model->Delete( _connection );
                continue;

            case EQueryType::Select: break;
            case EQueryType::Upsert: break;
            default:
                break;
            }
        }

        for ( auto& dbJob : _lazyDbJobList )
        {
            result = dbJob( _connection );
            if ( !result )
                return false;
        }

        return true;
    }();

    if ( !result )
    {
        _txState = TxState::Failed;
        _connection->Rollback();
    }
    else
    {
        _connection->Commit();
    }

    _owner->PostJob( [ tx = shared_from_this() ]() { tx->Execute(); });
}

void TxContext::Execute_Sync()
{
    if ( _txState != TxState::ExecutingDb )
    {
        // TODO 로그
        return;
    }

    _txState = TxState::ExecutingSync;
    for ( auto& [ queryType, dbModel ] : _modelList )
    {
        auto* cache = dbModel->GetTable().GetDbModelCache();

        switch ( queryType )
        {
        case EQueryType::Insert:
            cache->Add( dbModel );
            dbModel->Sync( queryType );
            break;

        case EQueryType::Update:
			cache->Update( dbModel );
            dbModel->Sync( queryType );
            break;

        case EQueryType::Delete:
            cache->Remove( dbModel );
            dbModel->Sync( queryType );
            break;

        case EQueryType::Select:
        case EQueryType::Upsert:  // TODO: 미구현 로그
			//CRASH( "Not Supported Yet" );
            break;

        default: break;
        }
    }

    Execute();
}

void TxContext::Execute_Completed()
{
    _txState = ETxContextState::Completed;

    for ( const auto& job : _endJobList )
        job();

    if ( _resultJob )
        _resultJob( true );
}

void TxContext::Execute_Failed()
{
    _txState = ETxContextState::Failed;

    _resultJob( false );
}

bool TxContext::IsCompleted() const
{
    return _txState == ETxContextState::Completed;
}

bool TxContext::IsEmpty() const
{
    return _txState == ETxContextState::Created;
}

bool TxContext::IsFailed() const
{
    return _txState == ETxContextState::Failed;
}

bool TxContext::IsCanceled() const
{
    return _txState == ETxContextState::Canceled;
}

void TxContext::SetDbJobExecutor( DbJobExecutor&& executor )
{
    ExecuteDbJob = executor;
}

TxContextPtr TxContext::New( SourceLocationRef where )
{
    return MakeShared< TxContext >( where );
}

void TxContext::SetDbConnectionAllocator( DbConnectionAllocator&& allocator )
{
    GetConnection = allocator;
}

void TxContext::SetDbConnectionDeallocator( DbConnectionDeleter&& deleter )
{
    ReleaseConnection = deleter;
}
