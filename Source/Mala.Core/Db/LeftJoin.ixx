export module Mala.Db.LeftJoin;

#include "../MalaMacro.h"

import <array>;
import <string>;
import <string_view>;
import <vector>;
import <functional>;
import <iostream>;
import <tuple>;
import <format>;

#define UTF16
#ifdef UTF16
using QueryBuffer = std::wstring;
using QueryBufferView = std::wstring_view;
#define _U(STR) (L##STR)
#else
using QueryBuffer = std::string;
using QueryBufferView = std::string_view;
#define _U(STR) (STR)
#endif // UNICODE

import Mala.Core.TypeList;
import Mala.Core.Types;
import Mala.Container;

import Mala.Db.Column;
import Mala.Db.ColumnInfo;
import Mala.Db.DbConnection;
import Mala.Db.DbModel;
import Mala.Db.Table;
import Mala.Db.DbTypes;

NAMESPACE_BEGIN( Mala::Db )

using namespace Mala::Container;

template< typename Join >
class LeftJoinToList;

/// <summary>
/// N개의 테이블간 LeftJoin
/// </summary>
template< typename... Types > requires ( DbModelType< Types > && ... )
struct LeftJoin
{
    friend class QueryBuilder;
    friend struct DbModel;

    using DbModelTypeList = TypeList< Types... >;
    using DbModelTuple = std::tuple< Types... >;
    constexpr static size_t Capacity = Length< DbModelTypeList >::value;

    using ColumnWithInfo = std::pair< Mala::Db::Column*, const Mala::Db::ColumnInfo* >;
    using BindColumnList = std::vector< ColumnWithInfo >;
    using BindColumnLists = std::array< BindColumnList, Capacity >;
    using QueryBufferViews = std::array< QueryBufferView, 10 >;

    static_assert( std::tuple_size< DbModelTuple >::value > 1, "Join 둘 이상의 테이블에 사용" );

    /// <summary>
    /// 필수
    /// </summary>
    using T1 = TypeAt< DbModelTypeList, 0 >::Result;

public:
    /// <summary>
    /// Join에 사용되는 Db모델들 </summary>
    /// </summary>
    DbModelTuple _models;

    /// <summary>
    /// On조건 쿼리문
    /// </summary>
    QueryBuffer _onCondition{};

    /// <summary>
    /// Where조건 쿼리문
    /// </summary>
    QueryBuffer _whereCondition{};

    /// <summary>
    /// Group By 조건 쿼리문
    /// </summary>
    QueryBuffer _groupByCondition{};

    /// <summary>
    /// Limit 조건 값
    /// </summary>
    size_t _limit{};

    /// <summary>
    /// 마지막 실행된 연산자
    /// </summary>
    EConditionOperator _lastConditionOperator{ EConditionOperator::On };

    /// <summary>
    /// Db 커넥션
    /// </summary>
    DbConnection* _connection{};

    /// <summary>
    /// 조인에 사용시 바인딩된 필드 목록
    /// </summary>
    BindColumnLists _bindFieldLists;

    /// <summary>
    /// 컬럼 동기화시 사용되는 커서
    /// </summary>
    int _columnCount = 0;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    LeftJoin() = default;

    template< typename T > requires Contains< DbModelTypeList, T >
    [[ nodiscard ]] T& Get()
    {
        return std::get< IndexOfV< DbModelTypeList, T > >( _models );
    }

    template< typename T > requires Contains< DbModelTypeList, T >
    [[ nodiscard ]] consteval T& Get() const
    {
        return std::get< IndexOfV< DbModelTypeList, T > >( _models );
    }

    template< typename T > requires Contains< DbModelTypeList, T >
    [[ nodiscard ]] T& As()
    {
        return std::get< IndexOfV< DbModelTypeList, T > >( _models );
    }

    template< typename T > requires Contains< DbModelTypeList, T >
    [[ nodiscard ]] consteval T& As() const
    {
        return std::get< IndexOfV< DbModelTypeList, T > >( _models );
    }

    template< typename T > requires Contains< DbModelTypeList, T >
    constexpr bool IsNull()
    {
        return true;
    }

    //template< typename T >
    //constexpr bool IsNull() = delete
    //{
    //    return true;
    //}

    template< typename T > requires ( Contains< DbModelTypeList, T > )
    BindColumnList& GetBindingColumns()
    {
        return _bindFieldLists[ IndexOfV< DbModelTypeList, T > ];
    }

    template< typename T > requires ( Contains< DbModelTypeList, T > )
    const BindColumnList& GetBindingColumns() const
    {
        return _bindFieldLists[ IndexOfV< DbModelTypeList, T > ];
    }

    template< typename T > requires ( Contains< DbModelTypeList, T > )
    [[ nodiscard ]] static constexpr QueryBufferView Alias()
    {
        constexpr static QueryBufferViews views
        {
            _U( "t1" ),
            _U( "t2" ),
            _U( "t3" ),
            _U( "t4" ),
            _U( "t5" ),
            _U( "t6" ),
            _U( "t7" ),
            _U( "t8" ),
            _U( "t9" ),
            _U( "t10" )
        };

        return views[ IndexOfV< DbModelTypeList, T > ];
    }

    template< typename T > requires ( Contains< DbModelTypeList, T > )
    [[ nodiscard ]] static constexpr QueryBufferView AliasDot()
    {
        constexpr static QueryBufferViews views
        {
            _U( "t1." ),
            _U( "t2." ),
            _U( "t3." ),
            _U( "t4." ),
            _U( "t5." ),
            _U( "t6." ),
            _U( "t7." ),
            _U( "t8." ),
            _U( "t9." ),
            _U( "t10." )
        };

        return views[ IndexOfV< DbModelTypeList, T > ];
    }

    template< typename T > requires ( Contains< DbModelTypeList, T > )
    [[ nodiscard ]] BindColumnList& GetBindFields()
    {
        return _bindFieldLists[ IndexOf< DbModelTypeList, T >::value ];
    }

    template< typename T >
    [[ nodiscard ]] QueryBuffer MakeBindingString()
    {
        QueryBuffer _query;

        for ( Column* col : Get< T >()._columnList )
        {
            if ( !Get< T >().IsBindingColumn( col->_info->_columnNo ) )
                continue;

            _query += AliasDot< T >();
            _query += col->_info->_nameW;
            _query += L", ";

            GetBindFields< T >().emplace_back( col, col->_info );
        }

        return std::move( _query );
    }

    QueryBuffer ToSelectQuery()
    {
        /*
        SELECT t1.col1, t2.col2
        FROM Table1 AS t1
        LEFT JOIN Table2 AS t2
        on t1.col1 = t2.col2
        */
        QueryBuffer _query;
        _query.reserve( 1024 );

        _query += L"SELECT ";
        ForEachMakeBindString< 0 >::Execute( _models, _query, *this );

        _query.resize( _query.size() - 2 );
        _query += std::format( L" FROM {}.{} AS {}",
            _connection->_schemaW,
            Get< T1 >().GetTableName(),
            Alias< T1 >() );

        if ( HasOnCondition() )
            _query += _onCondition;

        if ( HasWhereCondition() )
            _query += _whereCondition;

        if ( HasGroupByCondition() )
            _query += _groupByCondition;

        if ( HasLimitCondition() )
            _query += std::format( L" LIMIT {}", _limit );

        return std::move( _query );
    }

    QueryBuffer MakeDeleteQuery()
    {
        /*
DELETE t1
FROM HUMAN_TABLE t1
LEFT JOIN PHONE_TABLE t2
ON t1.human_id = t2.human_id
WHERE t2.phone_number IS NULL;
        */
        QueryBuffer _query;
        _query.reserve( 1024 );

        _query += L"DELETE ";
        ForEachMakeDeleteAlias< 0 >::Execute( _models, _query, *this );

        _query.push_back( L' ' );
        _query += std::format( L" FROM {}.{} AS {}",
            _connection->_schemaW,
            Get< T1 >().GetTableName(),
            Alias< T1 >() );

        if ( HasOnCondition() )
            _query += _onCondition;

        if ( HasWhereCondition() )
            _query += _whereCondition;

        if ( HasGroupByCondition() )
            _query += _groupByCondition;

        if ( HasLimitCondition() )
            _query += std::format( L" LIMIT {}", _limit );

        return std::move( _query );
    }

    template< std::size_t IndexInfo = 0 >
    struct ForEachMakeBindString
    {
        static void Execute( const DbModelTuple& t, QueryBuffer& buffer, LeftJoin& join )
        {
            if constexpr ( IndexInfo < std::tuple_size_v< DbModelTuple > )
            {
                buffer += join.MakeBindingString< TypeAt< DbModelTypeList, IndexInfo >::Result >();

                ForEachMakeBindString< IndexInfo + 1 >::Execute( t, buffer, join );
            }
            else // OnFinsih
            {
                //buffer.pop_back();
                //buffer.pop_back();
            }
        }
    };

    template< std::size_t IndexInfo = 0 >
    struct ForEachMakeDeleteAlias
    {
        static void Execute( const DbModelTuple& t, QueryBuffer& buffer, LeftJoin& join )
        {
            if constexpr ( IndexInfo < std::tuple_size_v< DbModelTuple > )
            {
                buffer += join.Alias< TypeAt< DbModelTypeList, IndexInfo >::Result >();
                buffer += L", ";

                ForEachMakeDeleteAlias< IndexInfo + 1 >::Execute( t, buffer, join );
            }
            else // OnFinsih
            {
                buffer.pop_back();
                buffer.pop_back();
            }
        }
    };

    template< std::size_t IndexInfo = 0 >
    struct ForEachBindAllColumn
    {
        static void Execute( const DbModelTuple& t, LeftJoin& join )
        {
            if constexpr ( IndexInfo < std::tuple_size_v< DbModelTuple > )
            {
                join.Get< TypeAt< DbModelTypeList, IndexInfo >::Result >().BindAllColumn();

                ForEachBindAllColumn< IndexInfo + 1 >::Execute( t, join );
            }
        }
    };

    void BindAllColumn()
    {
        ForEachBindAllColumn< 0 >::Execute( _models, *this );
    }

    /// <summary>
    /// 셀렉트 쿼리문을 실행한다.
    /// </summary>
    [[ nodiscard ]] constexpr EDbResult Select()
    {
		if ( !_connection )
			return EDbResult::QueryFailure;

        QueryBuffer joinQuery = ToSelectQuery();
        if ( !_connection->Query( joinQuery ) )
        {
            std::wcout << joinQuery << std::endl;
            return EDbResult::QueryFailure;
        }

        if ( !FetchRow() )
            return EDbResult::NoData;

        return EDbResult::Success;
    }

    /// <summary>
    ///
    /// </summary>
    [[ nodiscard ]] constexpr EDbResult Delete( Mala::Db::DbConnection* connection )
    {
        /*
DELETE t1
FROM A_TABLE t1
LEFT JOIN B_TABLE t2
ON t1.id = t2.a_id
WHERE t1.id 2;
        */
        if ( !connection )
            return EDbResult::QueryFailure;

        _connection = connection;
        QueryBuffer joinDeleteQuery = MakeDeleteQuery();
        if ( !connection->Query( joinDeleteQuery ) )  /// 쿼리가 성공 여부
        {
            std::wcout << joinDeleteQuery << std::endl;
            return EDbResult::QueryFailure;
        }

        if ( !connection->_affectedRowCount ) /// 지운 데이터 보유 여부?
            return EDbResult::NoData;

        return EDbResult::Success;
    }

    [[ nodiscard ]] constexpr bool HasOnCondition() const
    {
        return _onCondition.size() > 4; // " on "
    }

    [[ nodiscard ]] constexpr bool HasGroupByCondition() const
    {
        return _onCondition.size() > 10; // " group by "
    }

    [[ nodiscard ]] constexpr bool HasWhereCondition() const
    {
        return _onCondition.size() > 7; // " where "
    }

    [[ nodiscard ]] constexpr bool HasLimitCondition() const
    {
        return _limit > 0;
    }

    template< typename T >
    int SyncBindingField()
    {
        auto row = _connection->GetRow();
        if ( !row[ _columnCount ] )
            return _columnCount;

        for ( auto& [ column, info ] : GetBindingColumns< T >() )
        {
            if ( !row[ _columnCount ] )
            {
                _columnCount += 1;
                continue;
            }

            if ( info->_isInPrimaryIndex || info->_isInSecondaryIndex )
                Get< T >().SetKeyColumn( info->_columnNo );

            column->_var.Set( row[ _columnCount ] );
            _columnCount += 1;
        }

        return _columnCount;
    }

    template< std::size_t IndexInfo = 0 >
    struct ForEachSyncBindingField
    {
        static void Execute( const DbModelTuple& t, LeftJoin& join )
        {
            if constexpr ( IndexInfo < std::tuple_size_v< DbModelTuple > )
            {
                join.SyncBindingField< TypeAt< DbModelTypeList, IndexInfo >::Result >();

                ForEachSyncBindingField< IndexInfo + 1 >::Execute( t, join );
            }
        }
    };

    [[ nodiscard ]] constexpr bool FetchRow()
    {
        if ( !_connection->Fetch() )
            return false;

		ForEachSyncBindingField< 0 >::Execute( _models, *this );
        _columnCount = 0;

        return true;
    }

    struct LeftJoinWhere
    {
        LeftJoinWhere( LeftJoin& join )
        : _join{ join }
        {
            join._onCondition += _U( " WHERE " );
            join._lastConditionOperator = EConditionOperator::Where;
        }

        template< DbColumnDerived Field, typename Condition >
        LeftJoin& Equal( Condition&& condition )
        {
            return _join._CompareCondition< Field >( _U( " = " ), std::forward< Condition >( condition ) );
        }

        template< DbColumnDerived Field, typename Condition >
        LeftJoin& NotEqual( Condition&& condition )
        {
            return _join._CompareCondition< Field >( _U( " != " ), std::forward< Condition >( condition ) );
        }

        template< DbColumnDerived Field, typename Condition >
        LeftJoin& Like( Condition&& condition )
        {
            return _join._CompareCondition< Field >( _U( " LIKE " ), std::forward< Condition >( condition ) );
        }

        template< DbColumnDerived Field >
        LeftJoin& IsNull()
        {
            return _join._CompareCondition< Field >( _U( " IS NULL" ), _U( "" ) );
        }

        template< DbColumnDerived Field >
        LeftJoin& IsNotNull()
        {
            return _join._CompareCondition< Field >( _U( " IS NOT NULL" ), _U( "" ) );
        }

        template< DbColumnDerived Field, typename Condition >
        LeftJoin& In( Condition&& notInCondition )
        {
            return _join._CompareCondition< Field >( _U( " IN " ), _U( "()" ) );
        }

        template< DbColumnDerived Field, typename Condition >
        LeftJoin& NotIn( Condition&& notInCondition )
        {
            return _join._CompareCondition< Field >( _U( " NOT IN " ), _U( "()" ) );
        }

        LeftJoin& _join;
    };

    struct LeftJoinAnd
    {
        LeftJoinAnd( LeftJoin& join )
        : _join{ join }
        {
            //join._lastConditionOperator = EConditionOperator::And;
        }

        LeftJoin& _join;
    };

    struct LeftJoinOn
    {
        LeftJoinOn( LeftJoin& join )
        : _join{ join }
        {
            join._lastConditionOperator = EConditionOperator::On;
        }

        template< DbColumnDerived T1Field, DbColumnDerived T2Field >
        LeftJoin& Equal()
        {
            using DbInfo1 = typename T1Field::DbType::DbInfoType;
            using DbInfo2 = typename T2Field::DbType::DbInfoType;

            //static_assert( DbInfo1::columnTypes[ T1Field::no ] == DbInfo2::columnTypes[ T2Field::no ],
            //    "LeftJoin을 사용하는 두 컬럼의 타입이 동일해야 합니다." );

            _join._onCondition += std::format( _U( " LEFT JOIN {}.{} AS {} ON " ),
                _join._connection->_schemaW,
                _join.Get< typename T2Field::DbType >().GetTableName(),
                _join.Alias< typename T2Field::DbType >() );

            _join._CompareTwoField< T1Field, T2Field >( _U( " = " ) );
            return _join;
        }

        LeftJoin& _join;
    };

    struct LeftJoinLimit
    {
        LeftJoinLimit( LeftJoin& join, int limitCount )
        : _join{ join }
        {
            join._limit = limitCount;
            join._lastConditionOperator = EConditionOperator::Max;
        }

        LeftJoin& _join;
    };

    LeftJoin& Limit( int n ) { LeftJoinLimit( *this, n ); return *this; }
    LeftJoinAnd And() { return LeftJoinAnd( *this ); }
    LeftJoinOn On() { return LeftJoinOn( *this ); }
    LeftJoinWhere Where() { return LeftJoinWhere( *this ); }

    template< DbIndexDerived... TDbModelIndex >
    auto ToList()
    {
        return LeftJoinToList{ *this }.ToList< TDbModelIndex... >();
    }

private:
    [[ nodiscard ]] QueryBuffer& GetConditionString()
    {
        switch ( _lastConditionOperator )
        {
        case EConditionOperator::On:
            return _onCondition;
        case EConditionOperator::Where:
            return _whereCondition;
        case EConditionOperator::GroupBy:
            return _groupByCondition;
        }

        return _onCondition;
    }

    template< DbColumnDerived T1Field, DbColumnDerived T2Field >
    LeftJoin& _CompareTwoField( QueryBufferView operation )
    {
        QueryBuffer& conditionStr = GetConditionString();

        conditionStr += AliasDot< typename T1Field::DbType >();
        conditionStr += T1Field::nameViewW;
        conditionStr += operation;
        conditionStr += AliasDot< typename T2Field::DbType >();
        conditionStr += T2Field::nameViewW;

        return *this;
    }

    template< DbColumnDerived Field, typename Condition >
    LeftJoin& _CompareCondition( QueryBufferView operation, Condition&& condition )
    {
        QueryBuffer& conditionStr = GetConditionString();
        conditionStr += AliasDot< typename Field::DbType >();
        conditionStr += Field::nameViewW;
        conditionStr += operation;

        if constexpr ( std::is_same_v< std::remove_cvref< Condition >::type, QueryBuffer > ||
                       std::is_same_v< std::remove_cvref< Condition >::type, String > ||
                       std::is_same_v< std::remove_cvref< Condition >::type, QueryBufferView > )
        {
            conditionStr += '\"';
            conditionStr += condition;
            conditionStr += '\"';
        }
        else if constexpr ( std::is_same_v< std::remove_cvref< Condition >::type, std::string > ||
                            std::is_same_v< std::remove_cvref< Condition >::type, std::string_view > )
        {
            //conditionStr += '\"';
            //conditionStr += condition;
            //conditionStr += '\"';
        }
        else if constexpr ( std::is_same_v< std::decay_t< Condition >, const wchar_t* > )
        {
            conditionStr += '\"';
            conditionStr += std::wstring_view{ condition };
            conditionStr += '\"';
        }
        else
        {
            conditionStr += std::to_wstring( condition );
        }

        return *this;
    }

};

template< typename Join >
class LeftJoinToList
{
    Join& _join;

public:
    LeftJoinToList( Join& join )
    : _join( join )
    {
    }

public:
    template< DbIndexDerived TDbModelIndex >
    auto ToList()
    {
        using T = typename TDbModelIndex::DbType;

        HashSet< decltype( _join.Get< T >().GetIndexKey< TDbModelIndex >() ) > tDbModelFilter;
        TDbModelPtrVector< T > tDbModelList;

        do
        {
            if ( tDbModelFilter.emplace( _join.Get< T >().GetIndexKey< TDbModelIndex >() ).second )
            {
                auto& dbModel = tDbModelList.emplace_back( T::GetFactory() );
                _join.Get< T >().CopyTo( *dbModel );
            }
        } while ( _join.FetchRow() );

        return std::move( tDbModelList );
    }

    template< DbIndexDerived TDbModelIndex1, DbIndexDerived TDbModelIndex2 >
    auto ToList()
    {
        using TDbModel1 = typename TDbModelIndex1::DbType;
        using TDbModel2 = typename TDbModelIndex2::DbType;

        HashSet< decltype( _join.Get< TDbModel1 >().GetIndexKey< TDbModelIndex1 >() ) > tDbModelFilter1;
        HashSet< decltype( _join.Get< TDbModel2 >().GetIndexKey< TDbModelIndex2 >() ) > tDbModelFilter2;
        TDbModelPtrVector< TDbModel1 > tDbModelList1;
        TDbModelPtrVector< TDbModel2 > tDbModelList2;

        do
        {
            if ( tDbModelFilter1.emplace( _join.Get< TDbModel1 >().GetIndexKey< TDbModelIndex1 >() ).second )
            {
                auto& dbModel = tDbModelList1.emplace_back( TDbModel1::GetFactory() );
                _join.Get< TDbModel1 >().CopyTo( *dbModel );
            }
            if ( tDbModelFilter2.emplace( _join.Get< TDbModel2 >().GetIndexKey< TDbModelIndex2 >() ).second )
            {
                auto& dbModel = tDbModelList2.emplace_back( TDbModel2::GetFactory() );
                _join.Get< TDbModel2 >().CopyTo( *dbModel );
            }
        } while ( _join.FetchRow() );

        return std::tuple< decltype( tDbModelList1 ), decltype( tDbModelList2 ) >
        {
            std::move( tDbModelList1 ),
            std::move( tDbModelList2 )
        };
    }

    template< DbIndexDerived TDbModelIndex1, DbIndexDerived TDbModelIndex2, DbIndexDerived TDbModelIndex3 >
    auto ToList()
    {
        using TDbModel1 = typename TDbModelIndex1::DbType;
        using TDbModel2 = typename TDbModelIndex2::DbType;
        using TDbModel3 = typename TDbModelIndex3::DbType;

        TDbModelPtrVector< TDbModel1 > tDbModelList1;
        TDbModelPtrVector< TDbModel2 > tDbModelList2;
        TDbModelPtrVector< TDbModel3 > tDbModelList3;
        HashSet< decltype( _join.Get< TDbModel1 >().GetIndexKey< TDbModelIndex1 >() ) > tDbModelFilter1;
        HashSet< decltype( _join.Get< TDbModel2 >().GetIndexKey< TDbModelIndex2 >() ) > tDbModelFilter2;
        HashSet< decltype( _join.Get< TDbModel3 >().GetIndexKey< TDbModelIndex3 >() ) > tDbModelFilter3;

        do
        {
            if ( tDbModelFilter1.emplace( _join.Get< TDbModel1 >().GetIndexKey< TDbModelIndex1 >() ).second )
            {
                auto& dbModel = tDbModelList1.emplace_back( TDbModel1::GetFactory() );
                _join.Get< TDbModel1 >().CopyTo( *dbModel );
            }
            if ( tDbModelFilter2.emplace( _join.Get< TDbModel2 >().GetIndexKey< TDbModelIndex2 >() ).second )
            {
                auto& dbModel = tDbModelList2.emplace_back( TDbModel2::GetFactory() );
                _join.Get< TDbModel2 >().CopyTo( *dbModel );
            }
            if ( tDbModelFilter3.emplace( _join.Get< TDbModel3 >().GetIndexKey< TDbModelIndex3 >() ).second )
            {
                auto& dbModel = tDbModelList3.emplace_back( TDbModel3::GetFactory() );
                _join.Get< TDbModel3 >().CopyTo( *dbModel );
            }
        } while ( _join.FetchRow() );

        return std::tuple< decltype( tDbModelList1 ), decltype( tDbModelList2 ), decltype( tDbModelList3 ) >
        {
            std::move( tDbModelList1 ),
            std::move( tDbModelList2 ),
            std::move( tDbModelList3 )
        };
    }

    template< DbIndexDerived TDbModelIndex1, DbIndexDerived TDbModelIndex2, DbIndexDerived TDbModelIndex3, DbIndexDerived TDbModelIndex4 >
    auto ToList()
    {
        using TDbModel1 = typename TDbModelIndex1::DbType;
        using TDbModel2 = typename TDbModelIndex2::DbType;
        using TDbModel3 = typename TDbModelIndex3::DbType;
        using TDbModel4 = typename TDbModelIndex4::DbType;

        TDbModelPtrVector< TDbModel1 > tDbModelList1;
        TDbModelPtrVector< TDbModel2 > tDbModelList2;
        TDbModelPtrVector< TDbModel3 > tDbModelList3;
        TDbModelPtrVector< TDbModel4 > tDbModelList4;
        HashSet< decltype( _join.Get< TDbModel1 >().GetIndexKey< TDbModelIndex1 >() ) > tDbModelFilter1;
        HashSet< decltype( _join.Get< TDbModel2 >().GetIndexKey< TDbModelIndex2 >() ) > tDbModelFilter2;
        HashSet< decltype( _join.Get< TDbModel3 >().GetIndexKey< TDbModelIndex3 >() ) > tDbModelFilter3;
        HashSet< decltype( _join.Get< TDbModel4 >().GetIndexKey< TDbModelIndex4 >() ) > tDbModelFilter4;

        do
        {
            if ( tDbModelFilter1.emplace( _join.Get< TDbModel1 >().GetIndexKey< TDbModelIndex1 >() ).second )
            {
                auto& dbModel = tDbModelList1.emplace_back( TDbModel1::GetFactory() );
                _join.Get< TDbModel1 >().CopyTo( *dbModel );
            }
            if ( tDbModelFilter2.emplace( _join.Get< TDbModel2 >().GetIndexKey< TDbModelIndex2 >() ).second )
            {
                auto& dbModel = tDbModelList2.emplace_back( TDbModel2::GetFactory() );
                _join.Get< TDbModel2 >().CopyTo( *dbModel );
            }
            if ( tDbModelFilter3.emplace( _join.Get< TDbModel3 >().GetIndexKey< TDbModelIndex3 >() ).second )
            {
                auto& dbModel = tDbModelList3.emplace_back( TDbModel3::GetFactory() );
                _join.Get< TDbModel3 >().CopyTo( *dbModel );
            }
            if ( tDbModelFilter4.emplace( _join.Get< TDbModel4 >().GetIndexKey< TDbModelIndex4 >() ).second )
            {
                auto& dbModel = tDbModelList4.emplace_back( TDbModel4::GetFactory() );
                _join.Get< TDbModel4 >().CopyTo( *dbModel );
            }
        } while ( _join.FetchRow() );

        return std::tuple< decltype( tDbModelList1 ), decltype( tDbModelList2 ), decltype( tDbModelList3 ), decltype( tDbModelList4 ) >
        {
            std::move( tDbModelList1 ),
            std::move( tDbModelList2 ),
            std::move( tDbModelList3 ),
            std::move( tDbModelList4 )
        };
    }
};


NAMESPACE_END( Mala::Db )
