export module Mala.Db.MySql;

#include "mysql/mysql.h"
#include "../MalaMacro.h"

#pragma comment( lib , "libmysql" )

NAMESPACE_BEGIN( Mala::Db )

namespace MySql
{
    using Row = ::MYSQL_ROW;
    using Res = ::MYSQL_RES;
    using Conn = ::MYSQL;

    auto Init( Conn* mysql ) -> Conn*
    {
        return ::mysql_init( mysql );
    }

    auto DisableSslMode( Conn* mysql ) -> bool
    {
        unsigned int opt_ssl = SSL_MODE_DISABLED;
        return ::mysql_options( mysql, MYSQL_OPT_SSL_MODE, &opt_ssl ) == 0;
    }

    auto Connect( Conn* mysql, const char* host, const char* user, const char* passwd, const char* db, unsigned int port, const char* unix_socket, unsigned long clientflag ) -> Conn*
    {
        return ::mysql_real_connect( mysql, host, user, passwd, db, port, unix_socket, clientflag );
    }

    auto Error( Conn* mysql ) -> const char*
    {
        return ::mysql_error( mysql );
    }

    auto ErrorNo( Conn* mysql ) -> unsigned int
    {
        return ::mysql_errno( mysql );
    }

    auto Close( Conn* mysql ) -> void
    {
        ::mysql_close( mysql );
    }

    auto Query( Conn* mysql, const char* q, int length ) -> int
    {
        return ::mysql_real_query( mysql, q, length );
    }

    auto StoreResult( Conn* mysql ) -> Res*
    {
        return ::mysql_store_result( mysql );
    }

    auto FetchRow( Res* res ) -> Row
    {
        return ::mysql_fetch_row( res );
    }

    auto NumRows( Res* res ) -> uint64_t
    {
        return ::mysql_num_rows( res );
    }

    auto NumFields( Res* res ) -> unsigned int
    {
        return ::mysql_num_fields( res );
    }

    auto FreeResult( Res* res ) -> void
    {
        ::mysql_free_result( res );
    }

    auto FetchField( Res* res ) -> MYSQL_FIELD*
    {
        return ::mysql_fetch_field( res );
    }

    auto FetchFields( Res* res ) -> MYSQL_FIELD*
    {
        return ::mysql_fetch_fields( res );
    }

    auto AffectedRows( Conn* mysql ) -> uint64_t
    {
        return ::mysql_affected_rows( mysql );
    }

    auto AutoCommit( Conn* mysql, bool auto_mode ) -> bool
    {
        return ::mysql_autocommit( mysql, auto_mode );
    }

    auto Commit( Conn* mysql ) -> bool
    {
        return ::mysql_commit( mysql );
    }

    auto Rollback( Conn* mysql ) -> bool
    {
        return ::mysql_rollback( mysql );
    }

    auto SetCharacterSet( Conn* mysql, const char* csname ) -> int
    {
        return ::mysql_set_character_set( mysql, csname );
    }
}

NAMESPACE_END( Mala::Db )
