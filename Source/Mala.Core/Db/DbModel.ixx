export module Mala.Db.DbModel;

#include "../CoreMacro.h"

import <memory>;

import Mala.Core.Types;
import Mala.Core.Variant;
import Mala.Container;
import Mala.Container.String;
import Mala.Db.DbTypes;
import Mala.Db.DbConnection;

using namespace Mala::Core;
using namespace Mala::Container;

NAMESPACE_BEGIN( Mala::Db )

struct QueryBuildable{};
struct TransactionExecutable{};
struct NullColumn{};
struct NullIndex{};

class  DbConnection;
struct DbModel;
struct TableInfo;
struct Column;
struct IndexInfo;

/// <summary>
/// DbType 컨셉트
/// </summary>
template< typename T >
concept DbModelType = std::is_base_of_v< DbModel, T >;
template< typename T >
concept DbColumnDerived = std::is_base_of_v< NullColumn, T >;
template< typename T >
concept DbIndexDerived = std::is_base_of_v< NullIndex, T >;

/// <summary>
/// Db모델 포인터 타입 정의
/// </summary>
template< DbModelType T >
using TDbModelPtr = std::shared_ptr< T >;
template< DbModelType T >
using TDbModelAtomicPtr = Atomic< TDbModelPtr< T > >;
template< DbModelType T >
using TDbModelPtrDeque = Deque< TDbModelPtr< T > >;
template< DbModelType T >
using TDbModelPtrVector = Vector< TDbModelPtr< T > >;

/// <summary>
/// Db모델 Select 결과 타입 정의
/// 1. 쿼리 실패 => std::nullopt 반환
/// 2. 쿼리 성공 => 조회 결과 0건, nullptr OR vector.size() == 0
/// 3. 쿼리 성공 => 조회 결과 N건, not null OR vector.size() > 0
/// </summary>
template< DbModelType T >
using DbSelectResult = Optional< TDbModelPtr< T > >;
template< DbModelType T >
using DbSelectListResult = Optional< TDbModelPtrVector< T > >;

/// <summary>
/// Db 동기화 객체
/// </summary>
struct DbModel : public QueryBuildable
{
public:
    using ColumnList = Vector< Column* >;
    using DbModelType = DbModel;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    DbModel( TableInfo& table );
    DbModel( DbModel& root );
    DbModel( DbModel&& root ) noexcept;

    /// <summary>
    /// 소멸자
    /// </summary>
    virtual ~DbModel() = default;

    /// <summary>
    /// Db 동작
    /// </summary>
    MALA_NODISCARD bool Delete( DbConnection* connection );
    MALA_NODISCARD bool Insert( DbConnection* connection );
    MALA_NODISCARD i64  Update( DbConnection* connection );
    MALA_NODISCARD bool Select( DbConnection* connection );
    MALA_NODISCARD void Upsert( DbConnection* connection ) {}

    void ResetUpdateFlag();
    void SetUpdateColumn( i32 columnNo );
    void SetBindColumn( i32 columnNo );
    void SetKeyColumn( i32 columnNo );
    void SetUpdateAndSetupKeyColumn( i32 columnNo );

    MALA_NODISCARD bool IsUpdateColumn( i32 no ) const;
    MALA_NODISCARD bool IsBindingColumn( i32 no ) const;
    MALA_NODISCARD bool IsKeySetupColumn( i32 no ) const;
    MALA_NODISCARD bool IsValidIndex( const IndexInfo* index ) const;
    MALA_NODISCARD bool HasUpdateColumn() const;
    MALA_NODISCARD bool HasBindingColumn() const;
    MALA_NODISCARD bool HasKeySetupColumn() const;

    /// <summary>
    /// 동기화 한다
    /// </summary>
    virtual void Sync( EQueryType queryType );

    /// <summary>
    /// 객체를 초기 상태로 되돌린다
    /// </summary>
    void Reset();
    void BindAllColumn();
    void ResetBinding();

    const std::string& GetTableNameA();
    const std::wstring& GetTableName() const;
    const TableInfo& GetTable();
    const TableInfo& GetTable() const;
    const Column& GetColumn( i32 index ) const;
    const Variant& GetColumnVar( i32 index ) const;
    template< DbColumnDerived TColumn >
    const Column& GetColumn() const;
    template< DbIndexDerived TDbIndex >
    auto GetColumns() const;
    template< DbIndexDerived TDbIndex >
    constexpr auto GetIndexKey();
    template< DbIndexDerived TDbIndex >
    constexpr auto GetIndexRefKey();
    template< DbIndexDerived TDbIndex >
    constexpr auto GetCacheIndex();

    void Clone();
    void CopyTo( DbModel& target ) const;
    void CopyToOnlyUpdateColumn( DbModel& target ) const;
    const IndexInfo* GetFirstValidIndex() const;

    /// <summary>
    /// 컬럼 목록
    /// </summary>
    ColumnList _columnList{};

    /// <summary>
    /// 테이블 정보
    /// </summary>
    const TableInfo& _table;

    /// <summary>
    /// 컬럼 갱신 플래그
    /// </summary>
    Flag _columnUpdated{};

    /// <summary>
    /// DB 데이터 세팅 플래그
    /// </summary>
    Flag _columnBinding{};

    /// <summary>
    /// 키 세팅 플래그
    /// </summary>
    Flag _columnKeySetup{};

    /// <summary>
    /// 원본 객체
    /// </summary>
    DbModelPtr _root;
};

NAMESPACE_END( Mala::Db )

using namespace Mala::Db;

template< DbColumnDerived TColumn >
inline const Column& DbModel::GetColumn() const
{
    return GetColumn( TColumn::no );
}

template< std::size_t Index, std::size_t Max, typename TDbIndex >
struct ForEachEmplaceBack
{
    static void EmplaceBack( const DbModel& model, DbModel::ColumnList& list )
    {
        if constexpr ( Index < Max )
        {
            list.emplace_back( model._columnList[ TDbIndex::cols[ Index ] ] );
            ForEachEmplaceBack< Index + 1, Max, TDbIndex >::EmplaceBack( model, list );
        }
    }
};

template< DbIndexDerived TDbIndex >
auto DbModel::GetColumns() const
{
    DbModel::ColumnList list;
    list.reserve( ArraySize( TDbIndex::cols ) );

    ForEachEmplaceBack< 0, ArraySize( TDbIndex::cols ), TDbIndex >::EmplaceBack( *this, list );

    return std::move( list );
}

template< typename T, std::size_t N >
constexpr std::size_t ArrSize( T( & )[ N ] )
{
    return N;
}

/// <summary>
/// 원본 객체가 변경되면 키의 값이 변경되기 때문에 알아서 잘 써야함.
/// </summary>
template< DbIndexDerived TDbIndex >
constexpr auto DbModel::GetIndexRefKey()
{
    using namespace Mala::Core;

    constexpr auto arrSize = ArrSize( TDbIndex::cols );
    auto& cols{ _columnList };

    auto& col1Var{ GetColumnVar( TDbIndex::cols[ 0 ] ) };
    if constexpr ( 1 == arrSize )
        return std::make_tuple< Variant& >( col1Var );

    auto& col2Var{ GetColumnVar( TDbIndex::cols[ 1 ] ) };
    if constexpr ( 2 == arrSize )
        return std::make_tuple< Variant&, Variant& >( col1Var, col2Var );

    auto& col3Var{ GetColumnVar( TDbIndex::cols[ 2 ] ) };
    if constexpr ( 3 == arrSize )
        return std::make_tuple< Variant&, Variant&, Variant& >( col1Var, col2Var, col3Var );

    auto& col4Var{ GetColumnVar( TDbIndex::cols[ 3 ] ) };
    if constexpr ( 4 == arrSize )
        return std::make_tuple< Variant&, Variant&, Variant&, Variant& >( col1Var, col2Var, col3Var, col4Var );

    auto& col5Var{ GetColumnVar( TDbIndex::cols[ 4 ] ) };
    if constexpr ( 5 == arrSize )
        return std::make_tuple< Variant&, Variant&, Variant&, Variant&, Variant& >( col1Var, col2Var, col3Var, col4Var, col5Var );
}

template< DbIndexDerived TDbIndex >
constexpr auto DbModel::GetCacheIndex()
{
    using ResultType = typename TDbIndex::Tuple;
    constexpr auto arrSize = ArrSize( TDbIndex::cols );
    auto& cols{ _columnList };

    if constexpr ( 1 == arrSize )
        return ResultType{ { cols[ TDbIndex::cols[ 0 ] ]->_var._ulong } };
}

template< DbIndexDerived TDbIndex >
constexpr auto DbModel::GetIndexKey()
{
    using namespace Mala::Core;

    constexpr auto arrSize = ArrSize( TDbIndex::cols );
    auto& cols{ _columnList };

    const auto& col1Var{ GetColumnVar( TDbIndex::cols[ 0 ] ) };
    if constexpr ( 1 == arrSize )
        return std::make_tuple( Variant{ col1Var } );

    const auto& col2Var{ GetColumnVar( TDbIndex::cols[ 1 ] ) };
    if constexpr ( 2 == arrSize )
        return std::make_tuple( Variant{ col1Var }, Variant{ col2Var } );

    const auto& col3Var{ GetColumnVar( TDbIndex::cols[ 2 ] ) };
    if constexpr ( 3 == arrSize )
        return std::make_tuple( Variant{ col1Var }, Variant{ col2Var }, Variant{ col3Var } );

    const auto& col4Var{ GetColumnVar( TDbIndex::cols[ 3 ] ) };
    if constexpr ( 4 == arrSize )
        return std::make_tuple( Variant{ col1Var }, Variant{ col2Var }, Variant{ col3Var }, Variant{ col4Var } );

    const auto& col5Var{ GetColumnVar( TDbIndex::cols[ 4 ] ) };
    if constexpr ( 5 == arrSize )
        return std::make_tuple( Variant{ col1Var }, Variant{ col2Var }, Variant{ col3Var }, Variant{ col4Var }, Variant{ col5Var } );
}
