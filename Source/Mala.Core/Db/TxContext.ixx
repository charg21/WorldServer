export module Mala.Db.TxContext;

#include "../MalaMacro.h"

import <memory>;
import <functional>;

import Mala.Core.Types;
import Mala.Core.SourceLocation;
import Mala.Container;
import Mala.Db.DbConnection;
import Mala.Db.DbModel;
import Mala.Memory;
import Mala.Db.DbTypes;

using namespace Mala::Container;

NAMESPACE_BEGIN( Mala::Db )

/// <summary>
/// 트랜잭션 컨텍스트 상태
/// </summary>
enum class ETxContextState : u8
{
    Created,          //< 최초 상태
    WaitingToExecute, //< 실행하기 위해 대기중( 예약된 상태 )
    Executing,        //< 실행중 로직 작업
    ExecutingDb,      //< 실행중 Db 작업
    ExecutingSync,    //< 실행중 동기화 작업
    Completed,        //< 완료됨
    Canceled,         //< 작업 취소 상태, 요청으로 인해 취소
    Failed,           //< 작업중 실패
};

/// <summary>
/// 트랜잭션 컨텍스트 상태
/// </summary>
enum class ETxResult
{
    /// <summary>
    /// 성공
    /// </summary>
    Success,

    /// <summary>
    /// 쿼리 실패
    /// </summary>
    QueryFailed,

    /// <summary>
    /// DB 작업 강제 취소( 로직단 )
    /// </summary>
    DbJobCancel,
};


using TxState = ETxContextState;


template< typename TState >
struct IFsm{};

/// <summary>
/// 트랜잭션 작업 문맥 인터페이스
/// </summary>
struct ITxContext {};
class DbConnection;

struct FsmState
{
    virtual void Execute( TxContextRef tx ) = 0;
};

struct LogicJobState : public FsmState
{
    void Execute( TxContextRef tx ) final
    {

    }
};

struct SyncState : public FsmState
{
    void Execute( TxContextRef tx ) final
    {

    }
};

struct CompletedState
{
    virtual void Execute()
    {
    }
};

/// <summary>
/// 트랜잭션 작업 문맥
/// Db를 이용한 STM( Software Transactional Memory )을 구현하기 위한 인터페이스
/// </summary>
class TxContext final
    : public ITxContext
    , public IFsm< ETxContextState >
    , public std::enable_shared_from_this< TxContext >
{
    friend class TxExecutor;

public:
    // Job
    using DbJob        = Func< bool, DbConnection* >;
    using DbJobList    = Deque< DbJob >;
    using ReadyJob     = Func< bool, TxContextRef >;
    using Job          = Func< void >;
    using ReadyJobList = Deque< ReadyJob >;
    using JobList      = Deque< Job >;
    using ResultJob    = Action< bool >;

    using DbConnectionAllocator = Func< DbConnection* >;
    using DbConnectionDeleter   = Action< DbConnection* >;
    using DbJobExecutor         = Action< Job&& >;

    using DbModelRawPtr  = DbModel*;
    using DbModelPtr     = std::shared_ptr< DbModel >;
    using DbModelPtrList = Vector< std::pair< EQueryType, DbModelPtr > >;
    using DbModelSet     = HashSet< DbModel* >;

private:
    // 임시 인터페이스
    inline static DbConnectionAllocator GetConnection     = []{ return nullptr; };
    inline static DbConnectionDeleter   ReleaseConnection = []( DbConnection* ){};
    inline static DbJobExecutor         ExecuteDbJob      = []( Job&& ) {};

public:
    static void SetDbConnectionAllocator( DbConnectionAllocator&& allocator );
    static void SetDbConnectionDeallocator( DbConnectionDeleter&& deleter );
    static void SetDbJobExecutor( DbJobExecutor&& executor );

    static TxContextPtr New( SourceLocationRef where = SourceLocation::Here() );

public:
    /// <summary>
    /// 생성자
    /// </summary>
    TxContext( SourceLocationRef where = SourceLocation::Here() );
    TxContext( const TxContext& other ) = delete;
    TxContext( TxContext&& other ) noexcept = delete;

    /// <summary>
    /// 소멸자
    /// </summary>
    ~TxContext() = default;

    /// <summary>
    /// 실행한다
    /// </summary>
    void Execute();

    /// <summary>
    /// 취소한다
    /// </summary>
    void Cancel();

    template< std::predicate< TxContextRef > TReadyJob >
    void AddReadyJob( TReadyJob&& job );
    template< std::predicate< DbConnection* > TDbJob >
    void AddDbJob( TDbJob&& dbJob );
    template< std::invocable TJob >
    void EndWith( TJob&& job );
    bool IsCompleted() const;
    bool IsCanceled() const;
    bool IsEmpty() const;
    bool IsFailed() const;
    void SetResultJob( ResultJob&& resultJob )
    {
        _resultJob = std::move( resultJob );
    }

public:
    template< DbModelType T, typename... Args >
    TDbModelPtr< T > Create( Args&&... args );
    template< DbModelType T >
    TDbModelPtr< T > Update( std::shared_ptr< T >& origin );
    template< DbModelType T >
    TDbModelPtr< T > Delete( std::shared_ptr< T >& origin );

    // For Fsm
    void ChangeState( TxState newState );

private:
    void Execute_LogicJob();
    void Execute_DbJob();
    void Execute_Sync();
    void Execute_Completed();
    void Execute_Failed();

public:
    /// <summary> 트랜잭션 식별자 </summary>
    u64 _txId{};

    /// <summary> DbModel목록 </summary>
    DbModelPtrList _modelList;

    /// <summary> 커넥션 </summary>
    DbConnection* _connection;

    /// <summary> DB작업 목록 </summary>
    DbJobList _dbJobList;

    /// <summary> 지연된 DB 작업 목록 </summary>
    DbJobList _lazyDbJobList;

    /// <summary> 트랜잭션 작업 주인 </summary>
    TxExecutorPtr _owner;

    /// <summary> 준비 작업 목록 </summary>
    ReadyJobList _readyJobList;

    /// <summary> 완료 작업 목록 </summary>
    JobList _endJobList;

    /// <summary> 결과 작업 </summary>
    ResultJob _resultJob;

    /// <summary> 상태 </summary>
    ETxContextState _txState{ ETxContextState::Created };

    /// <summary> 우리의 근원은 어디서 부터 시작된것인가.... </summary>
    SourceLocation _from;
};

template< std::predicate< DbConnection* > TDbJob >
void TxContext::AddDbJob( TDbJob&& dbJob )
{
    _dbJobList.emplace_back( std::forward< TDbJob >( dbJob ) );
}

template< std::invocable TJob >
void TxContext::EndWith( TJob&& job )
{
    _endJobList.emplace_back( std::forward< TJob >( job ) );
}

template< std::predicate< TxContextRef > TReadyJob >
void TxContext::AddReadyJob( TReadyJob&& job )
{
    _readyJobList.emplace_back( std::forward< TReadyJob >( job ) );
}

template< DbModelType T >
TDbModelPtr< T > TxContext::Update( std::shared_ptr< T >& origin )
{
    /*
        User      user;
        TxContext txContext;

        User* userCopy = txContext.Update( user );
        userCopy->SetLastPlayTime( DateTime::Now() );

        user->PostTx( txContext, []( bool txResult ){} );
    */
    auto copy{ std::make_shared< T >( *origin ) };
    copy->_root = origin;

    _modelList.emplace_back( EQueryType::Update, std::move( copy ) );

    return copy;
}

template< DbModelType T >
TDbModelPtr< T > TxContext::Delete( std::shared_ptr< T >& origin )
{
    auto copy{ std::make_shared< T >( *origin ) };
    copy->_root = origin;

    _modelList.emplace_back( EQueryType::Delete, std::move( copy ) );

    return copy;
}


template< DbModelType T, typename... Args >
TDbModelPtr< T > TxContext::Create( Args&&... args )
{
    /*
        TxContext txContext;
        Pc*   pc = txContext.Create< Pc >();

        pc->SetKeyId       ( 1               );
        pc->SetLastPlayTime( DateTime::Now() );

        pc->PostTx( txContext, []( bool txResult ){} );
    */
    auto origin{ std::make_shared< T >( std::forward< Args >( args )... ) };
    _modelList.emplace_back( EQueryType::Insert, std::move( origin ) );

    return origin;
}

NAMESPACE_END( Mala::Db )
