export module Mala.Db.IndexInfo;

#include "../MalaMacro.h"

import <string>;
import <string_view>;
import <vector>;

import Mala.Core.Types;
import Mala.Container.String;

NAMESPACE_BEGIN( Mala::Db )

struct ColumnInfo;
struct TableInfo;

/// <summary>
/// 인덱스 메타 데이터
/// </summary>
struct IndexInfo
{
    /// <summary>
    /// 타입 재정의
    /// </summary>
    using ColumnInfoList = std::vector< const ColumnInfo* >;

    std::string_view   _name;        //< 인덱스 명
    StringView         _nameW;       //< 인덱스 명
    Flag               _mask;        //< 인덱스 마스크
    ColumnInfoList     _columnInfos; //<
};

NAMESPACE_END( Mala::Db )
