//export module Mala.Db.DbModel:impl;

#include "../CoreMacro.h"

import <functional>;

import Mala.Core.Crash;
import Mala.Core.Types;
import Mala.Core.Variant;
import Mala.Container;
import Mala.Container.String;

import Mala.Db.DbModel;
import Mala.Db.DbModelMapper;
import Mala.Db.Column;
import Mala.Db.ColumnInfo;
import Mala.Db.DbConnection;
import Mala.Db.IndexInfo;
import Mala.Db.QueryBuilder;
import Mala.Db.Table;
import Mala.Db.DbTypes;
import Mala.Db.TxContext;

using namespace Mala::Core;
using namespace Mala::Db;
using namespace Mala::Container;


// DbModel
DbModel::DbModel( TableInfo& table )
: _table         { table }
, _columnUpdated {       }
, _columnBinding {       }
, _columnKeySetup{       }
{
}

DbModel::DbModel( DbModel& root )
: _table{ root._table }
{
    root.CopyTo( *this );
}

DbModel::DbModel( DbModel&& root ) noexcept
: _table         { root._table          }
, _columnUpdated { root._columnUpdated  }
, _columnBinding { root._columnBinding  }
, _columnKeySetup{ root._columnKeySetup }
{
	for ( i32 index{}; index < root._columnList.size(); index += 1 )
		*_columnList[ index ] = std::move( *root._columnList[ index ] );

    _root = std::move( root._root );
}

//DbModel::~DbModel()
//{
//}

MALA_NODISCARD bool DbModel::Delete( DbConnection* connection )
{
    auto deleteQuery{ Mala::Db::QueryBuilder::MakeDeleteQuery( *this, connection ) };
    if ( !connection->Query( deleteQuery ) )
        return false;

    ResetUpdateFlag();

    return connection->_affectedRowCount > 0;
}

MALA_NODISCARD bool DbModel::Insert( DbConnection* connection )
{
    auto insertQuery{ Mala::Db::QueryBuilder::ToInsertQuery( *this, connection ) };
    if ( !connection->Query( insertQuery ) )
        return false;

    ResetUpdateFlag();

    return connection->_affectedRowCount > 0;
}

MALA_NODISCARD i64 DbModel::Update( DbConnection* connection )
{
    if ( !connection )
        return -1;

    auto updateQuery{ Mala::Db::QueryBuilder::ToUpdateQuery( *this, connection ) };
    if ( !connection->Query( updateQuery ) )
        return -1;

    ResetUpdateFlag();

    // 쿼리 성공 이후 변경이 안된 경우에 _affected_row_count == 0 인경우도 성공으로 간주
    return connection->_affectedRowCount;
}

MALA_NODISCARD bool DbModel::Select( DbConnection* connection )
{
    if ( !connection )
        return false;

    auto selectQuery{ Mala::Db::QueryBuilder::ToSelectQuery( *this, connection ) };
    if ( !connection->Query( selectQuery ) || !connection->Fetch() )
        return false;

	return DbModelMapper::Map( this, connection->GetRow() ) > 0;
}

//MALA_NODISCARD const bool is_all_column_updated()
//{
//    return _columnUpdated == 1;
//}

void DbModel::ResetUpdateFlag()
{
    _columnUpdated = 0;
}

void DbModel::SetUpdateColumn( i32 columnNo )
{
    _columnUpdated |= ( 1ULL << columnNo );
}

void DbModel::SetBindColumn( i32 columnNo )
{
    _columnBinding |= ( 1ULL << columnNo );
}

void DbModel::SetKeyColumn( i32 columnNo )
{
    _columnKeySetup |= ( 1ULL << columnNo );
}

void DbModel::SetUpdateAndSetupKeyColumn( i32 columnNo )
{
    _columnUpdated  |= ( 1ULL << columnNo );
    _columnKeySetup |= ( 1ULL << columnNo );
}

MALA_NODISCARD bool DbModel::IsUpdateColumn( i32 no ) const
{
    const size_t mask = ( 1ULL << no );
    const bool result = 1ULL <= ( _columnUpdated & ( mask ) );
    return result;
}

MALA_NODISCARD bool DbModel::IsBindingColumn( i32 no ) const
{
    const size_t mask = ( 1ULL << no );
    const bool result = 1ULL <= ( _columnBinding & ( mask ) );
    return result;
}

MALA_NODISCARD bool DbModel::IsKeySetupColumn( i32 no ) const
{
    const size_t mask = ( 1ULL << no );
    const bool result = 1ULL <= ( _columnKeySetup & ( mask ) );
    return result;
}

MALA_NODISCARD bool DbModel::IsValidIndex( const IndexInfo* index ) const
{
    const size_t maskedResult = _columnKeySetup & index->_mask;

    return maskedResult == index->_mask;
}

void DbModel::Sync( EQueryType queryType )
{
}

MALA_NODISCARD bool DbModel::HasUpdateColumn() const
{
    return _columnUpdated > 0;
}

MALA_NODISCARD bool DbModel::HasBindingColumn() const
{
    return _columnBinding > 0;
}

MALA_NODISCARD bool DbModel::HasKeySetupColumn() const
{
    return _columnKeySetup > 0;
}


void DbModel::Reset()
{
    ResetUpdateFlag();
    ResetBinding();
}

void DbModel::BindAllColumn()
{
    _columnBinding = (unsigned long long)( -1LL );// 0b11111111'11111111'11111111'11111111'11111111'11111111'11111111'11111111;
}

void DbModel::ResetBinding()
{
    _columnBinding = 0;
}

const std::string& DbModel::GetTableNameA()
{
    return _table._name;
}

const std::wstring& DbModel::GetTableName() const
{
    return _table._nameW;
}

const TableInfo& DbModel::GetTable()
{
    return _table;
}

const TableInfo& DbModel::GetTable() const
{
    return _table;
}

const Column& DbModel::GetColumn( i32 index ) const
{
	return *_columnList[ index ];
}

const Variant& DbModel::GetColumnVar( i32 index ) const
{
    return _columnList[ index ]->_var;
}

void DbModel::CopyTo( DbModel& target ) const
{
    target._columnBinding  = _columnBinding;
    target._columnUpdated  = _columnUpdated;
    target._columnKeySetup = _columnKeySetup;

    for ( i32 index{}; index < _columnList.size(); index += 1 )
    {
        *target._columnList[ index ] = *_columnList[ index ];
    }
}

void DbModel::CopyToOnlyUpdateColumn( DbModel& target ) const
{
    target._columnBinding  = _columnBinding;
    target._columnUpdated  = _columnUpdated;
    target._columnKeySetup = _columnKeySetup;

    for ( i32 index{}; index < _columnList.size(); index += 1 )
    {
        if ( IsUpdateColumn( index ) )
            *target._columnList[ index ] = *_columnList[ index ];
    }
}

const IndexInfo* DbModel::GetFirstValidIndex() const
{
    for ( const IndexInfo* indexInfo : _table._indexs )
    {
        if ( IsValidIndex( indexInfo ) )
			return indexInfo;
    }

    return {};
}
