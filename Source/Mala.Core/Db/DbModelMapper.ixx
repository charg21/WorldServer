export module Mala.Db.DbModelMapper;

#include "../CoreMacro.h"

import <memory>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Container.String;
import Mala.Db.Column;
import Mala.Db.DbTypes;
import Mala.Db.DbConnection;
import Mala.Db.DbModel;
import Mala.Db.DbModelFactory;
import Mala.Db.QueryBuilder;
import Mala.Db.MySql;

using namespace Mala::Container;

NAMESPACE_BEGIN( Mala::Db )

class DbModelMapper
{
public:
    static i32 Map( DbModel* dbModel, const MySql::Row& row )
    {
        i32 columnCount{};
        for ( Column* col : dbModel->_columnList )
        {
            if ( !row[ columnCount ] )
                continue;

            // todo: bind 여부로 필요한 컬럼만
            col->_var.Set( row[ columnCount ] );
            columnCount += 1;
        }

        return columnCount;
    }

    template< typename T >
    static TDbModelPtr< T > Map( const MySql::Row& row )
    {
        auto modelPtr{ T::GetFactory() };

        Map( modelPtr.get(), row );

        return std::static_pointer_cast< T >( std::move( modelPtr ) );
    }
};

NAMESPACE_END( Mala::Db )
