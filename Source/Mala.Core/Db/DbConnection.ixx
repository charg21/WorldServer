module;

#include "../MalaMacro.h"

export module Mala.Db.DbConnection;

import Mala.Core.Types;
import Mala.Container.LockFreeStack;
import Mala.Container.String;
import Mala.Text;
import Mala.Db.DbTypes;
import Mala.Db.MySql;

NAMESPACE_BEGIN( Mala::Db )

class DbConnection
{
public:
    DbConnection() = default;
    ~DbConnection() = default;

    // https://mariadb.com/kb/en/mariadb-connectorc-types-and-definitions/

    bool Connect(
        const std::string& host,
        const std::string& user,
        const std::string& password,
        StringRef schema,
        const int port );

    void Disconnect();
    bool Fetch();
    bool Query( const std::string& queryString );
    const static bool IsSelectQuery( const std::string& query );
    const static bool IsSelectQuery( const std::wstring& query );

    bool Query( StringView queryWString );
    MySql::Row GetRow();
    void Reset();

    bool BeginTransaction();
    bool SetAutoCommit( bool autoMode );
    bool SetForeignKeyCheck( bool check );
    bool Commit();
    bool Rollback();

    /// <summary>
    // 주의 Utf16, utf32는 사용이 불가능하다
    /// </summary>
    void SetCharacterSet();

    std::wstring_view GetSchemaW();

public:
    MySql::Conn* _connection{};
    MySql::Res*  _results{};
    MySql::Row   _cursor;
    i32          _error_code{};
    i32          _affectedRowCount{};
    i32          _affected_column_count{};

    std::string_view  _schema;
    std::wstring_view _schemaW;
};

class DbConnectionPool
{
    using ConnectionContanier = Mala::Container::LockfreeStack< DbConnection* >;

public:
    bool Connect(
        StringRef host,
        StringRef user,
        StringRef password,
        StringRef schema,
        const int port,
        size_t    connectionCount = 10 );

    DbConnection* Get();
    DbConnection* GetOrDefault();

    void Release( DbConnection* connection );
    void ForEachDbConnection( const Action< DbConnection* >& job );

private:
    /// <summary>
    /// 컨테이너
    /// </summary>
    ConnectionContanier _connections;

    std::string	_host     = "127.0.0.1";
    std::string	_user     = "root";
    std::string	_password = "1q2w3e4r";
    std::string	_schema   = "test";
    String      _schemaW  = L"test";
    i32         _port;
};

NAMESPACE_END( Mala::Db )
