export module Mala.Db.DbModelFactory;

#include "../CoreMacro.h"

import <memory>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Container.String;
import Mala.Db.DbTypes;
import Mala.Db.DbConnection;
import Mala.Db.QueryBuilder;
import Mala.Db.DbModel;

using namespace Mala::Container;


NAMESPACE_BEGIN( Mala::Db )

struct DbModel;

template< DbModelType T >
class DbModelFactory
{
public:
    inline static std::shared_ptr< T >( *_factory )(){};

    static std::shared_ptr< T > Create()
    {
        return _factory();
    }

    static void Register( std::shared_ptr< T >( *task )() )
    {
        _factory = task;
        RegisterParent< T >( task );
    }

private:
    template< DbModelType U >
    static void RegisterParent( std::shared_ptr< U >( *task )() )
    {
        /// 현재는 BaseDbModel을 등록한 클래스만 가능
        using ParentType = typename U::BaseDbModel;
        if constexpr ( DbModelType< ParentType > && !std::is_same_v< ParentType, DbModel > )
        {
            DbModelFactory< ParentType >::Register( reinterpret_cast< std::shared_ptr< ParentType >( * )()>( task ) );
            RegisterParent< ParentType >( reinterpret_cast< std::shared_ptr< ParentType >( * )()>( task ) );
        }
    }
};

NAMESPACE_END( Mala::Db )
