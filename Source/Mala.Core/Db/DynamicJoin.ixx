export module Mala.Db.DynamicJoin;

#include "../CoreMacro.h"

import <array>;
import <string>;
import <string_view>;
import <vector>;
import <functional>;
import <iostream>;
import <tuple>;

#define UTF16
#ifdef UTF16
using QueryBuffer = std::wstring;
using QueryBufferView = std::wstring_view;
#define _U(STR) (L##STR)
#else
using QueryBuffer = std::string;
using QueryBufferView = std::string_view;
#define _U(STR) (STR)
#endif // UNICODE


import Mala.Core.TypeList;
import Mala.Core.Types;
import Mala.Container;

import Mala.Db.Column;
import Mala.Db.ColumnInfo;
import Mala.Db.DbConnection;
import Mala.Db.DbModel;
import Mala.Db.Table;
import Mala.Db.DbTypes;
import Mala.Db.Join;

using namespace Mala::Container;

NAMESPACE_BEGIN( Mala::Db )

struct MALA_DB_API DynamicJoin
{
public:
    friend class QueryBuilder;
    friend struct DynamicJoinOn;

    using DbModelList  = std::vector< const DbModel& >;
    using OnColumnList = std::vector< std::tuple< const DbModel&, const Column&, const DbModel&, const Column& > >;

    struct DynamicJoinOn
    {
		DynamicJoin& _join;
        DynamicJoin& Equal( const DbModel&, const Column&, const DbModel&, const Column& );
    };

public:
    [[nodiscard]] bool Select( DbConnection* connection );

private:
    DynamicJoin& _OnEqual(
        const DbModel& lhs,
        const Column& lhsColumn,
        const DbModel& rhs,
        const Column& rhsColumn );

private:
    OnColumnList _onColumnList;
};

NAMESPACE_END( Mala::Db )

using namespace Mala::Db;

bool DynamicJoin::Select( DbConnection* connection )
{
    return true;
}

DynamicJoin& DynamicJoin::_OnEqual(
    const DbModel& lhs,
    const Column& lhsColumn,
    const DbModel& rhs,
    const Column& rhsColumn )
{
    _onColumnList.emplace_back( lhs, lhsColumn, rhs, rhsColumn );
    return *this;
}

DynamicJoin& DynamicJoin::DynamicJoinOn::Equal(
    const DbModel& lhs,
    const Column&  lhsColumn,
    const DbModel& rhs,
    const Column&  rhsColumn )
{
    _join._OnEqual( lhs, lhsColumn, rhs, rhsColumn );
    return _join;
}
