export module Mala.Db.TxExecutor;

#include "../CoreMacro.h"

import <memory>;

import Mala.Core.Types;
import Mala.Container.WriteFreeQueue;
import Mala.Db.TxContext;
import Mala.Threading.JobExecutor;

using namespace Mala::Threading;
using namespace Mala::Container;

NAMESPACE_BEGIN( Mala::Db )

/// <summary>
/// 트랜잭션 실행기
/// </summary>
class TxExecutor
{
    friend class TxContext;

public:
    /// <summary>
    /// 트랜잭션 작업 큐 타입 정의
    /// </summary>
    using TxContextQueue = WriteFreeQueue< TxContextPtr >;
    using ResultJob      = Func< void, bool >;
    using Job            = Action< void >;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    TxExecutor( JobExecutor& executor );

    /// <summary>
    /// 소멸자
    /// </summary>
    virtual ~TxExecutor() = default;

    /// <summary>
    /// 트랜잭션을 실행한다
    /// </summary>
    void PostTx( TxContextPtr&& tx, ResultJob&& resultJob );
    void PostTx( TxContextPtr&& tx );

    template< std::predicate< TxContextRef > TReadyJob >
    void PostTx( TReadyJob&& readyJob )
    {
        auto tx{ TxContext::New() };
        tx->AddReadyJob( std::forward< TReadyJob >( readyJob ) );
        PostTx( std::move( tx ) );
    }

    bool IsBusy() const;

private:
    /// <summary>
    /// 실행한다
    /// </summary>
    int ExecuteTxAsync();

    virtual void PostJob( Job&& job );
    virtual void PostJob( const Job& job );
    virtual TxExecutorPtr SharedFromTxExecutor() = 0;
    virtual TxExecutorWeakPtr WeakFromTxExecutor() = 0;

protected:
    /// <summary>
    /// 트랜잭션 작업 큐
    /// </summary>
    TxContextQueue _txContexts;

    /// <summary>
    /// 실행중 여부
    /// </summary>
    Atomic< i64 > _txPostCount;

    /// <summary>
    /// 작업 실행기
    /// </summary>
    JobExecutor& _executor;
};

NAMESPACE_END( Mala::Db )
