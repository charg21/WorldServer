#include "../CoreMacro.h"

import <memory>;

import Mala.Core.Types;
import Mala.Core.Crash;
import Mala.Container.WriteFreeQueue;
import Mala.Container;
import Mala.Db.TxContext;
import Mala.Db.TxExecutor;
import Mala.Threading.JobExecutor;

using namespace Mala::Threading;
using namespace Mala::Container;
using namespace Mala::Db;

/// <summary>
/// 생성자
/// </summary>
TxExecutor::TxExecutor( JobExecutor& executor )
: _executor{ executor }
{
}

/// <summary>
/// 트랜잭션을 실행한다
/// </summary>
void TxExecutor::PostTx( TxContextPtr&& tx, ResultJob&& resultJob )
{
    tx->_resultJob = std::move( resultJob );
    PostTx( std::move( tx ) );
}

/// <summary>
/// 트랜잭션을 실행한다
/// </summary>
void TxExecutor::PostTx( TxContextPtr&& tx )
{
    tx->_txState = ETxContextState::WaitingToExecute;
    tx->_owner   = SharedFromTxExecutor();


    i64 postTxCount = _txPostCount.fetch_add( 1 );
    _txContexts.Enqueue( std::move( tx ) );

    /// 실행
    if ( postTxCount == 0 )
        PostJob( [ this ] { ExecuteTxAsync(); } );
}

bool TxExecutor::IsBusy() const
{
	return _executor.Count();
}

int TxExecutor::ExecuteTxAsync()
{
    TxContextPtr outTx;
    i64 completedTxCount = 0;

    LOOP
    {
        if ( !_txContexts.TryPeek( outTx ) )
            return 1;

        // 실행
        outTx->Execute();
        if ( outTx->_txState == ETxContextState::Completed )
        {
			_txContexts.TryDequeue( outTx );
            completedTxCount += 1;
            continue;
        }

        // 현재 상태에서 끝나지 않았다면, 타이머로 다음 프레임으로 넘김.
        // DB작업등이 끝나지 않았으니...
        if ( _txPostCount.fetch_sub( completedTxCount ) == completedTxCount )
        {
            break;
        }
        else
        {
            _executor.PostAfter( 10ms, [ this ]()
            {
                ExecuteTxAsync();
            } );
        }
    }

    /// 현재 프레임에는 끝내지 못햇으니 다음 프레임으로 넘긴다.
    /// 현재 프레임에 끝냈으면 종료

    return 0;
}


void TxExecutor::PostJob( Job&& job )
{
    _executor.Post( std::move( job ) );
}

void TxExecutor::PostJob( const Job& job )
{
    _executor.Post( job );
}
