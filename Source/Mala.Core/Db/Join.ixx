export module Mala.Db.Join; // data or

#include "../MalaMacro.h"

import <array>;
import <string_view>;
import <iostream>;
import <format>;

import Mala.Container;

#define UTF16
#ifdef UTF16
using QueryBuffer = String;
using QueryBufferView = std::wstring_view;
#define _U(STR) (L##STR)
#else
using QueryBuffer     = std::string;
using QueryBufferView = std::string_view;
#define _U(STR) (STR)
#endif // UNICODE

import Mala.Db.Column;
import Mala.Db.ColumnInfo;
import Mala.Db.DbConnection;
import Mala.Db.DbModel;
import Mala.Db.Table;
import Mala.Db.DbTypes;

using namespace Mala::Container;

NAMESPACE_BEGIN( Mala::Db )

//template< typename TQueryMakeable >
//concept QueryMakeable = requires( TQueryMakeable queryMakeable )
//{
//    //queryMakeable.Select();
//};

///// <summary>
///// 전방 선언
///// </summary>
//template< typename T1, typename T2, EJoinType Type >
//struct Join;

/// <summary>
/// 두 테이블간 Join
/// </summary>
template< DbModelType T1, DbModelType T2, EJoinType Type = EJoinType::Left >
struct Join
{
    friend class QueryBuilder;

    using ColumnWithInfo = std::pair< Column*, const ColumnInfo* >;
    using BindColumnList = Vector< ColumnWithInfo >;

    using t1 = T1;
    using t2 = T2;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    Join() = default;

    template< typename T >
    T& Get() = delete;

    template<>
    [[ nodiscard ]] T1& Get< T1 >()
    {
        return _t1;
    }

    template<>
    [[ nodiscard ]] T2& Get< T2 >()
    {
        return _t2;
    }

    template< typename T >
    T& As() = delete;

    template<>
    T1& As< T1 >()
    {
        return _t1;
    }

    template<>
    T2& As< T2 >()
    {
        return _t2;
    }

    template< typename T >
    constexpr QueryBufferView Alias() = delete;

    template<>
    [[ nodiscard ]] constexpr QueryBufferView Alias< T1 >()
    {
        return _U( "t1" );
    }

    template<>
    [[ nodiscard ]] constexpr QueryBufferView Alias< T2 >()
    {
        return _U( "t2" );
    }

    template< typename T >
    constexpr QueryBufferView AliasDot()
    {
        if constexpr ( std::is_base_of< T, T1 >::value )
            return AliasDot< T1 >();
        else if constexpr ( std::is_base_of< T, T2 >::value )
            return AliasDot< T2 >();
        else
            return "";// __FUNCSIG__;
    }

    template<>
    [[ nodiscard ]] constexpr QueryBufferView AliasDot< T1 >()
    {
        return _U( "t1." );
    }

    template<>
    [[ nodiscard ]] constexpr QueryBufferView AliasDot< T2 >()
    {
        return _U( "t2." );
    }

    [[ nodiscard ]] bool Select( Mala::Db::DbConnection* connection )
    {
        _connection = connection;

        if ( !_t1.HasBindingColumn() && !_t2.HasBindingColumn() )
            return "";

        QueryBuffer joinQuery = ToSelectQuery();
        if ( !connection->Query( joinQuery ) )
            return false;

        /*if ( !FetchRow() )
            return false;*/

        return true;
    }

    /*
DELETE t1
FROM HUMAN_TABLE t1
LEFT JOIN PHONE_TABLE t2
ON t1.human_id = t2.human_id
WHERE t2.account IS NULL;
    */
    [[ nodiscard ]] bool Delete( Mala::Db::DbConnection* connection )
    {

        return true;
    }


    template< typename T >
    const bool IsNull() = delete;

    template<>
    const bool IsNull< T1 >()
    {
        return _columnCount < _t1BindColumns.size();
    }

    template<>
    const bool IsNull< T2 >()
    {
        return _columnCount < ( _t1BindColumns.size() + _t2BindColumns.size() );
    }

    template< typename T >
    BindColumnList& GetBindingColumns() = delete;

    template<>
    BindColumnList& GetBindingColumns< T1 >()
    {
        return _t1BindColumns;
    }

    template<>
    BindColumnList& GetBindingColumns< T2 >()
    {
        return _t2BindColumns;
    }

    template< typename T >
    std::string MakeBindingString()
    {
        std::string _query;

        for ( Column* col : Get< T >()._columnList )
        {
            if ( !get< T >().IsBindingColumn( col->_info->_columnNo ) )
                continue;

            _query += AliasDot< T >();
            _query += col->_info->_name;
            _query += ", ";

            GetBindingColumns< T >().emplace_back( col, col->_info );
        }

        return std::move( _query );
    }

    template< typename T >
    QueryBuffer MakeBindingWString()
    {
        QueryBuffer _query;

        for ( Column* col : Get< T >()._columnList )
        {
            if ( !Get< T >().IsBindingColumn( col->_info->_columnNo ) )
                continue;

            _query += AliasDot< T >();
            _query += col->_info->_nameW;
            _query += L", ";

            GetBindingColumns< T >().emplace_back( col, col->_info );
        }

        return std::move( _query );
    }

    QueryBuffer GetBindingWstring()
    {
        QueryBuffer _query = MakeBindingWString< T1 >() + MakeBindingWString< T2 >();

        return std::move( _query );
    }

    QueryBuffer ToSelectQuery()
    {
        /*
        SELECT t1.col1, t2.col2
        FROM Table1 as t1
        LEFT JOIN Table2 as t2
        on t1.col1 = t2.col2
        */

        const TableInfo& t1_table = _t1._table;

        QueryBuffer _query;
        _query.reserve( _max_query_length );

        _query += L"SELECT ";
        _query += MakeBindingWString< T1 >();
        _query += MakeBindingWString< T2 >();

        _query.resize( _query.size() - 2 );
        _query += ' ';

        _query += L"FROM ";
        _query += _connection->_schemaW;
        _query += L'.';
        _query += _t1.GetTableName();
        _query += L" as ";
        _query += Alias< T1 >();

        //if constexpr ( Type == EJoinType::Inner )
        //    _query += L" join ";
        //else if constexpr ( Type == EJoinType::Left )
        //    _query += L" left join ";

        _query += _connection->_schemaW;
        _query += L'.';
        _query += _t2.GetTableName();
        _query += L" as ";
        _query += Alias< T2 >();

        if ( HasOnCondition() )
            _query += _onCondition;

        if ( HasWhereCondition() )
            _query += _whereCondition;

        if ( HasGroupByCondition() )
            _query += _groupByCondition;

        if ( HasLimitCondition() )
        {
            _query += L" limit ";
            _query += std::to_wstring( _limit );
        }

        _max_query_length = std::max( _max_query_length, _query.size() );

        return std::move( _query );
    }

    template< typename T >
    int SyncBindingColumn()
    {
        auto row = _connection->GetRow();

        if ( row[ _columnCount ] == nullptr )
            return _columnCount;

        for ( auto& [ column, meta_data ] : GetBindingColumns< T >() )
        {
            if ( meta_data->_isInPrimaryIndex || meta_data->_isInSecondaryIndex )
                Get< T >().SetKeyColumn( meta_data->_columnNo );

            column->_var.Set( row[ _columnCount ] );
            _columnCount += 1;
        }

        return _columnCount;
    }

    [[ nodiscard ]] constexpr bool FetchRow()
    {
        if ( !_connection->Fetch() )
            return false;

        _columnCount = 0;

        _columnCount = SyncBindingColumn< T1 >();
        _columnCount += SyncBindingColumn< T2 >();

        return true;
    }

    Join& And()
    {
        QueryBuffer& condition_string = GetConditionString();
        condition_string += _U( " AND " );

        return *this;
    }

    Join& Or()
    {
        QueryBuffer& condition_string = GetConditionString();
        condition_string += _U( " OR " );

        return *this;
    }

    Join& On()
    {
        _onCondition += _U( " ON " );

        _lastConditionOperator = EConditionOperator::On;

        return *this;
    }

    Join& Where()
    {
        _whereCondition += _U( " WHERE " );

        _lastConditionOperator = EConditionOperator::Where;

        return *this;
    }

    Join& GroupBy()
    {
        _groupByCondition += _U( " GROUP BY " );

        _lastConditionOperator = EConditionOperator::GroupBy;

        return *this;
    }

    template< DbColumnDerived Column, typename Iterator >
    Join& NotIn( Iterator begin, Iterator end )
    {
        std::string condition;
        for ( ; begin != end; ++begin )
        {
            if constexpr ( std::is_same_v< Iterator::value_type, QueryBuffer > )
                condition += "\"" + ( *begin ) + "\"";
            else
                condition += std::to_string( *begin );

            condition += ", ";
        }

        condition.resize( condition.size() - 2 );

        return NotIn< Column >( std::move( condition ) );
    }

    template< DbColumnDerived Column, typename Iterator >
    Join& _in( Iterator begin, Iterator end )
    {
        std::string condition;

        for ( ; begin != end; ++begin )
        {
            if constexpr ( std::is_same_v< Iterator::value_type, std::string > )
                condition += "\"" + ( *begin ) + "\"";
            else
                condition += std::to_string( *begin );

            condition += ", ";
        }

        condition.resize( condition.size() - 2 );

        return _in< Column >( std::move( condition ) );
    }

    template< DbColumnDerived Column >
    Join& _not_in( const QueryBuffer& condition )
    {
        QueryBuffer& condition_string = GetConditionString();

        condition_string += AliasDot< Column::DbType >();
        condition_string += Column::name_view.data();
        condition_string += _U( " NOT IN ( " );
        condition_string += condition;
        condition_string += _U( " )" );
        return *this;
    }

    template< DbColumnDerived Column >
    Join& _in( const QueryBuffer& condition )
    {
        QueryBuffer& conditionString = GetConditionString();

        conditionString += AliasDot< Column::DbType >();
        conditionString += Column::name_view.data();
        conditionString += _U( " IN ( " );
        conditionString += condition;
        conditionString += _U( " )" );

        return *this;
    }

    template< DbColumnDerived T1Column, DbColumnDerived T2Column >
    Join& Equal()
    {
        return _CompareTwoColumn< T1Column, T2Column >( _U( " = " ) );
    }

    template< DbColumnDerived Column, typename Condition >
    Join& Equal( Condition&& condition )
    {
        return _compare< Column >( _U( " = " ), std::forward< Condition >( condition ) );
    }

    template< DbColumnDerived T1Column, DbColumnDerived T2Column >
    Join& NotEqual()
    {
        return _CompareTwoColumn< T1Column, T2Column >( _U( " <> " ) );
    }

    template< DbColumnDerived Column, typename Condition >
    Join& NotEqual( Condition&& condition )
    {
        return _compare< Column >( _U( " <> " ), std::forward< Condition >( condition ) );
    }

    template< DbColumnDerived T1Column, DbColumnDerived T2Column >
    Join& Greater()
    {
        return _CompareTwoColumn< T1Column, T2Column >( _U( " > " ) );
    }

    template< DbColumnDerived Column, typename Condition >
    Join& Greater( Condition&& condition )
    {
        return _compare< Column >( _U( " > " ), std::forward< Condition >( condition ) );
    }

    template< DbColumnDerived T1Column, DbColumnDerived T2Column >
    Join& GreaterOrEqual()
    {
        return _CompareTwoColumn< T1Column, T2Column >( _U( " >= " ) );
    }

    template< DbColumnDerived Column, typename Condition >
    Join& GreaterOrEqual( Condition&& condition )
    {
        return _compare< Column >( _U( " >= " ), std::forward< Condition >( condition ) );
    }

    template< DbColumnDerived T1Column, DbColumnDerived T2Column >
    Join& Less()
    {
        return _CompareTwoColumn< T1Column, T2Column >( _U( " < " ) );
    }

    template< DbColumnDerived Column, typename Condition >
    Join& Less( Condition&& condition )
    {
        return _compare< Column >( _U( " < " ), std::forward< Condition >( condition ) );
    }

    template< DbColumnDerived T1Column, DbColumnDerived T2Column >
    Join& LessOrEqual()
    {
        return _CompareTwoColumn< T1Column, T2Column >( _U( " <= " ) );
    }

    template< DbColumnDerived Column, typename Condition >
    Join& LessOrEqual( Condition&& condition )
    {
        return _compare< Column >( _U( " <= " ), std::forward< Condition >( condition ) );
    }

    Join& Limit( size_t count = 1 )
    {
        _limit = count;

        return *this;
    }

    template< DbColumnDerived T1Column, DbColumnDerived T2Column >
    Join& _CompareTwoColumn( QueryBufferView operation )
    {
        // typename T1Column::Type forDeclType;
        QueryBuffer& conditionString = GetConditionString();
        conditionString += AliasDot< typename T1Column::DbType >();
        conditionString += T1Column::nameViewW;
        conditionString += operation;
        conditionString += AliasDot< typename T2Column::DbType >();
        conditionString += T2Column::nameViewW;

        return *this;
    }

    template< DbColumnDerived Column, typename Condition >
    Join& _compare( QueryBufferView operation, Condition&& condition )
    {
        QueryBuffer& conditionString = GetConditionString();
        conditionString += AliasDot< typename Column::DbType >();
        conditionString += Column::nameViewW;

        conditionString += operation;

        if constexpr ( std::is_same_v< typename std::remove_cvref< Condition >::type, QueryBuffer > )
        {
            conditionString += '\"';
            conditionString += condition;
            conditionString += '\"';
        }
        else if constexpr ( std::is_same_v< typename std::remove_cvref< Condition >::type, std::string > )
        {
            //conditionString += '\"';
            //conditionString += condition;
            //conditionString += '\"';
        }
        else
        {
            conditionString += std::to_wstring( condition );
        }

        return *this;
    }

    [[ nodiscard ]] QueryBuffer& GetConditionString()
    {
        switch ( _lastConditionOperator )
        {
        case EConditionOperator::On:
            return _onCondition;
        case EConditionOperator::Where:
            return _whereCondition;
        case EConditionOperator::GroupBy:
            return _groupByCondition;
        }
    }

    [[ nodiscard ]] constexpr bool HasOnCondition() const
    {
        return _onCondition.size() > 4; // " ON "
    }

    [[ nodiscard ]] constexpr bool HasGroupByCondition() const
    {
        return _onCondition.size() > 10; // " GROUP BY "
    }

    [[ nodiscard ]] constexpr bool HasWhereCondition() const
    {
        return _onCondition.size() > 7; // " WHERE "
    }

    [[ nodiscard ]] constexpr bool HasLimitCondition() const
    {
        return _limit > 0;
    }

#pragma region JoinWhere

    struct JoinWhere
    {
        JoinWhere( Join& join )
        : _join{ join }
        {
            join._onCondition += _U(" WHERE ");

            join._lastConditionOperator = EConditionOperator::Where;
        }

        template< DbColumnDerived Column, typename Condition >
        Join& Equal( Condition&& condition )
        {
            return _join.Equal< Column >( std::forward< Condition >( condition ) );
        }

        template< DbColumnDerived Column, typename Condition >
        Join& NotEqual( Condition&& condition )
        {
            return _join.NotEqual< Column >( std::forward< Condition >( condition ) );
        }

        Join& _join;
    };

    JoinWhere Where_()
    {
        return JoinWhere( *this );
    }

#pragma endregion

#pragma region JoinOn

    /*join.On_().Equal< User::PcId, Pc::Id >()
          .Where_().Equal< User::Account >( account )
          .And_().Equal< User::Password >( password )
          .Limit( 1 );*/

    struct JoinOn
    {
        JoinOn( Join& join )
        : _join{ join }
        {
            join._onCondition += _U(" ON ");

            join._lastConditionOperator = EConditionOperator::On;
        }

        template< DbColumnDerived T1Column, DbColumnDerived T2Column >
        Join& Equal()
        {
            return _join.Equal< T1Column, T2Column >();
        }

        template< DbColumnDerived T1Column, DbColumnDerived T2Column >
        Join& NotEqual()
        {
            return _join.NotEqual< T1Column, T2Column >();
        }

        template< DbColumnDerived T1Column, DbColumnDerived T2Column >
        Join& Greater()
        {
            return _join.Greater< T1Column, T2Column >();
        }

        template< DbColumnDerived T1Column, DbColumnDerived T2Column >
        Join& GreaterOrEqual()
        {
            return _join.GreaterOrEqual< T1Column, T2Column >();
        }

        template< DbColumnDerived T1Column, DbColumnDerived T2Column >
        Join& Less()
        {
            return _join.Less< T1Column, T2Column >();
        }

        template< DbColumnDerived T1Column, DbColumnDerived T2Column >
        Join& LessOrEqual()
        {
            return _join.LessOrEqual< T1Column, T2Column >();
        }

        Join& _join;
    };

    JoinOn On_()
    {
        return JoinOn( *this );
    }

#pragma endregion JoinOn

private:
    T1 _t1; //<
    T2 _t2; //<

    QueryBuffer             _onCondition;       //<
    QueryBuffer             _whereCondition;    //<
    QueryBuffer             _groupByCondition;  //<
    size_t                  _limit = 0;         //< Limit 조건( 기본값 0 )
    EConditionOperator      _lastConditionOperator = EConditionOperator::On; //

    Mala::Db::DbConnection* _connection;

    BindColumnList          _t1BindColumns;
    BindColumnList          _t2BindColumns;

    int _columnCount = 0;

    inline static size_t     _max_query_length = 64;
};

NAMESPACE_END( Mala::Db )
