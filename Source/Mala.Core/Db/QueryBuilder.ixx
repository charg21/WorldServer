export module Mala.Db.QueryBuilder;

#include "../MalaMacro.h"

import Mala.Container;
import Mala.Container.String;
import Mala.Db.DbTypes;
import Mala.Db.DbModel;
import Mala.Db.DbConnection;

using namespace Mala::Container;

NAMESPACE_BEGIN( Mala::Db )

struct DbModel;

/// <summary>
/// 쿼리 생성기
/// </summary>
struct QueryBuilder
{
public:
    using ModelSet    = HashSet< DbModel* >;
    using ModelList   = Vector< DbModel* >;
    using QueryBuffer = String;

    /// <summary>
    /// 생성자
    /// </summary>
    QueryBuilder() = default;

    /// <summary>
    /// Flush 쿼리를 생성한다
    /// </summary>
    static const String ToUpdateQuery( const DbModel& model, DbConnection* connection );

    /// <summary>
    /// Insert 쿼리를 생성한다
    /// </summary>
    static const String ToInsertQuery( const DbModel& model, DbConnection* connection );

    /// <summary>
    /// bulk insert 쿼리를 생성한다
    /// </summary>
    static const String ToBulkInsertQuery( HashSet< DbModel* >& modelSet, DbConnection* connection );

    /// <summary>
    /// upsert 쿼리를 생성한다
    /// </summary>
    static const String ToUpsertQuery( const DbModel& model, DbConnection* connection );

    /// <summary>
    /// select 쿼리를 생성한다
    /// </summary>
    static const String ToSelectQuery( const DbModel& model, DbConnection* connection );
    static const String ToSelectManyQuery( const DbModel& model, DbConnection* connection );
    static const String ToSelectAllQuery( const DbModel& model, DbConnection* connection );
    static const String ToSelectCountQuery( const DbModel& model, DbConnection* connection );

    /// <summary>
    /// Delete 쿼리를 생성한다
    /// </summary>
    static const String MakeDeleteQuery( const DbModel& model, DbConnection* connection );
    static void AppendWhereConditionUsingIndex( const IndexInfo* indexInfo, const DbModel& model, String& queryBuffer );

    /// <summary>
    /// Insert2 쿼리를 생성한다
    /// </summary>
    const String ToInsertQuery2( DbConnection* connection );

    void Add( DbModel* model )
    {
        if ( !model )
            return;

        if ( _modelSet.insert( model ).second )
            _modelList.push_back( model );
    }

public:
    /// <summary>
    ///
    /// </summary>
    ModelSet  _modelSet;

    /// <summary>
    ///
    /// </summary>
    ModelList _modelList;
};

NAMESPACE_END( Mala::Db )
