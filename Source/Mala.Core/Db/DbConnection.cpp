
import <string>;
import <iostream>;
import <functional>;

import Mala.Container.LockFreeStack;
import Mala.Container.String;
import Mala.Text;
import Mala.Db.MySql;
import Mala.Db.DbTypes;
import Mala.Db.DbConnection;

using namespace Mala::Db;
using namespace Mala::Text;

// https://mariadb.com/kb/en/mariadb-connectorc-types-and-definitions/
bool DbConnection::Connect(
    const std::string& host,
    const std::string& user,
    const std::string& password,
    StringRef schema,
    const int port )
{
    _connection = MySql::Init( nullptr );
    MySql::DisableSslMode( _connection );

    _schema   = Utf16::ToUtf8( schema );
    _schemaW = schema;

    auto schema_utf8 = Utf16::ToUtf8( schema );

    _connection = MySql::Connect(
        _connection,
        host.c_str(),
        user.c_str(),
        password.c_str(),
        schema_utf8.c_str(),
        port,
        nullptr,
        0 );

    if ( _connection == nullptr )
    {
        printf( "Mysql connection error : %s\n", MySql::Error( _connection ) );
        _error_code = 1;
        return false;
    }

    //SetCharacterSet();
    //printf( "Mysql connection error : %s\n", mysql_get_ssl_cipher( _connection ) );

    return true;
}

void DbConnection::Disconnect()
{
    if ( _connection == nullptr )
        return;

    MySql::Close( _connection );
}

bool DbConnection::Fetch()
{
    if ( !_results )
        return false;

    _cursor = MySql::FetchRow( _results );
    if ( !_cursor )
        return false;

    return true;
}

bool DbConnection::Query( const std::string& queryString )
{
    int result = MySql::Query( _connection, queryString.c_str(), queryString.size() );
    if ( 0 != result )
    {
        printf( "Mysql connection error %d : %s, query: %s\n",
            MySql::ErrorNo( _connection ),
            MySql::Error( _connection ), queryString.c_str() );
        _error_code = 1;
        return false;
    }

    // 결과 영향받은 로우수
    if ( IsSelectQuery( queryString ) )
    {
        _results = MySql::StoreResult( _connection );
        _affectedRowCount = MySql::NumRows( _results );
        _affected_column_count = MySql::NumFields( _results );
    }
    else
    {
        _affectedRowCount = MySql::AffectedRows( _connection );
    }

    return true;
}

const bool DbConnection::IsSelectQuery( const std::string& query )
{
    return 0 == strncmp( query.c_str(), "SELECT", 6 );
}

const bool DbConnection::IsSelectQuery( const std::wstring& query )
{
    return 0 == wcsncmp( query.c_str(), L"SELECT", 6 );
}

bool DbConnection::Query( StringView queryWString )
{
    auto queryString = ::Utf16::ToUtf8( queryWString.data(), queryWString.size() );
    return Query( queryString );
}

MySql::Row DbConnection::GetRow()
{
    return _cursor;
}

void DbConnection::Reset()
{
    if ( _results )
    {
        MySql::FreeResult( _results );
        _results = nullptr;
    }

    _affectedRowCount = 0;
}

bool DbConnection::BeginTransaction()
{
    return 0 == MySql::AutoCommit( _connection, false );
    //return Query( "START TRANACTION" );
}

bool DbConnection::SetAutoCommit( bool autoMode )
{
    return 0 == MySql::AutoCommit( _connection, autoMode );
}

bool DbConnection::SetForeignKeyCheck( bool check )
{
    return Query( std::format( "SET foreign_key_checks = {};", check ? 1 : 0 ) );
}

bool DbConnection::Commit()
{
    return 0 == MySql::Commit( _connection );

    //return set_autocommit( true );
}

bool DbConnection::Rollback()
{
    //return set_autocommit( true );
    return 0 == MySql::Rollback( _connection );
}

/// <summary>
/// 주의 Utf16, utf32는 사용이 불가능하다
/// </summary>
void DbConnection::SetCharacterSet()
{
    MySql::SetCharacterSet( _connection, "Utf16" );
}

std::wstring_view DbConnection::GetSchemaW()
{
    return _schemaW;
}

bool DbConnectionPool::Connect( StringRef host, StringRef user, StringRef password, StringRef schema, const int port, size_t connectionCount )
{
    _host = ::Utf16::ToUtf8( host );
    _user = ::Utf16::ToUtf8( user );
    _password = ::Utf16::ToUtf8( password );
    _schema = ::Utf16::ToUtf8( schema );

    _schemaW = schema;
    _port = port;

    std::vector< DbConnection* > connections;
    for ( int n{}; n < connectionCount; n += 1 )
    {
        DbConnection* newConnection = GetOrDefault();
        if ( !newConnection )
        {
            return false;
        }

        connections.push_back( newConnection );
    }

    for ( auto* connection : connections )
    {
        Release( connection );
    }

    return true;
}

DbConnection* DbConnectionPool::Get()
{
    DbConnection* outDbConnection;

    if ( _connections.TryPop( outDbConnection ) )
        return outDbConnection;

    return nullptr;
}

inline DbConnection* DbConnectionPool::GetOrDefault()
{
    DbConnection* outDbConnection{};
    if ( _connections.TryPop( outDbConnection ) )
        return outDbConnection;

    outDbConnection = new DbConnection();
    if ( !outDbConnection->Connect( _host, _user, _password, _schemaW, _port ) )
    {
        delete outDbConnection;
        return nullptr;
    }

    return outDbConnection;
}

inline void DbConnectionPool::Release( DbConnection* connection )
{
    connection->Reset();

    _connections.push( connection );
}

inline void DbConnectionPool::ForEachDbConnection( const Action< DbConnection* >& job )
{
}
