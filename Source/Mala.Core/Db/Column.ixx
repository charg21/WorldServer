export module Mala.Db.Column;

#include "../MalaMacro.h"

import Mala.Core.Variant;

using namespace Mala::Core;

NAMESPACE_BEGIN( Mala::Db )

class ColumnInfo;

/// <summary>
/// Db 모델 컬럼 객체
/// </summary>
struct Column
{
    using ColumnInfoRef = const ColumnInfo*;

    /// <summary>
    /// 생성자
    /// </summary>
    Column( ColumnInfo* info );
    Column( const Column& column );
    Column( Column&& column ) noexcept;

    void operator=( Column& field );
    void operator=( Column&& field ) noexcept;

    Variant        _var;  //< 값
    ColumnInfoRef  _info; //< 컬럼 정보
};

template< typename T >
concept DbColumnType = std::is_same< Column, T >::value;

NAMESPACE_END( Mala::Db )
