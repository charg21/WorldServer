import Mala.Db.Column;
import Mala.Db.ColumnInfo;

import Mala.Core.Variant;
import Mala.Db.ColumnInfo;

using namespace Mala::Db;

Column::Column( ColumnInfo* info )
: _var { info->_type }
, _info{ info        }
{
};

Column::Column( const Column& column )
: _var { column._var  }
, _info{ column._info }
{
}

Column::Column( Column&& column ) noexcept
: _var { std::move( column._var ) }
, _info{ column._info             }
{
}

void Column::operator=( Column& other )
{
	if ( this == &other )
		return;

    _var  = other._var;
	_info = other._info;
}


void Column::operator=( Column&& other ) noexcept
{
	if ( this == &other )
		return;

	_var  = std::move( other._var );
	_info = other._info;
}
