import <string>;

import Mala.Container;
import Mala.Container.String;
import Mala.Container.StringHelper;

import Mala.Db.Column;
import Mala.Db.ColumnInfo;
import Mala.Db.DbConnection;
import Mala.Db.DbModel;
import Mala.Db.IndexInfo;
import Mala.Db.Join;
import Mala.Db.QueryBuilder;
import Mala.Db.Table;

#define UTF16
#ifdef UTF16
#define _U(STR) (L##STR)
#else
#define _U(STR) (STR)
#endif // UNICODE

using namespace Mala::Container;
using namespace Mala::Db;



/// <summary>
/// 이미 생성된 객체 pk 가지고 있음.
/// </summary>
const String QueryBuilder::ToUpdateQuery( const DbModel& model, DbConnection* dbConn )
{
    /*
        UPDATE table_name
        SET column1 = value1, column2 = value2, ...
        WHERE condition;
    */
    if ( !model.HasUpdateColumn() )
        return { _U( "can't find update column...." ) };

    const TableInfo& table = model._table;

    String queryBuffer;
    queryBuffer.reserve( 512 );
    queryBuffer += std::format( _U( "UPDATE {}.{} SET " ),
        dbConn->_schemaW,
        table._nameW );

    /// set 문
    bool isAllField = true;
    for ( Column* col : model._columnList )
    {
        const ColumnInfo* columnInfo = col->_info;
        if ( !model.IsUpdateColumn( columnInfo->_columnNo ) )
        {
            isAllField = false;
            continue;
        }

        // 해당 컬럼이 업데이트 됐는지
        // pass;
        // else
        queryBuffer += std::format( _U( "{} = {}, " ),
            columnInfo->_nameW,
            col->_var.ToString() );
    }

    queryBuffer.pop_back();
    queryBuffer.push_back( _U( ' ' ) ); // remove ' ';

    // pk 부터 체크.
	if ( auto* indexInfo = model.GetFirstValidIndex() )
	{
		queryBuffer += _U( "WHERE " );
		AppendWhereConditionUsingIndex( indexInfo, model, queryBuffer );
	}
    queryBuffer.push_back( _U( ';' ) );
    return queryBuffer;
}

// note DbModel 객체에 PK 설정이 필요함
const String QueryBuilder::ToInsertQuery( const DbModel& model, DbConnection* dbConn )
{
/*
    INSERT INTO table_name (column1, column2, column3, ...)
    VALUES (value1, value2, value3, ...);

    INSERT INTO table_name
    VALUES (value1, value2, value3, ...);
*/
    String queryBuffer;
    queryBuffer.reserve( 512 );

    queryBuffer += std::format( _U( "INSERT INTO {}.{} VALUES (" ),
        dbConn->_schemaW,
        model._table._nameW );

    int beforeQueryLength = queryBuffer.size();
    for ( /*todo: const */ Column* col : model._columnList )
    {
        queryBuffer += col->_var.ToString();
        queryBuffer += _U( ", " );
    }

    if ( beforeQueryLength < queryBuffer.size() )
    {
        queryBuffer = _U( "error..." );
        return queryBuffer;
    }

	RemoveLast( queryBuffer, 2 );
    queryBuffer += _U( " );" );

    return std::move( queryBuffer );
}

// note DbModel 객체에 PK 설정이 필요함
const String QueryBuilder::ToInsertQuery2( DbConnection* dbConn )
{
    DbModel* model = *_modelSet.begin();
    /*if ( !model )
        return "null db model";*/

    String queryBuffer;
    queryBuffer.reserve( 512 );
    queryBuffer += std::format( _U( "INSERT INTO {}.{} VALUES ( " ),
        dbConn->_schemaW,
        model->_table._nameW );

    int insertedCount = 0;
    for ( /*todo: const */ Column* col : model->_columnList )
    {
        queryBuffer += col->_var.ToString();
        queryBuffer += _U( ", " );

        insertedCount += 1;
    }

    if ( insertedCount == 0 )
    {
        queryBuffer = _U( "error..." );
        return queryBuffer;
    }

    RemoveLast( queryBuffer, 2 );
    queryBuffer += _U( " );" );

    return std::move( queryBuffer );
}

/// bulk insert query를 만든다
const String QueryBuilder::ToBulkInsertQuery( HashSet< DbModel* >& modelSet, DbConnection* dbConn )
{
    if ( modelSet.empty() )
        return _U( "empty object set" );

    const DbModel* model = *modelSet.begin();

    String queryBuffer;
    queryBuffer.reserve( 1024 );

    queryBuffer += std::format( _U( "INSERT INTO {}.{} VALUES" ),
        dbConn->_schemaW,
        model->_table._nameW );

    int insertedCount = 0;
    for ( DbModel* model : modelSet )
    {
        queryBuffer += _U( "( " );

        for ( /*todo: const */ Column* col : model->_columnList )
        {
            queryBuffer += col->_var.ToString();
            queryBuffer += _U( ", " );

            insertedCount += 1;
        }

        RemoveLast( queryBuffer, 2 );
        queryBuffer += _U( " ), " );
    }

    RemoveLast( queryBuffer, 2 );
    queryBuffer += _U( ';' );

    return std::move( queryBuffer );
}

const String QueryBuilder::ToUpsertQuery( const DbModel& model, DbConnection* connection )
{
    auto* indexInfo = model.GetFirstValidIndex();
    if ( !indexInfo )
        return _U( "Can't Find Valid Index....  QueryBuilder::MakeUpsertQuery()" );

    /*
    INSERT INTO 테이블명 (userId, name, age)
    values (1, '엄준식', 25)
    ON DUPLICATE KEY UPDATE name = '엄준식', age = 19;
    */
    String queryBuffer;
    queryBuffer.reserve( 512 );

    queryBuffer += ToInsertQuery( model, connection );
    queryBuffer.pop_back();
    queryBuffer += _U( " ON DUPLICATE KEY UPDATE " );

    for ( const ColumnInfo* columnInfo : indexInfo->_columnInfos )
    {
        Column* col = model._columnList[ columnInfo->_columnNo ];

        queryBuffer += std::format( _U( "{} = {}, " ),
            columnInfo->_nameW,
            col->_var.ToString() );
    }

    size_t beforeQueryLength = queryBuffer.size();
    for ( /* todo : const */Column* col : model._columnList )
    {
        if ( !model.IsUpdateColumn( col->_info->_columnNo ) )
            continue;
        // 해당 컬럼이 업데이ㅡ 됐는지
        // pass;
        // else
        queryBuffer += std::format( _U( "{} = {}, " ),
            col->_info->_nameW,
            col->_var.ToString() );
    }

    if ( beforeQueryLength < queryBuffer.size() )
    {
        RemoveLast( queryBuffer, 2 );
        queryBuffer.push_back( _U( ' ' ) ); // remove ' ';

        RemoveLast( queryBuffer, 2 );
        queryBuffer.push_back( _U( ';' ) ); // remove ','
    }
    else
    {
        queryBuffer = _U( "can't find updated column...." );
    }

    return std::move( queryBuffer );
}

const String QueryBuilder::ToSelectQuery( const DbModel& model, DbConnection* connection )
{
    auto* indexInfo = model.GetFirstValidIndex();
    if ( !indexInfo )
        return _U( "Can't Find Valid Index....  QueryBuilder::ToSelectQuery()" );

    String queryBuffer;
    queryBuffer.reserve( 512 );
    queryBuffer += std::format( _U( "SELECT * FROM {}.{} WHERE " ),
        connection->_schemaW,
        model._table._nameW );

    // pk 부터 체크.
	AppendWhereConditionUsingIndex( indexInfo, model, queryBuffer );
    queryBuffer.push_back( _U( ';' ) );

    return std::move( queryBuffer );
}

const String QueryBuilder::ToSelectManyQuery( const DbModel& model, DbConnection* dbConn )
{
    if ( !model.HasKeySetupColumn() )
		return _U( "Can't Find Key Setup Column....  QueryBuilder::ToSelectManyQuery()" );

    String queryBuffer;
    queryBuffer.reserve( 512 );
    queryBuffer += std::format( _U( "SELECT * FROM {}.{} WHERE " ),
        dbConn->_schemaW,
        model._table._nameW );

    const TableInfo& table = model._table;

    // pk 부터 체크.
    for ( const ColumnInfo* columnInfo : table._columnInfos )
    {
		if ( !model.IsKeySetupColumn( columnInfo->_columnNo ) )
			continue;

        Column* col = model._columnList[ columnInfo->_columnNo ];
        queryBuffer += std::format( _U( "{} = {} AND " ), columnInfo->_nameW, col->_var.ToString() );
    }

    // remove " AND "
    RemoveLast( queryBuffer, 5 );
    queryBuffer += _U( ';' );

    return std::move( queryBuffer );
}

const String QueryBuilder::ToSelectAllQuery( const DbModel& model, DbConnection* dbConn )
{
    String queryBuffer{ std::format( _U( "SELECT * FROM {}.{}" ),
        dbConn->_schemaW,
        model._table._nameW ) };
    return std::move( queryBuffer );
}

const String QueryBuilder::ToSelectCountQuery( const DbModel& model, DbConnection* dbConn )
{
    String queryBuffer;
    queryBuffer.reserve( 256 );

    /// 단순 Count시 SELECT (*) 보다 SELECT( ColumnName )로 DB 내부 조회량을 줄임
    queryBuffer += std::format( _U( "SELECT count( {} ) FROM {}.{}" ),
        model._table.GetColumnInfoList()[ 0 ]->_nameW,
        dbConn->_schemaW,
        model._table._nameW );

    return std::move( queryBuffer );
}

const String QueryBuilder::MakeDeleteQuery( const DbModel& model, DbConnection* dbConn )
{
    auto* indexInfo = model.GetFirstValidIndex();
    if ( !indexInfo )
		return _U( "Can't Find Valid Index....  QueryBuilder::MakeDeleteQuery()" );

    /*
    DELETE FROM table_name WHERE condition;
    */
    String queryBuffer;
    queryBuffer.reserve( 512 );
    queryBuffer += std::format( _U( "DELETE FROM {}.{} WHERE " ),
        dbConn->_schemaW,
        model._table._nameW );

    // make condition, using pk
    AppendWhereConditionUsingIndex( indexInfo, model, queryBuffer );

    queryBuffer.push_back( _U( ';' ) );

    return std::move( queryBuffer );
}

void QueryBuilder::AppendWhereConditionUsingIndex(
    const IndexInfo* indexInfo,
    const DbModel&   model,
          String&    queryBuffer )
{
    for ( const ColumnInfo* columnInfo : indexInfo->_columnInfos )
    {
        Column* col = model._columnList[ columnInfo->_columnNo ];
        queryBuffer += std::format( _U( "{} = {} AND " ),
            columnInfo->_nameW,
            col->_var.ToString() );
    }

    RemoveLast( queryBuffer, 5 );
}
