export module Mala.Db.DbModelCache;

#include "../CoreMacro.h"

import <memory>;

import Mala.Core.Types;
import Mala.Core.Variant;
import Mala.Core.StrongType;
import Mala.Container;
import Mala.Container.String;
import Mala.Container.LruCache;
import Mala.Container.LockFreeStack;
import Mala.Container.LockDeque;
import Mala.Db.DbTypes;
import Mala.Db.DbModel;
import Mala.Db.DbConnection;
import Mala.Threading.Lock;

using namespace Mala::Core;
using namespace Mala::Container;
using namespace Mala::Threading;

NAMESPACE_BEGIN( Mala::Db )

using Db1Key = std::tuple< Variant >;
using Db2Key = std::tuple< Variant, Variant >;
using Db3Key = std::tuple< Variant, Variant, Variant >;
using Db4Key = std::tuple< Variant, Variant, Variant, Variant >;
using Db5Key = std::tuple< Variant, Variant, Variant, Variant, Variant >;

/// <summary>
/// DB모델 캐시의 인터페이스
/// </summary>
class IDbModelCache
{
protected:
    EDbModelCachePolicy _cachePolicy;

public:
    IDbModelCache( EDbModelCachePolicy cachePolicy )
    : _cachePolicy{ cachePolicy }
    {
    }

    virtual void Update() = 0;
    virtual void LoadFromDb() = 0;
    virtual ~IDbModelCache() = default;

    //virtual bool Contains( const Db1Key& key ) { CRASH( "Contains( const Db1Key& key )" ); return {}; }
    //virtual bool Contains( const Db2Key& key ) { CRASH( "Contains( const Db2Key& key )" ); return {}; }
    //virtual bool Contains( const Db3Key& key ) { CRASH( "Contains( const Db3Key& key )" ); return {}; }
    //virtual bool Contains( const Db4Key& key ) { CRASH( "Contains( const Db4Key& key )" ); return {}; }
    //virtual bool Contains( const Db5Key& key ) { CRASH( "Contains( const Db5Key& key )" ); return {}; }
    virtual void Add( const std::shared_ptr< DbModel >& dbModel ) = 0;
    virtual void Add( std::shared_ptr< DbModel >&& dbModel ) = 0;
    virtual void Remove( const std::shared_ptr< DbModel >& dbModel ){};
    virtual void Remove( std::shared_ptr< DbModel >&& dbModel ){};
    virtual void Update( const std::shared_ptr< DbModel >& dbModel ){};
    virtual void Update( std::shared_ptr< DbModel >&& dbModel ){};

    const EDbModelCachePolicy GetCachePolicy() const { return _cachePolicy; }
};

class NullDbModelCache final : public IDbModelCache
{
public:
    NullDbModelCache() : IDbModelCache( EDbModelCachePolicy::None ){}
    ~NullDbModelCache() final = default;
    void Update() final {}
    void Add( const std::shared_ptr< DbModel >& dbModel ) final { return; }
    void Add( std::shared_ptr< DbModel >&& dbModel ) final { return; }
    void LoadFromDb(){};
};

template< DbModelType T, DbIndexDerived TDbIndex >
class DbModelCache : public IDbModelCache
{
public:
    using KeyType = std::decay_t< decltype( T{}.GetIndexKey< TDbIndex >() ) > ;

public:
    DbModelCache( EDbModelCachePolicy cachePolicy )
    : IDbModelCache( cachePolicy )
    {
    }

    ~DbModelCache() override = default;

    virtual TDbModelPtr< T > GetModel( const KeyType& key ){ return {}; }
};

template< DbModelType T, DbIndexDerived TDbIndex >
class PersistentDbModelCache : public DbModelCache< T, TDbIndex >
{
    /// <summary>
    /// PersistentDbModelCache에서 사용하는 DbModel객체는 버전별로 관리됨
    /// 이전 버전의 객체를 스레드 세이프하게 접근하기 위해
    /// </summary>
    using TDbModelHistory = LockDeque< TDbModelPtr< T > >;
    using KeyType         = typename DbModelCache< T, TDbIndex >::KeyType;

public:
    PersistentDbModelCache()
    : DbModelCache< T, TDbIndex >( EDbModelCachePolicy::Persistent )
    , _container{ 8192 }
    {
    }

    ~PersistentDbModelCache() override = default;
    void Update() final {}

    void Add( const std::shared_ptr< DbModel >& dbModel ) final
    {
        auto& history{ GetHistory( dbModel->GetIndexKey< TDbIndex >() ) };

        history.push_front( std::static_pointer_cast< T >( dbModel ) );
    }

    void Add( std::shared_ptr< DbModel >&& dbModel ) final
    {
        auto& history{ GetHistory( dbModel->GetIndexKey< TDbIndex >() ) };

        history.push_front( std::static_pointer_cast< T >( std::move( dbModel ) ) );
    }

    void Update( const std::shared_ptr< DbModel >& dbModel ) final
    {
        Add( dbModel );
    }

    void Update( std::shared_ptr< DbModel >&& dbModel ) final
    {
        Add( std::move( dbModel ) );
    }

    bool Contains( const KeyType& key )
    {
        /// contains가 없는것..
        return _container.find( key ) != _container.end();
    }

    TDbModelHistory& GetHistory( const KeyType& key )
    {
        return _container[ key ];
    }

    TDbModelPtr< T > GetModel( const KeyType& key ) final
    {
        auto& history{ GetHistory( key ) };

        return{ history.front() };
    }

private:
    ConcurrentHashMap< KeyType, TDbModelHistory > _container;
};

template< DbModelType T, DbIndexDerived TDbIndex >
class LruDbModelCache : public DbModelCache< T, TDbIndex >
{
public:
    using KeyType = typename DbModelCache< T, TDbIndex >::KeyType;

public:
    LruDbModelCache( u32 capacity )
    : DbModelCache< T, TDbIndex >( EDbModelCachePolicy::Lru )
    {
    }

    ~LruDbModelCache() final = default;
    void Update() final {}

private:
    RWLock _lock;
    LruCache< KeyType, TDbModelPtr< T > > _container;
};

NAMESPACE_END( Mala::Db )
