export module Mala.Db.DbModelHelper;

#include "../CoreMacro.h"

import <memory>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Db.DbTypes;
import Mala.Db.DbConnection;
import Mala.Db.QueryBuilder;
import Mala.Db.DbModel;
import Mala.Db.DbModelFactory;
import Mala.Db.DbModelMapper;

using namespace Mala::Container;

NAMESPACE_BEGIN( Mala::Db )

template< DbModelType T, typename TDbIndex >
struct ToIndexKey
{
    using Type = decltype( T{}.GetIndexKey< TDbIndex >() );
};

template< DbModelType T, typename TDbIndex >
using ToIndexKeyType = ToIndexKey< T, TDbIndex >::Type;

extern i64 Count( DbModel& forSelect, DbConnection* dbConn );
extern DbConnection* GetDbConnection();

template< typename T > requires DbModelType< T >
TDbModelPtr< T > Select( T& forSelect, DbConnection* dbConn )
{
    if ( !dbConn )
        return {};

    auto selectQuery{ QueryBuilder::ToSelectQuery( forSelect, dbConn ) };
    if ( !dbConn->Query( selectQuery ) || !dbConn->Fetch() )
        return {};

    return DbModelMapper::Map< T >( dbConn->GetRow() );
}

template< DbModelType T >
DbSelectListResult< T > SelectMany( T& forSelect, DbConnection* dbConn )
{
    if ( !dbConn )
        return {};

    auto selectManyQuery{ QueryBuilder::ToSelectManyQuery( forSelect, dbConn ) };
    if ( !dbConn->Query( selectManyQuery ) || !dbConn->Fetch() )
        return {};

    TDbModelPtrVector< T > list;

    do
    {
        list.emplace_back( DbModelMapper::Map< T >( dbConn->GetRow() ) );
    } while ( dbConn->Fetch() );

    return list;
}

template< DbModelType T >
DbSelectListResult< T > SelectAll( T& forSelect, DbConnection* dbConn )
{
    i64 rowCount{ Count( forSelect, dbConn ) };
    if ( rowCount == 0 )
        return {};

    dbConn->Reset();
    auto selecAllQuery{ QueryBuilder::ToSelectAllQuery( forSelect, dbConn ) };
    if ( !dbConn->Query( selecAllQuery ) || !dbConn->Fetch() )
        return {};

    TDbModelPtrVector< T > list;
    list.reserve( rowCount );

    do
    {
        list.emplace_back( DbModelMapper::Map< T >( dbConn->GetRow() ) );
    } while ( dbConn->Fetch() );

    return list;
}

NAMESPACE_END( Mala::Db )
