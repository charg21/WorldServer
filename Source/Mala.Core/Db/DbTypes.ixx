export module Mala.Db.DbTypes;

#include "../MalaMacro.h"
import <memory>;
import Mala.Core.Types;


NAMESPACE_BEGIN( Mala::Db )

/// <summary>
/// DB 변수 타입
/// </summary>
enum class EDbType : u8
{
    Bool,
    Short,
    UShort,
    Int,
    UInt,
    Long,
    ULong,
    Single,
    Double,

    String,
    WString,
    Timestamp,
    DateTime,
};

/// <summary>
/// 조인 타입
/// </summary>
enum class EJoinType : u8
{
    Left, // 레프트 조인
    Inner,// 이너 조인
    Self, // 셀프 조인
};

/// <summary>
/// 쿼리 타입
/// </summary>
enum class EQueryType : u8
{
    Insert, // 인서트
    Update, // 업데이트
    Upsert, // 업서트
    Delete, // 델리트
    Select, // 셀렉트
    Max
};

/// <summary>
/// DB 결과
/// </summary>
enum class EDbResult
{
    /// <summary>
    /// 쿼리 실패
    /// </summary>
    QueryFailure = 0,

    /// <summary>
    /// 성공
    /// </summary>
    Success = 1 << 0,

    /// <summary>
    /// 데이터가 없음
    /// </summary>
    NoData = 1 << 1,
};

/// <summary>
/// 관계 연산자( 추후에 DB외에서도 쓰일듯 )
/// </summary>
enum class ERelationComparer
{
    /// <summary>
    /// ==
    /// </summary>
    Equal,

    /// <summary>
    /// !=
    /// </summary>
    NotEqual,

    /// <summary>
    /// <
    /// </summary>
    Greater,

    /// <summary>
    /// <=
    /// </summary>
    GreaterThan,

    /// <summary>
    /// >
    /// </summary>
    Less,

    /// <summary>
    /// >=
    /// </summary>
    LessThan,

    /// <summary>
    /// in ( ... )
    /// </summary>
    In,

    /// <summary>
    /// not in ( ... )
    /// </summary>
    NotIn,
};

/// <summary>
/// 논리 연산자
/// </summary>
enum class ELogicalOperator
{
    Or,
    And,
    Not
};

/// <summary>
/// 조건 연산자
/// </summary>
enum class EConditionOperator
{
    On,
    GroupBy,
    Not,
    Where,

    Max
};

/// <summary>
/// DbModel 캐싱 정책
/// </summary>
enum class EDbModelCachePolicy : u8
{
    /// <summary>
    /// 없음
    /// </summary>
    None,

    /// <summary>
    /// 영원함( 프로세스의 종료시까지 유지 )
    /// </summary>
    Persistent,

    /// <summary>
    /// 최근에 사용된 것을 우선으로
    /// </summary>
    Lru,

    /// <summary>
    /// 마지막 사용 시간
    /// </summary>
    Timeout,
};

NAMESPACE_END( Mala::Db )
