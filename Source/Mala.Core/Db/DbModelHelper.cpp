import Mala.Db.DbModelHelper;

import Mala.Core.Types;
import Mala.Container;
import Mala.Container.String;
import Mala.Db.DbTypes;
import Mala.Db.DbConnection;
import Mala.Db.QueryBuilder;
import Mala.Db.DbModel;

i64 Mala::Db::Count( DbModel& forSelect, DbConnection* dbConn )
{
    if ( !dbConn )
        return {};

    auto selectCountQuery{ QueryBuilder::ToSelectCountQuery( forSelect, dbConn ) };
    if ( !dbConn->Query( selectCountQuery ) || !dbConn->Fetch() )
        return {};

    return std::stoi( dbConn->GetRow()[ 0 ] );
}
