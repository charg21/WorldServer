export module Mala.Http.HttpRequest;

import Mala.Core.Types;
import Mala.Http.HttpTypes;

export namespace Mala::Http
{

struct Uri
{
    std::string _path;
    std::string _params;
};

struct UriView
{
    std::string_view _path;
    std::string_view _params;
};

/// <summary>
/// Http 요청
/// </summary>
struct HttpRequest
{
    /// <summary>
    ///
    /// </summary>
    EHttpMethod _method;

    /// <summary>
    ///
    /// </summary>
    Uri _uri;

    /// <summary>
    ///
    /// </summary>
    std::string _body;
};

struct HttpRequestView
{
    /// <summary>
    ///
    /// </summary>
    EHttpMethod _method{};

    /// <summary>
    ///
    /// </summary>
    UriView _uri{};

    /// <summary>
    ///
    /// </summary>
    std::string_view _body{};
};


}

