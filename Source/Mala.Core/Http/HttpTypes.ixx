export module Mala.Http.HttpTypes;


export namespace Mala::Http
{

enum class EHttpMethod
{
    None,

    Get,
    Post,
    Delete,
    Put
};

enum class EHttpVersion
{
    None,

    Http1_0,
    Http1_1,
};

}

