export module Mala.Http.HttpClient;

import Mala.Core.Types;
import Mala.Http.HttpController;
import Mala.Http.HttpTypes;
import Mala.Net.HttpSession;


using namespace Mala::Net;

export namespace Mala::Http
{

class HttpClient : public HttpSession
{
public:
    void Post( const std::string& uri, const std::string& body );
    void Get( const std::string& uri );

private:
    std::unique_ptr< IHttpController > _controller;
};

}

