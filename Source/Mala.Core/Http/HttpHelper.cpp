import Mala.Core.Types;
import Mala.Http.HttpTypes;
import Mala.Http.HttpRequest;
import Mala.Http.HttpHelper;

using namespace Mala::Http;
//using namespace Mala::Http::HttpHelper;


size_t HttpHelper::GetContentLength( const std::string_view& request )
{
    size_t contentLengthStart = request.find( "Content-Length: ", 8 ); // 16글자
    if ( contentLengthStart == std::string::npos )
        return 0;

    size_t contentLength = 0;
    contentLengthStart += 16; // "Content-Length: "( 16 ) 문자열의 길이를 더합니다.
    auto endPos = request.find( "\r\n", contentLengthStart );
    if ( endPos != std::string::npos )
    {
        auto contentLengthStr = std::string( request.substr( contentLengthStart + 16 ) );
        contentLength = std::stoi( contentLengthStr );
    }

    return contentLength;
}

HttpRequestView HttpHelper::ParseHttpReqeustView( const std::string_view& request )
{
    auto methodEnd = request.find( ' ' );
    if ( methodEnd == std::string_view::npos )
    {
        // 요청 메소드가 없습니다.
        return HttpRequestView{};
    }

    std::string_view method = request.substr( 0, methodEnd );
    EHttpMethod httpMethod = ExtractMethod( method );
    if ( httpMethod == EHttpMethod::None ) // 지원하지 않는 메소드입니다.
        return HttpRequestView{};

    auto uriStart = methodEnd + 1;
    auto uriEnd = request.find( ' ', uriStart );
    if ( uriEnd == std::string_view::npos ) // 요청 메소드가 없습니다.
        return HttpRequestView{};

    std::string_view uri = request.substr( uriStart, uriEnd - uriStart );
    std::string_view uriParams;
    auto uriParamStart = uri.find( '?' ) + 1;
    if ( uriParamStart != std::string_view::npos )
        uriParams = uri.substr( uriParamStart );

    uri = uri.substr( 0, uriParamStart - 1 );

    return { httpMethod, UriView{ std::move( uri ), std::move( uriParams ) }, {} };
}

EHttpMethod HttpHelper::ExtractMethod( const std::string_view& method )
{
    if ( method == "GET" )
        return EHttpMethod::Get;

    if ( method == "POST" )
        return EHttpMethod::Post;

    if ( method == "PUT" )
        return EHttpMethod::Put;

    if ( method == "DELETE" )
        return EHttpMethod::Delete;

    //if ( method == "HEAD" )
    //	return EHttpMethod::Head;

    //if ( method == "OPTIONS" )
    //	return EHttpMethod::Options;

    //if ( method == "TRACE" )
    //	return EHttpMethod::Trace;

    //if ( method == "CONNECT" )
    //	return EHttpMethod::Connect;

    EHttpMethod::None;
}

std::tuple< std::string_view, std::string_view > HttpHelper::GetDefaultNotFoundResponse()
{
    return
    {
        "HTTP/1.1 404 NOT FOUND\r\nContent-Length:174\r\n\r\n",
        "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"utf-8\"><title>Not Found Page!</title></head><body>\
<h1>Oops!</h1><p>Mala.Http.HttpHelper Not Found Error!</p></body></html>"
    };
}
