export module Mala.Http.HttpController;

#pragma once

#include "../MalaMacro.h"

import Mala.Core.Types;
import Mala.Container;
import Mala.Http.HttpTypes;
import Mala.Http.HttpRequest;
import Mala.Net;
import Mala.Reflection;

using namespace Mala::Container;
using namespace Mala::Net;

export namespace Mala::Http
{

/// <summary>
/// Http 컨트롤러 인터페이스
/// </summary>
class IHttpController
{
    GENERATE_CLASS_TYPE_INFO( IHttpController );
};

/// <summary>
/// Http 컨트롤러, MVC 패턴에서 컨트롤러 역할
/// </summary>
class HttpController : public IHttpController
{
    GENERATE_CLASS_TYPE_INFO( HttpController );

public:
    using UriPath        = std::string_view;
    using Request        = std::string_view;
    using HttpRequestRef = const HttpRequest&;
    using UriHandler     = Action< HttpSessionRef, HttpRequestRef >;
    using RoutingTable   = HashMap< UriPath, UriHandler >;

public:
	METHOD( Handle )
	virtual void Handle( HttpSessionRef httpSession, HttpRequestRef request )
	{
	}

public:
	/// <summary>
	/// 생성자
	/// </summary>
	HttpController() = default;
	virtual ~HttpController() = default;

	/// <summary>
	/// 초기화한다
	/// </summary>
	void Initialize();

	/// <summary>
	/// 라우팅 테이블을 반환한다.
	/// </summary>
	RoutingTable& GetRoutingTable() { return _routingTable; }

protected:
	/// <summary>
	/// 라우팅 테이블
	/// </summary>
	RoutingTable _routingTable;
};

void HttpController::Initialize()
{
    auto& typeInfo = GetTypeInfo();
    const TypeInfo* curTypeInfo = &typeInfo;
    for ( ; curTypeInfo; curTypeInfo = curTypeInfo->GetSuper() )
    {
        for ( const auto* method : curTypeInfo->GetMethods() )
        {
            const auto* methodName = method->GetName();

            _routingTable[ methodName ] =
                [ this, method ]( HttpSessionRef httpSession, HttpRequestRef request )
                {
                    method->Invoke< HttpController, void, HttpSessionRef, HttpRequestRef >(
                        this,
                        httpSession,
                        request );
                };
        }
    }
}

}
