export module Mala.Http.HttpHelper;

import Mala.Core.Types;
import Mala.Http.HttpTypes;
import Mala.Http.HttpRequest;


export namespace Mala::Http
{

struct HttpHelper
{
    static size_t GetContentLength( const std::string_view& request );
    static HttpRequestView ParseHttpReqeustView( const std::string_view& request );
    static EHttpMethod ExtractMethod( const std::string_view& method );
    static std::tuple< std::string_view, std::string_view > GetDefaultNotFoundResponse();
};

}

