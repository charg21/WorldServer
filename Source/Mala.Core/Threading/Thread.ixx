export module Mala.Threading.Thread;

import <memory>;
import <functional>;
import <thread>;

import Mala.Threading.Job;

export namespace Mala::Threading
{

/// <summary>
/// 스레드 래핑 객체
/// </summary>
class Thread
{
    using Base      = std::thread;
    using ThreadJob = std::function< void() >;
    using ThreadPtr = std::shared_ptr< Base >;

public:
    Thread() = default;

    virtual ~Thread()
    {
        _thread.reset();
    }

    void Initialize()
    {
        OnInitialize();
    }

    void Finalize()
    {
        OnFinalize();
    }

    virtual void OnFinalize()
    {
    }

    virtual void OnInitialize()
    {
    }

    void Start()
    {
        _thread = std::make_unique< Base >( [ this ]()
            {
                Initialize();
                OnStart();
                Finalize();
            } );
    }

    virtual void OnStart()
    {
    }

    void Join()
    {
        _thread->join();
    }

protected:
    ThreadPtr _thread;
};


/// <summary>
/// Thread클래스 이름 별칭
/// </summary>
using Runnable = Thread;


}