module; // module 정의

#include "../MalaMacro.h"

import <stddef.h>;

export module Mala.Threading.JobQueue;

import Mala.Core.Crash;
import Mala.Core.Types;
import Mala.Core.TypeTraits;
import Mala.Memory;
import Mala.Threading.Job;

NAMESPACE_BEGIN( Mala::Threading )

class JobQueue
{
public:
    ///// <summary>
    /////
    ///// </summary>
    //int64_t _offset{ OFFSET_OF( class IJob, _entry ) };

    /// <summary>
    ///
    /// </summary>
    constexpr inline static int64_t _offset{ 8 };

public:
    JobQueue();
    ~JobQueue();

    void Push( IJob* newJob );
    IJob* Pop();
    void Flush();

private:
    /// <summary>
    /// 헤드
    /// </summary>
    ALIGN_CACHE Atomic< JobEntry* > _head;

    /// <summary>
    /// 더미
    /// </summary>
    ALIGN_CACHE JobEntry _stub;

    /// <summary>
    /// 테일
    /// </summary>
    JobEntry* _tail;
};

/*

struct Human
{
    void Do()
    {
        std::cout << "A" << std::endl;
    }
};

void Do()
{
    std::cout << "B" << std::endl;
}

void Do1( int n )
{
    std::cout << "B " << n << std::endl;
}

/// <summary>
/// 프로그램 진입접
/// </summary>
int main()
{
    FunctionJob func( &Do );
    func();

    FunctionJob func1( &Do1, 1 );
    func1();

    Human h;
    MethodJob memFunc( &h, &Human::Do);
    memFunc();
}

*/


NAMESPACE_END( Mala::Threading )
