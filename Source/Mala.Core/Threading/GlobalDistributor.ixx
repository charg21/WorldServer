export module Mala.Threading.GlobalDistributor;

#include "../CoreMacro.h"

import Mala.Core.Types;
import Mala.Container.WaitfreeCircularQueue;
import Mala.Memory;
import Mala.Threading.JobExecutor;

using namespace Mala::Container;

NAMESPACE_BEGIN( Mala::Threading )

/// <summary>
/// 전역 분배기
/// </summary>
class MALA_CORE_API GlobalDistributor
{
    /// <summary>
    /// 전역 분배기 컨테이너 타입 정의
    /// </summary>
    using JobExecutorList = WaitFreeCircularQueue< JobExecutor >;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    GlobalDistributor() = default;

    /// <summary>
    /// 소멸자
    /// </summary>
    ~GlobalDistributor() = default;

    /// <summary>
    /// 삽입한다.
    /// </summary>
    void Push( JobExecutorRef jobQueue );
    void Push( JobExecutorPtr&& jobQueue );

    /// <summary>
    /// 제거한다.
    /// </summary>
    JobExecutorPtr Pop();

private:
    /// <summary>
    ///
    /// </summary>
    JobExecutorList _jobExecutorList;
};

NAMESPACE_END( Mala::Threading )
