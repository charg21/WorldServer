import Mala.Core.Types;
import Mala.Core.CoreTLS;
import Mala.Core.CoreGlobal;
import Mala.Core.Types;
import Mala.Threading.Job;
import Mala.Threading.JobExecutor;
import Mala.Threading.JobTimer;
import Mala.Threading.GlobalDistributor;
import Mala.Memory;

using namespace Mala::Threading;

/// <summary>
/// 모든 작업을 실행한다
/// </summary>
void JobExecutor::Execute()
{
    LCurrentExecutor = this;

    for ( ;; )
    {
        int executeCount = 0;
        while ( auto* job = _jobQueue.Pop() )
        {
            job->Execute();
            xdelete( job );

            executeCount += 1;
        }

        OnFlush();
        if ( _remainJobCount.fetch_sub( executeCount ) == executeCount )
            break;
    }

    LCurrentExecutor = nullptr;
}

/// <summary>
/// 작업을 예약한다.
/// </summary>
void JobExecutor::DispatchJob( IJob* job )
{
    if ( _remainJobCount.fetch_add( 1 ) != 0 )
    {
        _jobQueue.Push( job );
    }
    else
    {
        _jobQueue.Push( job );

        if ( LCurrentExecutor )
        {
            /// 이렇게 되면 로컬 분배
            // LExecuterList->emplace_back( SharedFromJobExecutor() ) ;
            /// 전역 분배기 이용
            GGlobalDistributor->Push( SharedFromJobExecutor() );
        }
        else
        {
            Execute();

            while ( !LExecuterList->empty() )
            {
                auto dispatcher = std::move( LExecuterList->front() );
                LExecuterList->pop_front();
                dispatcher->Execute();
            }
        }
    }
}

void JobExecutor::DispatchJob2( IJob* job )
{
    if ( _remainJobCount.fetch_add( 1 ) != 0 )
    {
        _jobQueue.Push( job );
    }
    else
    {
        _jobQueue.Push( job );
        GGlobalDistributor->Push( SharedFromJobExecutor() );
    }
}

bool JobExecutor::IsCurrentExecutor() const
{
    return LCurrentExecutor == this;
}

const i64 JobExecutor::Count() const
{
    return _remainJobCount;
}

/// <summary>
/// 작업을 예약한다.
/// </summary>
void JobExecutor::PostAfter( Millisecond afterMs, IJob* job )
{
    /// 롱텀 타이머 작업은 글로벌 타이머로 옮긴다.
    if ( LJobTimer->GetMaxInterval() >= afterMs.count() )
    {
        LJobTimer->Reserve( afterMs, SharedFromJobExecutor(), job );
    }
    else
    {
        // GJobTimer->Reserve( afterTick, SharedFromJobExecutor(), job );
    }
}



