#include "../CoreMacro.h"
#include <WS2tcpip.h>

import Mala.Threading.GlobalJobExecutor;
import Mala.Core.Crash;
import Mala.Core.Types;
import Mala.Core.TypeTraits;
import Mala.Core.CoreGlobal;
import Mala.Container;
import Mala.Math.MathUtil;
import Mala.Memory;

using namespace Mala::Core;
using namespace Mala::Container;
using namespace Mala::Threading;

/// <summary>
/// 생성자
/// </summary>
IGlobalJob::IGlobalJob( i32 initRefCount )
: _refCount{ initRefCount }
{
}

/// <summary>
/// 레퍼런스 카운트를 증가시킨다
/// </summary>
i32 IGlobalJob::AddRef( i32 refCount )
{
    return _refCount += refCount;
}

/// <summary>
/// 레퍼런스 카운트를 감소시킨다
/// </summary>
i32 IGlobalJob::ReleaseRef()
{
    return --_refCount;
}

/// <summary>
/// 추가한다.
/// </summary>
void GlobalJobExecutor::Post( IGlobalJob* job )
{
    i64 jobPosition = ++_jobIssueCount;

    CrashIfFalse( ( ( jobPosition & JOB_SIZE_MASK ) - LJobReader ) < JOB_CAPACITY );

    void* mustNull = InterlockedExchangePointer( (void**)( &_jobs[ jobPosition & JOB_SIZE_MASK ] ), job );

    CrashIfFalse( !mustNull );
}

/// <summary>
/// 잡을 획득한다
/// </summary>
IGlobalJob* GlobalJobExecutor::Peek()
{
    return _jobs[ LJobReader & JOB_SIZE_MASK ];
}

/// <summary>
/// 갱신한다
/// </summary>
void GlobalJobExecutor::Flush()
{
    LOOP
    {
        IGlobalJob* job = Peek();
        if ( !job )
            break;

        if ( bool finished = job->Execute() )
        {
            CrashIfNull( Dequeue( LJobReader ) );
            xdelete( job );
        }

        LJobReader += 1;
    }
}

/// <summary>
/// 잡을 제거한다
/// </summary>
IGlobalJob* GlobalJobExecutor::Dequeue( i64 index )
{
    //IGlobalJob* dequeJob{};

    //std::swap( _jobs[ LJobReader & JOB_SIZE_MASK ], dequeJob );

    //return dequeJob;
    return (IGlobalJob*)( InterlockedExchangePointer( (void**)( &_jobs[ LJobReader & JOB_SIZE_MASK ] ), nullptr ) );
}
