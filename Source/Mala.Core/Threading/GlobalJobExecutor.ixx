module;

#include "../CoreMacro.h";

export module Mala.Threading.GlobalJobExecutor;

import Mala.Core.Crash;
import Mala.Core.Types;
import Mala.Core.TypeTraits;
import Mala.Core.CoreGlobal;
import Mala.Container;
import Mala.Math.MathUtil;
import Mala.Memory;

using namespace Mala::Core;
using namespace Mala::Container;
using namespace Mala::Threading;

NAMESPACE_BEGIN( Mala::Threading )

/// <summary>
/// 글로벌 작업 인터페이스
/// </summary>
struct MALA_CORE_API IGlobalJob
{
    /// <summary>
    /// 생성자
    /// </summary>
    IGlobalJob( i32 initRefCount );

    /// <summary>
    /// 소멸자
    /// </summary>
    virtual ~IGlobalJob() = default;

    /// <summary>
    /// 실행한다.
    /// </summary>
    virtual bool Execute() = 0;

    /// <summary>
    /// 레퍼런스 카운트를 증가시킨다
    /// </summary>
    i32 AddRef( i32 refCount );

    /// <summary>
    /// 레퍼런스 카운트를 감소시킨다
    /// </summary>
    i32 ReleaseRef();

protected:
    /// <summary>
    /// 참조 카운트
    /// </summary
    Atomic< i32 > _refCount;
};

/// <summary>
/// 글로벌 작업
/// </summary>
template< std::invocable TExecuteJob >
struct MALA_CORE_API GlobalLambdaJob final : public IGlobalJob
{
public:
    using ExecuteJobType = TExecuteJob;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    GlobalLambdaJob(
        i32           initialRefCount,
        TExecuteJob&& job )
    : IGlobalJob( initialRefCount                    )
    , _job      { std::forward< TExecuteJob >( job ) }
    {
    }

    /// <summary>
    /// 소멸자
    /// </summary>
    ~GlobalLambdaJob() final = default;

    /// <summary>
    /// 갱신한다
    /// </summary>
    bool Execute() final
    {
        _job();

        if ( const i32 refCount = ReleaseRef(); !refCount ) [[ likely ]]
            return true;
        else
            return false;
    }

private:
    /// <summary>
    /// 작업
    /// </summary>
    TExecuteJob _job;
};

/// <summary>
/// 클러스터 업데이트 잡
/// </summary>
template< std::invocable TExecuteJob, std::invocable TFinishJob >
struct MALA_CORE_API GlobalLambdaJobEx final : public IGlobalJob
{
public:
    using ExecuteJobType = TExecuteJob;

    using FinishJobType  = TFinishJob;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    GlobalLambdaJobEx(
        i32           initialRefCount,
        TExecuteJob&& job,
        TFinishJob&&  finishJob )
        : IGlobalJob( initialRefCount                         )
        , _job      { std::forward< TExecuteJob >( job )      }
        , _finishJob{ std::forward< TFinishJob >( finishJob ) }
    {
    }

    /// <summary>
    /// 소멸자
    /// </summary>
    ~GlobalLambdaJobEx() final = default;

    /// <summary>
    /// 갱신한다
    /// </summary>
    bool Execute() final
    {
        _job();

        if ( const i32 refCount = ReleaseRef(); !refCount ) [[ unlikely ]]
        {
            _finishJob();
            return true;
        }
        else
        {
            return false;
        }
    }

private:
    /// <summary>
    /// 작업
    /// </summary>
    TExecuteJob _job;

    /// <summary>
    /// 마무리 작업
    /// </summary>
    TFinishJob _finishJob;

};

/// <summary>
/// TLS 갱신을 직렬화 하기 위한 전역 실행기
/// MPMC, Wait-free, CircularBuffer, Fixed Size
/// </summary>
class GlobalJobExecutor
{
    inline static constexpr i64 JOB_CAPACITY  { Mala::Math::PowOfTwo< 19 >() };
    inline static constexpr i64 JOB_SIZE_MASK { JOB_CAPACITY - 1             };

    using JobArray = Array< IGlobalJob*, JOB_CAPACITY >;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    GlobalJobExecutor() = default;
    GlobalJobExecutor( GlobalJobExecutor&& ) noexcept = delete;
    GlobalJobExecutor( const GlobalJobExecutor& ) = delete;

    /// <summary>
    /// 소멸자
    /// </summary>
    ~GlobalJobExecutor() = default;

    /// <summary>
    /// 추가한다.
    /// </summary>
    template< std::invocable TJob >
    auto Post( TJob&& job )
    {
        return Post( xnew< GlobalLambdaJob< TJob > >(
            GThreadCount,
            std::forward< TJob >( job ) ) );
    }

    /// <summary>
    /// 추가한다.
    /// </summary>
    template< std::invocable TJob, std::invocable TFinishJob >
    auto Post( TJob&& job, TFinishJob&& finishJob )
    {
        return Post( xnew< GlobalLambdaJobEx< TJob, TFinishJob > >(
            GThreadCount,
            std::forward< TJob >( job ),
            std::forward< TFinishJob >( finishJob ) ) );
    }

    /// <summary>
    /// 추가한다.
    /// </summary>
    void Post( IGlobalJob* job );

    /// <summary>
    /// 잡을 획득한다
    /// </summary>
    IGlobalJob* Peek();

    /// <summary>
    /// 갱신한다
    /// </summary>
    void Flush();

    /// <summary>
    /// 잡을 제거한다
    /// </summary>
    IGlobalJob* Dequeue( i64 index );

    ///// <summary>
    /////
    ///// </summary>
    //bool IsEmpty()
    //{
    //    return LJobReader == _jobIssueCount;
    //}

    //int64_t GetSize() const
    //{
    //    return _jobIssueCount - LJobReader;
    //}

private:
    ALIGN_CACHE volatile Atomic< i64 > _jobIssueCount{ -1 };
                         JobArray      _jobs         {};

    inline static thread_local volatile i64 LJobReader{};
};

NAMESPACE_END( Mala::Threading )
