module; // module 정의

#include "../MalaMacro.h"

import <stddef.h>;
import <atomic>;

export module Mala.Threading.Job;

import Mala.Core.Crash;
import Mala.Core.Types;
import Mala.Core.TypeTraits;
import Mala.Memory;

NAMESPACE_BEGIN( Mala::Threading )

/// <summary>
///
/// </summary>
struct JobEntry
{
    JobEntry* volatile _next{};
};

/// <summary>
/// 작업 노드 엔트리
/// </summary>
struct IJob
{
    /// <summary>
    /// 소멸자
    /// </summary>
    virtual ~IJob() = default;

    /// <summary>
    /// 실행한다
    /// </summary>
    virtual void Execute()
    {
    }

    /// <summary>
    /// 실행한다
    /// </summary>
    void operator()()
    {
        Execute();
    }

    JobEntry _entry;
};

/// <summary>
/// 람다를 래핑한 잡 객체
/// </summary>
template< typename TLambda, typename... TArgs > requires std::invocable< TLambda, TArgs... >
struct LambdaJob : public IJob
{
public:
    /// <summary>
    /// 인자 타입
    /// </summary>
    using Args = std::tuple< TArgs... >;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    LambdaJob( TLambda&& lambda, TArgs&&... args )
    : _lambda{ std::forward< TLambda >( lambda ) }
    , _args  { std::forward< TArgs >( args )...  }
    {
    }

    /// <summary>
    /// 소멸자
    /// </summary>
    ~LambdaJob() final = default;

    /// <summary>
    /// 실행한다
    /// </summary>
    void Execute() final
    {
        std::apply( _lambda, _args );
    }

private:
    /// <summary>
    /// 람다 인스턴스
    /// </summary>
    TLambda _lambda;

    /// <summary>
    /// 메서드의 인자
    /// </summary>
    Args _args;
};

/// <summary>
/// 람다를 래핑한 잡 객체( 인자를 받지 않는 버전 )
///
/// 파라미터를 받지 않는 메모리 최적화 버전
/// 인자를 받는 LambdaJob으로도 사용이 가능하지만,
/// Args로 인해 8바이트( 실제 1바이트 지만 얼라인으로 인해 )가 낭비되는 부분 발생
/// 이 특수화 버전은 해당 낭비되는 부분을 없애기 위해 사용
/// </summary>
template< std::invocable TLambda >
struct LambdaJob< TLambda > : public IJob
{
    /// <summary>
    /// 생성자
    /// </summary>
    LambdaJob( TLambda&& lambda )
    : _lambda{ std::forward< TLambda >( lambda ) }
    {
    }

    /// <summary>
    /// 소멸자
    /// </summary>
    ~LambdaJob() final = default;

    /// <summary>
    /// 실행한다
    /// </summary>
    void Execute() final
    {
        _lambda();
    }

    /// <summary>
    /// 람다 인스턴스
    /// </summary>
    TLambda _lambda;
};


/// <summary>
/// 오브젝트의 thiscall 메서드를 래핑한 태스크
/// </summary>
template< class TObject, typename... TArgs >
class MethodJob : public IJob
{
public:
    /// <summary>
    /// 인자 타입
    /// </summary>
    using Args = std::tuple< TArgs... >;

    /// <summary>
    /// T객체의 메서드 타입 정의
    /// </summary>
    using TMethod = void( TObject::* )( TArgs... );

public:
    /// <summary>
    /// 생성자
    /// </summary>
    MethodJob( TObject* owner, TMethod method, TArgs&&... args )
    : _owner { owner }
    , _method{ method }
    , _args  { std::forward< TArgs >( args )... }
    {
    }

    /// <summary>
    /// 소멸자
    /// </summary>
    ~MethodJob() final
    {
    }

    /// <summary>
    /// 실행한다
    /// </summary>
    void Execute() final
    {
        std::apply( [ this ]( auto&&... args )
            {
                std::invoke( _method, _owner, std::forward< decltype( args ) >( args )... );
            },
            _args );
    }

private:
    /// <summary>
    /// T 클래스 객체의 포인터
    /// </summary>
    TObject* _owner;

    /// <summary>
    /// thiscall 메서드 함수 포인터
    /// </summary>
    TMethod _method;

    /// <summary>
    /// 메서드의 인자
    /// </summary>
    Args _args;
};

/*

struct Human
{
    void Do()
    {
        std::cout << "A" << std::endl;
    }
};

void Do()
{
    std::cout << "B" << std::endl;
}

void Do1( int n )
{
    std::cout << "B " << n << std::endl;
}

/// <summary>
/// 프로그램 진입접
/// </summary>
int main()
{
    FunctionJob func( &Do );
    func();

    FunctionJob func1( &Do1, 1 );
    func1();

    Human h;
    MethodJob memFunc( &h, &Human::Do);
    memFunc();
}

*/


NAMESPACE_END( Mala::Threading )
