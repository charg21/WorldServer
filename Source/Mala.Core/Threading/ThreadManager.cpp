#include "../CoreMacro.h"

import <atomic>;
import <thread>;

import Mala.Core.Types;
import Mala.Core.TickCounter;
import Mala.Core.Crash;
import Mala.Core.CoreTLS;
import Mala.Core.CoreGlobal;
import Mala.Core.SourceLocation;
import Mala.Memory;
import Mala.Windows;

import Mala.Threading.Lock;
import Mala.Threading.GlobalDistributor;
import Mala.Threading.Thread;
import Mala.Threading.ThreadManager;
import Mala.Threading.JobWheelTimer;
import Mala.Threading.JobExecutor;
import Mala.Container.WaitFreeQueue;

using namespace Mala::Core;
using namespace Mala::Windows;
using namespace Mala::Threading;


/// <summary>
/// 생성자
/// </summary>
ThreadManager::ThreadManager()
{
    InitTLS();
}

/// <summary>
/// 소멸자
/// </summary>
ThreadManager::~ThreadManager()
{
    DestroyTLS();
}

/// <summary>
/// 실행한다
/// </summary>
void ThreadManager::Launch( ThreadJob threadJob )
{
    _threads.Emplace( ( [ = ]()
    {
        InitTLS();
        threadJob();
        DestroyTLS();
    } ) );
}

void ThreadManager::InitTLS()
{
    LThreadId     = IssueThreadId();
    //LJobTimer     = xnew_ex< JobWheelTimer< NullLock, 16, 12 > >( SrcLoc::Here() );
    LJobTimer     = xnew< JobWheelTimer< NullLock, 16, 12 > >();
    LExecuterList = xnew< std::deque< JobExecutorPtr > >();
}

void ThreadManager::DestroyTLS()
{
    xdelete( LJobTimer );
    xdelete( LExecuterList );
}

void ThreadManager::DoGlobalQueueWork()
{
    for ( ;; )
    {
        //u64 now = GetTick();
        //if ( now > LEndTickCount )
        //    break;

        auto jobQueue = GGlobalDistributor->Pop();
        if ( !jobQueue )
            break;

        jobQueue->Execute();
    }
}

void ThreadManager::DistibuteReservedJobs()
{
    const u64 now = GetTick();
    LJobTimer->Distribute( now );
    //GJobTimer->Distribute( now );
}

void ThreadManager::Join()
{
    _threads.ForEach( []( auto& thread )
    {
        thread.join();
    } );
}

/// <summary>
/// 스레드 식별자를 발급한다
/// </summary>
i32 ThreadManager::IssueThreadId()
{
    return ++_threadIdGenerator;
}
