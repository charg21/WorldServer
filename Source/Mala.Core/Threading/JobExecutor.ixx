export module Mala.Threading.JobExecutor;

#pragma once

#include "../MalaMacro.h"

import Mala.Core.Types;
import Mala.Reflection;
import Mala.Memory;
import Mala.Threading.Job;
import Mala.Threading.JobQueue;


NAMESPACE_BEGIN( Mala::Threading )

/// <summary>
/// 작업 실행기
/// </summary>
class JobExecutor
{
    GENERATE_CLASS_TYPE_INFO( JobExecutor );

public:
    friend class GlobalDistributor;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    JobExecutor() = default;

    /// <summary>
    /// 생성자
    /// </summary>
    virtual ~JobExecutor() = default;

    /// <summary>
    /// 작업을 실행한다.
    /// </summary>
    template< typename... Args, std::invocable< Args... > TLambda >
    void Post( TLambda&& lambda, Args&&... args )
    {
        auto* job = xnew< LambdaJob< TLambda, Args... > >(
            std::forward< TLambda >( lambda ),
            std::forward< Args >( args )... );

        DispatchJob( job );
    }

    template< typename T, typename... Args > requires std::derived_from< T, JobExecutor >
    void Post( const std::shared_ptr< T >& owner, void( T::*method )( Args... ), Args&&... args )
    {
        auto* job = xnew< MethodJob< T, Args... > >(
            owner.get(),
            method,
            std::forward< Args >( args )... );

        DispatchJob2( job );
    }

    template< typename T, typename... Args >
    void Post( T* owner, void( T::* method )( Args... ), Args&&... args )
    {
        auto* job = xnew< MethodJob< T, Args... > >(
            owner,
            method,
            std::forward< Args >( args )... );

        DispatchJob2( job );
    }

    /// <summary>
    /// 작업을 예약한다.
    /// </summary>
    template< typename... Args, std::invocable< Args... > TLambda >
    void PostAfter( Millisecond afterMs, TLambda&& lambda, Args&&... args )
    {
        auto* job = xnew< LambdaJob< TLambda, Args... > >(
            std::forward< TLambda >( lambda ),
            std::forward< Args >( args )... );

        PostAfter( afterMs, job );
    }

    /// <summary>
    /// 작업을 예약한다.
    /// </summary>
    void PostAfter( Millisecond afterMs, IJob* job );

    /// <summary>
    /// 모든 작업을 실행한다
    /// </summary>
    void Execute();

    /// <summary>
    ///
    /// </summary>
    void DispatchJob( IJob* job );
    void DispatchJob2( IJob* job );

    /// <summary>
    /// 현재 익스큐터 여부 반환한다.
    /// </summary>
    bool IsCurrentExecutor() const;

    const i64 Count() const;
    virtual void OnFlush() {}

    /// <summary>
    /// std::shared_from_this() 인터페이스
    /// </summary>
    virtual JobExecutorPtr SharedFromJobExecutor() = 0;

    /// <summary>
    /// std::weak_from_this() 인터페이스
    /// </summary>
    virtual JobExecutorWeakPtr WeakFromJobExecutor() = 0;

private:
    /// 작업 수
    Field( _remainJobCount )
    Atomic< i64 > _remainJobCount{};

    ///
    JobExecutorPtr _holder;

    /// 작업 큐
    Field( _jobQueue )
    JobQueue _jobQueue;

};

class ExecutorDistributor
{
};

NAMESPACE_END( Mala::Threading )
