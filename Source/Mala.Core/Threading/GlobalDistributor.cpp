import Mala.Core.Types;
import Mala.Core.Crash;

import Mala.Threading.GlobalDistributor;

using namespace Mala::Threading;

/// <summary>
/// �����Ѵ�.
/// </summary>
void GlobalDistributor::Push( JobExecutorRef executor )
{
	executor->_holder = executor;
	_jobExecutorList.push( executor.get() );
}

/// <summary>
/// �����Ѵ�.
/// </summary>
void GlobalDistributor::Push( JobExecutorPtr&& executor )
{
	auto executorPtr = executor.get();
	executor->_holder = std::move( executor );
	_jobExecutorList.push( executorPtr );
}

/// <summary>
/// �����Ѵ�.
/// </summary>
JobExecutorPtr GlobalDistributor::Pop()
{
	JobExecutor* executor = _jobExecutorList.Dequeue();
	if ( !executor )
		return nullptr;

	return std::move( executor->_holder );
}
