#include "../MalaMacro.h"

import <stddef.h>;

import Mala.Core.Crash;
import Mala.Core.Types;
import Mala.Core.TypeTraits;
import Mala.Memory;
import Mala.Threading.Job;
import Mala.Threading.JobQueue;

using namespace Mala::Threading;

#define OFFSET_OF( type, field )                                          \
  ( reinterpret_cast< intptr_t >( &( reinterpret_cast< type* >( 4 )->field ) ) - 4 )

JobQueue::JobQueue()
: _tail{ &_stub }
{
	_head = &_stub;

	Mala::Core::CrashIfFalse( _head.is_lock_free() );
}

/// <summary>
///
/// </summary>
JobQueue::~JobQueue()
{
	Flush();
}

/// <summary>
///
/// </summary>
void JobQueue::Push( IJob* newJob )
{
	auto* prevEntry = reinterpret_cast< JobEntry* >( std::atomic_exchange_explicit(
		&_head,
		&newJob->_entry,
		std::memory_order_acq_rel ) );

	prevEntry->_next = &( newJob->_entry );
}

IJob* JobQueue::Pop()
{
	JobEntry* tail = _tail;
	JobEntry* next = tail->_next;

	if ( tail == &_stub )
	{
		if ( !next )
			return nullptr;

		_tail = next;
		tail = next;
		next = next->_next;
	}

	if ( next )
	{
		_tail = next;

		return reinterpret_cast< IJob* >( reinterpret_cast< int64_t >( tail )- _offset );
	}

	JobEntry* head = _head;
	if ( tail != head )
		return nullptr;

	_stub._next = nullptr;

	auto* prevEntry = reinterpret_cast< JobEntry* >( std::atomic_exchange_explicit(
		&_head,
		&_stub,
		std::memory_order_acq_rel ) );

	prevEntry->_next = &_stub;

	next = tail->_next;
	if ( next )
	{
		_tail = next;

		return reinterpret_cast< IJob* >( reinterpret_cast< int64_t >( tail ) - _offset );
	}

	return nullptr;
}

void JobQueue::Flush()
{
	auto* job = Pop();
	while ( job )
	{
		job->Execute();
		xdelete( job );
		job = Pop();
	}
}

/*

struct Human
{
	void Do()
	{
		std::cout << "A" << std::endl;
	}
};

void Do()
{
	std::cout << "B" << std::endl;
}

void Do1( int n )
{
	std::cout << "B " << n << std::endl;
}

/// <summary>
/// 프로그램 진입접
/// </summary>
int main()
{
	FunctionJob func( &Do );
	func();

	FunctionJob func1( &Do1, 1 );
	func1();

	Human h;
	MethodJob memFunc( &h, &Human::Do);
	memFunc();
}

*/
