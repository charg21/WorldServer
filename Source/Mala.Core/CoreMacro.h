#pragma once

#define MALA_CORE_API
#define MALA_CONTAINER_API
#define MALA_MATH_API
#define MALA_DB_API
#define MALA_THREADING_API
#define MALA_NET_API
#define MALA_REFLECTION_API

//#define MALA_CORE_API       __declspec( dllexport )
//#define MALA_CONTAINER_API  __declspec( dllexport )
//#define MALA_MATH_API       __declspec( dllexport )
//#define MALA_DB_API         __declspec( dllexport )
//#define MALA_THREADING_API  __declspec( dllexport )
//#define MALA_NET_API        __declspec( dllexport )
//#define MALA_REFLECTION_API __declspec( dllexport )

//#include "Reflection/TypeInfoMacro.h"
//#include "Reflection/FieldMacro.h"
//#include "Reflection/MethodMacro.h"

#pragma region KEYWORD

#define OUT
#define LOOP       for ( ;; )
#define abstract =0
#define SUBLOUTINE for ( int charg2__ {}; charg2__ == 0; charg2__ +=1 )
#define READONLY  const
#define ALIGN_CACHE alignas( 64 )
#define ALIGN_PAGE  alignas( 4096 )
#define OFFSET_OF( type, field )( reinterpret_cast< intptr_t >( &( reinterpret_cast< type* >( 4 )->field ) ) - 4 )

#pragma endregion

#pragma region ATTRIBUTE
#define    MALA_NODISCARD [[ nodiscard ]]
#pragma endregion

#define NAMESPACE_BEGIN( NameSpace_Name ) export namespace NameSpace_Name {
#define NAMESPACE_END( NS )  }
#define EXPORT_BEGIN export{
#define EXPORT_END  }

#define ASSERT_IF_FALSE( expr ) \
    {    \
        if ( !( expr ) ) \
            *( ( size_t* )( nullptr ) ) = 0xDEADBEEF'DEADBEEF; \
    }
#define CRASH_IF_NULL( expr ) ASSERT_IF_FALSE( expr )

/// <summary>
/// Crash
/// </summary>
#define CRASH( reason )                     \
{                                           \
    u32* crash = nullptr;                   \
    _Analysis_assume_( crash != nullptr );  \
    *crash = 0XDEADBEEF;                    \
}

#define ASSERT_CRASH( expr )                \
{                                           \
    if ( !( expr ) ) [[unlikely]]           \
    {                                       \
        CRASH( L"ASSERT_CRASH" );           \
        _Analysis_assume_( expr );          \
    }                                       \
}

/// <summary>
/// Forwarding
/// </summary>
#define MOVE( object ) object = std::move( object )

/// <summary>
/// Raw Pointer
/// </summary>
#define USING_REF( CLASS )              \
class CLASS;                            \
using CLASS##Ref     = const CLASS##*

/// <summary>
/// Smart Pointer
/// </summary>
#define USING_SHARED_PTR( CLASS )                      \
using CLASS##Ptr     = std::shared_ptr< class CLASS >; \
using CLASS##Ref     = const CLASS##Ptr&;              \
using CLASS##WeakPtr = std::weak_ptr< class CLASS >;   \
using CLASS##WeakRef = const CLASS##WeakPtr&;

/// <summary>
/// Lock
/// </summary>
#define USE_MANY_LOCKS( count )     Mala::Threading::Lock _locks[ count ];
#define USE_LOCK                    USE_MANY_LOCKS( 1 );
#define READ_LOCK_IDX( idx )        Mala::Threading::ReadLockGuard readLockGuard_##idx( _locks[ idx ] );//, typeid( this ).name() );
#define READ_LOCK                   READ_LOCK_IDX( 0 );
#define WRITE_LOCK_IDX( idx )       Mala::Threading::WriteLockGuard writeLockGuard_##idx( _locks[ idx ] );//, typeid( this ).name() );
#define WRITE_LOCK                  WRITE_LOCK_IDX( 0 );

#define EXCLUSIVE_LOCK              Mala::Threading::WriteLock< std::remove_all_extents_t< decltype( _lock ) > > writeLock( _lock );
#define EXCLUSIVE_LOCK2( Object )   Mala::Threading::WriteLock< std::remove_all_extents_t< decltype( Object->GetLock() ) > > writeLock( Object->GetLock() );
#define SHARED_LOCK                 Mala::Threading::ReadLock< std::remove_all_extents_t< decltype( _lock ) > > readLock( _lock );
#define SHARED_LOCK2( Object )      Mala::Threading::ReadLock< std::remove_all_extents_t< decltype( Object->GetLock() ) > > writeLock( Object->GetLock() );

/// <summary>
/// Strong Type
/// </summary>
#define USING_STRONG_TYPE( Name, Base ) \
struct Name ## StrongTypeTag : public StrongTypeTag {}; \
using Name = StrongType< Base, Name ## StrongTypeTag >;

#define USING_STRONG_VALUE_TYPE( Name, Base ) \
struct Name ## StrongTypeTag : public StrongTypeTag {}; \
using Name = StrongValueType< Base, Name ## StrongTypeTag >;

/// <summary>
/// TypeList
/// </summary>
#define DECLARE_TL( TTL ) using TL = TTL; i32 _typeId;
#define INIT_TL( Type ) _typeId = IndexOfV< TL, Type >;

/// <summary>
/// Time
/// </summary>
#define NOW_TICK ( *reinterpret_cast< u64* >( 0x7FFE'0008 ) / 1'0000 )

/// <summary>
/// ThreadPool
/// </summary>
#define ASYNC( ... ) GNetCore->Post( [](){ __VA_ARGS__; } )
#define WAIT_ALL( FUTURES ) for ( auto& future : FUTURES ) \
    while ( !*future ) \
        Sleep( 16 );
