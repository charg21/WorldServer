module;

//#define DEALLOC_CHECK

export module Mala.Buffers.ObjectPool;

import <memory>;

import Mala.Core.Types;
import Mala.Memory;
import Mala.Memory.MemoryPool;

using namespace std;

export namespace Mala::Buffers
{
// 클래스 객체를 넣을 수 있는 Pool
template< typename Type >
class ObjectPool
{
public:
    template< typename... Args >
    static Type* Pop( Args&&... args )
    {
#ifdef _STOMP
        MemoryHeader* ptr = reinterpret_cast<MemoryHeader*>( StompAllocator::Alloc( s_allocSize ) );
        Type* memory = static_cast<Type*>( MemoryHeader::AttachHeader( ptr, s_allocSize ) );
#else
        Type* memory = static_cast<Type*>( MemoryHeader::AttachHeader( s_pool.Pop(), s_allocSize ) );
#endif
        new( memory ) Type( std::forward< Args >( args )... ); // placement new
        return memory;
    }

    static void Push( Type* obj )
    {
        obj->~Type();
#ifdef _STOMP
        StompAllocator::Release( MemoryHeader::DetachHeader( obj ) );
#else
        s_pool.Push( MemoryHeader::DetachHeader( obj ) );
#endif
    }

    template< typename... Args >
    static std::shared_ptr< Type > MakeShared( Args&&... args )
    {
        return { Pop( std::forward< Args >( args )... ), Push };
    }

    template< typename... Args >
    static Atomic< std::shared_ptr< Type > > MakeAtomicShared( Args&&... args )
    {
        return { Pop( std::forward< Args >( args )... ), Push };
    }
    template< typename... Args >
    static std::unique_ptr< Type, void( * )( Type* ) > MakeUnique( Args&&... args )
    {
        return { Pop( std::forward< Args >( args )... ), Push };
    }

private:
    static i32        s_allocSize;
    static MemoryPool s_pool;
};

template< typename Type >
i32 ObjectPool< Type >::s_allocSize{ sizeof( Type ) + sizeof( MemoryHeader ) };

template< typename Type >
MemoryPool ObjectPool< Type >::s_pool{ s_allocSize };

} // namespace Mala::Core
