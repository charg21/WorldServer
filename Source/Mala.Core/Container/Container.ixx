module;

#pragma once

export module Mala.Container;

import <memory>;
import <vector>;
import <array>;
import <list>;
import <forward_list>;
//#import <flat_map>;
import <queue>;
import <deque>;
import <stack>;
import <set>;
import <map>;
import <unordered_set>;
import <unordered_map>;
import <tuple>;
import <concurrent_queue.h>;
import <concurrent_priority_queue.h>;
import <concurrent_unordered_map.h>;
import <concurrent_unordered_set.h>;

import Mala.Core.Allocator;

export import Mala.Core.Variant;
export import Mala.Core.StrongType;
export import Mala.Container.FlatMap;
import Mala.Container.FlatHashMap;
export import Mala.Container.FlatSet;
export import Mala.Container.LockFreeStack;
export import Mala.Container.StaticVector;
export import Mala.Container.String;
export import Mala.Container.WaitFreeQueue;



export namespace Mala::Container
{

template< typename T >
using ContainerAllocator = Mala::Core::StlAllocator< T >;

template< typename T >
using Vector = std::vector< T, ContainerAllocator< T > >;

template< typename T, size_t N >
using Array = std::array< T, N >;

template< typename T >
using ForwardList = std::forward_list< T, ContainerAllocator< T > >;

template< typename T >
using List = std::list< T, ContainerAllocator< T > >;

template< typename T >
using Set = std::set< T, ContainerAllocator< T > >;

template< typename T >
using Deque = std::deque< T, ContainerAllocator< T > >;

template< typename T >
using Queue = std::queue< T, Deque< T > >;

template< typename T >
using Stack = std::stack< T, Deque< T > >;

template< typename T, typename Container = Vector< T >, typename Pred = std::less< typename Container::value_type > >
using PriorityQueue = std::priority_queue< T, Container, Pred >;

template< typename K, typename V >
using Map = std::map< K, V, std::less< K >, ContainerAllocator< std::pair< const K, V > > >;

template< typename T >
using HashSet = std::unordered_set< T, std::hash< T >, std::equal_to< T >, ContainerAllocator< T > >;

template< typename K, typename V >
using HashMap = std::unordered_map< K, V, std::hash< K >, std::equal_to< K >, ContainerAllocator< std::pair< const K, V > > >;

template< typename K, typename V >
using FlatHashMap = ska::flat_hash_map< K, V, std::hash< K >, std::equal_to< K >, ContainerAllocator< std::pair< K, V > > >;

template< typename... Types >
using Tuple = std::tuple< Types... >;

template< typename T >
using ConcurrentQueue = concurrency::concurrent_queue< T, ContainerAllocator< T > >;

template< typename T, typename Comparer = std::less< T > >
using ConcurrentPriorityQueue = concurrency::concurrent_priority_queue< T, Comparer, ContainerAllocator< T > >;

template< typename K, typename V >
using ConcurrentHashMap = concurrency::concurrent_unordered_map< K, V, std::hash< K >, std::equal_to< K >, ContainerAllocator< std::pair< const K, V > > >;

template< typename K, typename V >
using ConcurrentHashMultiMap = concurrency::concurrent_unordered_multimap< K, V, std::hash< K >, std::equal_to< K >, ContainerAllocator< std::pair< const K, V > > >;

};

using namespace Mala::Core;

export namespace std
{
    template< typename T1, typename T2 >
    struct hash< std::pair< T1, T2 > >
    {
        size_t operator()( const std::pair< T1, T2 >& p ) const noexcept
        {
            size_t h1 = std::hash< T1 >{}( p.first );
            size_t h2 = std::hash< T2 >{}( p.second );
            return h1 ^ ( h2 << 1 ); // 간단한 해시 조합
        }
    };

    template< typename Tuple, typename Func, std::size_t... Indices >
    void apply_with_index_impl( Tuple&& t, Func&& f, std::index_sequence< Indices... > )
    {
        ( f( std::get< Indices >( t ), Indices ), ... );
    }

    template <typename Tuple, typename Func>
    void apply_with_index( Tuple&& t, Func&& f )
    {
        constexpr std::size_t N = std::tuple_size_v< std::decay_t< Tuple > >;
        apply_with_index_impl( std::forward< Tuple >( t ), std::forward< Func >( f ), std::make_index_sequence< N >{} );
    }

    template< typename... Types >
    struct hash< std::tuple< Types... > >
    {
        size_t operator()( const std::tuple< Types... >& t ) const noexcept
        {
            size_t seed = 0;
            apply_with_index( t, [ &seed ]( const auto& elem, std::size_t index )
            {
                using ElemType = std::decay_t< decltype( elem ) >;
                //if constexpr ( std::is_base_of_v< IStrongType, ElemType > )
                //{
                //    seed ^= std::hash< typename ElemType::UnderlyingType >{}( elem._value ) + 0x9e3779b9 + ( seed << 6 ) + ( seed >> 2 );
                //}
                //else
                //{
                    seed ^= std::hash< ElemType >{}( elem ) + 0x9e3779b9 + ( seed << 6 ) + ( seed >> 2 );
                //}
            } );

            return seed;
        }
    };

    template< typename TTuple, std::size_t TupleElemIndex = 0 >
    struct ForEachTupleElem
    {
        static bool Exec( const TTuple& lhs, const TTuple& rhs )
        {
            if constexpr ( TupleElemIndex < std::tuple_size_v< TTuple > )
            {
                using namespace Mala::Core;
                using ElemType = std::decay_t< decltype( std::get< TupleElemIndex >( lhs ) ) >;
                if constexpr ( std::is_base_of_v< IStrongType, ElemType > )
                {
                    if ( !std::equal_to< ElemType >{}( std::get< TupleElemIndex >( lhs ), std::get< TupleElemIndex >( rhs ) ) )
                        return false;
                    /*if ( std::get< TupleElemIndex >( lhs ) != std::get< TupleElemIndex >( rhs ) )
                        return false;*/
                }
                else if constexpr ( std::is_base_of_v< Variant, ElemType > )
                {
                    //if ( std::get< TupleElemIndex >( lhs ) != std::get< TupleElemIndex >( rhs ) )
                    //    return false;
                }
                else
                {
                    if ( std::get< TupleElemIndex >( lhs ) != std::get< TupleElemIndex >( rhs ) )
                        return false;
                }
                return ForEachTupleElem< TTuple, TupleElemIndex + 1 >::Exec( lhs, rhs );
            }
            else
            {
                return true;
            }
        }
    };

    template< typename... Types >
    struct equal_to< std::tuple< Types...>  >
    {
        bool operator()( const std::tuple< Types... >& lhs, const std::tuple< Types... >& rhs ) const noexcept
        {
            return ForEachTupleElem< std::tuple< Types... >, 0 >::Exec( lhs, rhs );
        }
    };
}

