export module Mala.Container.StaticVector;

import <array>;
import Mala.Core.Types;
import Mala.Core.TypeTraits;

export namespace Mala::Container
{

/// <summary>
/// 벡터의 인터페이스를 가진 정적 배열
/// 벡터같은 resize()가 일어나지 않는다.
/// </summary>
template< typename T, size_t Capacity, typename SizeT = size_t >
class StaticVector
{
public:
    using ValueType      = T;
    using SizeType       = usize;
    using Pointer        = T*;
    using Reference      = T&;
    using ConstPointer   = const T*;
    using ConstReference = const T&;

    /// <summary>
    /// 생성자
    /// </summary>
    StaticVector()
    : _size {}
    {
    }

    StaticVector( std::initializer_list< T > initList )
    : _size{}
    {
        for ( auto& elem : initList )
            push_back( std::move( elem ) );
    }

    auto begin()
    {
        return _array.begin();
    }

    const auto cbegin() const
    {
        return _array.cbegin();
    }

    auto end()
    {
        return _array.begin() + _size;
    }

    const auto cend() const
    {
        return _array.cbegin() + _size;
    }

    template< typename U >
    void push_back( U&& src )
    {
        if ( _size >= capacity() )
        {
            int* ptr = nullptr;
            *ptr = 0xDEADBEEF;
            return;
        }

        _array[ _size++ ] = std::forward< U >( src );
    }

    void pop_back()
    {
        if ( _size <= 0 )
            return;

        if constexpr ( !std::is_trivially_destructible_v<T> )
            _array[ _size - 1 ].~T();

        --_size;
    }

    T& back()
    {
        return _array[ _size -1 ];
    }

    bool empty()
    {
        return _size == 0;
        /// _array[ size ] = src;
    }

    constexpr size_t capacity() const
    {
        return Capacity;
    }

    constexpr size_t size() const
    {
        return _size;
    }

    constexpr bool is_full() const
    {
        return _size == Capacity;
    }

    Reference operator[]( size_t Index )
    {
        return _array[ Index ];
    }

    ConstReference operator[]( size_t Index ) const
    {
        return _array[ Index ];
    }

    void clear()
    {
        _size = 0;
    }

private:
    std::array< T, Capacity > _array; //<
    SizeT                     _size;  //<
};

template< typename T, size_t Capacity >
using AtomicStaticVector = StaticVector< T, Capacity, Atomic< size_t > >;

} // namespace Mala::Container
