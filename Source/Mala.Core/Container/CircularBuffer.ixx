export module Mala.Container.CircularBuffer;

import <array>;

export namespace Mala::Container
{

template< typename T, size_t Capacity >
class CircularBuffer
{
    static constexpr size_t CAPACITY    = Capacity;
    static constexpr size_t BUFFER_SIZE = CAPACITY + 1ull;

    static_assert( CAPACITY > 0, "Capacity must greater than 1" );

public:
    /// 생성자
    CircularBuffer()
        : _buffer{ new T[ BUFFER_SIZE ]() }
        , _writer{                        }
        , _reader{                        }
    {
    }

    CircularBuffer( const CircularBuffer& other )     = delete;
    CircularBuffer( CircularBuffer&& other ) noexcept = delete;

    /// 소멸자
    ~CircularBuffer()
    {
        delete[] _buffer;
    }

    template< typename... Args >
    [[ nodiscard ]] const bool try_emplace( Args&&... args )
    {
        const size_t enqueueable_size = std::min( 1ULL, get_free_size() );
        if ( enqueueable_size == 0 )
            return false;

        ::new( &_buffer[ _writer ] ) T( std::forward< Args >( args )... );

        CommitOnEnqueue( 1 );// enqueueable_size );

        return true;
    }

    [[ nodiscard ]] const bool try_enqueue( const T& value )
    {
        return try_emplace( value );
    }

    [[ nodiscard ]] const bool try_enqueue( T&& value )
    {
        return try_emplace( std::move( value ) );
    }


    [[ nodiscard ]] const bool try_dequeue( T& dest )
    {
        const size_t dequeueable_size = std::min( 1ULL, get_stored_size() );
        if ( dequeueable_size == 0 )
            return false;

        dest = _buffer[ _reader ];
        _buffer[ _reader ].~T();

        return CommitOnDequeue( 1ULL );
    }

    size_t CommitOnEnqueue( size_t enqueued_size )
    {
        // 원본에 처리시 타 스레드에서 계산시 문제 발생 가능
        // ex ) 버퍼를 넘는 처리
        size_t writer_capture = _writer;

        writer_capture += enqueued_size;
        writer_capture %= BUFFER_SIZE;

        // consumer thread에서 영향 받지 않도록 한다.
        _writer = writer_capture;

        return enqueued_size;
    }

    size_t CommitOnDequeue( size_t dequeued_size )
    {
        // 원본에 처리시 타 스레드에서 계산시 문제 발생 가능
        size_t reader_capture = _reader;

        reader_capture += dequeued_size;
        reader_capture %= BUFFER_SIZE;

        _reader = reader_capture;

        return dequeued_size;
    }

    [[ nodiscard ]] constexpr size_t Capacity() const
    {
        return Capacity;
    }

    [[ nodiscard ]] constexpr bool Empty() const
    {
        return _writer == _reader;
    }

    // 현재 사용 중인 버퍼의 크기.
    constexpr size_t get_stored_size() const noexcept
    {
        // _reader나 _writer가 조건 비교 이후 다른 스레드에서 값이 변경되어
        // 계산에 영향을 주는것을 방지하기 위해 사본을 통해 계산
        size_t reader_capture = _reader;
        size_t writer_capture = _writer;

        if ( writer_capture >= reader_capture )
            return writer_capture - reader_capture;
        else
            return BUFFER_SIZE + writer_capture - reader_capture;
    }

    constexpr size_t get_free_size() const noexcept
    {
        return CAPACITY - get_stored_size();
    }

    /* TODO
    auto end()
    {
        return &_buffer[ _writer ];
    }
    
    const auto cend() const
    {
        return &_buffer[ _writer ];
    }
    
    auto begin()
    {
        return &_buffer[ _reader ];
    }
    
    const auto cbegin() const
    {
        return &_buffer[ _reader ];
    }
    
    T& operator[]( size_t index )
    {
        return _buffer[ index ];
    }
    
    const T& operator[]( size_t index )
    {
        return _buffer[ index ];
    }
    */

private:
    T*     _buffer; //<
    size_t _writer; //< next Write postion
    size_t _reader; //< next Read postion
};

/// <summary>
/// WaitFree 환형 큐
/// MPMC, Wait-free, CircularBuffer, Fixed Size
/// </summary>
template< typename T, size_t N >
class CircularQueueBase
{
    using i64   = long long;
    
    /// <summary>
    /// 컴파일 타임에 Base의 N승을 구해 반환한다.
    /// </summary>
    template< i64 N >
    static consteval i64 PowerOfTwo()
    {
        if constexpr ( N == 0 )
            return 1;
        else
            return 2 * PowerOfTwo< N - 1 >();
    }

    inline static constexpr i64 CAPACITY  = PowerOfTwo< N >();
    inline static constexpr i64 SIZE_MASK = CAPACITY - 1;

    using TArray = std::array< T, CAPACITY >;

    TArray _array;
};

} // namespace Mala::Container
