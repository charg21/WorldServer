export module Mala.Container.LockDeque;

import <cstdint>;
import <deque>;
import <type_traits>;
import <shared_mutex>;

import Mala.Core.Allocator;
import Mala.Core.Types;

using namespace Mala::Core;


export namespace Mala::Container
{


template< typename T, typename TAllocator = Mala::Core::StlAllocator< T > >
class LockDeque : public std::deque< T, TAllocator >
{
public:
    using Base            = std::deque< T, TAllocator >;
    using value_type      = typename Base::value_type;
    using reference       = typename Base::reference;
    using const_reference = typename Base::const_reference;

    LockDeque() = default;
    LockDeque( const LockDeque& other )
    {
        std::shared_lock lock( other._lock );
        Base::operator=( other );
    }

    LockDeque( LockDeque&& other )
    {
        std::unique_lock lock( other._lock );
        Base::operator=( std::move( other ) );
    }

    auto push_back( const value_type& value )
    {
        std::unique_lock lock( _lock );
        return Base::push_back( value );
    }

    auto push_front( const value_type& value )
    {
        std::unique_lock lock( _lock );
        return Base::push_front( value );
    }

    auto push_back( value_type&& value )
    {
        std::unique_lock lock( _lock );
        return Base::push_back( std::move( value ) );
    }

    auto push_front( value_type&& value )
    {
        std::unique_lock lock( _lock );
        return Base::push_front( std::move( value ) );
    }

    auto pop_back()
    {
        std::unique_lock lock( _lock );
        return Base::pop_back();
    }

    auto pop_front()
    {
        std::unique_lock lock( _lock );
        return Base::pop_front();
    }

    reference back()
    {
        std::shared_lock lock( _lock );
        return Base::back();
    }

    reference front()
    {
        std::shared_lock lock( _lock );
        return Base::front();
    }

    const_reference back() const
    {
        std::shared_lock lock( _lock );
        return Base::back();
    }

    const_reference front() const
    {
        std::shared_lock lock( _lock );
        return Base::front();
    }

    bool empty()
    {
        std::shared_lock lock( _lock );
        return Base::empty();
    }

    size_t size()
    {
        std::shared_lock lock( _lock );
        return Base::size();
    }

    bool unsafe_empty()
    {
        return Base::empty();
    }

    size_t unsafe_size()
    {
        return Base::size();
    }

    void clear()
    {
        std::unique_lock lock( _lock );
        Base::clear();
    }

    void swap( LockDeque& other )
    {
        std::unique_lock lock( _lock );
        Base::swap( other );
    }

private:
    std::shared_mutex _lock;
};


} // namespace Mala::InnerContainer::lockfree
