module;

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#pragma comment(lib, "ntdll.lib")
#include <Windows.h>

#include "../MalaMacro.h"


export module Mala.Container.LockFreeStack;

import <cstdint>;
import <type_traits>;

import Mala.Core.Allocator;
import Mala.Core.TaggedPtr;
import Mala.Core.Types;

using namespace Mala::Core;


export namespace Mala::Container
{

template< typename T, typename TAllocator = Mala::Core::StlAllocator< T > >
class LockfreeStack
{
public:
    struct ALIGN_CACHE Element
    {
        Element* next;
        T        data;
    };

public:
    using NodeAllocator = typename TAllocator::template rebind< Element >::other;

public:
    LockfreeStack()
    : _top { MakeTagged< Element >() }
    {
    }

    ~LockfreeStack()
    {
        unsafe_clear();
    }

	void unsafe_clear()
	{
		T t;
		while ( TryPop( t ) ) { }
	}

	void push( const T& src )
	{
        emplace( src );
	}

    void push( T&& src )
    {
        emplace( std::move( src ) );
    }

    template< class... Args >
    decltype( auto ) emplace( Args&&... args )
    {
        Element* newNode = NodeAllocator{}.allocate( 1 );
        new( newNode ) Element{ .next { nullptr }, .data{ std::forward< Args >( args )... } };

        TaggedPtr< Element > localTop{ _top }; // 탑 캡처

        TaggedPtr< Element > newTop = newNode; // 새로 만든 노드를 탑 노드로 변환

        LOOP
        {
            if ( _top.GetTag2() != localTop.GetTag2() )
            {
                localTop = _top;
            }
            else // 로컬탑과 현재탑이 같은... 아직까진 변경이 안된 경우...
            {
                localTop._rawPtr = _top._rawPtr; // 탑 캡처

                newNode->next = localTop.GetPtr(); // newNode next를 탑을 가리킨다.

                newTop.SetTag( localTop.GetTag2() + 1 ); // 새 노드의 tag를 0으로 변경

                // 현재 탑이 캡처한 탑과 같은 경우 newTop을 새 탑으로 만든다.
                if ( localTop._value != InterlockedCompareExchange(
                    &_top._value,
                    newTop._value,
                    localTop._value ) )
                    continue;
                else
                    return;
            }
        }
    }

    bool TryPop( T& dest )
    {
        LOOP
        {
            TaggedPtr< Element > localTop = _top; // 캡처 시작
            if ( !localTop->next )
                return false;

            if ( _top.GetTag2() != localTop.GetTag2() )      // Test
                continue;

            TaggedPtr< Element > next_top( localTop->next ); // Register
            next_top.SetTag( localTop.GetTag2() + 1 );

            // Atomic test
            if ( localTop._value != InterlockedCompareExchange( &_top._value, next_top._value, localTop._value ) )
                continue;

            dest = localTop->data;

			localTop.GetPtr()->~Element();
            NodeAllocator{}.deallocate( localTop.GetPtr(), 1 );

            return true;
        }
    }

    bool Empty()
    {
        TaggedPtr< Element > localTop = _top;
        if ( !localTop->next )
            return false;

        return true;
    }

    bool TryPeek( T& dest )
    {
        TaggedPtr< Element > localTop = _top; // 캡처
        if ( !localTop->next )
            return false;

        dest = localTop->data;
        return true;
    }

    size_t unsafe_size()
    {
    	return _size;
    }

private:
    ALIGN_CACHE TaggedPtr< Element > _top;
    ALIGN_CACHE size_t			     _size{};
};


template< typename T, typename TAllocator = Mala::Core::StlAllocator< T > >
class LockFreeStack
{
public:
    inline static constexpr i32 SLIST_ALIGNMENT{ 16 };

	struct alignas( SLIST_ALIGNMENT ) Node : public SLIST_ENTRY
	{
		Node* next;
		T     data;
	};

public:
    using NodeAllocator = typename TAllocator::template rebind< Node >::other;

public:
    LockFreeStack()
    {
        ::InitializeSListHead( &_header );
    }

    ~LockFreeStack()
    {
        while ( auto* node = static_cast< Node* >( ::InterlockedPopEntrySList( &_header ) ) )
        {
            NodeAllocator{}.deallocate( node, 1 );
        }

        /*auto oldList = InterlockedFlushSList( &_header );
        while ( oldList )
        {
            Node* node = static_cast< Node* >( oldList );
            NodeAllocator{}.deallocate( node, 1 );
            oldList = InterlockedFlushSList( &_header );
        }*/

    }

    void Push( T&& arg )
    {
        Node* newNode = NodeAllocator{}.allocate( 1 );
        new( newNode ) Node{ .next { nullptr }, .data{ std::move( arg ) } };

        ::InterlockedPushEntrySList( &_header, static_cast< PSLIST_ENTRY >( newNode ) );
    }

    bool TryPop( T& dest )
    {
        PSLIST_ENTRY entry = ::InterlockedPopEntrySList( &_header );
        if ( !entry )
            return false;

        Node* node = static_cast< Node* >( entry );
        dest = std::move( node->data );
        NodeAllocator{}.deallocate( node, 1 );
        return true;
    }

    bool TryPeek( T& dest )
    {
        PSLIST_ENTRY entry = ::RtlFirstEntrySList( &_header );
        if ( !entry )
            return false;

        Node* node = static_cast< Node* >( entry );
        dest = node->data;
        return true;
    }

private:
    SLIST_HEADER _header{};
};


} // namespace Mala::InnerContainer::lockfree
