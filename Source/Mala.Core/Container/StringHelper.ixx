module;

export module Mala.Container.StringHelper;
import Mala.Core.Concepts;
import Mala.Container.String;
import Mala.Container;

using namespace Mala::Container;

export
{

bool StartsWith( const String& str, const String& prefix )
{
    return str.rfind( prefix, 0 ) == 0;
}

bool EndsWith( const String& str, const String& suffix )
{
    if ( str.size() < suffix.size() )
        return false;

    return str.rfind( suffix ) == str.size() - suffix.size();
}

bool Contains( const String& str, const StringView& substr )
{
    return str.find( substr ) != String::npos;
}

bool Contains( const String& str, const String& substr )
{
    return str.find( substr ) != String::npos;
}

bool IsEmptyOrWhiteSpace( const String& str )
{
    if ( str.empty() )
        return true;

    return str.find_first_not_of( L" \t\n" ) == String::npos;
}

bool IsEmptyOrWhiteSpace( const StringView& str )
{
    if ( str.empty() )
        return true;

    return str.find_first_not_of( L" \t\n" ) == StringView::npos;
}

String Trim( const String& str )
{
    const auto start = str.find_first_not_of( L" \t\n" );
    if ( start == String::npos )
        return L"";

    const auto end = str.find_last_not_of( L" \t\n" );

    return str.substr( start, end - start + 1 );
}

String RTrim( const String& str )
{
    const auto end = str.find_last_not_of( L" \t\n" );
    if ( end == String::npos )
        return L"";

    return str.substr( 0, end + 1 );
}

String LTrim( const String& str )
{
    const auto start = str.find_first_not_of( L" \t\n" );
    if ( start == String::npos )
        return L"";

    return str.substr( start );
}

String ToLower( const String& str )
{
    String lowerStr = str;

    for ( auto& c : lowerStr )
        c = std::tolower( c );

    return lowerStr;
}

String ToUpper( const String& str )
{
    String upperStr = str;

    for ( auto& c : upperStr )
        c = std::toupper( c );

    return upperStr;
}

auto Split( const String& str, const String& delimiter )
{
    Vector< String > result;
    size_t pos = 0;
    size_t prev = 0;

    while ( ( pos = str.find( delimiter, prev ) ) != String::npos )
    {
        result.emplace_back( str.substr( prev, pos - prev ) );
        prev = pos + delimiter.size();
    }

    result.emplace_back( str.substr( prev ) );
    if ( result.back().empty() )
        result.pop_back();

    return result;
}

auto Split( const StringView& str, const String& delimiter )
{
    Vector< String > result;
    size_t pos = 0;
    size_t prev = 0;

    while ( ( pos = str.find( delimiter, prev ) ) != String::npos )
    {
        result.emplace_back( str.substr( prev, pos - prev ) );
        prev = pos + delimiter.size();
    }

    result.emplace_back( str.substr( prev ) );
    if ( result.back().empty() )
        result.pop_back();

    return result;
}

auto SplitView( const StringView& str, const StringView& delimiter )
{
    Vector< StringView > result;
    size_t pos = 0;
    size_t prev = 0;

    while ( ( pos = str.find( delimiter, prev ) ) != String::npos )
    {
        result.emplace_back( str.substr( prev, pos - prev ) );
        prev = pos + delimiter.size();
    }

    result.emplace_back( str.substr( prev ) );
    if ( result.back().empty() )
        result.pop_back();

    return result;
}

String Join( const Vector< String >& strings, const String& delimiter )
{
    String result;
    for ( const auto& str : strings )
    {
        result += str;
        result += delimiter;
    }
    if ( !result.empty() )
        result.resize( result.size() - delimiter.size() );

    return result;
}

void RemoveLast( String& str, size_t n )
{
    if ( n >= str.size() )
        return;

    str.resize( str.size() - n );
    return;
}

}
