export module Mala.Container.LruCache;

import <vector>;
import <unordered_map>;
import <algorithm>;

import Mala.Core.Allocator;

export namespace Mala::Container
{

/// <summary>
/// 최근에 사용된 값을 저장하는 캐시 컨테이너
/// </summary>
template< typename TKey, typename TValue >//, typename TKeyComparer = std::unordered_map< TKey, TValue >::key_compare >
class LruCache
{
public:
    using KeyType = TKey;
    using ValueType = TValue;
    using MapType = std::unordered_map< TKey, TValue >;//, TKeyComparer >;
    using PairType = std::pair< TKey, TValue >;
    using ListType = std::vector< PairType >;

public:
    LruCache( size_t capacity )
        : _capacity( capacity )
    {
        _list.reserve( capacity );
    }

    ~LruCache() = default;

public:
    /// <summary>
    /// 캐시에 값을 추가합니다.
    /// </summary>
    void Add( const TKey& key, const TValue& value )
    {
        // 이미 존재하는 키라면 값을 갱신합니다.
        auto it = _map.find( key );
        if ( it != _map.end() )
        {
            _list.erase( std::find_if( _list.begin(), _list.end(), [ &key ]( const PairType& pair )
                {
                    return pair.first == key;
                } ) );
        }

        // 캐시에 추가합니다.
        _list.push_back( { key, value } );
        _map[ key ] = value;
        // 캐시가 꽉 찼다면 가장 오래된 값을 제거합니다.
        if ( _list.size() > _capacity )
        {
            _map.erase( _list.front().first );
            _list.erase( _list.begin() );
        }
    }
    /// <summary>
    /// 캐시에서 값을 제거합니다.
    /// </summary>
    void Remove( const TKey& key )
    {
        auto it = _map.find( key );
        if ( it != _map.end() )
        {
            _list.erase( std::find_if( _list.begin(), _list.end(), [ &key ]( const PairType& pair )
                {
                    return pair.first == key;
                } ) );
            _map.erase( key );
        }
    }
    /// <summary>
    /// 캐시에서 값을 가져옵니다.
    /// </summary>
    TValue Get( const TKey& key )
    {
        auto it = _map.find( key );
        if ( it != _map.end() )
        {
            _list.erase( std::find_if( _list.begin(), _list.end(), [ &key ]( const PairType& pair )
                {
                    return pair.first == key;
                } ) );
            _list.push_back( { key, it->second } );
            return it->second;
        }

        return {};
    }

    /// <summary>
    /// 캐시에 키가 존재하는지 확인합니다.
    /// </summary>
    bool Contains( const TKey& key )
    {
        return _map.contains( key );
    }

private:
    size_t _capacity;
    MapType _map;
    ListType _list;
};

};
