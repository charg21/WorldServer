module;

export module Mala.Container.EnumFlagMap;

import Mala.Container;
import Mala.Core.EnumHelper;
import Mala.Core.Concepts;

using namespace Mala;
using namespace Mala::Container;

export namespace Mala::Container
{

template< typename TEnumKey, typename TValue >
class EnumFlagMap
{
public:
    EnumFlagMap()
    {
        _values.resize( 64 );
        _iteration.reserve( 64 );
    }

    bool Add( TEnumKey key, TValue value )
    {
        if ( Has( key ) )
            return false;

        auto index = EnumHelper< TEnumKey >::AsIndex( key );
        _values[ index ] = value;

        return true;
    }

    bool emplace( TEnumKey key, TValue value )
    {
        return Add( key, value );
    }

    void Remove( TEnumKey key )
    {
        if ( Has( key ) )
            return;

        auto index = EnumHelper< TEnumKey >::AsIndex( key );
        _values[ index ] = {};
    }

    void erase( TEnumKey key )
    {
        Remove( key );
    }

    bool Has( TEnumKey key )
    {
        auto index = EnumHelper< TEnumKey >::AsIndex( key );
        return _values[ index ];
    }

    bool contains( TEnumKey key )
    {
        return Has( key );
    }

    TValue& Get( TEnumKey key )
    {
        auto index = EnumHelper< TEnumKey >::AsIndex( key );
        return _values[ index ];
    }

    bool TryGet( TEnumKey key, TValue& outValue )
    {
        if ( !Has( key ) )
            return false;

        outValue = _values[ EnumHelper< TEnumKey >::AsIndex( key ) ];
        return true;
    }

    TValue& operator[]( TEnumKey key )
    {
        return Get( key );
    }

    auto begin()
    {
        _iteration.begin();
    }

    auto end()
    {
        _iteration.end();
    }

    auto extract( TEnumKey key )
    {
        auto index = EnumHelper< TEnumKey >::AsIndex( key );
        TValue value{};
        if ( _values[ index ] )
            std::swap( _values[ index ], value );

        return value;
    }

    template< std::invocable IComponetntTask >
    void ForEach( IComponetntTask job )
    {
        for ( auto& value : _values )
            job( value );
    }

private:
    Vector< TValue > _values;
    Vector< TValue > _iteration;
};

};
