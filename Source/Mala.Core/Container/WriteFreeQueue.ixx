module;

#include "../MalaMacro.h"
import <memory>;

export module Mala.Container.WriteFreeQueue;

import Mala.Container.WaitFreeQueue;
import Mala.Container;
import Mala.Core.Allocator;
import Mala.Core.Types;
import Mala.Threading.Lock;

using namespace Mala::Threading;

NAMESPACE_BEGIN( Mala::Container )

// WaitfreeQueue의 Empty() 메서드가, 컨슘 권한을 가지지 않는 곳에서
// 사용시 매우 매우 위험할 수 있기 때문에, Empty()를 안전하게 쓸 수 있도록 래핑한 클래스
// _head에서 접근하는 경우 락이 걸리기 때문에, Empty(), TryDeque()가 안전하다.
// 하지만, 락을 사용하기 때문에 ConsumeAll(), ForEach() 등의
// 락을 오래 잡을 수 있는 함수들의 사용을 비추천합니다.
// TakeAll등으로 외부로 가져와, 락잡는 시간을 최소한으로 잡길 추천합니다.
template< typename T >
class WriteFreeQueue : public WaitFreeQueue< T >
{
    using Base = WaitFreeQueue< T >;

public:
    MALA_NODISCARD void ForEach( const typename Base::ConsumeJob& consumer ) = delete;

    MALA_NODISCARD int64_t ConsumeAll( Base::ConsumeJob&& consumer )
    {
        WriteLock< Mala::Threading::Lock > writeLock( _lock );

        return Base::ConsumeAll( consumer );
    }

    MALA_NODISCARD int64_t ConsumeAll( const typename Base::ConsumeJob& consumer )
    {
        WriteLock< Mala::Threading::Lock > writeLock( _lock );

        return Base::ConsumeAll( consumer );
    }

    MALA_NODISCARD bool TryDequeue( T& dest )
    {
        WriteLock< Mala::Threading::Lock > writeLock( _lock );

        return Base::TryDequeue( dest );
    }

    template< typename Container = std::vector< T > >
    MALA_NODISCARD Container TakeAll()
    {
        WriteLock< Mala::Threading::Lock > writeLock( _lock );

        Container container = Base::TakeAll();

        return std::move( container );
    }

    template< typename Container = std::vector< T > >
    void TakeAll( Container& outCountainer )
    {
        ReadLock< Mala::Threading::Lock > readLock( _lock );

        Base::TakeAll( outCountainer );
    }

    MALA_NODISCARD constexpr bool Empty() noexcept
    {
        ReadLock< Mala::Threading::Lock > readLock( _lock );

        return Base::UnsafeEmpty();
    }

private:
    Lock _lock;
};

template< typename TKey, typename TValue >
class LazySyncMap
{
    using TElement       = std::pair< TKey, TValue >;
    using TMap           = Mala::Container::HashMap< TKey, TValue >;
    using TLazySyncQueue = WaitFreeQueue< TElement >;
public:
    template< typename... Args >
    void Emplace( Args&&... args )
    {
        _syncQueue.Emplace( std::forward< Args >( args )... );
    }

    void Sync()
    {
        TElement outElement;
        while( _syncQueue.TryDequeue( outElement ) )
        {
            _map.emplace( outElement );
        }
    }


private:
    TLazySyncQueue _syncQueue;
    TMap           _map;

    //Lock       _lock;
};

NAMESPACE_END( Mala::Container )
