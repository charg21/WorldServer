module;

#pragma once

export module Mala.Container.MapHelper;

import Mala.Core.Random;
import Mala.Core.TypeTraits;
import Mala.Core.Concepts;

import <ranges>;
import <optional>;
import <vector>;
import <memory>;

template< typename T, typename Enable = void >
struct InnermostMappedType
{
    using type = T;
};

template< typename TMap >
struct InnermostMappedType< TMap, std::void_t< typename TMap::mapped_type > >
{
    using type = typename InnermostMappedType< typename TMap::mapped_type >::type;
};

export namespace Mala::Container
{

class MapHelper
{
public:
    template< typename TMap >
    static bool TryGetRandomKey( const TMap& map, typename TMap::key_type& outKey )
    {
        if ( map.empty() )
            return false;

        auto iter = std::next( map.begin(), Mala::Core::FastRand() % map.size() );
        outKey = iter->first;
        return true;
    }

    template< typename TMap >
    static bool TryGetRandomValue( const TMap& map, typename TMap::mapped_type& outValue )
    {
        if ( map.empty() )
            return false;

        auto iter = std::next( map.begin(), Mala::Core::FastRand() % map.size() );
        outValue = iter->second;
        return true;
    }

    template< typename TList >
    static bool TryGetRandomValue( const TList& list, typename TList::value_type& outValue )
    {
        if ( list.empty() )
            return false;

        auto randIndex = Mala::Core::FastRand() % list.size();
        outValue = list[ randIndex ];
        return true;
    }

    /// <summary>
    /// N차원 map 컨테이너에서 키를 통해 값을 찾는다
    /// </summary>
    template< typename TMap, typename... TKeys >
    static bool TryGetValue( const TMap& map, auto& outValue, TKeys... keys )
    {
        return TryGetValueImpl( map, outValue, keys... );
    }

    /// <summary>
    /// N차원 map 컨테이너에서 키를 통해 값을 찾는다
    /// </summary>
    template< typename TMap, typename... TKeys >
    static auto Find( const TMap& map, TKeys... keys )
    {
        return FindImpl( map, keys... );
    }

    /// <summary>
    /// N차원 map 컨테이너에서 키를 통해 값을 찾는다( shared_ptr, weak_ptr )
    /// </summary>
    template< typename TMap, typename... TKeys > requires SmartPtrType< typename InnermostMappedType< TMap >::type >
    static auto Find( TMap& map, TKeys... keys )
    {
        return FindImpl( map, keys... );
    }

    /// <summary>
    /// N차원 map 컨테이너에서 키를 통해 값을 찾거나 값이 없다면 할당해준다.
    /// </summary>
    template< typename TMap, typename... TKeys >
    static auto& GetOrDefault( TMap& map, TKeys... keys )
    {
        return GetOrDefaultImpl( map, keys... );
    }

    template< typename TMap, typename TKey, typename... TKeys >
    static bool ContainsKey( const TMap& map, const TKey& key, TKeys... leftKeys )
    {
        if constexpr ( sizeof...( leftKeys ) == 0 )
        {
            return map.contains( key );
        }
        else
        {
            if ( !map.contains( key ) )
                return false;

            return ContainsKey( map.find( key )->second, leftKeys... );
        }
    }

    template< typename TMap >
    static auto In( const TMap& map, const std::vector< typename TMap::key_type >& keys )
    {
        using TKey = TMap::key_type;

        return keys | std::views::filter( [ &map ]( const TKey& key )
            {
                return map.contains( key );
            } )
            |
            std::views::transform( [ &map ]( const TKey& key ) -> const auto&
                {
                    return map.at( key );
                } );
    }

private:
    template< typename TMap, typename TKey, typename... TKeys >
    static bool TryGetValueImpl( TMap& map, auto& outValue, const TKey& key, TKeys... leftKeys )
    {
        auto iter = map.find( key );
        if ( iter == map.end() )
            return false;

        if constexpr ( sizeof...( leftKeys ) == 0 )
        {
            outValue = iter->second;
            return true;
        }
        else
        {
            return TryGetValueImpl( iter->second, outValue, leftKeys... );
        }
    }

    template< typename TMap, typename TKey, typename... TKeys >
    static auto& GetOrDefaultImpl( TMap& map, const TKey& key, TKeys... leftKeys )
    {
        if constexpr ( sizeof...( leftKeys ) == 0 )
        {
            return map[ key ];
        }
        else
        {
            return GetOrDefaultImpl( map[ key ], leftKeys... );
        }
    }

    template< typename TMap, typename TKey, typename... TKeys >
    static const InnermostMappedType< TMap >::type* FindImpl( TMap& map, const TKey& key, TKeys... leftKeys )
    {
        auto iter = map.find( key );
        if ( iter == map.end() )
            return nullptr;

        if constexpr ( sizeof...( leftKeys ) == 0 )
        {
            return &iter->second;
        }
        else
        {
            return FindImpl( iter->second, leftKeys... );
        }
    }

    template< typename TMap, typename TKey, typename... TKeys > requires SmartPtrType< typename InnermostMappedType< TMap >::type >
    static InnermostMappedType< TMap >::type FindImpl( TMap& map, const TKey& key, TKeys... leftKeys )
    {
        auto iter = map.find( key );
        if ( iter == map.end() )
            return nullptr;

        if constexpr ( sizeof...( leftKeys ) == 0 )
        {
            return iter->second;
        }
        else
        {
            return FindImpl( iter->second, leftKeys... );
        }
    }

};

/*

    std::unordered_map< int, int > map
    {
        { 1, 10 },
        { 2, 20 },
        { 3, 30 },
        { 4, 40 },
    };

    std::unordered_map< int, std::map< int, int > > map2d
    {
        { 1, { { 10, 100 } } },
        { 2, { { 20, 200 } } },
        { 3, { { 30, 300 } } },
        { 4, { { 40, 400 } } },
    };

    int key{};
    if ( MapHelper::TryGetRandomKey( map, key ) )
    {
        std::cout << "성공: " << key << std::endl;

        if ( MapHelper::TryGetRandomKey( map, key ) )
        {
            std::cout << "성공: " << key << std::endl;
        }
    }

    int outValue{};
    if ( MapHelper::TryGetValue( map2d, outValue, 1, 10 ) )
    {
        std::cout << "성공: " << outValue << std::endl;
    }

    int i = MapHelper::GetOrDefault( map, 1 );// , 10 );
    std::cout << "성공: " << i << std::endl;

    int ii = MapHelper::GetOrDefault( map2d, 2, 34 );
    std::cout << "성공: " << ii << std::endl;

    if ( MapHelper::ContainsKey( map2d, 2, 34 ) )
        std::cout << "성공: " << std::endl;


    std::map< int, std::map< int, std::shared_ptr< int > > > smap2d
    {
        { 1, { { 10, MakeShared< int >( 100 ) } } },
        { 2, { { 20, MakeShared< int >( 200 ) } } },
        { 3, { { 30, MakeShared< int >( 300 ) } } },
        { 4, { { 40, MakeShared< int >( 400 ) } } },
    };

    auto* p = MapHelper::Find( map2d, 3, 30 );
    if ( p )
        std::cout << "MapHelper::Find 성공: " << *p << std::endl;

    auto sp = MapHelper::Find( smap2d, 2, 20 );
    if ( sp )
        std::cout << "MapHelper::Find shared_ptr 성공: " << *sp << std::endl;

*/

};
