//export module Mala.Container.StaticString;
//
//
//import <cstring>;
//import <cwchar>;
//import <string>;
//import <string_view>;
//
//import Mala.Core.Types;
//import Mala.Container;
//
//export namespace Mala::Container
//{
///**
// * \brief 
// * \tparam TChar 
// * \tparam Capacity 
// */
//
//template< typename TChar, size_t Capacity >
//class StaticStringBase
//{
//    using StaticCharArray = Array< TChar, Capacity >;
//
//public:
//    StaticStringBase()
//    : _size{}
//    {
//        memset( _str, 0, sizeof( _str ) );
//    }
//
//    StaticStringBase( const TChar* str )
//    : _size{ std::char_traits< TChar >::length( str ) }
//    {
//        if (_size > Capacity - 1) {
//            _size = Capacity - 1;
//        }
//        memcpy(_str, str, _size * sizeof(TChar));
//        _str[_size] = TChar('\0');
//    }
//
//    StaticStringBase( const StaticStringBase& other )
//    {
//        _size = other._size;
//        memcpy( _str, other._str, ( _size + 1 ) * sizeof( TChar ) );
//    }
//
//    StaticStringBase& operator=( const TChar* str )
//    {
//        _size = std::char_traits< TChar >::length( str );
//        if (_size > Capacity - 1 ) 
//            _size = Capacity - 1;
//
//        std::memcpy( _str, str, _size * sizeof( TChar ) );
//        _str[ _size ] = TChar( '\0' );
//
//        return *this;
//    }
//
//    StaticStringBase& operator=( const StaticStringBase& other )
//    {
//        _size = other._size;
//        memcpy( _str, other._str, ( _size + 1 ) * sizeof( TChar ) );
//
//        return *this;
//    }
//
//    TChar operator[](size_t i) const
//    {
//        return _str[i];
//    }
//
//    TChar& operator[](size_t i)
//    {
//        return _str[i];
//    }
//
//    const TChar* c_str() const
//    {
//        return _str;
//    }
//
//    const wchar_t* wc_str() const
//    {
//        static wchar_t wstr[ Capacity ];
//        std::mbstowcs( wstr, _str, _size );
//        wstr[ _size ] = L'\0';
//
//        return wstr;
//    }
//
//    size_t size() const
//    {
//        return _size;
//    }
//
//    static constexpr size_t capacity()
//    {
//        return Capacity;
//    }
//
//    bool operator==( const StaticStringBase& other ) const
//    {
//        return std::char_traits< TChar >::compare( _str, other._str, _size ) == 0;
//    }
//
//    bool operator==( const std::basic_string< TChar >& str ) const
//    {
//        return std::char_traits< TChar >::compare( _str, str.c_str(), _size ) == 0;
//    }
//
//    bool operator==( const std::basic_string_view< TChar >& strView ) const
//    {
//        return std::char_traits< TChar >::compare(_str, strView.data(), _size) == 0;
//    }
//
//    bool operator!=( const StaticStringBase& other ) const
//    {
//        return !( *this == other );
//    }
//
//    bool operator!=( const std::basic_string< TChar >& str ) const
//    {
//        return !( *this == str );
//    }
//
//    bool operator!=( const std::basic_string_view< TChar >& strView ) const
//    {
//        return !(*this == strView);
//    }
//
//    operator std::basic_string_view< TChar >() const
//    {
//        return std::basic_string_view< TChar >(_str, _size);
//    }
//
//private:
//    StaticCharArray  _str;  //
//    size_t           _size; //
//};
//
//template< size_t N >
//using StaticAnsiString = StaticStringBase< char, N > ;
//
//template< size_t N >
//using StaticString = StaticStringBase< wchar_t, N >;
//}