export module Mala.Math.Matrix4x4;

import <array>;
import <vector>;

import Mala.Math.Vector4;

namespace Mala::Math
{

struct Matrix4x4
{
public:
	/// <summary>
	/// ������
	/// </summary>
	constexpr Matrix4x4() = default;
	explicit constexpr Matrix4x4( 
		const Vector4& col0, 
		const Vector4& col1, 
		const Vector4& col2, 
		const Vector4& col3 )
	:_cols{ col0, col1, col2, col3 }
	{ 
	}

	///
//	const Vector4& operator[](BYTE InIndex) const;
//	Vector4& operator[](BYTE InIndex);
//	constexpr Matrix4x4 operator*(float InScalar) const;
//	constexpr Matrix4x4 operator*(const Matrix4x4& InMatrix) const;
//	constexpr Vector4 operator*(const Vector4& InVector) const;
//	friend Vector4 operator*=(Vector4& InVector, const Matrix4x4& InMatrix)
//	{
//		InVector = InMatrix * InVector;
//		return InVector;
//	}
//	constexpr Vector3 operator*(const Vector3& InVector) const;
//	friend Vector3 operator*=(Vector3& InVector, const Matrix4x4& InMatrix)
//	{
//		InVector = InMatrix * InVector;
//		return InVector;
//	}
//
//	// ����Լ� 
//	Matrix3x3 ToMatrix3x3() const;
	void SetIdentity();
//	constexpr Matrix4x4 Transpose() const;
//
//	std::vector<std::string> ToStrings() const;
//
//	// ����������� 
	static const Matrix4x4 Identity;
	inline static constexpr size_t Rank = 4;

	// ������� 
	std::array< Vector4, Rank > _cols;// = { Vector4::UnitX, Vector4::UnitY, Vector4::UnitZ, Vector4::UnitW };
};

const Matrix4x4 Matrix4x4::Identity{ Vector4::Zero, Vector4::Zero, Vector4::Zero, Vector4::Zero };

void Matrix4x4::SetIdentity()
{
	*this = Matrix4x4::Identity;
}

//constexpr Matrix4x4 Matrix4x4::Transpose() const
//{
//	return Matrix4x4(
//		Vector4(Cols[0].X, Cols[1].X, Cols[2].X, Cols[3].X),
//		Vector4(Cols[0].Y, Cols[1].Y, Cols[2].Y, Cols[3].Y),
//		Vector4(Cols[0].Z, Cols[1].Z, Cols[2].Z, Cols[3].Z),
//		Vector4(Cols[0].W, Cols[1].W, Cols[2].W, Cols[3].W)
//	);
//}
//
//const Vector4& Matrix4x4::operator[](BYTE InIndex) const
//{
//	assert(InIndex < Rank);
//	return Cols[InIndex];
//}
//
//Vector4& Matrix4x4::operator[](BYTE InIndex)
//{
//	assert(InIndex < Rank);
//	return Cols[InIndex];
//}
//
//constexpr Matrix4x4 Matrix4x4::operator*(float InScalar) const
//{
//	return Matrix4x4(
//		Cols[0] * InScalar,
//		Cols[1] * InScalar,
//		Cols[2] * InScalar,
//		Cols[3] * InScalar
//	);
//}
//
//constexpr Matrix4x4 Matrix4x4::operator*(const Matrix4x4 &InMatrix) const
//{
//	Matrix4x4 transposedMatrix = Transpose();
//	return Matrix4x4(
//		Vector4(transposedMatrix[0].Dot(InMatrix[0]), transposedMatrix[1].Dot(InMatrix[0]), transposedMatrix[2].Dot(InMatrix[0]), transposedMatrix[3].Dot(InMatrix[0])),
//		Vector4(transposedMatrix[0].Dot(InMatrix[1]), transposedMatrix[1].Dot(InMatrix[1]), transposedMatrix[2].Dot(InMatrix[1]), transposedMatrix[3].Dot(InMatrix[1])),
//		Vector4(transposedMatrix[0].Dot(InMatrix[2]), transposedMatrix[1].Dot(InMatrix[2]), transposedMatrix[2].Dot(InMatrix[2]), transposedMatrix[3].Dot(InMatrix[2])),
//		Vector4(transposedMatrix[0].Dot(InMatrix[3]), transposedMatrix[1].Dot(InMatrix[3]), transposedMatrix[2].Dot(InMatrix[3]), transposedMatrix[3].Dot(InMatrix[3]))
//	);
//}
//
//constexpr Vector4 Matrix4x4::operator*(const Vector4& InVector) const
//{
//	Matrix4x4 transposedMatrix = Transpose();
//	return Vector4(
//		transposedMatrix[0].Dot(InVector),
//		transposedMatrix[1].Dot(InVector),
//		transposedMatrix[2].Dot(InVector),
//		transposedMatrix[3].Dot(InVector)
//	);
//}
//
//constexpr Vector3 Matrix4x4::operator*(const Vector3& InVector) const
//{
//	Vector4 v4(InVector);
//	Vector4 result = *this * v4;
//
//	return Vector3(result.X, result.Y, result.Z);
//}
//
//Matrix3x3 Matrix4x4::ToMatrix3x3() const
//{
//	return Matrix3x3(Cols[0].ToVector3(), Cols[1].ToVector3(), Cols[2].ToVector3());
//}
//


}