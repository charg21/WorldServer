export module Mala.Math.Circle;

import Mala.Math.Vector2;
import Mala.Math.Vector3;

export namespace Mala::Math
{

/// <summary>
/// 
/// </summary>
struct Circle
{
    Circle() = default;
    Circle( const Circle& rhs )
    : _center{ rhs._center }
    , _radius{ rhs._radius }
    {}

    Circle( const Vector3& v, float r )
    : _center{ v }
    , _radius{ r }
    {}

    Vector3 _center{};
    float   _radius{};
};

}
