import Mala.Math;

import <cmath>;

using namespace Mala::Math;

constexpr Vector3::Vector3( float x, float y, float z )
: _x{ x }
, _y{ y }
, _z{ z }
{}

constexpr Vector3::Vector3( int x, int y, float z )
: _x( x )
, _y( y )
, _z( z )
{}

float Vector3::Magnitude() const
{
    return std::sqrtf( SqrMagnitude() );
}

float Vector3::Size() const
{
    return std::sqrtf( SqrMagnitude() );
}

float Vector3::SqrMagnitude() const
{
    return ( _x * _x ) + ( _y * _y ) + ( _z * _z );
}

float Vector3::SizeSquared() const
{
    return ( _x * _x ) + ( _y * _y ) + ( _z * _z );
}

void Vector3::Normalize()
{
    *this = GetNormalize();
}

Vector3 Vector3::GetNormalize() const
{
    float squareSum = SizeSquared();
    if ( squareSum == 1.f )
        return *this;

    if ( squareSum == 0.f )
        return Vector3::Zero;

    float invLength = Math::InvSqrt( squareSum );
    return Vector3{ _x * invLength, _y * invLength, _z * invLength };
}

const Vector3& Vector3::operator+=( const Vector3& rhs )
{
    _x += rhs._x;
    _y += rhs._y;
    _z += rhs._z;

    return *this;
}

Vector3 Vector3::FromYaw( float yawDegree )
{
    const float yawRadian = Deg2Rad( yawDegree );
    return Vector3{ std::sin( yawRadian ), 0.f, std::cos( yawRadian ) };
}

const Vector3 Vector3::One  {  1.0f, 1.0f,  1.0f };
const Vector3 Vector3::Zero {  0.0f, 0.0f,  0.0f };
const Vector3 Vector3::Unit {  1.0f, 1.0f,  0.0f };
const Vector3 Vector3::UnitX{  1.0f, 0.0f,  0.0f };
const Vector3 Vector3::UnitY{  0.0f, 1.0f,  0.0f };
const Vector3 Vector3::UnitZ{  0.0f, 0.0f,  1.0f };
const Vector3 Vector3::Up   {  0.0f, 0.0f,  1.0f };
const Vector3 Vector3::Down {  0.0f, 0.0f, -1.0f };
const Vector3 Vector3::Left { -1.0f, 0.0f,  0.0f };
const Vector3 Vector3::Right{  1.0f, 0.0f,  0.0f };