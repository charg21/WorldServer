export module Mala.Math.Base;

import <array>;

export namespace Mala::Math
{

template< typename TBaseType, int DimensionCount >
struct TVectorBase
{
    inline static constexpr int Dimension = DimensionCount;

    using ScalarType = std::array< TBaseType, DimensionCount >;
    using BaseType   = TBaseType;

    inline static consteval int GetDimension()
    {
        return DimensionCount;
    }
};

template< typename TBaseType, int Rank >
struct TMatrixBase
{
    inline static constexpr int Dimension = Rank;

    using ScalarType = std::array< TBaseType, Rank >;
    using BaseType   = TBaseType;

    inline static consteval int GetRank()
    {
        return Rank;
    }
};


} // namespace Mala::Math