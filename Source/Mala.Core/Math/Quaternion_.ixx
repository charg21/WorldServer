export module Mala.Math.Quaternion:impl;

import Mala.Math;
import Mala.Math.Quaternion;

namespace Mala::Math
{

constexpr Quaternion::Quaternion( float x, float y, float z, float w )
: _x{ x }
, _y{ y }
, _z{ z }
, _w{ w }
{
}

constexpr Quaternion::Quaternion( float yaw, float pitch, float roll )
{
}

constexpr Quaternion::Quaternion(const Vector3& axis, float angleDegree)
{
}

}