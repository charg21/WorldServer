import Mala.Math;
import <cmath>;

using namespace Mala::Math;

constexpr Vector3Int::Vector3Int( const Vector2Int& rhs )
: _x{ rhs._x }
, _y{ rhs._y }
, _z{ 0 }
{
}

/*constexpr */Vector3Int::Vector3Int( i32 x, i32 y, i32 z )
: _x{ x }
, _y{ y }
, _z{ z }
{}

/*constexpr */Vector3Int::Vector3Int( float x, float y, float z )
: _x( x )
, _y( y )
, _z( z )
{}

float Vector3Int::Magnitude()
{
    return std::sqrtf( SqrMagnitude() );;
}

i32 Vector3Int::SqrMagnitude()
{
    return ( _x * _x ) + ( _y * _y ) + ( _z * _z );
}

float Distance( const Vector3Int& lhs, const Vector3Int& rhs )
{
    return Vector3Int( lhs - rhs ).Magnitude();
}

const Vector3Int Vector3Int::Zero { 0, 0, 0 };
const Vector3Int Vector3Int::Unit { 1, 1, 0 };
const Vector3Int Vector3Int::UnitX{ 1, 0, 0 };
const Vector3Int Vector3Int::UnitY{ 0, 1, 0 };
const Vector3Int Vector3Int::UnitZ{ 0, 0, 1 };
