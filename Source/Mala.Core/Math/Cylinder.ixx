export module Mala.Math.Cylinder;

import Mala.Math.Vector3;

export namespace Mala::Math
{

/// <summary>
/// 
/// </summary>
struct Cylinder
{
    /// <summary>
    /// 기본 생성자
    /// </summary>
    Cylinder() = default;

    Cylinder( const Cylinder& rhs )
    : _center   { rhs._center    }
    , _direction{ rhs._direction }
    , _radius   { rhs._radius    }
    , _height   { rhs._height    }
    {}

    Cylinder( Cylinder&& rhs ) noexcept
    : _center   { rhs._center    }
    , _direction{ rhs._direction }
    , _radius   { rhs._radius    }
    , _height   { rhs._height    }
    {}


    
    Vector3 _center{};
    Vector3 _direction{};
    float   _radius{};
    float   _height{};
};

}
