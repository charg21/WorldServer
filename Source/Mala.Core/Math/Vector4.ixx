export module Mala.Math.Vector4;

import Mala.Core.Types;
import Mala.Math.Base;

export namespace Mala::Math
{

/// <summary>
/// 4차원 실수형 좌표
/// </summary>
struct Vector4 final : public TVectorBase< float, 3 >
{
    friend Vector4 operator+( const Vector4& lhs, const Vector4& rhs );
    friend Vector4 operator-( const Vector4& lhs, const Vector4& rhs );
    friend Vector4 operator*( const Vector4& lhs, i32 multiple );
    friend Vector4 operator*( i32 multiple, const Vector4& rhs );
    friend Vector4 operator/( const Vector4& lhs, i32 d );
    friend Vector4 operator/( i32 d, const Vector4& rhs );

    using BaseType = float;

    /// <summary>
    /// 생성자
    /// </summary>
    constexpr Vector4() = default;
    explicit constexpr Vector4( float x, float y, float z );
    explicit constexpr Vector4( int x, int y, float z );

    /// <summary>
    /// 벡터의 길이를 반환한다
    /// </summary>
    float Magnitude();

    /// <summary>
    /// 벡터의 제곱 길이를 반환한다
    /// </summary>
    int SqrMagnitude();


    const Vector4& operator+=( const Vector4& rhs );


    static const Vector4 Zero;
    static const Vector4 Unit;

    union
    {
        struct
        {
            BaseType _x;
            BaseType _y;
            BaseType _z;
        };

        ScalarType _scalars{};
    };
};

Vector4 operator*( const Vector4& lhs, i32 multiple );
Vector4 operator+( const Vector4& lhs, const Vector4& rhs );
Vector4 operator-( const Vector4& lhs, const Vector4& rhs );
Vector4 operator*( const Vector4& lhs, i32 multiple );
Vector4 operator*( i32 multiple, const Vector4& rhs );
Vector4 operator/( const Vector4& lhs, i32 d );
Vector4 operator/( i32 d, const Vector4& rhs );

bool operator!=( const Vector4& lhs, const Vector4& rhs );
void operator*=( Vector4& lhs, i32 multiple );


}
