export module Mala.Math.Vector3;

import Mala.Core.Types;
import Mala.Math.Base;

export namespace Mala::Math
{

/// <summary>
/// 3차원 실수형 좌표를 나타내는 객체
/// </summary>
struct Vector3 final : public TVectorBase< float, 3 >
{
    friend Vector3 operator+( const Vector3& lhs, const Vector3& rhs );
    friend Vector3 operator-( const Vector3& lhs, const Vector3& rhs );
    friend Vector3 operator*( const Vector3& lhs, f32 multiple );
    friend Vector3 operator*( const Vector3& lhs, i32 multiple );
    friend Vector3 operator*( i32 multiple, const Vector3& rhs );
    friend Vector3 operator/( const Vector3& lhs, i32 d );
    friend Vector3 operator/( i32 d, const Vector3& rhs );

    using BaseType = float;

    /// <summary>
    /// 생성자
    /// </summary>
    constexpr Vector3() = default;
    explicit constexpr Vector3( float x, float y, float z );
    explicit constexpr Vector3( int x, int y, float z );

    /// <summary>
    /// 벡터의 길이를 반환한다
    /// </summary>
    float Magnitude() const;
    float Size() const;

    /// <summary>
    /// 벡터의 제곱 길이를 반환한다
    /// </summary>
    float SqrMagnitude() const;
    float SizeSquared() const;

    /// <summary>
    /// 두 벡터간의 내적값을 반환한다.
    /// </summary>
    constexpr float Dot( const Vector3& V ) const
    {
        return X * V.X + Y * V.Y + Z * V.Z;
    }

    constexpr Vector3 Cross( const Vector3& V ) const
    {
        return Vector3
        (
            Y * V.Z - Z * V.Y,
            Z * V.X - X * V.Z,
            X * V.Y - Y * V.X
        );
    }

    ///
    Vector3 FromYaw( float yaw );

    /// <summary>
    /// 정규화 한다
    /// </summary>
    void Normalize();
    [[ nodiscard ]] Vector3 GetNormalize() const;

    const Vector3& operator+=( const Vector3& rhs );

public:
    static const Vector3 One;
    static const Vector3 Zero;
    static const Vector3 Unit;
    static const Vector3 UnitX;
    static const Vector3 UnitY;
    static const Vector3 UnitZ;
    static const Vector3 Up;
    static const Vector3 Down;
    static const Vector3 Left;
    static const Vector3 Right;

public:
    union
    {
        struct
        {
            BaseType _x;
            BaseType _y;
            BaseType _z;
        };

        struct
        {
            BaseType X;
            BaseType Y;
            BaseType Z;
        };

        struct
        {
            size_t   _compareValue;
            BaseType _z;
        };

        ScalarType _scalars{};
    };
};

Vector3 operator+( const Vector3& lhs, const Vector3& rhs )
{
    return Vector3{ lhs._x + rhs._x, lhs._y + rhs._y, lhs._z + rhs._z };
}

Vector3 operator-( const Vector3& lhs, const Vector3& rhs )
{
    return Vector3{ lhs._x - rhs._x, lhs._y - rhs._y, lhs._z - rhs._z };
}

Vector3 operator*( const Vector3& lhs, f32 multiple )
{
    return Vector3{ lhs._x * multiple, lhs._y * multiple, lhs._z * multiple };
}

Vector3 operator*( const Vector3& lhs, i32 multiple )
{
    return Vector3{ lhs._x * multiple, lhs._y * multiple, lhs._z * multiple };
}

Vector3 operator*( i32 multiple, const Vector3& rhs )
{
    return Vector3{ rhs._x * multiple, rhs._y * multiple, rhs._z * multiple };
}

Vector3 operator/( const Vector3& lhs, i32 d )
{
    return Vector3{ lhs._x / d, lhs._y / d, lhs._z / d };
}

Vector3 operator/( i32 d, const Vector3& rhs )
{
    return Vector3{ rhs._x / d, rhs._y / d, rhs._z / d };
}

bool operator==( const Vector3& lhs, const Vector3& rhs )
{
    return ( lhs._compareValue == rhs._compareValue ) && ( lhs._z == rhs._z );
}

bool operator!=( const Vector3& lhs, const Vector3& rhs )
{
    return ( lhs._compareValue != rhs._compareValue ) || ( lhs._z != rhs._z );
}

void operator*=( Vector3& lhs, i32 multiple )
{
    lhs._x *= multiple;
    lhs._y *= multiple;
    lhs._z *= multiple;
}


using FVector = Vector3;

}
