module;

export module Mala.Log;

import <functional>;
import <atomic>;
import Mala.Core.Format;
import Mala.Core.Types;
import Mala.Core.SourceLocation;
import Mala.Container;
import Mala.Math.MathUtil;
import Mala.Threading.Thread;

using namespace Mala::Container;
using namespace Mala::Math;

export struct LogRecord
{
    i32    _threadId = -1;
    String _message;
};

export template< size_t N >
class LogRecordQueue
{
private:
    inline static constexpr i64 LOG_CAPACITY  = PowOfTwo< N >();
    inline static constexpr i64 LOG_SIZE_MASK = LOG_CAPACITY - 1;

    using LogArray = Array< LogRecord, LOG_CAPACITY >;

public:
    void Emplace( i32 threadId, String&& log )
    {
        auto nextLogIndex = _writeIndex++;

        _logs[ nextLogIndex & LOG_SIZE_MASK ]._message  = std::move( log );
        _logs[ nextLogIndex & LOG_SIZE_MASK ]._threadId = threadId;
    }

    template< std::invocable< const LogRecord& > LogJob >
    void ConsumeAll( const LogJob& logJob )
    {
        u64 cnt = _writeIndex - _readIndex;

        u64 readCnt{};
        for ( readCnt; readCnt < cnt; readCnt += 1 )
        {
            u64 curReadIndex = ( _readIndex + readCnt ) & LOG_SIZE_MASK;
            if ( _logs[ curReadIndex ]._threadId == -1 )
                break;

            logJob( _logs[ curReadIndex ] );

            _logs[ curReadIndex ]._threadId = -1;
        }

        _readIndex += readCnt;
    }

private:
    LogArray _logs;

    // for multiple producer...
    std::atomic< u64 > _writeIndex;

    // for single consumer....
    u64 _readIndex;
};

export extern inline LogRecordQueue< 16 >* GLogQueue{};

export namespace LogWriter
{
    void _Write( String&& log );

    template< typename... Args >
    void Write( const StringView fmt = L"", const Args&... args )
    {
        _Write( xvformat( fmt, std::make_wformat_args( args... ) ) );
    }

    void Flush();
};

