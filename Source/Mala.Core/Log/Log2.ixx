export module Mala.Core.Log2;

import Mala.Container.String;

import <iostream>;

export namespace Mala::Core
{

enum class LogLevel;

extern inline LogLevel gLogLevelFilter;// = LogLevel::Debug;

enum class LogLevel
{
    Debug,
    Info,
    Warring,
    Critical,
};

struct LogRecord
{

};

class Logger
{
public:
    template< LogLevel TLogLevel, typename... Args >
    static void Log( const wchar_t* fmt, Args&&... args )
    {
        if ( TLogLevel < gLogLevelFilter )
          return;
    }

    LogRecord _redords;
};

//class console_log
//{
//    template< typename... Args >
//    void log( std::string_view& fmt, Args&&... args )
//    {
//        ::printf( fmt.data(), std::forward< Args >( args )... );
//    }
//};


} // namespace Mala::Net