#include "../MalaMacro.h";

import Mala.Core.Types;
import Mala.Core.CoreTLS;
import Mala.Container.String;
import Mala.Log.ConsoleLog;
import Mala.Log;

using namespace Mala::Log;

namespace LogWriter
{

void _Write( String&& log )
{
    GLogQueue->Emplace( LThreadId, std::move( log ) );
}

void Flush()
{
    GLogQueue->ConsumeAll( []( const LogRecord& log )
    {
        CONSOLE_LOG( White, log._message.c_str()  );
    } );
}

};

