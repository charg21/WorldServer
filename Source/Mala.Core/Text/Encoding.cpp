import Mala.Text;
import Mala.Core.TypeTraits;
import Mala.Core.Types;
import Mala.Container;
import Mala.Container.String;
import Mala.Windows;

using namespace std;
using namespace Mala::Text;
using namespace Mala::Windows;

namespace Encoding::Unicode
{
String GetString( BYTE* utf8, i32 count, size_t stringByteLength )
{
    if ( stringByteLength == 0 )
        return {};
    String utf16;
    utf16.resize( stringByteLength / 2 );
    std::memcpy( utf16.data(), utf8 + count, stringByteLength );
    return std::move( utf16 );
}

u16 GetBytes( const String& str, BYTE* buffer, i32 count )
{
    if ( str.empty() == 0 )
        return 0;

    std::memcpy( buffer + count, str.data(), str.size() * 2 );

    return str.size() * 2;
}

}

namespace Mala::Text::Utf8
{
String ToUtf16( const char* utf8, size_t stringLength )
{
    if ( stringLength == 0 )
        return L"";

    int ret = ::MultiByteToWideChar( CP_UTF8_, 0, utf8, stringLength, nullptr, 0 );
    if ( 0 == ret )
        return L"";

    String utf16;
    utf16.resize( ret );

    ret = ::MultiByteToWideChar( CP_UTF8_, 0, utf8, stringLength, (wchar_t*)utf16.c_str(), ret );

    return std::move( utf16 );
}
String ToUtf16( const std::string& utf8 )
{
    return ToUtf16( utf8.c_str(), utf8.size() );
}

String ToUtf16( const char* utf8 )
{
    return ToUtf16( utf8, strlen( utf8 ) );
}
}


std::string Utf16::ToUtf8( const wchar_t* utf16, size_t wstringLength )
{
    if ( wstringLength == 0 )
        return "";

    int ret = ::WideCharToMultiByte( CP_UTF8_, 0, utf16, wstringLength, nullptr, 0, nullptr, nullptr );
    if ( 0 == ret )
        return "";

    std::string utf8;
    utf8.resize( ret );

    ::WideCharToMultiByte( CP_UTF8_, 0, utf16, wstringLength, (char*)utf8.c_str(), ret, nullptr, nullptr );

    return std::move( utf8 );
}

std::string Utf16::ToUtf8( StringRef utf16 )
{
    return ToUtf8( utf16.c_str(), utf16.size() );
}

std::string Utf16::ToUtf8( const wchar_t* utf16 )
{
    return ToUtf8( utf16, wcslen( utf16 ) );
}
