module;


export module Mala.Text;

#include "../CoreMacro.h"

import Mala.Core.TypeTraits;
import Mala.Core.Types;
import Mala.Container.String;

using namespace std;


export
{

enum class EEncoding
{
    Utf8,
    Utf16,

    Max
};

namespace Encoding::Unicode
{
extern MALA_CORE_API String GetString( BYTE* utf8, i32 count, size_t strLen );
extern u16 GetBytes( const String& str, BYTE* buffer, i32 count );
}

namespace Mala::Text::Utf8
{
extern String ToUtf16( const char* utf8, size_t stringLength );
extern String ToUtf16( const std::string& utf8 );
extern String ToUtf16( const char* utf8 );
}

namespace Mala::Text
{

class MALA_CORE_API Utf16
{
public:
    static std::string ToUtf8( const wchar_t* utf16, size_t wstringLength );
    static std::string ToUtf8( StringRef utf16 );
    static std::string ToUtf8( const wchar_t* utf16 );
};

}

} // namespace Mala::Text