#pragma once

#define GENERATE_CLASS_TYPE_INFO( TypeName ) \
private: \
	friend SuperClassTypeDeduction; \
	friend TypeInfoInitializer; \
\
public: \
	using Super = typename SuperClassTypeDeduction< TypeName >::Type; \
	using ThisType = TypeName; \
\
	static TypeInfo& StaticTypeInfo() \
	{ \
		static TypeInfo s_typeInfo{ TypeInfoInitializer< ThisType >( #TypeName, L## #TypeName ) }; \
		return s_typeInfo;\
	} \
\
	virtual const TypeInfo& GetTypeInfo() const \
	{ \
		return _typeInfo; \
	} \
\
private: \
	inline static TypeInfo& _typeInfo = StaticTypeInfo(); \
\
private: \
