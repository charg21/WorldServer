#pragma once

#define Field( Name ) \
inline static struct RegistFieldExecutor_##Name \
{ \
	RegistFieldExecutor_##Name() \
	{ \
		static FieldRegister< ThisType, decltype( Name ), decltype( &ThisType::Name ), &ThisType::Name > property_register_##Name{ #Name, L## #Name, ThisType::StaticTypeInfo() }; \
	} \
} regist_##Name;