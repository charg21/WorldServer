export module Mala.Reflection;

/// <summary>
/// ref https://github.com/xtozero/SSR/blob/dev/Source/Core/Public/Reflection
/// </summary>
export import Mala.Reflection.TypeInfo;
export import Mala.Reflection.Field;
export import Mala.Reflection.Method;

export
{

template< typename To, typename From >
To* Cast( From* source )
{
	if ( !source )
	{
		return nullptr;
	}

	auto typeInfo = source->GetTypeInfo();
	// ���� ���� ����
	// if ( !typeInfo.TypeInfo::IsChildOf< To >() )
	if ( !typeInfo.TypeInfo:: template IsChildOf< To >() )
	{
		return nullptr;
	}

	return reinterpret_cast< To* >( source );
}

} // namespace Mala::Reflection

/*

enum class EClassType
{
    Knight,
    Rogue,
};

class WorldObject
{
    GENERATE_CLASS_TYPE_INFO( WorldObject )

    Field( _owner )
    WorldObject* _owner;
};

class Creature : public WorldObject
{
    GENERATE_CLASS_TYPE_INFO( Creature )

public:
    Field( _id )
    int _id;

    Field( _stats )
    int _stats[ 100 ];

    Field( _buffs )
    std::array< int, 100 > _buffs;

    Field( _factory )
    inline static std::function< WorldObject() > _factory{};
};

class Knight : public Creature
{
    GENERATE_CLASS_TYPE_INFO( Knight )

public:
    Field( _clsssType )
    EClassType _clsssType{ EClassType::Knight };
};

int main()
{
    //WorldObject worldObject;
    //Creature creature;
    Knight knight;

    WorldObject* worldObject = &knight;

    if ( knight.GetTypeInfo().IsA< Knight >() )
    {
        std::cout << "Same" << std::endl;
    }

    if ( knight.GetTypeInfo().IsChildOf< WorldObject >() )
    {
        std::cout << "Kinght is Child Of WorldObject "<< std::endl;
    }

    if ( knight.GetTypeInfo().IsChildOf< Creature >() )
    {
        std::cout << std::string( knight.GetTypeInfo().GetName() ) << " is Child Of Creature" << std::endl;
    }

    if ( Cast< Knight >( worldObject ) )
    {
        std::cout << "Cast Success WorldObject TO Knight" << std::endl;
    }

    auto knightTypeInfo = knight.GetTypeInfo();
    if ( auto p = knightTypeInfo.GetField( "_id" ) )
    {
        {
            //int value = p->Get< int >();
            p->Set( &knight, 4 );
            //std::cout << "Knight id2 is " << value << std::endl;
        }

        knight._id = 2;

        {
            /*int value = p->Get< int >();
            std::cout << "Knight id2 is " << value << std::endl;
        }
    }

    if ( auto p = knightTypeInfo.GetField( "_buffs" ) )
    {
    }
}

*/