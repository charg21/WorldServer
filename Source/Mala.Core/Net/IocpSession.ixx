export module Mala.Net.IocpSession;

#include "../MalaMacro.h"

import <memory>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Net.EndPoint;
import Mala.Net.NetEvent;
import Mala.Net.NetObject;
import Mala.Net.RecvBuffer;
import Mala.Windows;
import Mala.Threading.Lock;

using namespace Mala::Container;
using namespace Mala::Threading;

export namespace Mala::Net
{
/// <summary>
/// IocpSession
/// </summary>
class IocpSession : public INetObject
{
    friend class IocpListener;
    friend class IocpCore;
    friend class IocpService;

    inline static constexpr size_t BUFFER_SIZE = 0x10000;

public:
    IocpSession();
    virtual ~IocpSession();

public:
    void Send( SendBufferRef sendBuffer );
    void Send( SendBufferPtr&& sendBuffer );

    bool Connect();
    void Disconnect( const WCHAR* reason );

    IocpServicePtr GetService() { return _service.lock(); }
    void SetService( IocpServiceRef service );

public:
    //Info Funtion
    void SetEndPoint( EndPoint address ) { _endPoint = address; }
    EndPoint GetEndPoint() { return _endPoint; }

    SOCKET GetSocket() { return _socket; }
    bool IsConnected() { return _connected; }
    IocpSessionPtr GetSessionPtr();

private:
    //interface
    HANDLE GetHandle() final;
    virtual void Dispatch( struct INetEvent* netEvent, i32 numOfBytes ) final;

private:
    //전송 관련 Protocol
    bool    RegisterConnect();
    bool    RegisterDisConnect();
    void    RegisterRecv();
    void    RegisterSend();

    void    ProcessConnect();
    void    ProcessDisconnect();
    void    ProcessRecv( i32 numOfBytes );
    void    ProcessSend( i32 numOfBytes );
    void    ProcessJob( class INetEvent* iocpJobEvent );

    void    HandleError( i32 errorCode );

protected:
    //Override Function To Contents
    virtual void OnConnected();
    virtual i32  OnRecv( BYTE* buffer, i32 len ) { return len; }
    virtual void OnSend( i32 len ) {}
    virtual void OnDisconnected() {}

private:
    IocpServiceWeakPtr _service;
    SOCKET _socket = INVALID_SOCK;
    EndPoint _endPoint{};
    Atomic< bool > _connected{ false };//접속 여부

private:
    USE_LOCK;

    RecvBuffer _recvBuffer;
    WaitFreeQueue< SendBufferPtr > _sendQueue;
    Atomic< bool > _sendRegistered{ false };

private:
    RecvEvent _recvEvent;
    SendEvent _sendEvent;
    ConnectEvent _connectEvent;
    DisconnectEvent _disConnectEvent;
};
}
