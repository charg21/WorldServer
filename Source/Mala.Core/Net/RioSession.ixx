module;

#include "../MalaMacro.h"

export module Mala.Net.RioSession;

import Mala.Core.Types;
import Mala.Container;
import Mala.Net.DisconnectReason;
import Mala.Net.NetEvent;
import Mala.Net.NetObject;
import Mala.Net.EndPoint;
import Mala.Net.RecvBuffer;
import Mala.Windows;
import Mala.Threading.Lock;

using namespace Mala::Container;

export namespace Mala::Net
{

/// <summary>
/// Rio 소켓모델 네트워크 세션
/// </summary>
class RioSession : public INetObject
{
    friend class RioListener;
    friend class RioCore;
    friend class RioService;
    friend class IocpService;

    inline static constexpr size_t BUFFER_SIZE{ 0x10000 };

public:
    RioSession();
    virtual ~RioSession();

public:
    void Send( SendBufferRef sendBuffer );
    void Send( SendBufferPtr&& sendBuffer );
    bool LazySend( SendBufferPtr&& sendBuffer );
    void FlushSendQueue();

    bool Connect();
    void Disconnect( const WCHAR* reason );

    RioServicePtr GetService() { return _service.lock(); }
    void SetService( RioServiceRef service );

public:
    void SetEndPoint( EndPoint address ) { _endPoint = address; }
    EndPoint GetEndPoint() { return _endPoint; }

    SOCKET GetSocket() { return _socket; }
    bool IsConnected() { return _connected; }
    RioSessionPtr GetSessionPtr();

private:
    HANDLE GetHandle() override;
    void Dispatch( struct INetEvent* rioEvent, i32 numOfBytes ) final;

private:
    bool RegisterConnect();
    bool RegisterDisConnect();
    void RegisterRecv();
    void RegisterSend();

    void ProcessConnect();
    void ProcessDisconnect();
    void ProcessRecv( i32 numOfBytes );
    void ProcessSend( i32 numOfBytes );

    void HandleError( i32 errorCode );

protected:
    virtual void OnConnected();
    virtual i32  OnRecv( BYTE* buffer, i32 len ) { return len; }
    virtual void OnRegisterSend(){};
    virtual void OnSend( i32 len ) {}
    virtual void OnDisconnected() {}
    void MergeSendBuffer();

private:
    RioServiceWeakPtr _service;
    SOCKET _socket = INVALID_SOCK;
    EndPoint _endPoint{};
    Atomic< bool > _connected{ false }; //접속 여부

private:
    USE_LOCK;

    RecvBuffer _recvBuffer;
    WaitFreeQueue< SendBufferPtr > _sendQueue;
    Atomic< bool > _sendRegistered{ false };

private:
    SendEvent _sendEvent;
    RecvEvent _recvEvent;
    ConnectEvent _connectEvent;
    DisconnectEvent _disConnectEvent;

protected:
    char* _rioBufferPointer;
    RIO_BUFFERID _rioBufferId;
};

} // namespace Mala::Net
