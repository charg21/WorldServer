export module Mala.Net.RioCore;

import <vector>;
import <string>;
import <memory>;

import Mala.Core;
import Mala.Container;
import Mala.Core.Types;
import Mala.Net.NetObject;
import Mala.Net.NetEvent;
import Mala.Windows;
import Mala.Threading.ThreadManager;

using namespace Mala::Threading;
using namespace Mala::Container;

export namespace Mala::Net
{

class Acceptor;
class Connector;
class SessionManager;

/// <summary>
/// Rio 코어
/// </summary>
class RioCore : public INetObject
{
public:
    RioCore();
    virtual ~RioCore();

    bool Register( INetObjectRef netObjectRef );
    bool Dispatch( u32 timeoutMs = TIMEOUT_INFINITE );
    void Dispatch( INetEvent* netEvent, int numOfBytes ) final;

    HANDLE GetHandle() final { return {}; }
    HANDLE GetHandle( int threadId ) { return _iocpHandles[ threadId ]; }
    const RIO_CQ& GetCompletionQueue( int threadId ) { return _completionQueues[ threadId ]; }
    i32 GetConcurrentThreadCount() { return _concurrentThreadCount; }

    void Initialize( i32 threadCount = 1 );

    std::shared_ptr< bool > Post( std::function< void() >&& task );

private:
    Vector< RIO_CQ > _completionQueues;
    Vector< HANDLE > _iocpHandles;
    i32 _concurrentThreadCount{};
    IocpJobExecutorPtr _iocpJobExecutor;
    INetEvent _netEvent{ EEventType::All };
};

} // namespace Mala::Net
