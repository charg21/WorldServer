#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#include <Windows.h>
import <WinSock2.h>;
#include <MSWSock.h>
//import <WS2tcpip.h>;

#pragma comment( lib, "ws2_32" )

import <iostream>;

#include "../MalaMacro.h"


import Mala.Core.CoreGlobal;
//import Mala.Core.ErrorReason;
import Mala.Core.Types;
import Mala.Container;
import Mala.Container.StaticVector;

import Mala.Memory;

import Mala.Net.Buffer;
import Mala.Net.EndPoint;
import Mala.Net.PacketDispatcher;
import Mala.Net.IocpService;
import Mala.Net.RioCore;
import Mala.Net.RioSession;
import Mala.Net.RioService;
import Mala.Net.NetEvent;
import Mala.Net.SendBuffer;
import Mala.Net.SocketHelper;
import Mala.Threading.Lock;
import Mala.Windows;

using namespace Mala::Net;

RioSession::RioSession()
:_recvBuffer{ BUFFER_SIZE, true }
{
	_socket = SocketHelper::MakeSocketRio();
	_rioBufferId = _recvBuffer.GetRioBufferId();
}

RioSession::~RioSession()
{
	SocketHelper::Close( _socket );
}

void RioSession::Send( SendBufferRef sendBuffer )
{
	if ( !IsConnected() )
		return;

	_sendQueue.push( sendBuffer );

	if ( bool registerSend{ !_sendRegistered.exchange( true ) } )
		RegisterSend();
}

void RioSession::Send( SendBufferPtr&& sendBuffer )
{
	if ( !IsConnected() )
		return;

	_sendQueue.push( std::move( sendBuffer ) );

	bool registerSend = !_sendRegistered.exchange( true );
	if ( registerSend )
		RegisterSend();
}

bool RioSession::LazySend( SendBufferPtr&& sendBuffer )
{
	return false;
}

void RioSession::FlushSendQueue()
{
}

bool RioSession::Connect()
{
	return RegisterConnect();
}

void RioSession::Disconnect( const WCHAR* reason )
{
	if ( !_connected.exchange( false ) )
		return;

	int error = ::GetLastError();
	std::wcout << error << L" Disconnet Reason : " << reason << std::endl;

	RegisterDisConnect();
}

void RioSession::SetService( RioServiceRef service )
{
	_service = service;
}

RioSessionPtr RioSession::GetSessionPtr()
{
	return std::static_pointer_cast< RioSession >( INetObject::shared_from_this() );
}

HANDLE RioSession::GetHandle()
{
	return reinterpret_cast< HANDLE >( _socket );
}

void RioSession::Dispatch( INetEvent* netEvent, i32 numOfBytes )
{
    switch ( netEvent->eventType )
    {
    case EEventType::Connect:
        ProcessConnect();
        break;
    case EEventType::DisConnect:
        ProcessDisconnect();
        break;
    case EEventType::Recv:
        ProcessRecv( numOfBytes );
        break;
    case EEventType::Send:
        ProcessSend( numOfBytes );
        break;
    default:
        CRASH( "Invalid Net Event Type" );
        break;
    }
}

bool RioSession::RegisterConnect()
{
	if ( IsConnected() )
		return false;

	//Client 서비스에서만 가능
	if ( GetService()->GetServiceType() != EServiceType::Client )
		return false;

	if ( !SocketHelper::SetReuseAddress( _socket, true ) )
		return false;

	if ( !SocketHelper::BindAnyAddress( _socket, 0 ) )
		return false;

	_connectEvent.Reset();
	_connectEvent.owner = shared_from_this();

	DWORD numOfBytes = 0;
	if ( !SocketHelper::ConnectEx( _socket,
		GetService()->GetEndPoint().AsSockaddr(),
		sizeof( SOCKADDR ),
		nullptr,
		0,
		&numOfBytes,
		&_connectEvent.ToOverlapped() ) )
	{
		i32 errorCode = ::WSAGetLastError();
		if ( errorCode != WSA_IO_PENDING )
		{
			_connectEvent.owner = nullptr;
			return false;
		}
	}

	return true;
}

bool RioSession::RegisterDisConnect()
{
	_disConnectEvent.Reset();
	_disConnectEvent.owner = shared_from_this();

	if ( !SocketHelper::DisconnectEx(
		_socket,
		&_disConnectEvent.ToOverlapped(),
		TF_REUSE_SOCKET,
		0 ) )
	{
		i32 errorCode = ::WSAGetLastError();
		if ( errorCode != WSA_IO_PENDING )
		{
			_disConnectEvent.owner = nullptr;
			return false;
		}
	}

	return true;
}

void RioSession::RegisterRecv()
{
	if ( !IsConnected() )
		return;

	_recvEvent.owner = shared_from_this();
	_recvEvent.rioBuf.BufferId = _rioBufferId;
	_recvEvent.rioBuf.Length = static_cast< ULONG >( _recvBuffer.FreeSize() );
	_recvEvent.rioBuf.Offset = _recvBuffer.WritePosOffset();

	DWORD flags{};

	{
		WRITE_LOCK;

		/// start async recv
		if ( !SocketHelper::RioFunctionTable.RIOReceive(
			_requestQueue,
			(PRIO_BUF)( &_recvEvent ),
			1,
			flags,
			&_recvEvent ) )
		{
			i32 errorCode = ::WSAGetLastError();
			if ( errorCode != WSA_IO_PENDING )
			{
				HandleError( errorCode );
				_recvEvent.owner = nullptr;
			}
		}
	}
}

void RioSession::MergeSendBuffer()
{
	/// RIO에서 Scatter/Gather IO 미지원, 기존 IOCP인터페이스 유지를 위해 하나의 버퍼로 병합
	SendBufferPtr sendBuffer;
	while ( _sendQueue.TryPeek( sendBuffer ) )
	{
		if ( _sendEvent.rioBuf.Length + sendBuffer->WriteSize() >= ( 65536 * 2 ) )
			break;

		std::memcpy( &_sendEvent.buffer[ _sendEvent.rioBuf.Length ],
			sendBuffer->Buffer(),
			sendBuffer->WriteSize() );
		_sendEvent.rioBuf.Length += sendBuffer->WriteSize();

		_sendQueue.TryDequeue( sendBuffer );
	}
}

void RioSession::RegisterSend()
{
	if ( !IsConnected() )
		return;

	_sendEvent.owner = shared_from_this();
	_sendEvent.rioBuf.Offset = 0;
	_sendEvent.rioBuf.Length = 0;

	{
		WRITE_LOCK;

		MergeSendBuffer();

		if ( _sendEvent.rioBuf.Length == 0 )
		{
			printf_s( "[DEBUG] Send Size 0: %d\n", GetLastError() );
			_sendEvent.owner = nullptr;
			_sendRegistered.store( false );
			// RegisterSend
			return;
		}

		DWORD flags{};
		if ( !SocketHelper::RioFunctionTable.RIOSend(
			_requestQueue,
			&_sendEvent.ToRioBuf(),
			1,
			flags,
			&_sendEvent ) )
		{
			i32 errorCode = ::WSAGetLastError();
			if ( errorCode != WSA_IO_PENDING )
			{
				HandleError( errorCode );

				printf_s( "[DEBUG] RIOSend error: %d\n", GetLastError() );

				_sendEvent.owner = nullptr;
			}
		}
	}
}

void RioSession::ProcessConnect()
{
	_connectEvent.owner = nullptr;

	_connected.store( true );

	GetService()->AddSession( GetSessionPtr() );

	//컨텐츠코드 오버로딩
	OnConnected();

	//수신 등록
	RegisterRecv();
}

void RioSession::ProcessDisconnect()
{
	OnDisconnected();

	_disConnectEvent.owner = nullptr;

	GetService()->ReleaseSession( GetSessionPtr() );
}

void RioSession::ProcessRecv( i32 numOfBytes )
{
	_recvEvent.owner = nullptr;
	if ( numOfBytes == 0 )
	{
		Disconnect( L"Rio Recv 0" );
		return;
	}

	if ( !_recvBuffer.OnWrite( numOfBytes ) )
	{
		Disconnect( L"OnWrite OverFlow1" );
		return;
	}

	i32 dataSize = _recvBuffer.DataSize();
	i32 processLen = OnRecv( _recvBuffer.ReadPos(), dataSize );
	if ( processLen < 0 || dataSize < processLen || !_recvBuffer.OnRead( processLen ) )
	{
		Disconnect( L"OnRead OverFlow2" );
		return;
	}

	_recvBuffer.Clean();

	RegisterRecv();
}

void RioSession::ProcessSend( i32 numOfBytes )
{
	_sendEvent.owner = nullptr;
	if ( numOfBytes == 0 )
	{
		Disconnect( L"Rio Send 0" );
		return;
	}

	WRITE_LOCK;
	if ( _sendQueue.UnsafeEmpty() )
	{
		_sendRegistered.store( false );
	}
	else
	{
		RegisterSend();
	}
}

void RioSession::HandleError( i32 errorCode )
{
	switch ( errorCode )
	{
	case WSAECONNRESET:
	case WSAECONNABORTED:
		Disconnect( L"Handle Error" );
		break;
	default:
		//TODO Log.
		std::wcout << L" Handle Error : " << errorCode << std::endl;
		break;
	}
}

void RioSession::OnConnected()
{
	printf_s( "[DEBUG] Client Connected: ThreadId = %d\n", _threadId );
}
