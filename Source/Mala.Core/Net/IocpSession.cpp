#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

import <WinSock2.h>;
//import <WS2tcpip.h>;
#include <MSWSock.h>

#pragma comment( lib, "ws2_32" )

#include "../MalaMacro.h"

import <memory>;
import <iostream>;
import Mala.Core.Types;
import Mala.Container;
import Mala.Net.NetEvent;
import Mala.Net.NetObject;
import Mala.Net.IocpSession;
import Mala.Net.IocpService;
import Mala.Net.SendBuffer;
import Mala.Net.SocketHelper;
import Mala.Threading.Lock;
import Mala.Windows;


using namespace Mala::Container;
using namespace Mala::Net;


IocpSession::IocpSession()
:_recvBuffer{ BUFFER_SIZE }
{
    _socket = SocketHelper::MakeSocket();
}

IocpSession::~IocpSession()
{
    SocketHelper::Close( _socket );
}

void IocpSession::Send( SendBufferRef sendBuffer )
{
    if ( !IsConnected() )
        return;

    _sendQueue.push( std::move( sendBuffer ) );

    bool registerSend = !_sendRegistered.exchange( true );
    if ( registerSend )
        RegisterSend();
}

void IocpSession::Send( SendBufferPtr&& sendBuffer )
{
    if ( !IsConnected() )
        return;

    _sendQueue.push( std::move( sendBuffer ) );

    bool registerSend = !_sendRegistered.exchange( true );
    if ( registerSend )
        RegisterSend();
}

bool IocpSession::Connect()
{
    return RegisterConnect();
}

void IocpSession::Disconnect( const WCHAR* reason )
{
    //한번만 호출한다.
    if ( !_connected.exchange( false ) )
        return;

    // wcout << L"Disconnect: " << reason << endl;

    RegisterDisConnect();
}

HANDLE IocpSession::GetHandle()
{
    return reinterpret_cast< HANDLE >( _socket );
}

IocpSessionPtr IocpSession::GetSessionPtr()
{
    return std::static_pointer_cast< IocpSession >( shared_from_this() );
}

void IocpSession::SetService( IocpServiceRef service )
{
    _service = service;
}

void IocpSession::Dispatch( INetEvent* iocpEvent, i32 numOfBytes )
{
    switch ( iocpEvent->eventType )
    {
    case EEventType::Connect:
        ProcessConnect();
        break;
    case EEventType::DisConnect:
        ProcessDisconnect();
        break;
    case EEventType::Recv:
        ProcessRecv( numOfBytes );
        break;
    case EEventType::Send:
        ProcessSend( numOfBytes );
        break;
    case EEventType::Job:
        ProcessJob( iocpEvent );
        break;
    default:
        break;
    }
}

bool IocpSession::RegisterConnect()
{
    if ( IsConnected() )
        return false;

    //Client 서비스에서만 가능
    if ( GetService()->GetServiceType() != EServiceType::Client )
        return false;

    if ( !SocketHelper::SetReuseAddress( _socket, true ) )
        return false;

    if ( !SocketHelper::BindAnyAddress( _socket, 0 ) )
        return false;

    _connectEvent.Reset();
    _connectEvent.owner = shared_from_this();

    DWORD numOfBytes = 0;
    if ( !SocketHelper::ConnectEx( _socket,
        GetService()->GetEndPoint().AsSockaddr(),
        sizeof( SOCKADDR ),
        nullptr,
        0,
        &numOfBytes,
        &_connectEvent.ToOverlapped() ) )
    {
        i32 errorCode = ::WSAGetLastError();
        if ( errorCode != WSA_IO_PENDING )
        {
            _connectEvent.owner = nullptr;
            return false;
        }
    }

    return true;
}

bool IocpSession::RegisterDisConnect()
{
    _disConnectEvent.Reset();
    _disConnectEvent.owner = shared_from_this();

    if ( !SocketHelper::DisconnectEx( _socket, &_disConnectEvent.ToOverlapped(), TF_REUSE_SOCKET, 0 ) )
    {
        i32 errorCode = ::WSAGetLastError();
        if ( errorCode != WSA_IO_PENDING )
        {
            _disConnectEvent.owner = nullptr;
            return false;
        }
    }

    return true;
}

void IocpSession::RegisterRecv()
{
    if ( !IsConnected() )
        return;

    _recvEvent.Reset();
    _recvEvent.owner = shared_from_this();

    WSABUF wsaBuf;
    wsaBuf.buf = reinterpret_cast< char* >( _recvBuffer.WritePos() );
    wsaBuf.len = _recvBuffer.FreeSize();   //최대 버퍼보다 크게 잡는다.

    DWORD numOfBytes{};
    DWORD flags{};

    if ( SOCKET_ERROR == ::WSARecv( _socket,
        &wsaBuf,
        1,
        OUT &numOfBytes,
        OUT &flags,
        &_recvEvent.ToOverlapped(),
        nullptr ) )
    {
        i32 errorCode = ::WSAGetLastError();
        if ( errorCode != WSA_IO_PENDING )
        {
            HandleError( errorCode );
            _recvEvent.owner = nullptr; //레퍼런스 카운트 1 감소
        }
    }
}

void IocpSession::RegisterSend()
{
    if ( !IsConnected() )
        return;

    _sendEvent.Reset();
    _sendEvent.owner = shared_from_this();

    {
        WRITE_LOCK;

        i32 writeSize = 0;

        SendBufferPtr sendBuffer;
        while ( _sendQueue.TryDequeue( sendBuffer ) )
        {
            _sendEvent.sendBuffers.push_back( sendBuffer );

            writeSize += sendBuffer->WriteSize();
            //TODO : 예외 체크
        }
    }

    //Scatter-Gather
    static thread_local Vector< WSABUF > wsaBufs( 128 );
    wsaBufs.clear();

    //wsaBufs.reserve( _sendEvent.sendBuffers.size() );
    for ( SendBufferRef sendBuffer : _sendEvent.sendBuffers )
    {
        WSABUF wsaBuf;
        wsaBuf.buf = reinterpret_cast< char* >( sendBuffer->Buffer() );
        wsaBuf.len = static_cast< LONG >( sendBuffer->WriteSize() );
        wsaBufs.push_back( wsaBuf );
    }

    DWORD numofBytes = 0;
    if ( SOCKET_ERROR == ::WSASend( _socket, wsaBufs.data(), static_cast< DWORD >( wsaBufs.size() ), OUT & numofBytes, 0, &_sendEvent.ToOverlapped(), nullptr) )
    {
        i32 errorCode = ::WSAGetLastError();
        if ( errorCode != WSA_IO_PENDING )
        {
            HandleError( errorCode );
            _sendEvent.owner = nullptr;
            _sendEvent.sendBuffers.clear();
            _sendRegistered.store( false );
        }
    }
}
//
void IocpSession::ProcessConnect()
{
    _connectEvent.owner = nullptr; //release ref

    _connected.store( true );

    //IocpSession 등록
    GetService()->AddSession( GetSessionPtr() );

    //컨텐츠코드 오버로딩
    OnConnected();

    //수신 등록
    RegisterRecv();
}

void IocpSession::ProcessDisconnect()
{
    //할게 없다.
    _disConnectEvent.owner = nullptr; //release ref Owner;

    //Contents 단 Override
    OnDisconnected();
    GetService()->ReleaseSession( GetSessionPtr() );
}


void IocpSession::ProcessRecv( i32 numOfBytes )
{
    _recvEvent.owner = nullptr; //Release Ref Owner 에 대한 생명 주기 관리를 위해서 nullptr 로 날려준다.
    if ( numOfBytes == 0 )
    {
        Disconnect( L"Recv 0" );
        return;
    }

    //Write Pos을 numOfBytes만큼 땡긴다
    if ( !_recvBuffer.OnWrite( numOfBytes ) )
    {
        Disconnect( L"OnWrite OverFlow" );
        return;
    }

    i32 dataSize = _recvBuffer.DataSize();
    i32 processLen = OnRecv( _recvBuffer.ReadPos(), numOfBytes );
    if ( processLen < 0 || dataSize < processLen || !_recvBuffer.OnRead( processLen ) )
    {
        Disconnect( L"OnRead OverFlow" );
        return;
    }

    //커서 정리. 초기화 여부 확인
    _recvBuffer.Clean();

    //수신 등록
    RegisterRecv();
}
//
void IocpSession::ProcessSend( i32 numOfBytes )
{
    _sendEvent.owner = nullptr; //release event
    _sendEvent.sendBuffers.clear(); //SendBuffer를 모두 Clear 한다. 모두 사용 완료했기 때문에.

    if ( numOfBytes == 0 )
    {
        Disconnect( L"Send 0" );
        return;
    }

    OnSend( numOfBytes );

    WRITE_LOCK;
    if ( _sendQueue.UnsafeEmpty() )
    {
        _sendRegistered.store( false );
    }
    else
    {
        RegisterSend();
    }
}

void IocpSession::ProcessJob( INetEvent* jobEvent )
{
    /// 레퍼 카운트 증감도 해야함
	static_cast< JobEvent* >( jobEvent )->job();
}

void IocpSession::HandleError( i32 errorCode )
{
    switch ( errorCode )
    {
    case WSAECONNRESET:
    case WSAECONNABORTED:
        Disconnect( L"Handle Error" );
        break;
    default:
        //TODO Log.
        //wcout << L" Handle Error : " << errorCode << endl;
        break;
    }
}

void IocpSession::OnConnected()
{
}
