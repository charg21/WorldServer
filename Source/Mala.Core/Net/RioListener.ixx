module;

#pragma once

export module Mala.Net.RioListener;

import <memory>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Net.IocpCore;
import Mala.Net.NetObject;
import Mala.Net.RioCore;
import Mala.Net.EndPoint;
import Mala.Net.NetObject;
import Mala.Windows;

using namespace Mala::Container;

export namespace Mala::Net
{

class AcceptEvent;
class ServerService;

/// <summary>
/// Listenr
/// </summary>
class RioListener final : public INetObject
{
public:
    RioListener();
    virtual ~RioListener() final;

public:
    bool StartAccept( RioServerServiceRef service );
    void CloseSocket();

public:
    HANDLE GetHandle() final;
    void Dispatch( struct INetEvent* iocpEvent, i32 numofBytes ) final;

private:
    void RegisterAccept( AcceptEvent* acceptEvent );
    void ProcessAccept( AcceptEvent* acceptEvent );

protected:
    SOCKET _socket;
    Vector< AcceptEvent* > _acceptEvents;
    RioServerServicePtr _service;
};


/// <summary>
/// 작업 실행기
/// </summary>
class IocpJobExecutor final : public INetObject
{
    HANDLE GetHandle() final;
    void Dispatch( struct INetEvent* iocpEvent, i32 numofBytes ) final;
};

}
