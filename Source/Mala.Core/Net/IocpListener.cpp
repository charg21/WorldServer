#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#include "../MalaMacro.h"
import <WinSock2.h>;

#pragma comment( lib, "ws2_32" )

import <memory>;

import Mala.Core.Types;

import Mala.Net.IocpCore;
import Mala.Net.IocpListener;
import Mala.Net.IocpSession;
import Mala.Net.IocpService;
import Mala.Net.EndPoint;
import Mala.Net.NetEvent;
import Mala.Net.NetObject;
import Mala.Net.SocketHelper;
import Mala.Memory;

using namespace Mala::Net;

IocpListener::IocpListener()
: _socket{ INVALID_SOCKET }
{
}

IocpListener::~IocpListener()
{
    SocketHelper::Close( _socket );

    for ( AcceptEvent* acceptEvent : _acceptEvents )
    {
        xdelete( acceptEvent );
    }
}

bool IocpListener::StartAccept( IocpServerServiceRef service )
{
    _service = service;
    if ( !_service )
        return false;

    _socket = SocketHelper::MakeSocket();
    if ( _socket == INVALID_SOCKET )
        return false;

    //iocp 등록 <-Listen 소켓 iocp 등록
    if ( !_service->GetCoreRef()->Register( shared_from_this() ) )
        return false;

    if ( !SocketHelper::SetReuseAddress( _socket, true ) )
        return false;

    if ( !SocketHelper::SetLinger( _socket, 0, 0 ) )
        return false;

    if ( !SocketHelper::Bind( _socket, _service->GetEndPoint() ) )
        return false;

    if ( !SocketHelper::Listen( _socket ) )
        return false;

    //accept 등록
    const i32 acceptCount = _service->GetMaxSessionCount();
    _acceptEvents.reserve( acceptCount );
    for ( i32 i = 0; i < acceptCount; i++ )
    {
        auto* acceptEvent{ _acceptEvents.emplace_back( xnew< AcceptEvent >() ) };
        acceptEvent->owner = shared_from_this();
        RegisterAccept( acceptEvent );
    }

    return true;
}

void IocpListener::CloseSocket()
{
    Mala::Net::SocketHelper::Close( _socket );
}

HANDLE IocpListener::GetHandle()
{
    return reinterpret_cast< HANDLE >( _socket );
}

void IocpListener::Dispatch( INetEvent* netEvent, i32 numofBytes )
{
    ASSERT_CRASH( netEvent->eventType == EEventType::Accept );
    auto* acceptEvent = static_cast< AcceptEvent* >( netEvent );
    ProcessAccept( acceptEvent );
}

//iocp에서 처리할 수 있도록 accept를 등록한다
void IocpListener::RegisterAccept( AcceptEvent* acceptEvent )
{
    IocpSessionPtr session = _service->CreateSession();

    acceptEvent->Reset();
    acceptEvent->iocpSession = session;

    DWORD bytesReceived = 0;

    if ( !SocketHelper::AcceptEx(
        _socket,
        session->GetSocket(),
        session->_recvBuffer.WritePos(),
        0,
        sizeof( SOCKADDR_IN ) + 16,
        sizeof( SOCKADDR_IN ) + 16,
        OUT & bytesReceived,
        reinterpret_cast< LPOVERLAPPED >( &acceptEvent->ToOverlapped() ) ) )
    {
        const i32 errorCode = ::WSAGetLastError();
        if ( errorCode != WSA_IO_PENDING )
        {
            RegisterAccept( acceptEvent );
        }
    }
}

void IocpListener::ProcessAccept( AcceptEvent* acceptEvent )
{
    IocpSessionPtr session = std::static_pointer_cast< IocpSession >( acceptEvent->owner );
    if ( !SocketHelper::SetUpdateAcceptSocket( session->GetSocket(), _socket ) )
    {
        RegisterAccept( acceptEvent );
        return;
    }

    SOCKADDR_IN sockAddress;
    i32 sizeofSockAddr = sizeof( sockAddress );
    if ( SOCKET_ERROR == ::getpeername( session->GetSocket(),
        OUT reinterpret_cast< SOCKADDR* >( &sockAddress ),
        &sizeofSockAddr ) )
    {
        RegisterAccept( acceptEvent );
        return;
    }

    session->SetEndPoint( EndPoint( sockAddress ) );
    session->ProcessConnect();

    RegisterAccept( acceptEvent );
}
