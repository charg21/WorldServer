export module Mala.Net.NetEvent;

import Mala.Core.Types;
import Mala.Container;
import Mala.Windows;

using namespace Mala::Container;

export namespace Mala::Net
{

class RioSession;
enum class EEventType : unsigned char
{
    Connect    = 1 << 0,
    DisConnect = 1 << 1,
    Accept     = 1 << 2,
    Recv       = 1 << 3,
    Send       = 1 << 4,
    Job        = 1 << 5,

    /// 로직 체크 단순화 용
    SendRecv            = Send    | Recv,
    ConnectOrDisConnect = Connect | DisConnect,

    All = Connect | DisConnect | Accept | Recv | Send,
};

/// <summary>
/// 네트워크 이벤트
/// </summary>
struct INetEvent
{
    union
    {
        OVERLAPPED overlapped; // 32 bytes
        RIO_BUF    rioBuf;     // 16 bytes
    };

    const EEventType eventType;
    INetObjectPtr owner;

    INetEvent( EEventType type )
    : overlapped{}
    , eventType{ type }
    {
    }

    void Reset();
    OVERLAPPED& ToOverlapped() { return overlapped; }
    RIO_BUF& ToRioBuf() { return rioBuf; }
};

/// <summary>
/// Recv Event
/// </summary>
struct RecvEvent : public INetEvent
{
    RecvEvent();
};

/// <summary>
/// Send Event
/// </summary>
struct SendEvent : public INetEvent
{
    constexpr static int BUFFER_CAPACITY{ 65536 * 2 };

    SendEvent();
    ~SendEvent();

    char* buffer{};
    Vector< SendBufferPtr > sendBuffers;
};

/// <summary>
/// Connect Event
/// </summary>
struct ConnectEvent : public INetEvent
{
public:
    ConnectEvent()
    : INetEvent{ EEventType::Connect }
    {
    }
};

/// <summary>
/// Disconnect Event
/// </summary>
struct DisconnectEvent : public INetEvent
{
public:
    DisconnectEvent()
    : INetEvent{ EEventType::DisConnect }
    {
    }
};

/// <summary>
/// Accept Event
/// </summary>
struct AcceptEvent : public INetEvent
{
public:
    AcceptEvent()
    : INetEvent{ EEventType::Accept }
    {
    }

public:
    IocpSessionPtr iocpSession;
    RioSessionPtr rioSession;
};

/// <summary>
/// Job Event
/// </summary>
struct JobEvent : public INetEvent
{
public:
    JobEvent()
    : INetEvent( EEventType::Job )
    {
    }

    JobEvent( Task<>&& job )
    : INetEvent( EEventType::Job )
    , job{ std::move( job ) }
    {
    }

    Task<> job;
};

}
