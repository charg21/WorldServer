#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#include "../MalaMacro.h"
import <WinSock2.h>;

#pragma comment( lib, "ws2_32" )

import <memory>;

import Mala.Core.Types;

import Mala.Net.IocpCore;
import Mala.Net.RioCore;
import Mala.Net.RioService;
import Mala.Net.RioListener;
import Mala.Net.RioSession;
import Mala.Net.EndPoint;
import Mala.Net.NetEvent;
import Mala.Net.NetObject;
import Mala.Net.SocketHelper;
import Mala.Memory;

using namespace Mala::Net;


RioListener::RioListener()
: _socket{ INVALID_SOCKET }
{
}

RioListener::~RioListener()
{
    SocketHelper::Close( _socket );

    for ( AcceptEvent* acceptEvent : _acceptEvents )
    {
        xdelete( acceptEvent );
    }
}

bool RioListener::StartAccept( RioServerServiceRef service )
{
    _service = service;
    if ( !_service )
        return false;

    _socket = SocketHelper::MakeSocketRio();
    if ( _socket == INVALID_SOCKET )
        return false;

    //iocp 등록 <-Listen 소켓 iocp 등록
    if ( !_service->GetCoreRef()->Register( shared_from_this() ) )
        return false;

    if ( !SocketHelper::SetReuseAddress( _socket, true ) )
        return false;

    if ( !SocketHelper::SetLinger( _socket, 0, 0 ) )
        return false;

    if ( !SocketHelper::Bind( _socket, _service->GetEndPoint() ) )
        return false;

    if ( !SocketHelper::Listen( _socket ) )
        return false;

    //accept 등록
    const i32 acceptCount = _service->GetMaxSessionCount();
    _acceptEvents.reserve( acceptCount );
    for ( i32 i = 0; i < acceptCount; i++ )
    {
        auto* acceptEvent{ _acceptEvents.emplace_back( xnew< AcceptEvent >() ) };
        acceptEvent->owner = shared_from_this();
        RegisterAccept( acceptEvent );
    }

    return true;
}

void RioListener::CloseSocket()
{
    Mala::Net::SocketHelper::Close( _socket );
}

HANDLE RioListener::GetHandle()
{
    return reinterpret_cast< HANDLE >( _socket );
}

void RioListener::Dispatch( INetEvent* netEvent, i32 numofBytes )
{
    ASSERT_CRASH( netEvent->eventType == EEventType::Accept );
    auto* acceptEvent = static_cast< AcceptEvent* >( netEvent );
    ProcessAccept( acceptEvent );
}

void RioListener::RegisterAccept( AcceptEvent* acceptEvent )
{
    RioSessionPtr session = _service->CreateSession();

    acceptEvent->Reset();
    acceptEvent->rioSession = session;

    DWORD bytesReceived = 0;

    if ( !SocketHelper::AcceptEx(
        _socket,
        session->GetSocket(),
        session->_recvBuffer.WritePos(),
        0,
        sizeof( SOCKADDR_IN ) + 16,
        sizeof( SOCKADDR_IN ) + 16,
        OUT & bytesReceived,
        static_cast< LPOVERLAPPED >( &acceptEvent->ToOverlapped() ) ) )
    {
        const i32 errorCode = ::WSAGetLastError();
        if ( errorCode != WSA_IO_PENDING ) // Pending 아닐경우.
        {
            //다시 accpet 건다.
            RegisterAccept( acceptEvent );
        }
    }
}

void RioListener::ProcessAccept( AcceptEvent* acceptEvent )
{
    RioSessionPtr session = acceptEvent->rioSession;

    //_threadId = rand() % RIOManager::ConcurrentThreadCount;
    if ( !SocketHelper::SetUpdateAcceptSocket( session->GetSocket(), _socket ) )
    {
        RegisterAccept( acceptEvent );
        return;
    }

    SOCKADDR_IN sockAddress;
    i32 sizeofSockAddr = sizeof( sockAddress );
    if ( SOCKET_ERROR == ::getpeername( session->GetSocket(),
        OUT reinterpret_cast< SOCKADDR* >( &sockAddress ),
        &sizeofSockAddr ) )
    {
        RegisterAccept( acceptEvent );
        return;
    }

    session->SetEndPoint( EndPoint( sockAddress ) );
    session->ProcessConnect();

    RegisterAccept( acceptEvent );
}

HANDLE IocpJobExecutor::GetHandle()
{
    return {};
}

void IocpJobExecutor::Dispatch( INetEvent* netEvent, i32 numofBytes )
{
    ASSERT_CRASH( netEvent->eventType == EEventType::Job );

    auto* jobEvent = static_cast< JobEvent* >( netEvent );
    jobEvent->job();
    xdelete( jobEvent );
}
