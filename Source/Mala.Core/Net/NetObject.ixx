export module Mala.Net.NetObject;

import Mala.Core.Types;
import Mala.Container;
import Mala.Windows;

export namespace Mala::Net
{

/// <summary>
/// 네트워크 오브젝트 인터페이스
/// </summary>
class INetObject : public std::enable_shared_from_this< INetObject >
{
public:
    virtual HANDLE GetHandle() abstract;
    virtual void Dispatch( struct INetEvent* netEvent, int numofBytes ) abstract;
    int GetThreadId() { return _threadId; }
    RIO_RQ GetRequestQueue() { return _requestQueue; }
    virtual bool IsParkingObject(){ return false; }

public:
    /// <summary> 통지 받을 CompletionQueue의 스레드 식별자 </summary>
    int _threadId{};
    RIO_RQ _requestQueue{};
};

}
