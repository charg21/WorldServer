export module Mala.Net.HttpSession;

#pragma once

#include "../MalaMacro.h"

import <functional>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;
import Mala.Net;
import Mala.Http.HttpTypes;

using namespace Mala::Container;
using namespace Mala::Http;

export namespace Mala::Net
{

class HttpSession : public NetSession
{
    GENERATE_CLASS_TYPE_INFO( HttpSession );

public:
    /// <summary>
    /// 생성자
    /// </summary>
    HttpSession() = default;

    /// <summary>
    /// 소멸자
    /// </summary>
    ~HttpSession() override = default;

    HttpSessionPtr GetHttpSession() { return std::static_pointer_cast< HttpSession >( shared_from_this() ); }

protected:
    i32 OnRecv( BYTE* buffer, i32 len ) final;
    void OnSend( i32 len ) final;
    virtual void OnRecvRequest( const std::string_view& request );

    //private:
        //std::unique_ptr< IHttpController > _controller;
};


}
