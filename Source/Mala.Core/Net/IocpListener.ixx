export module Mala.Net.IocpListener;

import <memory>;

import Mala.Core.Types;

import Mala.Container;
import Mala.Net.IocpCore;
import Mala.Net.NetObject;
import Mala.Net.EndPoint;

using namespace Mala::Container;

export namespace Mala::Net
{

class AcceptEvent;
class ServerService;

/// <summary>
/// Listenr
/// </summary>
class IocpListener : public INetObject
{
public:
    IocpListener();
    ~IocpListener();

public:
    bool StartAccept( IocpServerServiceRef service );
    void CloseSocket();

public:
    HANDLE GetHandle() final;
    virtual void Dispatch( class INetEvent* netEvnet, i32 numofBytes ) final;

private:
    void RegisterAccept( AcceptEvent* acceptEvent );
    void ProcessAccept( AcceptEvent* acceptEvent );

protected:
    SOCKET _socket;
    Vector< AcceptEvent* > _acceptEvents;
    IocpServerServicePtr _service;
};

}
