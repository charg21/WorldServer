module;

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

import <WinSock2.h>;

#pragma comment( lib, "ws2_32" )

export module Mala.Net.EndPoint;

import Mala.Core.Types;
import Mala.Container.String;
import Mala.Windows;

export namespace Mala::Net
{

class EndPoint
{
public:
    EndPoint() = default;
    EndPoint( SOCKADDR_IN sockAddr );
    EndPoint( const std::string& address, uint16_t port );
    EndPoint( const String& address, uint16_t port );

    const sockaddr* AsSockaddr() const;
    sockaddr* AsSockaddr();
    sockaddr_in* AsSockaddrIn();
    const sockaddr_in* AsSockaddrIn() const;

    const uint16_t GetPort() const;
    std::wstring GetIp();
    std::string get_ip();

    constexpr size_t Size() const
    {
        return sizeof( sockaddr );
    }

    String ToString();

private:
    union
    {
        sockaddr    _sockaddr;
        sockaddr_in _sockaddr_in;
    };
};

} // namespace Mala::Net

