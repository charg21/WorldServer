#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

import <WinSock2.h>;
//import <WS2tcpip.h>;
#include "../MalaMacro.h"

#pragma comment( lib, "ws2_32" )

import <memory>;

import Mala.Core.Types;
import Mala.Net.IocpCore;
import Mala.Net.NetObject;
import Mala.Net.NetEvent;
import Mala.Windows;

using namespace Mala::Net;


IocpCore::IocpCore()
{
	_iocpHandle = ::CreateIoCompletionPort( INVALID_HANDLE_VALUE, 0, 0, 0 );
	ASSERT_CRASH( _iocpHandle != INVALID_HANDLE_VALUE );
}

IocpCore::~IocpCore()
{
	::CloseHandle( _iocpHandle );
}

bool IocpCore::Register( INetObjectRef netObject )
{
	return ::CreateIoCompletionPort( netObject->GetHandle(), _iocpHandle, /*key*/0, 0 );
}

bool IocpCore::Dispatch( u32 timeoutMs )
{
	DWORD       numofBytes{};
	ULONG_PTR   key{};
	INetObject* netObject{};
	INetEvent*  netEvent{};

	if ( ::GetQueuedCompletionStatus(
		_iocpHandle,
		OUT & numofBytes,
		OUT reinterpret_cast< PULONG_PTR >( &key ),
		OUT reinterpret_cast< LPOVERLAPPED* >( &netEvent ),
		timeoutMs ) )
	{
		INetObjectPtr object = netEvent->owner;
        object->Dispatch( netEvent, numofBytes );
	}
	else
	{
		i32 errorCode = ::WSAGetLastError();
		switch ( errorCode )
		{
		case WAIT_TIMEOUT:
			//타임 아웃은 에러가 아님,
			return false;
		default:
			//TODO : 로그 찍기.
			INetObjectPtr object = netEvent->owner;
			object->Dispatch( netEvent, numofBytes );
			break;
		}
	}

	return true;
}

void IocpCore::Initialize( i32 threadCount )
{
}

