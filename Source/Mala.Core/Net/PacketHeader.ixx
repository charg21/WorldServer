export module Mala.Net.PacketHeader;

import Mala.Core.CoreGlobal;
import Mala.Core.Types;

class StreamBuffer;

export namespace Mala::Net
{
#pragma pack( push ,1 )

struct PacketHeader
{
    unsigned short _length; /// 길이
    unsigned short _type;   /// 패킷의 타입
};

#pragma pack (pop)
}

