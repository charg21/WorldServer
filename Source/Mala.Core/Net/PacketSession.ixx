export module Mala.Net.PacketSession;

#pragma once

import Mala.Core.Types;
import Mala.Net;

export namespace Mala::Net
{

class PacketSession : public NetSession
{
public:
    PacketSession() = default;
    virtual ~PacketSession() override = default;

    PacketSessionPtr GetPacketSession() { return std::static_pointer_cast< PacketSession >( shared_from_this() ); }

protected:
    i32 OnRecv( BYTE* buffer, i32 len ) final;
    virtual void OnRecvPacket( BYTE* buffer, i32 len ) abstract;
};

}
