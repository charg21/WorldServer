export module Mala.Net.SendBuffer;

#include "../MalaMacro.h"

import <memory>;

import Mala.Core.Concepts;
import Mala.Core.Types;
import Mala.Container;
import Mala.Threading.Lock;
import Mala.Windows;

using namespace Mala::Container;
using namespace Mala::Threading;

export namespace Mala::Net
{

class SendBufferChunk;


/// <summary>
/// SendBuffer의 View 객체
/// 레퍼런스 카운트에 영향을 주지 않는 스택선에서 사용할 단순 객체( 절대 캡처등 해서 다른 스레드에서 사용하면 안된다. )
/// </summary>
template< typename T >
struct Span
{
    T* _buffer;
    u32 _offset;
    u32 _size;

    Span Slice( u32 start )
    {
        return { _buffer, _offset + start, _size };
    }

    Span Slice( u32 start, u32 offset )
    {
        return { _buffer, _offset + start, _size };
    }
};

class SendBuffer
{
public:
    /// <summary>
    /// 생성자
    /// </summary>
    SendBuffer( SendBufferChunkRef owner, BYTE* buffer, u32 allocSize );
    SendBuffer( SendBufferChunkPtr&& owner, BYTE* buffer, u32 allocSize );

    /// <summary>
    /// 소멸자
    /// </summary>
    ~SendBuffer() = default;

    /// <summary>
    /// 버퍼를 반환한다.
    /// </summary>
    /// <returns></returns>
    BYTE* Buffer() { return _buffer; }

    /// <summary>
    ///
    /// </summary>
    u32 AllocSize() { return _allocSize; }

    /// <summary>
    ///
    /// </summary>
    i32 WriteSize() { return _writeSize; }

    /// <summary>
    ///
    /// </summary>
    void Close( u32 writeSize );

    /// <summary>
    ///
    /// </summary>
    RIO_BUFFERID BufferId();
    i64 BufferOffset();

    /// <summary>
    ///
    /// </summary>
    bool Merge( SendBufferRef sendBuffer );

    /// <summary>
    /// Span을 반환한다.
    /// </summary>
    Span< BYTE > AsSpan()
    {
        return { _buffer, 0, _writeSize };
    }

    /// <summary>
    /// Span을 반환한다.
    /// </summary>
    Span< BYTE > AsSpan( u32 offset, u32 size )
    {
        return { _buffer, offset, size };
    }

    /// <summary>
    ///
    /// </summary>
    u32 _writeSize{};

private:
    /// <summary>
    /// 버퍼
    /// </summary>
    BYTE* _buffer{};

    /// <summary>
    /// 할당한 크기
    /// </summary>
    u32 _allocSize{};

    // <summary>
    // 청크
    //</summary>
    SendBufferChunkPtr _owner;
};

class SendBufferChunk : public std::enable_shared_from_this< SendBufferChunk >
{
public:
    constexpr inline static size_t SEND_BUFFER_CHUNK_SIZE = 64 * 1024 * 10;

public:
    friend class SendBufferManager;
    friend class SendBuffer;

public:
    SendBufferChunk() = default;
    ~SendBufferChunk();

    /// <summary>
    /// 초기 상태로 되돌린다.
    /// </summary>
    void Reset();

    /// <summary>
    /// 버퍼를 오픈한다.
    /// </summary>
    SendBufferPtr Open( u32 allocSize );

    /// <summary>
    /// 버퍼를 닫는다.
    /// </summary>
    void Close( u32 writeSize );

    /// <summary>
    ///
    /// </summary>
    bool IsOpen() { return _open; }
    BYTE* Buffer() { return &_buffer[ _usedSize ]; }
    u32 FreeSize() { return static_cast<u32>( _buffer.size() ) - _usedSize; }
    RIO_BUFFERID BufferId() { return _bufferId; }

private:
    /// <summary>
    /// 버퍼 메모리
    /// </summary>
    Array< BYTE, SEND_BUFFER_CHUNK_SIZE > _buffer{};

    /// <summary>
    /// 오픈 여부
    /// </summary>
    bool _open = false;

    /// <summary>
    /// 사용된 크기
    /// </summary>
    u32 _usedSize = 0;

    /// <summary>
    ///
    /// </summary>
    RIO_BUFFERID _bufferId{};
};

/// <summary>
/// 송신 버퍼 관리자
/// </summary>
class SendBufferManager
{
public:
    SendBufferPtr Open( u32 size );

private:
    /// <summary>
    /// 버퍼를 풀에서 꺼낸다.
    /// </summary>
    SendBufferChunkPtr Pop();

    void Push( SendBufferChunkRef buffer );

    void Push( SendBufferChunkPtr&& buffer );

    static void PushGlobal( SendBufferChunk* buffer );

private:
    USE_LOCK;

    Vector< SendBufferChunkPtr > _sendBufferChunks;
};


/// <summary>
/// 기본 데이터 형식을 바이트 배열로 변환하고 바이트 배열을 기본 데이터 형식으로 변환합니다.
/// </summary>
struct BitConverter
{
    static bool TryWriteBytes( SendBufferRef buffer, i32 offset, bool value )
    {
        if ( ( buffer->WriteSize() + offset + sizeof( bool ) ) > buffer->AllocSize() )
            return false;

        std::memcpy( &buffer->Buffer()[ offset ], &value, sizeof( bool ) );

        return true;
    }

    static bool TryWriteBytes( SendBufferRef buffer, i32 offset, u16 value )
    {
        if ( ( buffer->WriteSize() + offset + sizeof( u16 ) ) > buffer->AllocSize() )
            return false;

        std::memcpy( &buffer->Buffer()[ offset ], &value, sizeof( u16 ) );

        return true;
    }

    static bool TryWriteBytes( SendBufferRef buffer, i32 offset, u32 value )
    {
        if ( ( buffer->WriteSize() + offset + sizeof( u32 ) ) > buffer->AllocSize() )
            return false;

        std::memcpy( &buffer->Buffer()[ offset ], &value, sizeof( u32 ) );

        return true;
    }

    static bool TryWriteBytes( SendBufferRef buffer, i32 offset, u64 value )
    {
        if ( ( buffer->WriteSize() + offset + sizeof( u64 ) ) > buffer->AllocSize() )
            return false;

        std::memcpy( &buffer->Buffer()[ offset ], &value, sizeof( u64 ) );

        return true;
    }

    static bool TryWriteBytes( SendBufferRef buffer, i32 offset, i16 value )
    {
        if ( ( buffer->WriteSize() + offset + sizeof( i16 ) ) > buffer->AllocSize() )
            return false;

        std::memcpy( &buffer->Buffer()[ offset ], &value, sizeof( i16 ) );

        return true;
    }


    static bool TryWriteBytes( SendBufferRef buffer, i32 offset, long value )
    {
        if ( ( buffer->WriteSize() + offset + sizeof( long ) ) > buffer->AllocSize() )
            return false;

        std::memcpy( &buffer->Buffer()[ offset ], &value, sizeof( long ) );

        return true;
    }


    static bool TryWriteBytes( SendBufferRef buffer, i32 offset, i32 value )
    {
        if ( ( buffer->WriteSize() + offset + sizeof( i32 ) ) > buffer->AllocSize() )
            return false;

        std::memcpy( &buffer->Buffer()[ offset ], &value, sizeof( i32 ) );

        return true;
    }

    static bool TryWriteBytes( SendBufferRef buffer, i32 offset, i64 value )
    {
        if ( ( buffer->WriteSize() + offset + sizeof( i64 ) ) > buffer->AllocSize() )
            return false;

        std::memcpy( &buffer->Buffer()[ offset ], &value, sizeof( i64 ) );

        return true;
    }

    static bool TryWriteBytes( SendBufferRef buffer, i32 offset, f32 value )
    {
        if ( ( buffer->WriteSize() + offset + sizeof( f32 ) ) > buffer->AllocSize() )
            return false;

        std::memcpy( &buffer->Buffer()[ offset ], &value, sizeof( f32 ) );

        return true;
    }

    static bool TryWriteBytes( SendBufferRef buffer, i32 offset, f64 value )
    {
        if ( ( buffer->WriteSize() + offset + sizeof( f64 ) ) > buffer->AllocSize() )
            return false;

        std::memcpy( &buffer->Buffer()[ offset ], &value, sizeof( f64 ) );

        return true;
    }


    template< typename T >
    static bool TryWriteBytes( Span< BYTE > s, T value )
    {
        if ( s._offset + sizeof( T ) > s._size )
            return false;

        std::memcpy( &s._buffer[ s._offset ], &value, sizeof( T ) );

        return true;
    }

    static u16 TryWriteBytes( SendBufferRef buffer, i32 offset, const String& value )
    {
        u16 strFullLen = (u16)( ( value.size() * 2 ) + 2 );
        if ( buffer->AllocSize() - ( buffer->WriteSize() + offset ) < strFullLen )
            return 0;

        u16 strLength = value.size();
        std::memcpy( &buffer->Buffer()[ offset ], &strLength, 2 );
        std::memcpy( &buffer->Buffer()[ offset + 2 ], value.data(), value.size() * 2 );

        return strFullLen;
    }

    template< EnumType T >
    static bool TryWriteBytes( SendBufferRef buffer, i32 offset, T value )
    {
        if ( ( buffer->WriteSize() + offset + sizeof( T ) ) > buffer->AllocSize() )
            return false;

        std::memcpy( &buffer->Buffer()[ offset ], &value, sizeof( T ) );

        return true;
    }

    //template< typename T >
    //static bool TryWriteBytes( SendBufferRef buffer, i32 offset, const T& value )
    //{
    //  return 0;
    //}

    static bool ToBoolean( BYTE* buffer, i32 offset )
    {
        return buffer[ offset ];
    }

    static wchar_t ToChar( BYTE* buffer, i32 offset )
    {
        return *(wchar_t*)( &buffer[ offset ] );
    }

    static i16 ToInt16( BYTE* buffer, i32 offset )
    {
        return *(i16*)( &buffer[ offset ] );
    }

    static i32 ToInt24( BYTE* buffer, i32 offset )
    {
        i32 temp{};
        std::memcpy( &temp, &buffer[ offset ], 3 );
        return temp;
    }

    static i32 ToInt32( BYTE* buffer, i32 offset )
    {
        return *(i32*)( &buffer[ offset ] );
    }

    static i64 ToInt64( BYTE* buffer, i32 offset )
    {
        return *(i64*)( &buffer[ offset ] );
    }

    static u16 ToUInt16( BYTE* buffer, i32 offset )
    {
        return *(u16*)( &buffer[ offset ] );
    }

    static u32 ToUInt24( BYTE* buffer, i32 offset )
    {
        u32 temp{};
        std::memcpy( &temp, &buffer[ offset ], 3 );
        return temp;
    }

    static u32 ToUInt32( BYTE* buffer, i32 offset )
    {
        return *(u32*)( &buffer[ offset ] );
    }

    static u64 ToUInt64( BYTE* buffer, i32 offset )
    {
        return *(u64*)( &buffer[ offset ] );
    }

    static f32 ToSingle( BYTE* buffer, i32 offset )
    {
        return *(f32*)( &buffer[ offset ] );
    }

    static f64 ToDouble( BYTE* buffer, i32 offset )
    {
        return *(f64*)( &buffer[ offset ] );
    }

};

/// <summary>
///
/// </summary>
struct BufferReader
{
    template< typename T >
    static bool TryRead( BYTE* buffer, i32 offset, T& value )
    {
        /*if ( buffer->AllocSize() - ( buffer->WriteSize() + offset ) < sizeof( T ) )
            return 0; */

        std::memcpy( &value, &buffer[ offset ], sizeof( T ) );

        return true;// sizeof( T );
    }

    static bool TryRead( BYTE* buffer, i32 offset, String& value )
    {
        bool success{ true };
        u16 stringSize;
        success &= BufferReader::TryRead( buffer, offset, stringSize );
        offset += sizeof( u16 );
        value = String( (const wchar_t*)( &buffer[ offset ] ), stringSize );

        return success;
    }
};

}
