import <iostream>;
import <format>;
import <string>;

import Mala.Core.Types;
import Mala.Core.CoreGlobal;
import Mala.Container.String;
import Mala.Net.HttpSession;
import Mala.Net.SendBuffer;
import Mala.Http.HttpTypes;
import Mala.Http.HttpRequest;
import Mala.Http.HttpHelper;
import Mala.Memory;
import Mala.Reflection;


using namespace Mala::Net;
using namespace Mala::Http;


i32 HttpSession::OnRecv( BYTE* buffer, i32 len )
{
	i32 processLen = 0;

	for ( ;; )
	{
		// 최소한 헤더는 파싱할 수 있어야 함
		// HTTP METHOD 타입( 3(GET) ), 공백( 1 ), 패스( 1(/) ), 헤더 종료 구분 문자 "\r\n\r\n"( 4 );
		size_t dataSize = len - processLen;
		if ( dataSize < 9 )
			break;

		// 받아온 버퍼를 파싱하기 위해 스트링뷰 생성
		// 헤더의 끝이 도착하지 않았다면, 추가적으로 수신을 통해 헤더를 완성해야함
		std::string_view request{ (char*)( &buffer[ processLen ] ), dataSize };
		auto headerEnd = request.find( "\r\n\r\n", 3 );
		if ( headerEnd == std::string_view::npos )
			break;

		// 본문의 길이 확인
		/// 바디( 옵션 )까지 포함해서 도착했는지 다시 확인
		size_t contentLength = HttpHelper::GetContentLength( request );

		if ( request.size() < headerEnd + 4 + contentLength )
			break;

		/// 헤더 + 바디사이즈로 잘라냄
		request = request.substr( 0, headerEnd + 4 + contentLength );

		// Http 요청 조립 성공
		OnRecvRequest( request );

		processLen += request.size();
	}

	return processLen;
}

void HttpSession::OnSend( i32 len )
{
	/// 여기서 세션을 종료 시키면 안되지만 테스트용
	Disconnect( L"Good~" );
}

void HttpSession::OnRecvRequest( const std::string_view& request )
{
	auto httpRequestView = HttpHelper::ParseHttpReqeustView( request );

	// 임시적으로 404 페이지를 보냄
	auto [ notFoundHeader, notFoundPage ] = HttpHelper::GetDefaultNotFoundResponse();
	auto sendBuffer = GSendBufferManager->Open( 256 );

	std::memcpy( sendBuffer->Buffer(), notFoundHeader.data(), notFoundHeader.length() );
	std::memcpy( &sendBuffer->Buffer()[ notFoundHeader.length() ], notFoundPage.data(), notFoundPage.length() );

	sendBuffer->Close( notFoundHeader.length() + notFoundPage.length() );
	Send( sendBuffer );
}
