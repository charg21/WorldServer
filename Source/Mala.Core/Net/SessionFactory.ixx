export module Mala.Net.SessionFactory;

#include "../MalaMacro.h"

import <memory>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Threading.Lock;
import Mala.Windows;

using namespace Mala::Container;
using namespace Mala::Threading;

export namespace Mala::Net
{

class SessionFactory
{
    SessionFactory() = default;
    virtual ~SessionFactory() = default;
};

}
