#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#include <Windows.h>
//import <WS2tcpip.h>;

#include "../MalaMacro.h"

import <iostream>;

import Mala.Core.Crash;
import Mala.Core.CoreTLS;
import Mala.Core.CoreGlobal;
import Mala.Core.Types;
import Mala.Memory;
import Mala.Net;
import Mala.Net.NetObject;
import Mala.Net.RioCore;
import Mala.Net.RioListener;

import Mala.Windows;
import Mala.Threading.ThreadManager;

using namespace Mala::Net;

RioCore::RioCore()
: _concurrentThreadCount{}
{
}

RioCore::~RioCore()
{
    for ( auto iocpHandle : _iocpHandles )
    {
        ::CloseHandle( iocpHandle );
    }

    _iocpHandles.clear();
}

void RioCore::Initialize( i32 threadCount )
{
    _concurrentThreadCount = threadCount;
    _iocpHandles.resize( threadCount );
    _completionQueues.resize( threadCount );
	_iocpJobExecutor = MakeShared< IocpJobExecutor >();
    _netEvent.owner = shared_from_this();

    for ( i32 i{}; i < threadCount; i += 1 )
    {
        _iocpHandles[ i ] = CreateIoCompletionPort( INVALID_HANDLE_VALUE, NULL, 0, 1 );
        ASSERT_CRASH( _iocpHandles[ i ] != NULL );

        RIO_NOTIFICATION_COMPLETION completionType{};
        completionType.Type               = RIO_IOCP_COMPLETION;
        completionType.Iocp.IocpHandle    = _iocpHandles[ i ];
        completionType.Iocp.CompletionKey = (void*)( nullptr );
        completionType.Iocp.Overlapped    = &_netEvent.ToOverlapped();

        _completionQueues[ i ] = SocketHelper::RioFunctionTable.RIOCreateCompletionQueue( MAX_CQ_SIZE, &completionType );
        ASSERT_CRASH( _completionQueues[ i ] != RIO_INVALID_CQ_ );

        int notifyResult = SocketHelper::RioFunctionTable.RIONotify( _completionQueues[ i ] );
        ASSERT_CRASH( notifyResult == ERROR_SUCCESS );
    }
}

std::shared_ptr< bool > RioCore::Post( std::function< void() >&& task )
{
    auto  future = MakeShared< bool >( false );
    auto* jobEvent = xnew< JobEvent >();

    jobEvent->owner = _iocpJobExecutor;
    jobEvent->job = [ task = std::move( task ), future ]
    {
        task();
        *future = true;
    };

    // 일단 라운드 로빈으로 처리
    static std::atomic< i32 > postLoadBalancer{};
    auto result = ::PostQueuedCompletionStatus(
        _iocpHandles[ ++postLoadBalancer % _iocpHandles.size() ],
        0,
        0,
        reinterpret_cast< LPOVERLAPPED >( jobEvent ) );
    ASSERT_CRASH( result != 0 );
    return future;
}

bool RioCore::Register( INetObjectRef netObject )
{
    /// TODO: CQ 로드밸런싱하여 배정 되도록 수정
    static std::atomic< int > threadLoadBalancer = 0;

    if ( !netObject->IsParkingObject() )
        netObject->_threadId = threadLoadBalancer.fetch_add( 1 ) % _concurrentThreadCount;

    if ( auto rioSession = std::dynamic_pointer_cast< RioSession >( netObject ) )
    {
        /// create socket RQ
        auto requestQueue = SocketHelper::RioFunctionTable.RIOCreateRequestQueue(
            rioSession->GetSocket(),
            MAX_RECV_RQ_SIZE_PER_SOCKET,
            1,
            MAX_SEND_RQ_SIZE_PER_SOCKET,
            1,
            GetCompletionQueue( netObject->GetThreadId() ),
            GetCompletionQueue( netObject->GetThreadId() ),
            NULL );
        ASSERT_CRASH( requestQueue != RIO_INVALID_RQ_ );
        netObject->_requestQueue = requestQueue;
    }

    /// 컴플리션 포트 등록
    HANDLE resultHandle = ::CreateIoCompletionPort(
        netObject->GetHandle(),
        _iocpHandles[ netObject->GetThreadId() ],
        /*completionKey*/0,
        0 );

    return resultHandle == _iocpHandles[ netObject->GetThreadId() ];
}

bool RioCore::Dispatch( u32 timeoutMs )
{
    /// 메인스레드가 식별자 0을 가지고 있음.
	/// 일단 메인 스레드는 Dispatch()를 호출 하지 않을것을 가정하여, -1을 빼줌
    auto threadId = LThreadId - 1;

    HANDLE		iocpHandle{ _iocpHandles[ threadId ] };
    DWORD       numOfBytes{};
    ULONG_PTR   key       {};
    INetEvent*  event     {};

    if ( ::GetQueuedCompletionStatus(
        iocpHandle,
        OUT &numOfBytes,
        OUT reinterpret_cast< PULONG_PTR >( &key ),
        OUT reinterpret_cast< LPOVERLAPPED* >( &event ),
        timeoutMs ) )
    {
        ASSERT_CRASH( event != nullptr );
        auto& object{ event->owner };
        object->Dispatch( event, numOfBytes );
    }
    else
    {
        i32 errorCode = ::WSAGetLastError();
        switch ( errorCode )
        {
        case WAIT_TIMEOUT:
            return false;
        case ERROR_CONNECTION_REFUSED:
            {
                // Connect시 원격 컴퓨터가 네트워크 연결을 거부했습니다.
                auto object{ event->owner };
                numOfBytes = errorCode;
                object->Dispatch( event, numOfBytes );
                return true;
            }
        default:
            //TODO : 로그 찍기.
            auto object{ event->owner };
            object->Dispatch( event, numOfBytes );
            break;
        }
    }

    return true;
}

void RioCore::Dispatch( INetEvent* netEvent, int numOfBytes )
{
    auto threadId = LThreadId - 1;
    RIO_CQ cq{ _completionQueues[ threadId ] };
    RIORESULT results[ MAX_RIO_RESULT ];
    ULONG numResults = SocketHelper::RioFunctionTable.RIODequeueCompletion( cq, results, MAX_RIO_RESULT );
    if ( 0 == numResults || RIO_CORRUPT_CQ_CODE == numResults )
    {
        ASSERT_CRASH( false );
    }

    if ( numResults > ( MAX_RIO_RESULT / 2 ) )
        std::cout << "NumResults Count: " << numResults << std::endl;

    /// Completion 완료 후 CompletionQueue 사용 가능 통지
    auto notifyResult = SocketHelper::RioFunctionTable.RIONotify( cq );
    ASSERT_CRASH( notifyResult == ERROR_SUCCESS );

    for ( ULONG i = 0; i < numResults; ++i )
    {
        auto* netEvent{ reinterpret_cast< INetEvent* >( results[ i ].RequestContext ) };
        INetObjectRef netObjectRef{ netEvent->owner };
        ULONG transferred{ results[ i ].BytesTransferred };
        if ( !transferred )
            std::cout << "transferred Is Zero: " << results[ i ].Status << std::endl;

        netObjectRef->Dispatch( netEvent, transferred );
    }
}

