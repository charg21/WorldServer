import <format>;

import Mala.Core.Types;
import Mala.Net.PacketSession;
import Mala.Net.PacketHeader;

using namespace Mala::Net;

i32 PacketSession::OnRecv( BYTE* buffer, i32 len )
{
	i32 processLen = 0;

	for ( ;; )
	{
		i32 dataSize = len - processLen;
		if ( dataSize < sizeof( PacketHeader ) )
			break;

		PacketHeader header = *( reinterpret_cast< PacketHeader* >( &buffer[ processLen ] ) );

		if ( dataSize < header._length )
			break;

		OnRecvPacket( &buffer[ processLen ], header._length );

		processLen += header._length;
	}

	return processLen;
}
