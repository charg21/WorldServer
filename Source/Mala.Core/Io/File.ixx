export module Mala.Io.File;

import <filesystem>;
import Mala.Container;

export enum EEncoding
{
    Utf8,
    Utf16,

    Max
};

using std::filesystem::current_path;
using namespace Mala::Container;

export namespace Mala::Io
{
    
class File
{
public:
    static String ReadAllText( const StringView& path )
    {
        FILE* filePtr{};
        
        auto erorrCode = _wfopen_s( &filePtr, path.data(), L"r" );
        if ( !filePtr )
            return {};

        fseek( filePtr, 0, SEEK_END );
        const size_t fileSize = ftell( filePtr );

        Vector< char > buffer;
        buffer.resize( fileSize );

        fseek( filePtr, 0, SEEK_SET );
        fread( &buffer[ 0 ], 1, fileSize, filePtr );
        fclose( filePtr );

        return String{ buffer.cbegin(), buffer.cend() };
    }

    static std::string ReadAllText( const std::string_view& path )
    {
        FILE* filePtr{};
        
        auto curPath = path.empty() ? current_path() : path;
        auto erorrCode = fopen_s( &filePtr, path.data(), "r" );
        if ( !filePtr )
            return {};

        fseek( filePtr, 0, SEEK_END );
        const size_t fileSize = ftell( filePtr );

        Vector< char > buffer;
        buffer.resize( fileSize );

        fseek( filePtr, 0, SEEK_SET );
        fread( &buffer[ 0 ], 1, fileSize, filePtr );
        fclose( filePtr );

        return std::string{ buffer.begin(), buffer.end() };
    }

};

}