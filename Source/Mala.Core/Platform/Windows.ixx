module;

#pragma once
//#pragma warning( push, 0 )

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

export
{
#include <Windows.h>
}

import <WinSock2.h>;
#include <MSWSock.h>;
import <WS2tcpip.h>;
#include <synchapi.h>

#pragma comment( lib, "ws2_32" )
#pragma comment( lib, "Synchronization.lib" )
#pragma comment(lib, "ntdll.lib")


export module Mala.Windows;

import Mala.Core.Types;
import Mala.Core.Defer;
import Mala.Container.String;

// types
export
{

using WCHAR      = wchar_t;
using ULONG      = ::ULONG;
using ULONG_PTR  = ::ULONG_PTR;
using PULONG_PTR = ::PULONG_PTR;

/// <summary>
///
/// </summary>
using OVERLAPPED         = ::OVERLAPPED;
using LPOVERLAPPED       = ::LPOVERLAPPED;
using HANDLE             = ::HANDLE;
using SOCKET             = ::SOCKET;
using SOCKADDR_          = ::SOCKADDR;
using SOCKADDR_IN        = ::SOCKADDR_IN;
using sockaddr2           = ::sockaddr;
using sockaddrType       = ::sockaddr;
using sockaddr_in2        = ::sockaddr_in;
using WSABUF             = ::WSABUF;
using EXCEPTION_POINTERS = ::EXCEPTION_POINTERS;
using IN_ADDR            = ::IN_ADDR;
using GUID               = ::GUID;

/// <summary>
/// RIO
/// </summary>
using RIO_BUF                      = ::RIO_BUF;
using RIO_BUFFERID                 = ::RIO_BUFFERID;
using RIO_RQ                       = ::RIO_RQ;
using RIO_CQ                       = ::RIO_CQ;
using RIORESULT                    = ::RIORESULT;
using RIO_EXTENSION_FUNCTION_TABLE = ::RIO_EXTENSION_FUNCTION_TABLE;
using RIO_NOTIFICATION_COMPLETION  = ::RIO_NOTIFICATION_COMPLETION;

/// <summary>
/// Interlocked Single Linked List
/// </summary>
using SLIST_HEADER = ::SLIST_HEADER;
using SLIST_ENTRY  = ::SLIST_ENTRY;
using PSLIST_ENTRY = ::PSLIST_ENTRY;

/// <summary>
/// WinSock
/// </summary>
using LPFN_CONNECTEX    = ::LPFN_CONNECTEX;
using LPFN_DISCONNECTEX = ::LPFN_DISCONNECTEX;
using LPFN_ACCEPTEX     = ::LPFN_ACCEPTEX;

enum
{
    CP_UTF7_ = 65000,      // UTF-7 translation
    CP_UTF8_ = 65001,       // UTF-8 translation
};

enum
{
    SOMAXCONN_ = SOMAXCONN,
};

enum
{
    TIMEOUT_INFINITE     = INFINITE,
    INVALID_SOCK         = INVALID_SOCKET,

    CK_RIO              = 0xC0DE,
    RIO_CORRUPT_CQ_CODE = RIO_CORRUPT_CQ,

    SESSION_BUFFER_SIZE = 65536,

    MAX_RIO_RESULT              = 1024,
    MAX_SEND_RQ_SIZE_PER_SOCKET = 128,
    MAX_RECV_RQ_SIZE_PER_SOCKET = 8,
    MAX_CLIENT                  = 5000,
    MAX_CQ_SIZE = ( MAX_SEND_RQ_SIZE_PER_SOCKET + MAX_RECV_RQ_SIZE_PER_SOCKET ) * MAX_CLIENT,
};

//constexpr auto RIO_INVALID_BUFFERID_ = ( (RIO_BUFFERID)(ULONG_PTR)0xFFFFFFFF );
constexpr auto RIO_INVALID_CQ_ = (RIO_CQ)0;
constexpr auto RIO_INVALID_RQ_ = ((RIO_RQ)0);
//constexpr auto RIO_INVALID_RQ__ = RIO_INVALID_RQ;

// #define RIO_MSG_DONT_NOTIFY           0x00000001
// #define RIO_MSG_DEFER                 0x00000002
// #define RIO_MSG_WAITALL               0x00000004
// #define RIO_MSG_COMMIT_ONLY           0x00000008
//
// #define RIO_INVALID_BUFFERID          ((RIO_BUFFERID)(ULONG_PTR)0xFFFFFFFF)

//
// #define RIO_MAX_CQ_SIZE               0x8000000
// #define RIO_CORRUPT_CQ                0xFFFFFFFF
}

export // template< typename T >
    [[ nodiscard ]] void*
    interlocked_exchange_ptr( void* volatile* target, void* value )
{
    return InterlockedExchangePointer( target, value );
}

export std::string get_process_name()
{
    DWORD  processId    { ::GetCurrentProcessId()                                              };
    HANDLE processHandle{ ::OpenProcess( PROCESS_QUERY_LIMITED_INFORMATION, FALSE, processId ) };
    if ( INVALID_HANDLE_VALUE == processHandle )
        return "";

    Mala::Core::Defer handleDestroyer = [ processHandle ]{ ::CloseHandle( processHandle ); };

    char  buffer[ MAX_PATH ]{};
    DWORD bufferSize = MAX_PATH;
    if ( !QueryFullProcessImageNameA( processHandle, 0, buffer, &bufferSize ) )
        return "";

    std::string_view fullPath = buffer;
    size_t           pos      = fullPath.find_last_of( "\\" );
    if ( pos == std::string_view::npos )
        return "";

    // +1 [\\], -5 [.][e][x][e][\0]
    std::string processName = fullPath.substr( pos + 1, fullPath.length() - pos - 5 ).data();
    return std::move( processName );
}

export String GetProcessName()
{
    DWORD  processId     = ::GetCurrentProcessId();
    HANDLE processHandle = ::OpenProcess( PROCESS_QUERY_LIMITED_INFORMATION, FALSE, processId );
    if ( INVALID_HANDLE_VALUE == processHandle )
        return L"";

    Mala::Core::Defer handleDestroyer = [ processHandle ]{ ::CloseHandle( processHandle ); };

    wchar_t buffer[ MAX_PATH ]{};
    DWORD bufferSize = MAX_PATH;

    if ( !::QueryFullProcessImageNameW( processHandle, 0, buffer, &bufferSize ) )
        return L"";

    StringView fullPath = buffer;
    size_t     pos      = fullPath.find_last_of( L"\\" );
    if ( pos == std::string_view::npos )
        return L"";

    // +1은 \\을, -4는 .exe
    String processName = fullPath.substr( pos + 1, fullPath.length() - pos - 5 ).data();
    return std::move( processName );
}

/// <summary>
/// 블락킹을 통해 대기
/// </summary>
export inline void sleep( int n )
{
    ::Sleep( n );
}

/// <summary>
/// spin을 통해 대기
/// </summary>
export inline void nanosleep( i64 waitUntil )
{
    // 1ns    1
    // 100ns  1'00
    // 1us    1'000
    // 1ms    1'000'000
    // 1sec   1'000'000'000
    waitUntil = max( waitUntil, 100 );
    waitUntil /= 100; // windows 타이머 정밀도 최소 100ns

    size_t start;
    ::QueryPerformanceCounter( ( LARGE_INTEGER* )( &start ) );

    size_t current{ start };

    u64 diff{ current - start };

    do
    {
        ::QueryPerformanceCounter( ( LARGE_INTEGER* )( &current ) );

        diff = current - start;

    } while ( diff <= waitUntil );
}

/// <summary>
/// 타임 퀀텀을 양보한다
/// </summary>
export inline void yield()
{
    ::SwitchToThread();
}

export namespace Mala::Windows
{
//inline u64 GetTick()
//{
//    return ::GetTickCount64();
//}
void WakeByAddressSingle( void* address )
{
    ::WakeByAddressSingle( address );
}

void WakeByAddressAll( void* address )
{
    ::WakeByAddressAll( address );
}

void WaitOnAddress( void* address, void* compareAddress, usize size, u32 milliseconds )
{
    ::WaitOnAddress( address, compareAddress, size, milliseconds );
}

inline Millisecond GetTickAsMs()
{
    return 1ms * ::GetTickCount64();
}

inline u64 GetSystemTimeAsFileTime()
{
    FILETIME ft;
    ::GetSystemTimeAsFileTime( &ft );
    ULARGE_INTEGER uli
    {
        .LowPart = ft.dwLowDateTime,
        .HighPart = ft.dwHighDateTime,
    };
    return uli.QuadPart;
}

inline void* VirtualAllocEx( usize size )
{
    return ::VirtualAllocEx( GetCurrentProcess(), 0, size, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE );
}

inline void VirtualFreeEx( void* bufferPtr )
{
    ::VirtualFreeEx( GetCurrentProcess(), bufferPtr, 0, MEM_RELEASE );
}

inline void SetThisThreadDesc( const String& desc )
{
    ::SetThreadDescription( GetCurrentThread(), desc.c_str() );
}

inline int MultiByteToWideChar(
    _In_ UINT CodePage,
    _In_ DWORD dwFlags,
    _In_NLS_string_( cbMultiByte ) LPCCH lpMultiByteStr,
    _In_ int cbMultiByte,
    _Out_writes_to_opt_( cchWideChar, return ) LPWSTR lpWideCharStr,
    _In_ int cchWideChar )
{
    return ::MultiByteToWideChar( CodePage, dwFlags, lpMultiByteStr, cbMultiByte, lpWideCharStr, cchWideChar );
}

int
WINAPI
WideCharToMultiByte(
    _In_ UINT CodePage,
    _In_ DWORD dwFlags,
    _In_NLS_string_( cchWideChar ) LPCWCH lpWideCharStr,
    _In_ int cchWideChar,
    _Out_writes_bytes_to_opt_( cbMultiByte, return ) LPSTR lpMultiByteStr,
    _In_ int cbMultiByte,
    _In_opt_ LPCCH lpDefaultChar,
    _Out_opt_ LPBOOL lpUsedDefaultChar )
{
    return ::WideCharToMultiByte( CodePage, dwFlags, lpWideCharStr, cchWideChar, lpMultiByteStr, cbMultiByte, lpDefaultChar, lpUsedDefaultChar );
}

class Console
{
public:
/// <summary>
/// 콘솔의 제목을 설정한다
/// </summary>
static void SetTitle( String&& title )
{
    ::SetConsoleTitleW( title.c_str() );
}

};

}
