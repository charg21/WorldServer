#include "../CoreMacro.h";

#pragma once

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#include <Windows.h>

import Mala.Memory;
import Mala.Memory.MemoryPool;
import Mala.Windows;


MemoryHeader::MemoryHeader( i32 size )
: allocSize{ size }
{
}

void* MemoryHeader::AttachHeader( MemoryHeader* header, i32 size )
{
	new( header ) MemoryHeader( size );	// placement new
	return reinterpret_cast<void*>( ++header );
}

MemoryHeader* MemoryHeader::DetachHeader( void* ptr )
{
	MemoryHeader* header = reinterpret_cast< MemoryHeader* >( ptr ) - 1;
	return header;
}

/// <summary>
/// 생성자
/// </summary>
MemoryPool::MemoryPool( i32 allocSize )
: _allocSize{ allocSize }
{
	::InitializeSListHead( &_header );
}

/// <summary>
/// 소멸자
/// </summary>
MemoryPool::~MemoryPool()
{
	// 메모리 비우기
	while ( auto memory = static_cast< MemoryHeader* >( ::InterlockedPopEntrySList( &_header ) ) )
	{
		::_aligned_free( memory );
	}
}

/// <summary>
/// 추가한다
/// </summary>
void MemoryPool::Push( MemoryHeader* ptr )
{
	ptr->allocSize = 0;

	// Pool 메모리 반납
	::InterlockedPushEntrySList( &_header, static_cast< PSLIST_ENTRY >( ptr ) );
	_useCount.fetch_sub( 1 );
	_reserveCount.fetch_add( 1 );
}

/// <summary>
/// 제거한다
/// </summary>
MemoryHeader* MemoryPool::Pop()
{
	auto memory = static_cast< MemoryHeader* >( ::InterlockedPopEntrySList( &_header ) );
	if ( !memory ) [[unlikely]]
	{
		memory = reinterpret_cast< MemoryHeader* >( ::_aligned_malloc( _allocSize, SLIST_ALIGNMENT ) );
	}
	else
	{
		ASSERT_CRASH( memory->allocSize == 0 );
		_reserveCount.fetch_sub( 1 );
	}

	_useCount.fetch_add( 1 );

	return memory;
}
