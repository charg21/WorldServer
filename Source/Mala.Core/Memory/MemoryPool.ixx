export module Mala.Memory.MemoryPool;

import Mala.Core.Types;
import Mala.Windows;


export
{

constexpr auto SLIST_ALIGNMENT = 16;

/// <summary>
/// 메모리 헤더
/// </summary>
struct alignas( SLIST_ALIGNMENT ) MemoryHeader : public SLIST_ENTRY
{
	// [MemoryHeader][Data]
	MemoryHeader( i32 size );
	[[nodiscard]] static void* AttachHeader( MemoryHeader* header, i32 size );
	[[nodiscard]] static MemoryHeader* DetachHeader( void* ptr );

	i32 allocSize;
};

/// <summary>
/// 메모리 풀
/// </summary>
class alignas( SLIST_ALIGNMENT ) MemoryPool
{
public:
	/// <summary>
	/// 생성자
	/// </summary>
	MemoryPool( i32 allocSize );
	~MemoryPool();

	void Push( MemoryHeader* ptr );
	[[nodiscard]] MemoryHeader* Pop();

private:
	SLIST_HEADER _header;
	i32 _allocSize{};
	Atomic< i32 > _useCount{};
	Atomic< i32 > _reserveCount{};
};

}
