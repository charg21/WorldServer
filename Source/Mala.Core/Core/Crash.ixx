export module Mala.Core.Crash;

#include "../CoreMacro.h";

export namespace Mala::Core
{

/// <summary>
/// 크래시를 발생시킨다( 유효하지 않은 메모리 영역 접근 )
/// </summary>
extern "C" MALA_CORE_API void Crash();

/// <summary>
/// 표현식의 결과가 거짓이라면 크래시를 발생시킨다
/// </summary>
extern "C" MALA_CORE_API void CrashIfFalse( bool isFalse );

/// <summary>
/// 포인터가 널이라면 크래시를 발생시킨다
/// </summary>
extern "C" MALA_CORE_API void CrashIfNull( void* pointer );

} // namespace Mala::Core

