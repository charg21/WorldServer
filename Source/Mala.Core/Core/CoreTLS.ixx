export module Mala.Core.CoreTLS;

#pragma once

import <stack>;
import <deque>;
import Mala.Core.Types;
import Mala.Db.DbConnection;
import Mala.Threading.Lock;
import Mala.Threading.JobWheelTimer;
import Mala.Threading.JobExecutor;

import Mala.Memory;

using namespace Mala::Db;
using namespace Mala::Threading;
using namespace Mala::Net;


export // namespace Mala
{

inline extern thread_local  u32                                LThreadId{ (u32)( -1 ) };
inline extern thread_local  u64                                LEndTickCount{};
inline extern thread_local  u64                                LLastTickCount{};
inline extern thread_local  SendBufferChunkPtr                 LSendBufferChunk{};
inline extern thread_local  JobWheelTimer< NullLock, 16, 12 >* LJobTimer{};
inline extern thread_local  Mala::Threading::JobExecutor*      LCurrentExecutor{};
inline extern thread_local  std::deque< JobExecutorPtr >*      LExecuterList{};
inline extern thread_local  MemoryCache*                       LMemoryCache{};
inline extern thread_local  DbConnection*                      LDbConnection{};
inline extern thread_local  std::stack< i32 >                  LLockStack;

}
