﻿
import Mala.Core.CoreGlobal;
import Mala.Diagnostics.PerformanceCounter;
import Mala.Net.SendBuffer;
import Mala.Net.SocketHelper;
import Mala.Threading.ThreadManager;
import Mala.Threading.GlobalDistributor;
import Mala.Threading.JobTimer;
import Mala.Threading.JobWheelTimer;
import Mala.Threading.GlobalJobExecutor;
import Mala.Log;
import Mala.Memory;

using namespace Mala::Net;
using namespace Mala::Threading;
using namespace Mala::Diagnostics;

/// <summary>
/// 생성자
/// </summary>

CoreGlobal::CoreGlobal()
{
	GMemory             = new Memory();

	GThreadManager      = xnew< ThreadManager >();
	GSendBufferManager  = xnew< SendBufferManager >();
	GGlobalDistributor  = xnew< GlobalDistributor >();
	GGlobalJobExecutor  = xnew< GlobalJobExecutor >();
	GJobTimer           = xnew< JobTimer >();
	GPerfCounter        = xnew< PerformanceCounter >();

	SocketHelper::Initialize();
#ifndef USE_ASIO
#endif
}

/// <summary>
/// 소멸자
/// </summary>
CoreGlobal::~CoreGlobal()
{
	xdelete( GThreadManager );
	xdelete( GSendBufferManager );
	xdelete( GGlobalDistributor );
	xdelete( GGlobalJobExecutor );
	xdelete( GJobTimer );
	xdelete( GPerfCounter );

	delete GMemory;
}

