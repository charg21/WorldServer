export module Mala.Core.CoreGlobal;

#pragma once

import <memory>;
import Mala.Core.Types;
import Mala.Net;


#define NAMESPACE_BEGIN( NAMESPACE ) export namespace NAMESPACE{
#define NAMESPACE_END( NAMESPACE ) }

NAMESPACE_BEGIN( Mala::Threading )

inline extern class ThreadManager*        GThreadManager    {    };
inline extern class GlobalDistributor*    GGlobalDistributor{    };
inline extern class JobTimer*             GJobTimer         {    };
inline extern       i32                   GThreadCount      { 16 };
inline extern class GlobalJobExecutor*    GGlobalJobExecutor{    };

NAMESPACE_END( Mala::Threading )


NAMESPACE_BEGIN( Mala::Net )

inline extern class SendBufferManager*                    GSendBufferManager{};
inline extern       std::shared_ptr< Mala::Net::NetCore > GNetCore{};

NAMESPACE_END( Mala::Net )


NAMESPACE_BEGIN( Mala::Db )
NAMESPACE_END( Mala::Db )


NAMESPACE_BEGIN( Mala::Diagnostics )

inline extern class PerformanceCounter* GPerfCounter{};

NAMESPACE_END( Mala::Diagnostics )


export
{

inline extern class Memory* GMemory{};

}

export class CoreGlobal
{
public:
    CoreGlobal();
    ~CoreGlobal();
};

//export inline extern CoreGlobal GCoreGlobal{};
