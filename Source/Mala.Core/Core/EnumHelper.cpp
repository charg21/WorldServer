#pragma once

import Mala.Core.EnumHelper;

using namespace Mala;

/// 64비트 값의 최하위 비트가 1인 위치
int CustomFfs( unsigned int value )
{
    unsigned long index{}; // 비트 인덱스를 저장할 변수
    if ( _BitScanForward( &index, value ) )
        return static_cast< int >( index + 1 ); // 1-based 인덱스 반환

    return 0; // 모든 비트가 0인 경우
}

/// 64비트 값의 최하위 비트가 1인 위치를 반환한다.
int CustomFfsll( unsigned long long value )
{
    unsigned long index{}; // 비트 인덱스를 저장할 변수
    if ( _BitScanForward64( &index, value ) )
        return static_cast< int >( index + 1 ); // 1-based 인덱스 반환

    return 0; // 모든 비트가 0인 경우
}
