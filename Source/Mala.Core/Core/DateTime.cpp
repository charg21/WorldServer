module Mala.Core.DateTime;

import <time.h>;
import Mala.Core.Types;
import Mala.Core.TimeSpan;
import Mala.Container.String;
import Mala.Windows;

using namespace Mala::Core;



/// <summary>
/// 현재 시간에 대한 DateTime 객체를 반환한다
/// </summary>
DateTime DateTime::Now()
{
    // 100나노초 단위의 시간을 밀리초 단위로 변환
    return Mala::Windows::GetSystemTimeAsFileTime() / 10000;
    //auto millis = duration_cast< milliseconds >( duration ).count();

    //auto nowSec = std::chrono::system_clock::to_time_t( now );
    //tm when{};
    //_localtime64_s( &when, &nowSec );

    //DateTime nowDateTime;
    //nowDateTime._millis = _mktime64( &when ) * 1000;
    //return nowDateTime;
}

// "YYYY-MM-DD HH:MM:SS" 형식만 지원
DateTime::DateTime( const std::string& dateString )
{
    if ( dateString.size() < 19 )
        return;

    const char* date = dateString.data();

    tm when
    {
        .tm_sec  = ( date[ 17 ] - L'0' ) * 10 +
            ( date[ 18 ] - L'0' ) * 1,

        .tm_min  = ( date[ 14 ] - L'0' ) * 10 +
            ( date[ 15 ] - L'0' ) * 1,

        .tm_hour = ( date[ 11 ] - L'0' ) * 10 +
            ( date[ 12 ] - '0' ) * 1,

        .tm_mday = ( date[ 8 ] - L'0' ) * 10 +
            ( date[ 9 ] - L'0' ) * 1,

        .tm_mon  = ( ( date[ 5 ] - L'0' ) * 10 +
            ( date[ 6 ] - L'0' ) * 1 ) - 1,

        .tm_year = ( ( date[ 0 ] - L'0' ) * 1000 +
            ( date[ 1 ] - L'0' ) * 100 +
            ( date[ 2 ] - L'0' ) * 10  +
            ( date[ 3 ] - L'0' ) * 1     ) - 1900,
    };

    _millis = ::_mktime64( &when ) * 1000;
}

DateTime::DateTime( const String& dateString )
{
    if ( dateString.size() < 19 )
        return;

    const wchar_t* date = dateString.data();

    tm when
    {
        .tm_sec  = ( date[ 17 ] - L'0' ) * 10 +
            ( date[ 18 ] - L'0' ) * 1,

        .tm_min  = ( date[ 14 ] - L'0' ) * 10 +
            ( date[ 15 ] - L'0' ) * 1,

        .tm_hour = ( date[ 11 ] - L'0' ) * 10 +
            ( date[ 12 ] - '0' ) * 1,

        .tm_mday = ( date[ 8 ] - L'0' ) * 10 +
            ( date[ 9 ] - L'0' ) * 1,

        .tm_mon  = ( ( date[ 5 ] - L'0' ) * 10 +
            ( date[ 6 ] - L'0' ) * 1 ) - 1,

        .tm_year = ( ( date[ 0 ] - L'0' ) * 1000 +
            ( date[ 1 ] - L'0' ) * 100 +
            ( date[ 2 ] - L'0' ) * 10  +
            ( date[ 3 ] - L'0' ) * 1     ) - 1900,
    };

    _millis = ::_mktime64( &when ) * 1000;
}

DateTime::DateTime( u64 millis )
: _millis{ millis }
{}

/// <summary>
/// 생성자
/// </summary>
DateTime::DateTime( i32 year, i32 month, i32 days, i32 hours, i32 mins, i32 seconds, i32 millis )
{
    tm when
    {
        .tm_sec  = seconds,
        .tm_min  = mins,
        .tm_hour = hours,
        .tm_mday = days,
        .tm_mon  = month - 1,
        .tm_year = year - 1900,
    };

    _millis = ( _mktime64( &when ) * 1000 ) + millis;
}


DateTime& DateTime::operator+=( const TimeSpan& rhs )
{
    _millis += rhs._ticks;

    return *this;
}

DateTime& DateTime::operator+( const TimeSpan& rhs )
{
    _millis += rhs._ticks;

    return *this;
}
DateTime& DateTime::operator-=( const TimeSpan& rhs )
{
    _millis -= rhs._ticks;

    return *this;
}

DateTime& DateTime::operator-( const TimeSpan& rhs )
{
    _millis -= rhs._ticks;

    return *this;
}


i32 DateTime::Year() const
{
    time_t sec = _millis / 1000;
    tm tm;
    _localtime64_s( &tm, &sec );
    return tm.tm_year + 1900;
}

i32 DateTime::Month() const
{
    time_t sec = _millis / 1000;
    tm tm;
    _localtime64_s( &tm, &sec );
    return tm.tm_mon + 1;
}

i32 DateTime::Days() const
{
    time_t sec = _millis / 1000;
    tm tm;
    _localtime64_s( &tm, &sec );
    return tm.tm_mday;
}

i32 DateTime::Hours() const
{
    time_t sec = _millis / 1000;
    tm tm;
    _localtime64_s( &tm, &sec );
    return tm.tm_hour;
}

i32 DateTime::Mins() const
{
    time_t sec = _millis / 1000;
    tm tm;
    _localtime64_s( &tm, &sec );
    return tm.tm_min;
}

i32 DateTime::Seconds() const
{
    time_t sec = _millis / 1000;
    tm tm;
    _localtime64_s( &tm, &sec );
    return tm.tm_sec;
}

i32 DateTime::Millis() const
{
    return _millis % 1000;
}

std::tuple< i32, i32, i32, i32, i32, i32, i32 > Mala::Core::DateTime::ToTuple() const
{
    time_t sec = _millis / 1000;
    tm tm;
    //_localtime64_s( &tm, &sec );

    errno_t err = _localtime64_s( &tm, &sec );
    if ( err != 0 ) {
        // 오류 처리
        throw std::runtime_error( "Failed to convert time." );
    }

    return
    {
		tm.tm_year + 1900 - 369,
		tm.tm_mon + 1,
		tm.tm_mday,
		tm.tm_hour,
		tm.tm_min,
		tm.tm_sec,
        Millis()
    };
}
