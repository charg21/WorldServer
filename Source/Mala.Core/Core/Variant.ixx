module;

#include "../CoreMacro.h";

export module Mala.Core.Variant;

import <string>;
import <vector>;

import Mala.Core.Concepts;
import Mala.Core.DateTime;
//import Mala.Container;
import Mala.Container.String;

//using namespace Mala::Container;

/// <summary>
/// 빌드 실패로 임시 코드
/// </summary>
export using DateTime_ = Mala::Core::DateTime;

export class TypeInfo;

export namespace Mala::Core
{

template< typename T >
concept PodVarType =
std::is_same_v< T, bool >           ||
std::is_same_v< T, short >          ||
std::is_same_v< T, unsigned short > ||
std::is_same_v< T, int >            ||
std::is_same_v< T, unsigned int >   ||
std::is_same_v< T, long >           ||
std::is_same_v< T, unsigned long >  ||
std::is_same_v< T, float >          ||
std::is_same_v< T, double >;


/// <summary>
/// 오브젝트 타입 타입 리스트 정의
/// </summary>
//using VarTypeList = TypeList< bool, i16, u16, i32, u32, i64, u64, float, double, std::string, std::wstring >;
enum class MALA_CORE_API EVarType : unsigned char
{
    Bool,
    Short,
    UShort,
    Int,
    UInt,
    Long,
    ULong,
    Single,
    Double,
    String,
    WString,
    Timestamp,
    DateTime,
};

//struct Table;
struct MALA_CORE_API Variant
{
public:
    /// <summary>
    /// 생성자
    /// </summary>
    Variant( EVarType type = EVarType::Long );
    Variant( const Variant& rhs );
    Variant( Variant&& rhs ) noexcept;
    Variant( const TypeInfo& typeInfo, const String& value );
    ~Variant();

    template< std::size_t N >
    Variant( char( &value )[ N ] );
    template< std::size_t N >
    Variant( wchar_t( &value )[ N ] );

    Variant( explicit const wchar_t* v );
    Variant( explicit bool value );
    Variant( explicit short value );
    Variant( explicit unsigned short value );
    Variant( explicit int value );
    Variant( explicit unsigned int value );
    Variant( explicit long long value );
    Variant( explicit unsigned long long value );
    Variant( explicit float value );
    Variant( explicit double value );
    Variant( std::string&& value );
    Variant( const std::string& value );
    Variant( const char* value );
    Variant( String&& value );
    Variant( StringRef value );
    template< EnumType T >
    Variant( T enumValue );

    void operator=( const Variant& other );
    void operator=( Variant&& other ) noexcept;

    void Set( const char* value_string );
    void Set( const std::string& value_string );
    void Set( const String& valueString );

    const std::string to_string();
    String ToString();

    const TypeInfo& GeTypeInfo();
    static EVarType ToVarType( const TypeInfo& typeInfo );

    explicit operator bool() const;
    explicit operator char() const;
    explicit operator short() const;
    explicit operator unsigned short() const;
    explicit operator int() const;
    explicit operator unsigned int() const;
    explicit operator long long() const;
    explicit operator unsigned long long() const;
    explicit operator float() const;
    explicit operator double() const;
    template< EnumType T >
    explicit operator T() const;

    const EVarType _type;

    union
    {
        bool                  _bool;
        char                  _char;
        short                 _short;
        unsigned short        _ushort;
        int                   _int;
        unsigned int          _uint;
        long long             _long;
        unsigned long long    _ulong;
        float                 _single;
        double                _double;
        std::string*          _string;
        String*               _wstring;
        char*                 _bytes;
        DateTime_             _dateTime;
    };
};


///// <summary>
///// Variant 타입 컨테이너 정의
///// </summary>
//using VariantVector = Vector< Variant >;
//using VariantList   = List< Variant >;

template< std::size_t N >
Variant::Variant( char( &value )[ N ] )
: _type{ EVarType::String }
{
    _string = xnew< std::string >( value );
}

template< std::size_t N >
Variant::Variant( wchar_t( &value )[ N ] )
: _type{ EVarType::WString }
{
    _wstring = xnew< String >( value );
}

template< EnumType T >
Variant::Variant( T enumValue )
: _type{ EVarType::ULong }
{
    new( this ) Variant( std::underlying_type_t< T >( _ulong ) );
}

template< EnumType T >
Variant::operator T() const
{
    return static_cast< T >( static_cast< std::underlying_type_t< T > >( _ulong ) );
}


}

export bool operator==( const Mala::Core::Variant& lhs, const Mala::Core::Variant& rhs )
{
    using namespace Mala::Core;
    if ( lhs._type != rhs._type )
        return false;

    switch ( lhs._type )
    {
    case EVarType::Bool:
        return lhs._bool == rhs._bool;
    case EVarType::Short:
        return lhs._short == rhs._short;
    case EVarType::UShort:
        return lhs._ushort == rhs._ushort;
    case EVarType::Int:
        return lhs._int == rhs._int;
    case EVarType::UInt:
        return lhs._uint == rhs._uint;
    case EVarType::Long:
        return lhs._long == rhs._long;
    case EVarType::ULong:
        return lhs._ulong == rhs._ulong;
    case EVarType::Single:
        return lhs._single == rhs._single;
    case EVarType::Double:
        return lhs._double == rhs._double;
    case EVarType::String:
        return *lhs._string == *rhs._string;
    case EVarType::WString:
        return *lhs._wstring == *rhs._wstring;
    case EVarType::DateTime:
        return lhs._ulong == rhs._ulong;
    }

    //ASSERT_CRASH( "Invalid Variant Type" );
    return false;
}

export bool operator!=( const Mala::Core::Variant& lhs, const Mala::Core::Variant& rhs )
{
    return !( lhs == rhs );
}

export namespace std
{

template<>
struct hash< Mala::Core::Variant >
{
    size_t operator()( const Mala::Core::Variant& rhs ) const noexcept
    {
        using namespace Mala::Core;

        switch ( rhs._type )
        {
        case EVarType::Bool:
            return std::hash< bool >{}( rhs._bool );
        case EVarType::Short:
            return std::hash< short >{}( rhs._short );
        case EVarType::UShort:
            return std::hash< unsigned short >{}( rhs._ushort );
        case EVarType::Int:
            return std::hash< int >{}( rhs._int );
        case EVarType::UInt:
            return std::hash< unsigned int >{}( rhs._uint );
        case EVarType::Long:
            return std::hash< long long >{}( rhs._long );
        case EVarType::ULong:
            return std::hash< unsigned long long >{}( rhs._ulong );
        case EVarType::Single:
            return std::hash< float >{}( rhs._single );
        case EVarType::Double:
            return std::hash< double >{}( rhs._double );
        case EVarType::String:
            return std::hash< std::string >{}( *rhs._string );
        case EVarType::WString:
            return std::hash< String >{}( *rhs._wstring );
        case EVarType::Timestamp:
        case EVarType::DateTime:
            return std::hash< unsigned long long >{}( rhs._ulong );
        }

        return 0;
    }
};

template<>
struct equal_to< Mala::Core::Variant >
{
    bool operator()( explicit const Mala::Core::Variant& lhs, explicit const Mala::Core::Variant& rhs ) const noexcept
    {
        using namespace Mala::Core;
        if ( lhs._type != rhs._type )
            return false;

        switch ( lhs._type )
        {
        case EVarType::Bool:
            return lhs._bool == rhs._bool;
        case EVarType::Short:
            return lhs._short == rhs._short;
        case EVarType::UShort:
            return lhs._ushort == rhs._ushort;
        case EVarType::Int:
            return lhs._int == rhs._int;
        case EVarType::UInt:
            return lhs._uint == rhs._uint;
        case EVarType::Long:
            return lhs._long == rhs._long;
        case EVarType::ULong:
            return lhs._ulong == rhs._ulong;
        case EVarType::Single:
            return lhs._single == rhs._single;
        case EVarType::Double:
            return lhs._double == rhs._double;
        case EVarType::String:
            return *lhs._string == *rhs._string;
        case EVarType::WString:
            return *lhs._wstring == *rhs._wstring;
        case EVarType::DateTime:
            return lhs._ulong == rhs._ulong;
        }

        return false;
    }
};

} // namespace std


