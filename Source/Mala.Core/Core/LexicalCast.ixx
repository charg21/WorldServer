export module Mala.Core.LexicalCast;

import <memory>;

import Mala.Core.Types;


export template< typename To, typename From >
To LexicalCast( const From& from )
{
    To to;
    std::stringstream ss;
    ss << from;
    ss >> to;
    return to;
}
