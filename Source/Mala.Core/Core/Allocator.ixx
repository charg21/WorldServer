module;

#include "../CoreMacro.h";

export module Mala.Core.Allocator;

#pragma once

import Mala.Core.Types;

export namespace Mala::Core
{
/// <summary>
/// Base Allocator
/// </summary>
template< typename T >
class MALA_CORE_API BaseAllocator
{
public:
    /// <summary>
    /// 커스텀 얼로케이터 타입 정의
    /// </summary>
    using size_type       = size_t;
    using difference_type = ptrdiff_t;
    using pointer         = T*;
    using const_pointer   = const T*;
    using reference       = T&;
    using const_reference = const T&;
    using value_type      = T;

    /// <summary>
    /// 컨테이너 타입에 사용하기 위한 중첩 구조 템플릿
    /// </summary>
    template< typename Other >
    struct rebind
    {
        using other = BaseAllocator< Other >;
    };

public:
    BaseAllocator() = default;

    template< typename Other >
    BaseAllocator( const BaseAllocator< Other >& ) {}

    static T* Allocate( size_t count ) { return allocate( count ); }
    static T* allocate( size_t count );

    static void Deallocate( T* ptr ) { deallocate( ptr, sizeof( T ) ); }
    static void deallocate( T* ptr, size_t count );

};

/// <summary>
/// Stomp Allocator
/// </summary>
class MALA_CORE_API StompAllocator
{
    enum { PAGE_SIZE = 0x1000 };

public:
    static void* Alloc( int size );
    static void Release( void* ptr );
};

/// <summary>
/// Pool Allocator
/// </summary>
class MALA_CORE_API PoolAllocator
{
public:
    static void* Alloc( size_t size );
    static void Release( void* ptr );
};

template< typename T >
class MALA_CORE_API StlAllocator
{
public:
    /// <summary>
    /// 커스텀 얼로케이터 타입 정의
    /// </summary>
    using size_type       = size_t;
    using difference_type = ptrdiff_t;
    using pointer         = T*;
    using const_pointer   = const T*;
    using reference       = T&;
    using const_reference = const T&;
    using value_type      = T;

public:
    /// <summary>
    /// 컨테이너 타입에 사용하기 위한 중첩 구조 템플릿
    /// </summary>
    template< typename Other >
    struct rebind
    {
        using other = StlAllocator< Other >;
    };

public:
    /// <summary>
    ///
    /// </summary>
    StlAllocator() {}

    template< typename Other >
    StlAllocator( const StlAllocator< Other >& ) {}

    static T* Allocate( size_t count ) { return allocate( count ); }
    static T* allocate( size_t count );

    static void Deallocate( T* ptr ) { deallocate( ptr, sizeof( T ) ); }
    static void deallocate( T* ptr, size_t count );

};

template< typename T >
inline MALA_CORE_API __declspec( allocator ) T* StlAllocator< T >::allocate( size_t count )
{
    const size_t size = static_cast< size_t >( count * sizeof( T ) );
    return static_cast<T*>( PoolAllocator::Alloc( size ) );
}

template< typename T >
inline MALA_CORE_API void StlAllocator< T >::deallocate( T* ptr, size_t count )
{
    PoolAllocator::Release( ptr );
}

template< typename T >
inline MALA_CORE_API __declspec( allocator ) T* BaseAllocator< T >::allocate( size_t count )
{
    return reinterpret_cast<T*>( ::malloc( sizeof( T ) ) );
}

template< typename T >
void MALA_CORE_API BaseAllocator< T >::deallocate( T* ptr, size_t count )
{
    ::free( ptr );
}

}
