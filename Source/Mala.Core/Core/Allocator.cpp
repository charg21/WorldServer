#pragma once

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#include <Windows.h>

import Mala.Core.Types;
import Mala.Core.Allocator;
import Mala.Core.CoreGlobal;
import Mala.Core.CoreTLS;
//import Mala.Memory;

using namespace Mala::Core;

/// <summary>
/// Stomp Allocator
/// </summary>
void* StompAllocator::Alloc( i32 size )
{
	const i64 pageCount = ( size + PAGE_SIZE - 1 ) / PAGE_SIZE;
	return ::VirtualAlloc( NULL, pageCount * PAGE_SIZE, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE );
}

void StompAllocator::Release( void* ptr )
{
	const i64 address = reinterpret_cast< i64 >( ptr );
	const i64 baseAddress = address - ( address % PAGE_SIZE );

	::VirtualFree( reinterpret_cast< void* >( baseAddress ), 0, MEM_RELEASE );
}

class MemoryCacheInitializer
{
public:
	MemoryCacheInitializer()
	{
		if ( !LMemoryCache )
			LMemoryCache = new MemoryCache();
	}
};

/// <summary>
/// Pool Allocator
/// </summary>
void* PoolAllocator::Alloc( size_t size )
{
	thread_local static MemoryCacheInitializer initOncePerThread;

	return LMemoryCache->Allocate( size );
	//return GMemory->Allocate( size );
}

void PoolAllocator::Release( void* ptr )
{
	LMemoryCache->Release( ptr );
	//GMemory->Release( ptr );
}
