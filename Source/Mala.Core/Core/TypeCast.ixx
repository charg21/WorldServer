export module Mala.Core.TypeCast;

import <memory>;

import Mala.Core.Types;
import Mala.Core.TypeList;
import Mala.Core.InstrusivePtr;

using namespace Mala::Core;

export
{

#pragma region Conversion
template< typename From, typename To >
class Conversion
{
private:
	using Small = i8;
	using Big   = i32;

	static Small Test( const To& ) { return 0; }
	static Big Test( ... ) { return 0; }
	static From MakeFrom() { return 0; }

public:
	enum
	{
		exists = sizeof( Test( MakeFrom() ) ) == sizeof( Small )
	};
};
#pragma endregion

#pragma region TypeCast

template< i32 V >
struct Int2Type
{
	inline static constexpr i32 value{ V };
};

template< typename TL >
class TypeConversion
{
public:
	inline static constexpr i32 length{ Length< TL >::value };

	TypeConversion()
	{
		MakeTable( Int2Type< 0 >(), Int2Type< 0 >() );
	}

	template< i32 i, i32 j >
	static void MakeTable( Int2Type< i >, Int2Type< j > )
	{
		using FromType = typename TypeAt< TL, i >::Result;
		using ToType   = typename TypeAt< TL, j >::Result;

		s_convert[ i ][ j ] = Conversion< const FromType*, const ToType* >::exists;
		/*if ( Conversion< const FromType*, const ToType* >::exists )
			s_convert[ i ][ j ] = true;
		else
			s_convert[ i ][ j ] = false;*/

		MakeTable( Int2Type< i >(), Int2Type< j + 1 >() );
	}

	template< i32 i >
	static void MakeTable( Int2Type< i >, Int2Type< length > )
	{
		MakeTable( Int2Type< i + 1 >(), Int2Type< 0 >() );
	}

	template< i32 j >
	static void MakeTable( Int2Type< length >, Int2Type< j > )
	{
	}

	static inline bool CanConvert( i32 from, i32 to )
	{
		static TypeConversion conversion;
		return s_convert[ from ][ to ];
	}

public:
	static bool s_convert[ length ][ length ];
};

template< typename TL >
bool TypeConversion< TL >::s_convert[ length ][ length ];

template< typename To, typename From >
To* TypeCast( From* ptr )
{
	if ( !ptr )
		return nullptr;

	using TL = typename From::TL;

	if ( TypeConversion< TL >::CanConvert( ptr->_typeId, IndexOfV< TL, std::remove_pointer_t< To > > ) )
		return static_cast< To* >( ptr );

	return nullptr;
}

/// <summary>
///
/// </summary>
template< typename To, typename From >
TPtr< To > TypeCast( TPtr< From >& ptr )
{
	if ( !ptr )
		return nullptr;

	using FromTL = typename From::TL;

	if ( TypeConversion< FromTL >::CanConvert( ptr->_typeId, IndexOfV< FromTL, std::remove_pointer_t< To > > ) )
		return TPtr< To >( ptr );

	return nullptr;
}

template< typename To, typename From >
TPtr< To > TypeCast( const TPtr< From >& ptr )
{
	if ( !ptr )
		return nullptr;

	using TL = typename From::TL;

	if ( TypeConversion< TL >::CanConvert( ptr->_typeId, IndexOfV< TL, std::remove_pointer_t< To > > ) )
		return TPtr< To >( ptr );

	return nullptr;
}

/// <summary>
/// From 타입으로 부터 To타입 침습형 포인터 객체를 반환한다
/// </summary>
/// <typeparam name="To"> 목적 타입 </typeparam>
/// <typeparam name="From"> 원래 타입 </typeparam>
/// <param name="ptr"> From 타입 포인터 </param>
/// <returns> 변환이 가능한 경우 To타입 침습형 포인터, 불가능하다면 nullptr 침습형 포인터 객체 </returns>
template< typename To, typename From >
TPtr< To > TypeCast( TPtr< From >&& ptr )
{
	if ( !ptr )
		return nullptr;

	using FromTL = typename From::TL;

	if ( TypeConversion< FromTL >::CanConvert( ptr->_typeId, IndexOfV< FromTL, std::remove_pointer_t< To > > ) )
		return TPtr< To >( ptr );

	return nullptr;
}

/// <summary>
/// From 타입으로 부터 To타입 침습형 포인터 객체를 반환한다
/// </summary>
/// <typeparam name="To"> 목적 타입 </typeparam>
/// <typeparam name="From"> 원래 타입 </typeparam>
/// <param name="ptr"> From 타입 포인터 </param>
/// <returns> 변환이 가능한 경우 To타입 침습형 포인터, 불가능하다면 nullptr 침습형 포인터 객체 </returns>
template< typename To, typename From >
std::shared_ptr< To > TypeCast( std::shared_ptr< From >&& ptr )
{
	if ( !ptr )
		return nullptr;

	using FromTL = typename From::TL;

	if ( TypeConversion< FromTL >::CanConvert( ptr->_typeId, IndexOfV< FromTL, std::remove_pointer_t< To > > ) )
		return std::static_pointer_cast< To >( std::move( ptr ) );

	return nullptr;
}

/// <summary>
/// From 타입으로 부터 To타입 침습형 포인터 객체를 반환한다
/// </summary>
template< typename To, typename From >
std::shared_ptr< To > TypeCast( std::shared_ptr< From >& ptr )
{
	if ( !ptr )
		return nullptr;

	using FromTL = typename From::TL;

	if ( TypeConversion< FromTL >::CanConvert( ptr->_typeId, IndexOfV< FromTL, std::remove_pointer_t< To > > ) )
		return std::static_pointer_cast< To >( ptr );

	return nullptr;
}

/// <summary>
/// From 타입으로 부터 To타입으로 변환이 가능한지 여부를 반환한다.
/// </summary>
/// <typeparam name="To"> 목적 타입 </typeparam>
/// <typeparam name="From"> 원래 타입 </typeparam>
/// <param name="ptr"> From 타입 포인터 </param>
/// <returns> 변환 가능 여부 </returns>
template< typename To, typename From >
bool CanCast( From* ptr )
{
	if ( !ptr )
		return false;

	using TL = typename From::TL;
	return TypeConversion< TL >::CanConvert( ptr->_typeId, IndexOfV< TL, std::remove_pointer_t< To > > );
}

/// <summary>
/// From 타입으로 부터 To타입으로 변환이 가능한지 여부를 반환한다.
/// </summary>
/// <typeparam name="To"> 목적 타입 </typeparam>
/// <typeparam name="From"> 원래 타입 </typeparam>
/// <param name="ptr"> From 타입의 침습형 포인터 객체 </param>
/// <returns> 변환 가능 여부 </returns>
template< typename To, typename From >
bool CanCast( const TPtr< From >& ptr )
{
	if ( !ptr )
		return false;

	using TL = typename From::TL;

	return TypeConversion< TL >::CanConvert( ptr->_typeId, IndexOfV< TL, std::remove_pointer_t< To > > );
}

/// <summary>
/// From 타입으로 부터 To타입으로 변환이 가능한지 여부를 반환한다.
/// </summary>
template< typename To, typename From >
bool CanCast( const std::shared_ptr< From >& ptr )
{
	if ( !ptr )
		return false;

	using TL = typename From::TL;

	return TypeConversion< TL >::CanConvert( ptr->_typeId, IndexOfV< TL, std::remove_pointer_t< To > > );
}

#pragma endregion

#define DECLARE_TL		using TL = TL; i32 _typeId;
#define INIT_TL( Type )	_typeId = IndexOfV< TL, Type >;

/*

using TL = TypeList< class WorldObject, class Character >;

struct WorldObject
{
	using TL = TL;

	WorldObject()
	{
		_typeId = INIT_TL( WorldObject );
	}

	virtual ~WorldObject() {}

	i32 _typeId;
};

struct Character final : public WorldObject
{
	Character()
	{
		_typeId = INIT_TL( Character );
	}

	virtual ~Character() override final {}
};

*/

}