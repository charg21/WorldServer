export module Mala.Core.Program;

import Mala.Core.Types;

export namespace Mala::Core
{

/// <summary>
/// 프로그램 인터페이스
/// </summary>
class Program
{
public:
    Program() = default;
    virtual ~Program() = default;

    bool ParseCommands( int commandCount, char** commands );
    bool LoadConfig();
    bool Initialize();
    void Finalize();
    void Start();

    virtual bool OnLoadConfig() = 0;
    virtual void OnStart()      = 0;
    virtual bool OnInitialize() = 0;
    virtual void OnFinalize()   = 0;
};

bool Program::ParseCommands( int commandCount, char** commands )
{
    return true;
}

bool Program::LoadConfig()
{
    return OnLoadConfig();
}

bool Program::Initialize()
{
    return OnInitialize();
}

void Program::Finalize()
{
    OnFinalize();
}

void Program::Start()
{
    OnStart();
}

} // namespace Mala::Core