export module Mala.Core.TaggedPtr;

import <memory>;
import <type_traits>;

import Mala.Core.Types;

export namespace Mala::Core
{

template< class T >
struct TaggedPtr
{
    inline static constexpr size_t TAG_MAX_INDEX { sizeof( void* ) / sizeof( uint16_t ) };
    inline static constexpr size_t TAG_SELECTOR  { TAG_MAX_INDEX - 1     };
    inline static constexpr size_t PTR_MASK      { 0x0000'FFFF'FFFF'FFFF };
    inline static constexpr size_t TAG_MASK      { 0xFFFF'0000'0000'0000 };
    inline static constexpr size_t LEFT_SHIFTER  { 16                    };
    inline static constexpr size_t RIGHT_SHIFTER { 48                    };
    inline static constexpr size_t TAG_VALUE_1   { 0x0001'0000'0000'0000 };

    static_assert( TAG_SELECTOR > 0, "Beep Beep!!" );

    volatile union
    {
        T*       _rawPtr;
        size_t   _value;
        uint32_t _debugView;
        uint16_t _padAndTag[ TAG_MAX_INDEX ];
    };

    TaggedPtr()
    : _value{}
    {
    }

    TaggedPtr( std::nullptr_t )
    : _rawPtr{}
    {
    }

    TaggedPtr( T* inRawPtr )
    : _rawPtr{ inRawPtr }
    {
    }

    TaggedPtr( size_t value )
    : _value{ value }
    {
    }

    TaggedPtr( const TaggedPtr< T >& other )
    : _value{ other._value }
    {
    }

    TaggedPtr( TaggedPtr< T >&& other )
    : _value{ other._value }
    {
    }

    TaggedPtr< T >& operator=( const TaggedPtr< T >& other )
    {
        _value = other._value;

        return *this;
    }

    T* operator->()
    {
        return GetPtr();
    }

    const T* operator->() const
    {
        return GetPtr();
    }

    const T* GetPtr() const
    {
        return reinterpret_cast< T* >( _value & PTR_MASK );
    }

    T* GetPtr()
    {
        return reinterpret_cast< T* >( _value & PTR_MASK );
    }

    void SetPtr( T* inPtr )
    {
        TaggedPtr< T > ptrCapture{ _rawPtr };

        _value = ( ptrCapture._padAndTag[ TAG_SELECTOR ] ) | (size_t)( inPtr );
    }

    void SetPtr( size_t inPtr )
    {
        size_t ptrCapture{ _value };

        _value = ( ptrCapture.tag[ TAG_SELECTOR ] ) | ( inPtr );
    }

    uint16_t GetTag2()
    {
        return _padAndTag[ TAG_SELECTOR ];
    }

    void SetTag( uint16_t tag )
    {
        size_t ptrCapture{ _value };
        size_t extendedTag{ (size_t)( tag ) };

        extendedTag <<= RIGHT_SHIFTER;

        _value = extendedTag | ( ptrCapture & PTR_MASK );
    }

    TaggedPtr< T > IncreaseTag()
    {
        size_t local_taggedPtr__value{ _value };

        /*
        TaggedPtr< T >* tagged_ptr_capture = ( TaggedPtr< T >* )&local_taggedPtr__value;
        tagged_ptr_capture->ptr.pad_and_tag[ TAG_SELECTOR ] += 1;
        */

        uint16_t localTag{ (uint16_t)( local_taggedPtr__value >> RIGHT_SHIFTER ) };
        uint64_t localPtr{ local_taggedPtr__value & PTR_MASK };

        _value = ( (size_t)localTag + 1 << 48 ) | localPtr;

        return *this;
    }

    //TaggedPtr< T > IncreaseTag_atomic()
    //{
    //    return ::InterlockedAdd64( ( LONG64*)& _value, TAG_VALUE_1 );
    //}

}; // struct tag_ptr


template< typename T, typename... Args >
TaggedPtr< T > MakeTagged( Args&&... args )
{
    T* _rawPtr = new T( std::forward< Args >( args )... );
    return TaggedPtr< T >( _rawPtr );
}

}
