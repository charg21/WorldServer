export module Mala.Core.ErrorReason;

export namespace Mala::Core
{
/// <summary>
/// 코어 에러 열거형 타입
/// </summary>
enum class ErrorReason
{
    None,


    ReceivedZeroByte,
    ReceiveBufferFull,
    ReceiveBufferNull,
    SentZeroByte,
    AcceptorBindFailed,
    AcceptorListenFailed,

    Max,
};

}