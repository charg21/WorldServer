module;

#include "../CoreMacro.h";

export module Mala.Core.DateTime;

import <time.h>;
import <chrono>;
import Mala.Core.Types;
import Mala.Core.TimeSpan;
import Mala.Container.String;

export namespace Mala::Core
{

enum class EDateFormat
{
    YMDHmS, // 년월일시분초
    MDHmS,  // 월일시분초
    DHmS,   // 일시분초
    HmS,    // 시분초
};

/// <summary>
/// 시간 날짜 정보 객체
/// </summary>
struct DateTime
{
    /// <summary>
    /// 현재 시간에 대한 DateTime 객체를 반환한다
    /// </summary>
    static DateTime Now();
    //static const DateTime Today();

    /// <summary>
    /// 생성자
    /// </summary>
    DateTime() = default;

    // "YYYY-MM-DD HH:MM:SS" 형식만 지원
    DateTime( const std::string& dateString );
    DateTime( const String& dateString );
    DateTime( u64 millis );

    /// <summary>
    /// 생성자
    /// </summary>
    DateTime( i32 year, i32 month, i32 days, i32 hours, i32 mins, i32 seconds, i32 millis = 0 );

    i32 Year() const;
    i32 Month() const;
    i32 Days() const;
    i32 Hours() const;
    i32 Mins() const;
    i32 Seconds() const;
    i32 Millis() const;
    std::tuple< i32, i32, i32, i32, i32, i32, i32 > ToTuple() const;

    /// <summary>
    /// 문자열을 반환한다
    /// </summary>
    template< EDateFormat TFormat = EDateFormat::YMDHmS, typename T = String >
    auto ToString() const
    {
        if  constexpr ( std::is_same< String, T >::value )
        {
            return ToWString< TFormat >();
        }
        else
        {
            return ToAnsiString< TFormat >();
        }
    }

    /// <summary>
    /// Ansi 스트링을 문자열을 반환한다
    /// </summary>
    template< EDateFormat TFormat = EDateFormat::YMDHmS >
    std::string ToAnsiString() const
    {
        time_t sec = _millis / 1000;
        tm tm;
        _localtime64_s( &tm, &sec );

        char buf[ 20 ];

        if constexpr ( TFormat == EDateFormat::YMDHmS )
        {
            sprintf_s( buf, "%04d-%02d-%02d %02d:%02d:%02d",
                tm.tm_year + 1900,
                tm.tm_mon + 1,
                tm.tm_mday,
                tm.tm_hour,
                tm.tm_min,
                tm.tm_sec );
        }
        else if constexpr ( TFormat == EDateFormat::HmS )
        {
            sprintf_s( buf, "%02d:%02d:%02d",
                tm.tm_hour,
                tm.tm_min,
                tm.tm_sec );
        }

        return { buf, 19 };
    }

    /// <summary>
    /// 와이드 스트링을 문자열을 반환한다
    /// </summary>
    template< EDateFormat TFormat = EDateFormat::YMDHmS >
    String ToWString() const
    {
        time_t sec = _millis / 1000;
        tm tm;
        _localtime64_s( &tm, &sec );

        wchar_t buf[ 20 ];
        if constexpr ( TFormat == EDateFormat::YMDHmS )
        {
            swprintf_s( buf, L"%04d-%02d-%02d %02d:%02d:%02d",
                tm.tm_year + 1900,
                tm.tm_mon + 1,
                tm.tm_mday,
                tm.tm_hour,
                tm.tm_min,
                tm.tm_sec );
        }
        else if constexpr ( TFormat == EDateFormat::HmS )
        {
            swprintf_s( buf, L"%02d:%02d:%02d",
                tm.tm_hour,
                tm.tm_min,
                tm.tm_sec );
        }

        return { buf, 19 };
    }

    DateTime& operator+=( const TimeSpan& rhs );
    DateTime& operator+( const TimeSpan& rhs );
    DateTime& operator-=( const TimeSpan& rhs );
    DateTime& operator-( const TimeSpan& rhs );

    u64 _millis{}; // 밀리초를 나태내는 시간 구조체
    //time_t _sec; // 초를 나태내는 시간 구조체
};

const bool operator==( const DateTime& lhs, const DateTime& rhs )
{
    return lhs._millis == rhs._millis;
}

const bool operator!=( const DateTime& lhs, const DateTime& rhs )
{
    return lhs._millis != rhs._millis;
}

const bool operator<=( const DateTime& lhs, const DateTime& rhs )
{
    return lhs._millis <= rhs._millis;
}

const bool operator<( const DateTime& lhs, const DateTime& rhs )
{
    return lhs._millis < rhs._millis;
}

const bool operator>=( const DateTime& lhs, const DateTime& rhs )
{
    return lhs._millis >= rhs._millis;
}

const bool operator>( const DateTime& lhs, const DateTime& rhs )
{
    return lhs._millis > rhs._millis;
}

const TimeSpan operator-( const DateTime& lhs, const DateTime& rhs )
{
    return { ( static_cast< u64 >( lhs._millis ) - static_cast< u64 >( rhs._millis ) )
        * TimeSpan::TICK_PER_SEC };
}

const TimeSpan operator+( const DateTime& lhs, const DateTime& rhs )
{
    return { ( static_cast< u64 >( lhs._millis ) + static_cast< u64 >( rhs._millis ) )
        * TimeSpan::TICK_PER_SEC };
}

}
