import Mala.Core.Types;
import Mala.Core.TimeSpan;

using namespace Mala::Core;

/// <summary>
/// 생성자
/// </summary>
/// <param name="tick"> 시간 틱( ms ) </param>
TimeSpan::TimeSpan( const usize tick /* = 0 */ )
: _ticks{ tick }
{
}

/// <summary>
/// 생성자
/// </summary>
/// <param name="hours">   시간 </param>
/// <param name="mins">    분 </param>
/// <param name="seconds"> 초 </param>
TimeSpan::TimeSpan( i32 hours, i32 mins, i32 seconds )
{
    usize totalTick = ( hours * TICK_PER_HOUR );

    totalTick += ( mins    * TICK_PER_MIN );
    totalTick += ( seconds * TICK_PER_SEC );

    _ticks = totalTick;
}

/// <summary>
/// 생성자
/// </summary>
/// <param name="hours"> 일 </param>
/// <param name="hours"> 시간 </param>
/// <param name="mins"> 분 </param>
/// <param name="seconds"> 초 </param>
/// <param name="miliseconds"> 초 </param>
TimeSpan::TimeSpan( i32 days, i32 hours, i32 mins, i32 seconds, i32 miliseconds )
{
    usize totalTick = ( days * TICK_PER_DAY );

    totalTick += ( hours       * TICK_PER_HOUR );
    totalTick += ( mins        * TICK_PER_MIN  );
    totalTick += ( seconds     * TICK_PER_SEC  );
    totalTick += ( miliseconds * TICK_PER_MS   );

    _ticks = totalTick;
}

/// <summary>
/// 일수에 해당하는 부분을 반환한다
/// </summary>
i64 TimeSpan::GetDays() const
{
    return _ticks / TICK_PER_DAY;
}

/// <summary>
/// 시간에 해당하는 부분을 반환한다
/// </summary>
i64 TimeSpan::GetHours() const
{
    const usize hour_tick = _ticks % TICK_PER_DAY;

    return hour_tick / TICK_PER_HOUR;
}

/// <summary>
/// 분에 해당하는 부분을 반환한다
/// </summary>
i64 TimeSpan::GetMinutes() const
{
    const usize min_tick = _ticks % TICK_PER_HOUR;

    return min_tick / TICK_PER_MIN;
}

/// <summary>
/// 초에 해당하는 부분을 반환한다
/// </summary>
i64 TimeSpan::GetSeconds() const
{
    const usize sec_tick = _ticks % TICK_PER_MIN;

    return sec_tick / TICK_PER_SEC;
}

/// <summary>
/// 밀리초에 해당하는 부분을 반환한다
/// </summary>
i64 TimeSpan::GetMilliseconds() const
{
    const usize msTick = _ticks % TICK_PER_SEC;

    return msTick / TICK_PER_MS;
}

/// <summary>
/// 마이크로초에 해당하는 부분을 반환한다
/// </summary>
i64 TimeSpan::GetMicroseconds() const
{
    const usize us_tick = _ticks % TICK_PER_MS;

    return us_tick / TICK_PER_US;
}

/// <summary>
/// 나노초에 해당하는 부분을 반환한다
/// </summary>
i64 TimeSpan::GetNanoseconds() const
{
    const usize ns_tick = _ticks % TICK_PER_US;

    return ns_tick / TICK_PER_US * 100;
}

/// <summary>
/// 전체 일수를 반환한다
/// </summary>
i64 TimeSpan::GetTotalDays() const
{
    return _ticks / TICK_PER_DAY;
}

/// <summary>
/// 전체 시간을 반환한다
/// </summary>
i64 TimeSpan::GetTotalHours() const
{
    return _ticks / TICK_PER_HOUR;
}

/// <summary>
/// 전체 분을 반환한다
/// </summary>
i64 TimeSpan::GetTotalMinutes() const
{
    return _ticks / TICK_PER_MIN;
}

/// <summary>
/// 전체 초를 반환한다
/// </summary>
i64 TimeSpan::GetTotalSeconds() const
{
    return _ticks / TICK_PER_SEC;
}

/// <summary>
/// 전체 밀리초를 반환한다
/// </summary>
i64 TimeSpan::GetTotalMilliseconds() const
{
    return _ticks / TICK_PER_MS;
}

/// <summary>
/// 전체 마이크로초를 반환한다
/// </summary>
i64 TimeSpan::GetTotalMicroseconds() const
{
    return _ticks / TICK_PER_US;
}

/// <summary>
/// 전체 나노초를 반환한다
/// </summary>
i64 TimeSpan::GetTotalNanoseconds() const
{
    return _ticks / TICK_PER_US * 100;
}

i64 TimeSpan::Ticks() const
{
    return _ticks;
}

const bool TimeSpan::operator==( const TimeSpan& rhs ) const
{
    return _ticks == rhs._ticks;
}

const bool TimeSpan::operator!=( const TimeSpan& rhs ) const
{
    return _ticks != rhs._ticks;
}

const bool TimeSpan::operator<=( const TimeSpan& rhs ) const
{
    return _ticks <= rhs._ticks;
}

const bool TimeSpan::operator<( const TimeSpan& rhs ) const
{
    return _ticks < rhs._ticks;
}

const bool TimeSpan::operator>=( const TimeSpan& rhs ) const
{
    return _ticks >= rhs._ticks;
}

const bool TimeSpan::operator>( const TimeSpan& rhs ) const
{
    return _ticks > rhs._ticks;
}

TimeSpan& TimeSpan::operator+=( const TimeSpan& rhs )
{
    _ticks += rhs._ticks;

    return *this;
}

TimeSpan TimeSpan::operator+( const TimeSpan& rhs ) const
{
    return _ticks + rhs._ticks;
}

TimeSpan& TimeSpan::operator-=( const TimeSpan& rhs )
{
    _ticks -= rhs._ticks;

    return *this;
}

TimeSpan TimeSpan::operator-( const TimeSpan& rhs ) const
{
    return _ticks - rhs._ticks;
}

TimeSpan TimeSpan::FromTicks( usize ticks )
{
    return TimeSpan{ ticks };
}

TimeSpan TimeSpan::FromHours( i32 hours )
{
    return TimeSpan{ hours, 0, 0 };
}

TimeSpan TimeSpan::FromMinutes( i32 mins )
{
    return TimeSpan{ 0, mins, 0 };
}

TimeSpan TimeSpan::FromSeconds( i32 secs )
{
    return TimeSpan{ 0, 0, secs };
}
