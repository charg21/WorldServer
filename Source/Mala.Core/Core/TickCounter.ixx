module;

#include "../CoreMacro.h";

export module Mala.Core.TickCounter;

import Mala.Core.Types;

NAMESPACE_BEGIN( Mala::Core )

/*MALA_CORE_API */extern const u64 GetTick()
{
    return *reinterpret_cast< u64* >( 0x7FFE'0008 ) / 1'0000;
}

NAMESPACE_END( Mala::Core )
