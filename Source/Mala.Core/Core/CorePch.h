#pragma once

//#define USE_ASIO
#define PLATFORM_WINDOWS


#include "Types.h"
#include "CoreGlobal.h"
#include "CoreTLS.h"
#include "CoreMacro.h"
#include "CoreEnum.h"
#include "Container.h"

export import <iostream>;

#ifdef PLATFORM_WINDOWS

#include <Windows.h>
import <WinSock2.h>;
#include <mswsock.h>
#include <ws2tcpip.h>
#pragma comment(lib, "ws2_32.lib")

#endif

using namespace std;

//#include "Lock.h"
#include "ObjectPool.h"
#include "Memory.h"
#include "SendBuffer.h"
#include "AsioSession.h"
#include "AsioService.h"
#include "EndPoint.h"
#include "IocpSession.h"
#include "IocpService.h"

//#include "JobWheelTimer.h"

// #include "MathHelper.h"
//#include "BufferWriter.h"