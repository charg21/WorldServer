module;

export module Mala.Core.Defer;

#include "../CoreMacro.h";

import <memory>;

export namespace Mala::Core
{

/// <summary>
/// Go의 defer와 유사한 기능을 제공
/// 실행을 지연 시키는 객체 현재 스코프에서 정리시 실행
/// </summary>
template< std::invocable TJob >
class MALA_CORE_API Defer
{
public:
    Defer( TJob&& job )
    : _job{ std::forward< TJob >( job ) }
    {
    }

    Defer( const Defer& rhs )     = delete;
    Defer( Defer&& rhs ) noexcept = delete;

    ~Defer()
    {
        _job();
    }

private:
    TJob _job;
};

/// <summary>
/// 스레드간 작업 실행을 지연 시키는 객체
/// 최종적으로 객체가 소멸 되는 시점에 작업이 실행된다
/// </summary>
template< std::invocable TJob >
class MALA_CORE_API SharedDefer
{
public:
    SharedDefer( TJob&& job )
    : _job{ nullptr, [ job = std::forward< TJob >( job ) ]( void* p ) { job(); } }
    {
    }

private:
    std::shared_ptr< void > _job;
};

}