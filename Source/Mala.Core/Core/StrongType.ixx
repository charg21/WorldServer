module;

export module Mala.Core.StrongType;
import Mala.Core.Concepts;
import <xutility>;

export
{

struct StrongTypeTag{};
struct IStrongType{};

template< DefaultValueType T, typename TTag >
struct StrongType : public IStrongType
{
public:
    using UnderlyingType = T;
    using TagType        = TTag;

    StrongType() = default;
    constexpr StrongType( T value ) : _value{ value } {}

public:
    T _value{};
};

template< DefaultValueType T, typename TTag >
struct StrongValueType : public StrongType< T, TTag >
{
public:
    using BaseType       = StrongType< T, TTag >;
    using UnderlyingType = typename BaseType::UnderlyingType;
    using TagType        = typename BaseType::TagType;

    using BaseType::_value;

    bool operator==( StrongValueType rhs ) const
    {
        return BaseType::_value == rhs.BaseType::_value;
    }

    bool operator<( StrongValueType rhs ) const
    {
        return BaseType::_value < rhs.BaseType::_value;
    }

    bool operator<=( StrongValueType rhs ) const
    {
        return BaseType::_value <= rhs.BaseType::_value;
    }

    bool operator>( StrongValueType rhs ) const
    {
        return BaseType::_value > rhs.BaseType::_value;
    }

    bool operator>=( StrongValueType rhs ) const
    {
        return BaseType::_value >= rhs.BaseType::_value;
    }

    UnderlyingType operator*() const
    {
        return _value;
    }

};

template< DefaultValueType T, typename TTag >
bool operator==( const StrongValueType< T, TTag >& lhs, const StrongValueType< T, TTag >& rhs )
{
    return lhs._value == rhs._value;
}

template< DefaultValueType T, typename TTag >
bool operator!=( const StrongValueType< T, TTag >& lhs, const StrongValueType< T, TTag >& rhs )
{
    return lhs._value != rhs._value;
}

namespace std
{
    template< typename ValueType, typename Tag >
    struct hash< StrongValueType< ValueType, Tag > >
    {
        size_t operator()( const StrongValueType< ValueType, Tag >& rhs ) const noexcept
        {
            return std::hash< typename StrongValueType< ValueType, Tag >::UnderlyingType >{}( rhs._value );
        }
    };

    template< typename ValueType, typename Tag >
    struct equal_to< StrongValueType< ValueType, Tag > >
    {
        bool operator()( const StrongValueType< ValueType, Tag >& lhs, const StrongValueType< ValueType, Tag >& rhs ) const noexcept
        {
            return lhs._value == rhs._value;
        }
    };

} // namespace std

}
