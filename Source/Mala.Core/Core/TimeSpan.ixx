export module Mala.Core.TimeSpan;

import Mala.Core.Types;

export namespace Mala::Core
{

/// <summary>
/// 시간 간격을 나타내는 구조체
/// </summary>
struct TimeSpan
{
    static constexpr usize TICK_PER_100NS{ 1 };

    static constexpr usize TICK_PER_US   { TICK_PER_100NS * 10    };
    static constexpr usize TICK_PER_MS   { TICK_PER_US    * 1000  };
    static constexpr usize TICK_PER_SEC  { TICK_PER_MS    * 1000  };

    static constexpr usize TICK_PER_MIN  { TICK_PER_SEC  * 60 };
    static constexpr usize TICK_PER_HOUR { TICK_PER_MIN  * 60 };
    static constexpr usize TICK_PER_DAY  { TICK_PER_HOUR * 24 };


    /// <summary>
    /// 생성자
    /// </summary>
    /// <param name="tick"> 시간 틱( ms ) </param>
    TimeSpan( const usize tick = 0 );

    /// <summary>
    /// 생성자
    /// </summary>
    /// <param name="hours">   시간 </param>
    /// <param name="mins">    분 </param>
    /// <param name="seconds"> 초 </param>
    TimeSpan( i32 hours, i32 mins, i32 seconds );

    /// <summary>
    /// 생성자
    /// </summary>
    /// <param name="hours"> 일 </param>
    /// <param name="hours"> 시간 </param>
    /// <param name="mins"> 분 </param>
    /// <param name="seconds"> 초 </param>
    /// <param name="miliseconds"> 초 </param>
    TimeSpan( i32 days, i32 hours, i32 mins, i32 seconds, i32 miliseconds );

#pragma region Get Methods

    /// <summary>
    /// 일수에 해당하는 부분을 반환한다
    /// </summary>
    i64 GetDays() const;

    /// <summary>
    /// 시간에 해당하는 부분을 반환한다
    /// </summary>
    i64 GetHours() const;

    /// <summary>
    /// 분에 해당하는 부분을 반환한다
    /// </summary>
    i64 GetMinutes() const;

    /// <summary>
    /// 초에 해당하는 부분을 반환한다
    /// </summary>
    i64 GetSeconds() const;

    /// <summary>
    /// 밀리초에 해당하는 부분을 반환한다
    /// </summary>
    i64 GetMilliseconds() const;

    /// <summary>
    /// 마이크로초에 해당하는 부분을 반환한다
    /// </summary>
    i64 GetMicroseconds() const;

    /// <summary>
    /// 나노초에 해당하는 부분을 반환한다
    /// </summary>
    i64 GetNanoseconds() const;
#pragma endregion

#pragma region GetTotal Methods

    /// <summary>
    /// 전체 일수를 반환한다
    /// </summary>
    i64 GetTotalDays() const;

    /// <summary>
    /// 전체 시간을 반환한다
    /// </summary>
    i64 GetTotalHours() const;

    /// <summary>
    /// 전체 분을 반환한다
    /// </summary>
    i64 GetTotalMinutes() const;

    /// <summary>
    /// 전체 초를 반환한다
    /// </summary>
    i64 GetTotalSeconds() const;

    /// <summary>
    /// 전체 밀리초를 반환한다
    /// </summary>
    i64 GetTotalMilliseconds() const;

    /// <summary>
    /// 전체 마이크로초를 반환한다
    /// </summary>
    i64 GetTotalMicroseconds() const;

    /// <summary>
    /// 전체 나노초를 반환한다
    /// </summary>
    i64 GetTotalNanoseconds() const;

    i64 Ticks() const;

#pragma endregion

    const bool operator==( const TimeSpan& rhs ) const;
    const bool operator!=( const TimeSpan& rhs ) const;
    const bool operator<=( const TimeSpan& rhs ) const;
    const bool operator<( const TimeSpan& rhs ) const;
    const bool operator>=( const TimeSpan& rhs ) const;
    const bool operator>( const TimeSpan& rhs ) const;
    TimeSpan& operator+=( const TimeSpan& rhs );
    TimeSpan operator+( const TimeSpan& rhs ) const;
    TimeSpan& operator-=( const TimeSpan& rhs );
    TimeSpan operator-( const TimeSpan& rhs ) const;
    static TimeSpan FromTicks( usize ticks );
    static TimeSpan FromHours( i32 hours );
    static TimeSpan FromMinutes( i32 mins );
    static TimeSpan FromSeconds( i32 secs );

    usize _ticks;
};

bool operator==( const TimeSpan& lhs, const TimeSpan& rhs )
{
    return lhs._ticks == rhs._ticks;
}

bool operator!=( const TimeSpan& lhs, const TimeSpan& rhs )
{
    return lhs._ticks != rhs._ticks;
}

bool operator<=( const TimeSpan& lhs, const TimeSpan& rhs )
{
    return lhs._ticks <= rhs._ticks;
}

bool operator<( const TimeSpan& lhs, const TimeSpan& rhs )
{
    return lhs._ticks < rhs._ticks;
}

bool operator>=( const TimeSpan& lhs, const TimeSpan& rhs )
{
    return lhs._ticks >= rhs._ticks;
}

bool operator>( const TimeSpan& lhs, const TimeSpan& rhs )
{
    return lhs._ticks > rhs._ticks;
}

TimeSpan operator+( const TimeSpan& lhs, const TimeSpan& rhs )
{
    return lhs._ticks + rhs._ticks;
}

TimeSpan operator-( const TimeSpan& lhs, const TimeSpan& rhs )
{
    return lhs._ticks - rhs._ticks;
}

inline TimeSpan operator""_day( usize day )
{
    return TimeSpan{ (i32)( day ), 0, 0, 0, 0 };
}

inline TimeSpan operator""_hour( usize hour )
{
    return TimeSpan{ (i32)( hour ), 0, 0 };
}

inline TimeSpan operator""_min( usize min )
{
    return TimeSpan{ 0, (i32)( min ), 0 };
}

inline TimeSpan operator""_sec( usize sec )
{
    return TimeSpan{ 0, 0, (i32)( sec ) };
}

}

