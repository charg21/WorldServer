#include "../CoreMacro.h";

export module Mala.Core.Types;

#pragma once

export import <atomic>;
export import <chrono>;
export import <future>;
export import <functional>;
export import <memory>;
export import <string_view>;

export using namespace std::chrono_literals;

export
{

/// <summary>
/// 기본 타입 정의
/// </summary>
using i8    = char;
using i16   = short;
using i32   = int;
using i64   = long long;
using isize = long long;

using u8    = unsigned char;
using u16   = unsigned short;
using u32   = unsigned int;
using u64   = unsigned long long;
using usize = unsigned long long;

using f32 = float;
using f64 = double;

template< typename T >
using Atomic = std::atomic< T >;
using ai8    = Atomic< i8 >;
using ai16   = Atomic< i16 >;
using ai32   = Atomic< i32 >;
using ai64   = Atomic< i64 >;
using aisize = Atomic< long long >;

// byte
using BYTE = unsigned char;

/// <summary>
/// 시간 타입 정의
/// </summary>
using Hour        = std::chrono::hours;
using Minute      = std::chrono::minutes;
using Second      = std::chrono::seconds;
using Millisecond = std::chrono::milliseconds;
using Microsecond = std::chrono::microseconds;
using Nanosecond  = std::chrono::nanoseconds;

using Centimeter = Millisecond;
using Meter      = Second;

consteval auto operator""_cm( unsigned long long centimeter )
{
    return Centimeter( centimeter );
}

consteval auto operator""_m( unsigned long long meter )
{
    return Meter( meter );
}

template< typename TRet, typename... Args >
using Func = std::function< TRet( Args... ) >;
template< typename... Args >
using Action = std::function< void( Args... ) >;
template< typename... Args >
using Task = Action< Args... >;
template< typename... Args >
using Future = std::future< Args... >;

/// <summary>
/// 플래그 타입 정의
/// </summary>
using Flag = unsigned long long;

/// <summary>
/// 문자열 뷰 타입 정의
/// </summary>
using StringView    = std::wstring_view;
using StringViewRef = const StringView&;

template< typename T >
using Optional = std::optional< T >;

namespace Mala::Net
{
// TODO
USING_SHARED_PTR( AsioCore );
USING_SHARED_PTR( AsioEvent );
USING_SHARED_PTR( AsioObject );
USING_SHARED_PTR( AsioSession );
USING_SHARED_PTR( AsioService );
USING_SHARED_PTR( AsioClientService );
USING_SHARED_PTR( AsioServerService );
USING_SHARED_PTR( AsioListener );

USING_SHARED_PTR( IocpCore );
USING_SHARED_PTR( IocpSession );
USING_SHARED_PTR( IocpService );
USING_SHARED_PTR( IocpClientService );
USING_SHARED_PTR( IocpServerService );
USING_SHARED_PTR( IocpListener );
USING_SHARED_PTR( IocpJobExecutor );

USING_SHARED_PTR( RioCore );
USING_SHARED_PTR( RioSession );
USING_SHARED_PTR( RioService );
USING_SHARED_PTR( RioClientService );
USING_SHARED_PTR( RioServerService );
USING_SHARED_PTR( RioListener );


USING_SHARED_PTR( NetCore );
USING_SHARED_PTR( INetObject );
USING_SHARED_PTR( INetEvent );

USING_SHARED_PTR( SendBuffer );
USING_SHARED_PTR( SendBufferChunk );

USING_SHARED_PTR( PacketSession );
USING_SHARED_PTR( HttpSession );

}

namespace Mala::Threading
{
USING_SHARED_PTR( JobExecutor );
}

namespace Mala::Db
{
USING_SHARED_PTR( TxExecutor );
USING_SHARED_PTR( TxContext );
USING_SHARED_PTR( DbModel );
}

};

//void TestForType()
//{
//    Second      sec( 1s );
//    Millisecond msec( 1ms );
//    Microsecond usec( 1us );
//    Nanosecond  nsec( 1ns );
//    Centimeter  cem( 1cm );
//
//    msec += sec;
//    msec /= 2;
//    msec *= 50;
//    msec -= 10s;
//
//    Func< void, int > func = []( int n ){};
//    func( 10 );
//
//    Action< int, int > act = []( auto n, auto b ){};
//    act( 1, 2 );
//
//    Task< int, int > task = act;
//    task( 3, 4 );
//}
