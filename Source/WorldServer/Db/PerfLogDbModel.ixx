export module PerfLogDbModel;

/// <summary>
/// 툴에 의해 자동 생성된 코드입니다.
/// </summary>

import <string>;
import <string_view>;
import <vector>;
import <functional>;

import Mala.Core.Variant;
import Mala.Core.CoreTLS;
import Mala.Container;

import Mala.Db.DbTypes;
import Mala.Db.Table;
import Mala.Db.Column;
import Mala.Db.ColumnInfo;
import Mala.Db.IndexInfo;
import Mala.Db.DbModel;
import Mala.Db.DbModelHelper;

using namespace std;
using namespace Mala::Container;
using namespace Mala::Db;



/// <summary>
/// Db 컬럼 메타 데이터
/// </summary>
export struct PerfLogDbInfo
{
	enum EColumn
	{
		id,
		gc,
		memory,
		time,
		max,
	};
	inline static const char* columnStrings[ EColumn::max ]
	{
		"id",
		"gc",
		"memory",
		"time",
	};
	inline static const wchar_t* columnWStrings[ EColumn::max ]
	{
		L"id",
		L"gc",
		L"memory",
		L"time",
	};
	inline static constexpr Mala::Core::EVarType columnTypes[ EColumn::max ]
	{
		Mala::Core::EVarType::ULong,
		Mala::Core::EVarType::WString,
		Mala::Core::EVarType::WString,
		Mala::Core::EVarType::DateTime,
	};
	inline static Mala::Core::Variant columnDefaultValue[ EColumn::max ]
	{
		{ columnTypes[ id ] },
		{ columnTypes[ gc ] },
		{ columnTypes[ memory ] },
		{ columnTypes[ time ] },
	};
	inline static bool columnInIndex[ EColumn::max ]
	{
		true,
		false,
		false,
		false,
	};
};

/// <summary>
/// perf_log 테이블 인포 타입 정의
/// </summary>
export struct PerfLogDbTableInfo final : public Mala::Db::TableInfo
{
	using Base = Mala::Db::TableInfo;

	Func< std::shared_ptr< struct PerfLogDbModel > > Factory{ []{ return std::make_shared< struct PerfLogDbModel >(); } };

	
	/// <summary>
	/// 생성자
	/// </summary>
	PerfLogDbTableInfo()
	{
		_name  =  "perf_log";
		_nameW = L"perf_log";
		_columnInfos.reserve( PerfLogDbInfo::max );
		for ( int n = 0; n < PerfLogDbInfo::max; n += 1 )
		{
			auto* columnInfo{ _columnInfos.emplace_back( new Mala::Db::ColumnInfo( *this, n ) ) };
			columnInfo->_type  = PerfLogDbInfo::columnTypes[ n ];
			columnInfo->_name  = PerfLogDbInfo::columnStrings[ n ];
			columnInfo->_nameW = PerfLogDbInfo::columnWStrings[ n ];
			columnInfo->_isInPrimaryIndex   = PerfLogDbInfo::columnInIndex[ n ];
			columnInfo->_isInSecondaryIndex = PerfLogDbInfo::columnInIndex[ n ];
		}
		_indexs =
		{
			new Mala::Db::IndexInfo
			{
				._name {  "PRIMARY" },
				._nameW{ L"PRIMARY" },
				._mask { 0b1 },
				._columnInfos
				{
					_columnInfos[ PerfLogDbInfo::EColumn::id ],
				},
			},
		};
	}
};
#define DECLARE_COLUMN( SourceFieldName, DbFieldName ) \
struct SourceFieldName final : public NullColumn \
{ \
     using DbType = DbModelType; \
\
     inline static const char* name                 { PerfLogDbInfo::columnStrings[ DbFieldName ] };  \
     inline static const std::string_view  name_view{ name };\
     inline static const wchar_t*          nameW    { PerfLogDbInfo::columnWStrings[ DbFieldName ] };  \
     inline static const std::wstring_view nameViewW{ nameW          };  \
     inline static constexpr DbInfoType::EColumn no{ DbFieldName };  \
};
#define DECLARE_INDEX( TypeIndexName, ... )\
struct TypeIndexName final : public Mala::Db::NullIndex \
{\
     using DbType = DbModelType;\
\
    static constexpr DbInfoType::EColumn cols[]\
    {\
        __VA_ARGS__\
    };\
};

/// <summary>
/// perf_log테이블 메타 데이터 전역 객체
/// </summary>
export inline extern PerfLogDbTableInfo* GPerfLogDbTableInfo{ new PerfLogDbTableInfo() };

/// <summary>
/// perf_log테이블과 동기화 가능한 Db Orm 객체
/// </summary>
export struct PerfLogDbModel : public Mala::Db::DbModel
{
    using Base        = Mala::Db::DbModel;
    using NullColumn  = Mala::Db::NullColumn;
    using DbModelType = PerfLogDbModel;
    using DbInfoType  = PerfLogDbInfo;

    DECLARE_COLUMN( Id,     PerfLogDbInfo::EColumn::id );
    DECLARE_COLUMN( GC,     PerfLogDbInfo::EColumn::gc );
    DECLARE_COLUMN( Memory, PerfLogDbInfo::EColumn::memory );
    DECLARE_COLUMN( Time,   PerfLogDbInfo::EColumn::time );

    DECLARE_INDEX( PRIMARY, Id::no );

	/// <summary>
	/// 생성자
	/// </summary>
    PerfLogDbModel()
    : Base { *GPerfLogDbTableInfo }
    , _columns
        {
            GPerfLogDbTableInfo->_columnInfos[ PerfLogDbInfo::id ], 
            GPerfLogDbTableInfo->_columnInfos[ PerfLogDbInfo::gc ], 
            GPerfLogDbTableInfo->_columnInfos[ PerfLogDbInfo::memory ], 
            GPerfLogDbTableInfo->_columnInfos[ PerfLogDbInfo::time ], 
        }
    {
        _columnList.reserve( PerfLogDbInfo::max );
        for ( int n{}; n < PerfLogDbInfo::max; n += 1 )
            _columnList.push_back( &_columns[ n ] );
    }

	/// <summary>
	/// 소멸자
	/// </summary>
    ~PerfLogDbModel() override = default;

	/// <summary>
	/// Id를( 을 ) 반환한다
	/// </summary>
    const u64 GetId() const
    {
        return _columns[ PerfLogDbInfo::EColumn::id ]._var._ulong;
    }

	/// <summary>
	/// GC를( 을 ) 반환한다
	/// </summary>
    const String& GetGC() const
    {
        return *_columns[ PerfLogDbInfo::EColumn::gc ]._var._wstring;
    }

	/// <summary>
	/// Memory를( 을 ) 반환한다
	/// </summary>
    const String& GetMemory() const
    {
        return *_columns[ PerfLogDbInfo::EColumn::memory ]._var._wstring;
    }

	/// <summary>
	/// Time를( 을 ) 반환한다
	/// </summary>
    const DateTime GetTime() const
    {
        return _columns[ PerfLogDbInfo::EColumn::time ]._var._DateTime;
    }

	/// <summary>
	/// Id를( 을 ) 설정한다
	/// </summary>
    void SetId( const u64 id )
    {
        _columns[ PerfLogDbInfo::EColumn::id ]._var._ulong = id;
        SetUpdateColumn( PerfLogDbInfo::EColumn::id );
    }
    void SetKeyId( const u64 id )
    {
        _columns[ PerfLogDbInfo::EColumn::id ]._var._ulong = id;
        SetUpdateAndSetupKeyColumn( PerfLogDbInfo::EColumn::id );
    }

	/// <summary>
	/// GC를( 을 ) 설정한다
	/// </summary>
    void SetGC( const String& gc )
    {
        *_columns[ PerfLogDbInfo::EColumn::gc ]._var._wstring = gc;
        SetUpdateColumn( PerfLogDbInfo::EColumn::gc );
    }

	/// <summary>
	/// Memory를( 을 ) 설정한다
	/// </summary>
    void SetMemory( const String& memory )
    {
        *_columns[ PerfLogDbInfo::EColumn::memory ]._var._wstring = memory;
        SetUpdateColumn( PerfLogDbInfo::EColumn::memory );
    }

	/// <summary>
	/// Time를( 을 ) 설정한다
	/// </summary>
    void SetTime( const DateTime time )
    {
        _columns[ PerfLogDbInfo::EColumn::time ]._var._DateTime = time;
        SetUpdateColumn( PerfLogDbInfo::EColumn::time );
    }
    inline static std::shared_ptr< PerfLogDbModel > GetFactory()
    {
            return GPerfLogDbTableInfo->Factory();
    }

    template< typename T >
    inline static void SetFactory( T&& factory )
    {
            GPerfLogDbTableInfo->Factory = std::forward< T >( factory );
    }

protected:
    Mala::Db::Column _columns[ (int)( PerfLogDbInfo::EColumn::max ) ];
};
