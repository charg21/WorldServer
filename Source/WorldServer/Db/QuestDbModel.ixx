export module QuestDbModel;

/// <summary>
/// 툴에 의해 자동 생성된 코드입니다.
/// </summary>

import <string>;
import <string_view>;
import <vector>;
import <functional>;

import Mala.Core.Variant;
import Mala.Core.CoreTLS;
import Mala.Container;

import Mala.Db.DbTypes;
import Mala.Db.Table;
import Mala.Db.Column;
import Mala.Db.ColumnInfo;
import Mala.Db.IndexInfo;
import Mala.Db.DbModel;
import Mala.Db.DbModelHelper;

import CommonTypes;

using namespace std;
using namespace Mala::Container;
using namespace Mala::Db;



/// <summary>
/// Db 컬럼 메타 데이터
/// </summary>
export struct QuestDbInfo
{
    enum EColumn
    {
        owner_id,
        design_id,
        complete_count,
        max,
    };
    inline static const char* columnStrings[ EColumn::max ]
    {
        "owner_id",
        "design_id",
        "complete_count",
    };
    inline static const wchar_t* columnWStrings[ EColumn::max ]
    {
        L"owner_id",
        L"design_id",
        L"complete_count",
    };
    inline static constexpr Mala::Core::EVarType columnTypes[ EColumn::max ]
    {
        Mala::Core::EVarType::ULong,
        Mala::Core::EVarType::UInt,
        Mala::Core::EVarType::Int,
    };
    inline static Mala::Core::Variant columnDefaultValue[ EColumn::max ]
    {
        { columnTypes[ owner_id ] },
        { columnTypes[ design_id ] },
        { columnTypes[ complete_count ] },
    };
    inline static bool columnInIndex[ EColumn::max ]
    {
        true,
        true,
        false,
    };
};

/// <summary>
/// quest 테이블 인포 타입 정의
/// </summary>
export struct QuestDbTableInfo final : public TableInfo
{
    using Base = TableInfo;

    Func< std::shared_ptr< struct QuestDbModel > > Factory{ []{ return std::make_shared< struct QuestDbModel >(); } };


    /// <summary>
    /// 생성자
    /// </summary>
    QuestDbTableInfo()
    {
        _name  =  "quest";
        _nameW = L"quest";
        _columnInfos.reserve( QuestDbInfo::max );
        for ( int n = 0; n < QuestDbInfo::max; n += 1 )
        {
            auto* columnInfo{ _columnInfos.emplace_back( new ColumnInfo( *this, n ) ) };
            columnInfo->_type  = QuestDbInfo::columnTypes[ n ];
            columnInfo->_name  = QuestDbInfo::columnStrings[ n ];
            columnInfo->_nameW = QuestDbInfo::columnWStrings[ n ];
            columnInfo->_isInPrimaryIndex   = QuestDbInfo::columnInIndex[ n ];
            columnInfo->_isInSecondaryIndex = QuestDbInfo::columnInIndex[ n ];
        }
        _indexs =
        {
            new IndexInfo
            {
                ._name {  "PRIMARY" },
                ._nameW{ L"PRIMARY" },
                ._mask { 0b11 },
                ._columnInfos
                {
                    _columnInfos[ QuestDbInfo::EColumn::owner_id ],
                    _columnInfos[ QuestDbInfo::EColumn::design_id ],
                },
            },
        };
    }
};
#define DECLARE_COLUMN( SourceFieldName, DbFieldName ) \
struct SourceFieldName final : public NullColumn \
{ \
     using DbType = DbModelType; \
\
     inline static const char* name                 { QuestDbInfo::columnStrings[ DbFieldName ] };  \
     inline static const std::string_view  name_view{ name };\
     inline static const wchar_t*          nameW    { QuestDbInfo::columnWStrings[ DbFieldName ] };  \
     inline static const std::wstring_view nameViewW{ nameW          };  \
     inline static constexpr DbInfoType::EColumn no{ DbFieldName };  \
};

/// <summary>
/// quest테이블 메타 데이터 전역 객체
/// </summary>
export inline extern QuestDbTableInfo* GQuestDbTableInfo{ new QuestDbTableInfo() };

/// <summary>
/// quest테이블과 동기화 가능한 Db Orm 객체
/// </summary>
export struct QuestDbModel : public DbModel
{
    using BaseDbModel = DbModel;
    using NullColumn  = NullColumn;
    using DbModelType = QuestDbModel;
    using DbInfoType  = QuestDbInfo;

    DECLARE_COLUMN( OwnerId,       QuestDbInfo::EColumn::owner_id );
    DECLARE_COLUMN( DesignId,      QuestDbInfo::EColumn::design_id );
    DECLARE_COLUMN( CompleteCount, QuestDbInfo::EColumn::complete_count );

struct PRIMARY final : public NullIndex
{
    using DbType = DbModelType;
    using Tuple = std::tuple< u64, u32 >;
    static constexpr DbInfoType::EColumn cols[]
    {
        OwnerId::no, DesignId::no,
    };
};

    /// <summary>
    /// 생성자
    /// </summary>
    QuestDbModel()
    : BaseDbModel{ *GQuestDbTableInfo }
    , _columns
        {
            GQuestDbTableInfo->_columnInfos[ QuestDbInfo::owner_id ],
            GQuestDbTableInfo->_columnInfos[ QuestDbInfo::design_id ],
            GQuestDbTableInfo->_columnInfos[ QuestDbInfo::complete_count ],
        }
    {
        _columnList.reserve( QuestDbInfo::max );
        for ( int n{}; n < QuestDbInfo::max; n += 1 )
            _columnList.push_back( &_columns[ n ] );
    }

    /// <summary>
    /// 소멸자
    /// </summary>
    ~QuestDbModel() override = default;

    /// <summary>
    /// OwnerId를( 을 ) 반환한다
    /// </summary>
    u64 GetOwnerId() const
    {
        return _columns[ QuestDbInfo::EColumn::owner_id ]._var._ulong;
    }

    /// <summary>
    /// DesignId를( 을 ) 반환한다
    /// </summary>
    u32 GetDesignId() const
    {
        return _columns[ QuestDbInfo::EColumn::design_id ]._var._uint;
    }

    /// <summary>
    /// CompleteCount를( 을 ) 반환한다
    /// </summary>
    int GetCompleteCount() const
    {
        return _columns[ QuestDbInfo::EColumn::complete_count ]._var._int;
    }

    /// <summary>
    /// OwnerId를( 을 ) 설정한다
    /// </summary>
    void SetOwnerId( u64 owner_id )
    {
        _columns[ QuestDbInfo::EColumn::owner_id ]._var._ulong = owner_id;
        SetUpdateColumn( QuestDbInfo::EColumn::owner_id );
    }
    void SetKeyOwnerId( u64 owner_id )
    {
        _columns[ QuestDbInfo::EColumn::owner_id ]._var._ulong = owner_id;
        SetUpdateAndSetupKeyColumn( QuestDbInfo::EColumn::owner_id );
    }

    /// <summary>
    /// DesignId를( 을 ) 설정한다
    /// </summary>
    void SetDesignId( u32 design_id )
    {
        _columns[ QuestDbInfo::EColumn::design_id ]._var._uint = design_id;
        SetUpdateColumn( QuestDbInfo::EColumn::design_id );
    }
    void SetKeyDesignId( u32 design_id )
    {
        _columns[ QuestDbInfo::EColumn::design_id ]._var._uint = design_id;
        SetUpdateAndSetupKeyColumn( QuestDbInfo::EColumn::design_id );
    }

    /// <summary>
    /// CompleteCount를( 을 ) 설정한다
    /// </summary>
    void SetCompleteCount( int complete_count )
    {
        _columns[ QuestDbInfo::EColumn::complete_count ]._var._int = complete_count;
        SetUpdateColumn( QuestDbInfo::EColumn::complete_count );
    }
    inline static std::shared_ptr< QuestDbModel > GetFactory()
    {
            return GQuestDbTableInfo->Factory();
    }

    template< typename T >
    inline static void SetFactory( T&& factory )
    {
            GQuestDbTableInfo->Factory = std::forward< T >( factory );
    }

    template< std::derived_from< QuestDbModel > T >
    static DbSelectListResult< T > SelectMany( u64 owner_id )
    {
        T forSelect;
        forSelect.BindAllColumn();
        forSelect.SetKeyOwnerId( owner_id );

        return Mala::Db::SelectMany< T >( forSelect, LDbConnection );
    }
protected:
    Mala::Db::Column _columns[ (int)( QuestDbInfo::EColumn::max ) ];
};
