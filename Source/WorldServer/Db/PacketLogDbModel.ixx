export module PacketLogDbModel;

/// <summary>
/// 툴에 의해 자동 생성된 코드입니다.
/// </summary>

import <string>;
import <string_view>;
import <vector>;
import <functional>;

import Mala.Core.Variant;
import Mala.Core.CoreTLS;
import Mala.Container;

import Mala.Db.DbTypes;
import Mala.Db.Table;
import Mala.Db.Column;
import Mala.Db.ColumnInfo;
import Mala.Db.IndexInfo;
import Mala.Db.DbModel;
import Mala.Db.DbModelHelper;

using namespace std;
using namespace Mala::Container;
using namespace Mala::Db;



/// <summary>
/// Db 컬럼 메타 데이터
/// </summary>
export struct PacketLogDbInfo
{
	enum EColumn
	{
		id,
		time,
		body,
		max,
	};
	inline static const char* columnStrings[ EColumn::max ]
	{
		"id",
		"time",
		"body",
	};
	inline static const wchar_t* columnWStrings[ EColumn::max ]
	{
		L"id",
		L"time",
		L"body",
	};
	inline static constexpr Mala::Core::EVarType columnTypes[ EColumn::max ]
	{
		Mala::Core::EVarType::ULong,
		Mala::Core::EVarType::DateTime,
		Mala::Core::EVarType::WString,
	};
	inline static Mala::Core::Variant columnDefaultValue[ EColumn::max ]
	{
		{ columnTypes[ id ] },
		{ columnTypes[ time ] },
		{ columnTypes[ body ] },
	};
	inline static bool columnInIndex[ EColumn::max ]
	{
		true,
		false,
		false,
	};
};

/// <summary>
/// packet_log 테이블 인포 타입 정의
/// </summary>
export struct PacketLogDbTableInfo final : public Mala::Db::TableInfo
{
	using Base = Mala::Db::TableInfo;

	Func< std::shared_ptr< struct PacketLogDbModel > > Factory{ []{ return std::make_shared< struct PacketLogDbModel >(); } };

	
	/// <summary>
	/// 생성자
	/// </summary>
	PacketLogDbTableInfo()
	{
		_name  =  "packet_log";
		_nameW = L"packet_log";
		_columnInfos.reserve( PacketLogDbInfo::max );
		for ( int n = 0; n < PacketLogDbInfo::max; n += 1 )
		{
			auto* columnInfo{ _columnInfos.emplace_back( new Mala::Db::ColumnInfo( *this, n ) ) };
			columnInfo->_type  = PacketLogDbInfo::columnTypes[ n ];
			columnInfo->_name  = PacketLogDbInfo::columnStrings[ n ];
			columnInfo->_nameW = PacketLogDbInfo::columnWStrings[ n ];
			columnInfo->_isInPrimaryIndex   = PacketLogDbInfo::columnInIndex[ n ];
			columnInfo->_isInSecondaryIndex = PacketLogDbInfo::columnInIndex[ n ];
		}
		_indexs =
		{
			new Mala::Db::IndexInfo
			{
				._name {  "PRIMARY" },
				._nameW{ L"PRIMARY" },
				._mask { 0b1 },
				._columnInfos
				{
					_columnInfos[ PacketLogDbInfo::EColumn::id ],
				},
			},
		};
	}
};
#define DECLARE_COLUMN( SourceFieldName, DbFieldName ) \
struct SourceFieldName final : public NullColumn \
{ \
     using DbType = DbModelType; \
\
     inline static const char* name                 { PacketLogDbInfo::columnStrings[ DbFieldName ] };  \
     inline static const std::string_view  name_view{ name };\
     inline static const wchar_t*          nameW    { PacketLogDbInfo::columnWStrings[ DbFieldName ] };  \
     inline static const std::wstring_view nameViewW{ nameW          };  \
     inline static constexpr DbInfoType::EColumn no{ DbFieldName };  \
};
#define DECLARE_INDEX( TypeIndexName, ... )\
struct TypeIndexName final : public Mala::Db::NullIndex \
{\
     using DbType = DbModelType;\
\
    static constexpr DbInfoType::EColumn cols[]\
    {\
        __VA_ARGS__\
    };\
};

/// <summary>
/// packet_log테이블 메타 데이터 전역 객체
/// </summary>
export inline extern PacketLogDbTableInfo* GPacketLogDbTableInfo{ new PacketLogDbTableInfo() };

/// <summary>
/// packet_log테이블과 동기화 가능한 Db Orm 객체
/// </summary>
export struct PacketLogDbModel : public Mala::Db::DbModel
{
    using Base        = Mala::Db::DbModel;
    using NullColumn  = Mala::Db::NullColumn;
    using DbModelType = PacketLogDbModel;
    using DbInfoType  = PacketLogDbInfo;

    DECLARE_COLUMN( Id,   PacketLogDbInfo::EColumn::id );
    DECLARE_COLUMN( Time, PacketLogDbInfo::EColumn::time );
    DECLARE_COLUMN( Body, PacketLogDbInfo::EColumn::body );

    DECLARE_INDEX( PRIMARY, Id::no );

	/// <summary>
	/// 생성자
	/// </summary>
    PacketLogDbModel()
    : Base { *GPacketLogDbTableInfo }
    , _columns
        {
            GPacketLogDbTableInfo->_columnInfos[ PacketLogDbInfo::id ], 
            GPacketLogDbTableInfo->_columnInfos[ PacketLogDbInfo::time ], 
            GPacketLogDbTableInfo->_columnInfos[ PacketLogDbInfo::body ], 
        }
    {
        _columnList.reserve( PacketLogDbInfo::max );
        for ( int n{}; n < PacketLogDbInfo::max; n += 1 )
            _columnList.push_back( &_columns[ n ] );
    }

	/// <summary>
	/// 소멸자
	/// </summary>
    ~PacketLogDbModel() override = default;

	/// <summary>
	/// Id를( 을 ) 반환한다
	/// </summary>
    const u64 GetId() const
    {
        return _columns[ PacketLogDbInfo::EColumn::id ]._var._ulong;
    }

	/// <summary>
	/// Time를( 을 ) 반환한다
	/// </summary>
    const DateTime GetTime() const
    {
        return _columns[ PacketLogDbInfo::EColumn::time ]._var._DateTime;
    }

	/// <summary>
	/// Body를( 을 ) 반환한다
	/// </summary>
    const String& GetBody() const
    {
        return *_columns[ PacketLogDbInfo::EColumn::body ]._var._wstring;
    }

	/// <summary>
	/// Id를( 을 ) 설정한다
	/// </summary>
    void SetId( const u64 id )
    {
        _columns[ PacketLogDbInfo::EColumn::id ]._var._ulong = id;
        SetUpdateColumn( PacketLogDbInfo::EColumn::id );
    }
    void SetKeyId( const u64 id )
    {
        _columns[ PacketLogDbInfo::EColumn::id ]._var._ulong = id;
        SetUpdateAndSetupKeyColumn( PacketLogDbInfo::EColumn::id );
    }

	/// <summary>
	/// Time를( 을 ) 설정한다
	/// </summary>
    void SetTime( const DateTime time )
    {
        _columns[ PacketLogDbInfo::EColumn::time ]._var._DateTime = time;
        SetUpdateColumn( PacketLogDbInfo::EColumn::time );
    }

	/// <summary>
	/// Body를( 을 ) 설정한다
	/// </summary>
    void SetBody( const String& body )
    {
        *_columns[ PacketLogDbInfo::EColumn::body ]._var._wstring = body;
        SetUpdateColumn( PacketLogDbInfo::EColumn::body );
    }
    inline static std::shared_ptr< PacketLogDbModel > GetFactory()
    {
            return GPacketLogDbTableInfo->Factory();
    }

    template< typename T >
    inline static void SetFactory( T&& factory )
    {
            GPacketLogDbTableInfo->Factory = std::forward< T >( factory );
    }

protected:
    Mala::Db::Column _columns[ (int)( PacketLogDbInfo::EColumn::max ) ];
};
