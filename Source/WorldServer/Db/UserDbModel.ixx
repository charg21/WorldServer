export module UserDbModel;

/// <summary>
/// 툴에 의해 자동 생성된 코드입니다.
/// </summary>

import <string>;
import <string_view>;
import <vector>;
import <functional>;

import Mala.Core.Variant;
import Mala.Core.CoreTLS;
import Mala.Container;

import Mala.Db.DbTypes;
import Mala.Db.Table;
import Mala.Db.Column;
import Mala.Db.ColumnInfo;
import Mala.Db.IndexInfo;
import Mala.Db.DbModel;
import Mala.Db.DbModelCache;
import Mala.Db.DbModelHelper;

import CommonTypes;

using namespace std;
using namespace Mala::Container;
using namespace Mala::Db;



/// <summary>
/// Db 컬럼 메타 데이터
/// </summary>
export struct UserDbInfo
{
    enum EColumn
    {
        id,
        pc_id,
        account,
        password,
        mail,
        max,
    };
    inline static const char* columnStrings[ EColumn::max ]
    {
        "id",
        "pc_id",
        "account",
        "password",
        "mail",
    };
    inline static const wchar_t* columnWStrings[ EColumn::max ]
    {
        L"id",
        L"pc_id",
        L"account",
        L"password",
        L"mail",
    };
    inline static constexpr Mala::Core::EVarType columnTypes[ EColumn::max ]
    {
        Mala::Core::EVarType::ULong,
        Mala::Core::EVarType::ULong,
        Mala::Core::EVarType::WString,
        Mala::Core::EVarType::WString,
        Mala::Core::EVarType::WString,
    };
    inline static Mala::Core::Variant columnDefaultValue[ EColumn::max ]
    {
        { columnTypes[ id ] },
        { columnTypes[ pc_id ] },
        { columnTypes[ account ] },
        { columnTypes[ password ] },
        { columnTypes[ mail ] },
    };
    inline static bool columnInIndex[ EColumn::max ]
    {
        true,
        false,
        true,
        false,
        false,
    };
};

/// <summary>
/// user 테이블 인포 타입 정의
/// </summary>
export struct UserDbTableInfo final : public TableInfo
{
    using Base = TableInfo;

    Func< std::shared_ptr< struct UserDbModel > > Factory{ []{ return std::make_shared< struct UserDbModel >(); } };


    /// <summary>
    /// 생성자
    /// </summary>
    UserDbTableInfo()
    {
        _name  =  "user";
        _nameW = L"user";
        _columnInfos.reserve( UserDbInfo::max );
        _dbModelCache = new NullDbModelCache();
        for ( int n = 0; n < UserDbInfo::max; n += 1 )
        {
            auto* columnInfo{ _columnInfos.emplace_back( new ColumnInfo( *this, n ) ) };
            columnInfo->_type  = UserDbInfo::columnTypes[ n ];
            columnInfo->_name  = UserDbInfo::columnStrings[ n ];
            columnInfo->_nameW = UserDbInfo::columnWStrings[ n ];
            columnInfo->_isInPrimaryIndex   = UserDbInfo::columnInIndex[ n ];
            columnInfo->_isInSecondaryIndex = UserDbInfo::columnInIndex[ n ];
        }
        _indexs =
        {
            new IndexInfo
            {
                ._name {  "PRIMARY" },
                ._nameW{ L"PRIMARY" },
                ._mask { 0b1 },
                ._columnInfos
                {
                    _columnInfos[ UserDbInfo::EColumn::id ],
                },
            },
            new IndexInfo
            {
                ._name {  "IX_Account" },
                ._nameW{ L"IX_Account" },
                ._mask { 0b100 },
                ._columnInfos
                {
                    _columnInfos[ UserDbInfo::EColumn::account ],
                },
            },
        };
    }
};
#define DECLARE_COLUMN( SourceFieldName, DbFieldName ) \
struct SourceFieldName final : public NullColumn \
{ \
     using DbType = DbModelType; \
\
     inline static const char* name                 { UserDbInfo::columnStrings[ DbFieldName ] };  \
     inline static const std::string_view  name_view{ name };\
     inline static const wchar_t*          nameW    { UserDbInfo::columnWStrings[ DbFieldName ] };  \
     inline static const std::wstring_view nameViewW{ nameW          };  \
     inline static constexpr DbInfoType::EColumn no{ DbFieldName };  \
};

/// <summary>
/// user테이블 메타 데이터 전역 객체
/// </summary>
export inline extern UserDbTableInfo* GUserDbTableInfo{ new UserDbTableInfo() };

/// <summary>
/// user테이블과 동기화 가능한 Db Orm 객체
/// </summary>
export struct UserDbModel : public DbModel
{
    using BaseDbModel = DbModel;
    using NullColumn  = NullColumn;
    using DbModelType = UserDbModel;
    using DbInfoType  = UserDbInfo;

    DECLARE_COLUMN( Id,       UserDbInfo::EColumn::id );
    DECLARE_COLUMN( PcId,     UserDbInfo::EColumn::pc_id );
    DECLARE_COLUMN( Account,  UserDbInfo::EColumn::account );
    DECLARE_COLUMN( Password, UserDbInfo::EColumn::password );
    DECLARE_COLUMN( Mail,     UserDbInfo::EColumn::mail );

	struct PRIMARY final : public Mala::Db::NullIndex
	{
		using DbType = DbModelType;
		using Tuple = std::tuple< ::UserId >;
		static constexpr DbInfoType::EColumn cols[]{ Id::no };
	};
	struct IX_Account final : public Mala::Db::NullIndex
	{
		using DbType = DbModelType;
		using Tuple = std::tuple< String >;
		static constexpr DbInfoType::EColumn cols[]{ Account::no };
	};

    /// <summary>
    /// 생성자
    /// </summary>
    UserDbModel()
    : BaseDbModel{ *GUserDbTableInfo }
    , _columns
    {
            GUserDbTableInfo->_columnInfos[ UserDbInfo::id ],
            GUserDbTableInfo->_columnInfos[ UserDbInfo::pc_id ],
            GUserDbTableInfo->_columnInfos[ UserDbInfo::account ],
            GUserDbTableInfo->_columnInfos[ UserDbInfo::password ],
            GUserDbTableInfo->_columnInfos[ UserDbInfo::mail ],
        }
    {
        _columnList.reserve( UserDbInfo::max );
        for ( int n{}; n < UserDbInfo::max; n += 1 )
            _columnList.push_back( &_columns[ n ] );
    }

    /// <summary>
    /// 소멸자
    /// </summary>
    ~UserDbModel() override = default;

    /// <summary>
    /// Id를( 을 ) 반환한다
    /// </summary>
    ::UserId GetId() const
    {
        return { _columns[ UserDbInfo::EColumn::id ]._var._ulong };
    }

    /// <summary>
    /// PcId를( 을 ) 반환한다
    /// </summary>
    ::PcId GetPcId() const
    {
        return { _columns[ UserDbInfo::EColumn::pc_id ]._var._ulong };
    }

    /// <summary>
    /// Account를( 을 ) 반환한다
    /// </summary>
    const String& GetAccount() const
    {
        return *_columns[ UserDbInfo::EColumn::account ]._var._wstring;
    }

    /// <summary>
    /// Password를( 을 ) 반환한다
    /// </summary>
    const String& GetPassword() const
    {
        return *_columns[ UserDbInfo::EColumn::password ]._var._wstring;
    }

    /// <summary>
    /// Mail를( 을 ) 반환한다
    /// </summary>
    const String& GetMail() const
    {
        return *_columns[ UserDbInfo::EColumn::mail ]._var._wstring;
    }

    /// <summary>
    /// Id를( 을 ) 설정한다
    /// </summary>
    void SetId( ::UserId id )
    {
        _columns[ UserDbInfo::EColumn::id ]._var._ulong = *id;
        SetUpdateColumn( UserDbInfo::EColumn::id );
    }
    void SetKeyId( ::UserId id )
    {
        _columns[ UserDbInfo::EColumn::id ]._var._ulong = *id;
        SetUpdateAndSetupKeyColumn( UserDbInfo::EColumn::id );
    }

    /// <summary>
    /// PcId를( 을 ) 설정한다
    /// </summary>
    void SetPcId( ::PcId pc_id )
    {
        _columns[ UserDbInfo::EColumn::pc_id ]._var._ulong = *pc_id;
        SetUpdateColumn( UserDbInfo::EColumn::pc_id );
    }

    /// <summary>
    /// Account를( 을 ) 설정한다
    /// </summary>
    void SetAccount( const String& account )
    {
        *_columns[ UserDbInfo::EColumn::account ]._var._wstring = account;
        SetUpdateColumn( UserDbInfo::EColumn::account );
    }
    void SetKeyAccount( const String& account )
    {
        *_columns[ UserDbInfo::EColumn::account ]._var._wstring = account;
        SetUpdateAndSetupKeyColumn( UserDbInfo::EColumn::account );
    }

    /// <summary>
    /// Password를( 을 ) 설정한다
    /// </summary>
    void SetPassword( const String& password )
    {
        *_columns[ UserDbInfo::EColumn::password ]._var._wstring = password;
        SetUpdateColumn( UserDbInfo::EColumn::password );
    }

    /// <summary>
    /// Mail를( 을 ) 설정한다
    /// </summary>
    void SetMail( const String& mail )
    {
        *_columns[ UserDbInfo::EColumn::mail ]._var._wstring = mail;
        SetUpdateColumn( UserDbInfo::EColumn::mail );
    }
    inline static std::shared_ptr< UserDbModel > GetFactory()
    {
        return GUserDbTableInfo->Factory();
    }

    template< typename T >
    inline static void SetFactory( T&& factory )
    {
        GUserDbTableInfo->Factory = std::forward< T >( factory );
    }

    template< std::derived_from< UserDbModel > T >
    static DbSelectListResult< T > SelectAll()
    {
        T forSelect;
        forSelect.BindAllColumn();
        return Mala::Db::SelectAll< T >( forSelect, LDbConnection );
    }
protected:
    Mala::Db::Column _columns[ (int)( UserDbInfo::EColumn::max ) ];
};
