export module DbThreadManager;

import <functional>;

import Mala.Db.DbConnection;
import Mala.Threading.ThreadPool;
import Mala.Threading.JobExecutor;
import Mala.Core.CoreTLS;
import Config;
import WorldGlobal;

using namespace Mala::Db;
using namespace Mala::Threading;

export class DbThreadManager : public ThreadPool
{
public:
    /// <summary>
    /// 상위 타입 정의
    /// </summary>
    using Base = ThreadPool;

    /// <summary>
    /// Db 작업 타입 정의
    /// </summary>
    using DbJob = std::function< bool( DbConnection* ) > ;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    DbThreadManager( usize threadCapacity );

    /// <summary>
    /// 작업을 실행한다.
    /// </summary>
    template< typename TDbJob >
    void ExecuteDb( TDbJob&& job )
    {
        Base::Execute(
        [
            dbJob = std::forward< TDbJob >( job )
        ]
        {
            if ( !LDbConnection )
            {
                LDbConnection = GDbConnectionPool->GetOrDefault();
                LDbConnection->SetForeignKeyCheck( Config::UseDbForeginKeyCheck );
            }

            bool result = dbJob( LDbConnection );

            LDbConnection->Reset();

            return result;
        } );
    }
};

/// <summary>
///
/// </summary>
export inline extern DbThreadManager* GDbThreadManager{};

DbThreadManager::DbThreadManager( usize threadCapacity )
: Base{ threadCapacity }
{
}

//void DbThreadManager::ExecuteDb( DbJob&& job )
//{
//    Base::Post( [ dbJob = std::move( job ) ]
//    {
//        DbConnection* nullConnection{};
//        if ( !dbJob( nullConnection ) )
//            return;
//    } );
//}
//
//void DbThreadManager::ExecuteDb( const DbJob& job )
//{
//    Base::Post( [ dbJob = std::move( job ) ]
//    {
//        DbConnection* nullConnection{};
//        if ( !dbJob( nullConnection ) )
//            return;
//    } );
//}
