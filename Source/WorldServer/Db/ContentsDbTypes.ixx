export module ContentsDbTypes;

import <Macro.h>;

export import <memory>;
export import Mala.Core.Types;
export import Mala.Core.TypeList;

using namespace Mala::Core;

EXPORT_BEGIN

/// <summary>
/// Db객체 포인터 타입 정의
/// </summary>
USING_SHARED_PTR( ItemDbModel      );
USING_SHARED_PTR( PcDbModel        );
USING_SHARED_PTR( QuestDbModel     );
USING_SHARED_PTR( SkillSlotDbModel );
USING_SHARED_PTR( UserDbModel      );
USING_SHARED_PTR( EquipSlotDbModel );

EXPORT_END
