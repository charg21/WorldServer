export module ItemDbModel;

/// <summary>
/// 툴에 의해 자동 생성된 코드입니다.
/// </summary>

import <string>;
import <string_view>;
import <vector>;
import <functional>;

import Mala.Core.Variant;
import Mala.Core.CoreTLS;
import Mala.Container;

import Mala.Db.DbTypes;
import Mala.Db.Table;
import Mala.Db.Column;
import Mala.Db.ColumnInfo;
import Mala.Db.IndexInfo;
import Mala.Db.DbModel;
import Mala.Db.DbModelHelper;

import CommonTypes;

using namespace std;
using namespace Mala::Container;
using namespace Mala::Db;



/// <summary>
/// Db 컬럼 메타 데이터
/// </summary>
export struct ItemDbInfo
{
    enum EColumn
    {
        owner_id,
        root_owner_id,
        id,
        design_id,
        count,
        locked,
        max,
    };
    inline static const char* columnStrings[ EColumn::max ]
    {
        "owner_id",
        "root_owner_id",
        "id",
        "design_id",
        "count",
        "locked",
    };
    inline static const wchar_t* columnWStrings[ EColumn::max ]
    {
        L"owner_id",
        L"root_owner_id",
        L"id",
        L"design_id",
        L"count",
        L"locked",
    };
    inline static constexpr Mala::Core::EVarType columnTypes[ EColumn::max ]
    {
        Mala::Core::EVarType::ULong,
        Mala::Core::EVarType::ULong,
        Mala::Core::EVarType::ULong,
        Mala::Core::EVarType::UInt,
        Mala::Core::EVarType::Int,
        Mala::Core::EVarType::Bool,
    };
    inline static Mala::Core::Variant columnDefaultValue[ EColumn::max ]
    {
        { columnTypes[ owner_id ] },
        { columnTypes[ root_owner_id ] },
        { columnTypes[ id ] },
        { columnTypes[ design_id ] },
        { columnTypes[ count ] },
        { columnTypes[ locked ] },
    };
    inline static bool columnInIndex[ EColumn::max ]
    {
        true,
        true,
        true,
        false,
        false,
        false,
    };
};

/// <summary>
/// item 테이블 인포 타입 정의
/// </summary>
export struct ItemDbTableInfo final : public TableInfo
{
    using Base = TableInfo;

    Func< std::shared_ptr< struct ItemDbModel > > Factory{ []{ return std::make_shared< struct ItemDbModel >(); } };


    /// <summary>
    /// 생성자
    /// </summary>
    ItemDbTableInfo()
    {
        _name  =  "item";
        _nameW = L"item";
        _columnInfos.reserve( ItemDbInfo::max );
        for ( int n = 0; n < ItemDbInfo::max; n += 1 )
        {
            auto* columnInfo{ _columnInfos.emplace_back( new ColumnInfo( *this, n ) ) };
            columnInfo->_type  = ItemDbInfo::columnTypes[ n ];
            columnInfo->_name  = ItemDbInfo::columnStrings[ n ];
            columnInfo->_nameW = ItemDbInfo::columnWStrings[ n ];
            columnInfo->_isInPrimaryIndex   = ItemDbInfo::columnInIndex[ n ];
            columnInfo->_isInSecondaryIndex = ItemDbInfo::columnInIndex[ n ];
        }
        _indexs =
        {
            new IndexInfo
            {
                ._name {  "PRIMARY" },
                ._nameW{ L"PRIMARY" },
                ._mask { 0b101 },
                ._columnInfos
                {
                    _columnInfos[ ItemDbInfo::EColumn::owner_id ],
                    _columnInfos[ ItemDbInfo::EColumn::id ],
                },
            },
            new IndexInfo
            {
                ._name {  "IX_ROOT_OWNER_ID_AND_ID" },
                ._nameW{ L"IX_ROOT_OWNER_ID_AND_ID" },
                ._mask { 0b110 },
                ._columnInfos
                {
                    _columnInfos[ ItemDbInfo::EColumn::root_owner_id ],
                    _columnInfos[ ItemDbInfo::EColumn::id ],
                },
            },
            new IndexInfo
            {
                ._name {  "IX_ID" },
                ._nameW{ L"IX_ID" },
                ._mask { 0b100 },
                ._columnInfos
                {
                    _columnInfos[ ItemDbInfo::EColumn::id ],
                },
            },
        };
    }
};
#define DECLARE_COLUMN( SourceFieldName, DbFieldName ) \
struct SourceFieldName final : public NullColumn \
{ \
     using DbType = DbModelType; \
\
     inline static const char* name                 { ItemDbInfo::columnStrings[ DbFieldName ] };  \
     inline static const std::string_view  name_view{ name };\
     inline static const wchar_t*          nameW    { ItemDbInfo::columnWStrings[ DbFieldName ] };  \
     inline static const std::wstring_view nameViewW{ nameW          };  \
     inline static constexpr DbInfoType::EColumn no{ DbFieldName };  \
};

/// <summary>
/// item테이블 메타 데이터 전역 객체
/// </summary>
export inline extern ItemDbTableInfo* GItemDbTableInfo{ new ItemDbTableInfo() };

/// <summary>
/// item테이블과 동기화 가능한 Db Orm 객체
/// </summary>
export struct ItemDbModel : public DbModel
{
    using BaseDbModel = DbModel;
    using NullColumn  = NullColumn;
    using DbModelType = ItemDbModel;
    using DbInfoType  = ItemDbInfo;

    DECLARE_COLUMN( OwnerId,     ItemDbInfo::EColumn::owner_id );
    DECLARE_COLUMN( RootOwnerId, ItemDbInfo::EColumn::root_owner_id );
    DECLARE_COLUMN( Id,          ItemDbInfo::EColumn::id );
    DECLARE_COLUMN( DesignId,    ItemDbInfo::EColumn::design_id );
    DECLARE_COLUMN( Count,       ItemDbInfo::EColumn::count );
    DECLARE_COLUMN( Locked,      ItemDbInfo::EColumn::locked );

    struct PRIMARY final : public NullIndex
    {
        using DbType = DbModelType;
        using Tuple = std::tuple< u64, ::ItemId >;
        static constexpr DbInfoType::EColumn cols[]
        {
            OwnerId::no, Id::no,
        };
    };
    struct IX_ROOT_OWNER_ID_AND_ID final : public NullIndex
    {
        using DbType = DbModelType;
        using Tuple = std::tuple< u64, ::ItemId >;
        static constexpr DbInfoType::EColumn cols[]
        {
            RootOwnerId::no, Id::no,
        };
    };
    struct IX_ID final : public NullIndex
    {
        using DbType = DbModelType;
        using Tuple = std::tuple< ::ItemId >;
        static constexpr DbInfoType::EColumn cols[]
        {
            Id::no,
        };
    };

    /// <summary>
    /// 생성자
    /// </summary>
    ItemDbModel()
    : BaseDbModel{ *GItemDbTableInfo }
    , _columns
        {
            GItemDbTableInfo->_columnInfos[ ItemDbInfo::owner_id ],
            GItemDbTableInfo->_columnInfos[ ItemDbInfo::root_owner_id ],
            GItemDbTableInfo->_columnInfos[ ItemDbInfo::id ],
            GItemDbTableInfo->_columnInfos[ ItemDbInfo::design_id ],
            GItemDbTableInfo->_columnInfos[ ItemDbInfo::count ],
            GItemDbTableInfo->_columnInfos[ ItemDbInfo::locked ],
        }
    {
        _columnList.reserve( ItemDbInfo::max );
        for ( int n{}; n < ItemDbInfo::max; n += 1 )
            _columnList.push_back( &_columns[ n ] );
    }

    /// <summary>
    /// 소멸자
    /// </summary>
    ~ItemDbModel() override = default;

    /// <summary>
    /// OwnerId를( 을 ) 반환한다
    /// </summary>
    u64 GetOwnerId() const
    {
        return _columns[ ItemDbInfo::EColumn::owner_id ]._var._ulong;
    }

    /// <summary>
    /// RootOwnerId를( 을 ) 반환한다
    /// </summary>
    u64 GetRootOwnerId() const
    {
        return _columns[ ItemDbInfo::EColumn::root_owner_id ]._var._ulong;
    }

    /// <summary>
    /// Id를( 을 ) 반환한다
    /// </summary>
    ::ItemId GetId() const
    {
        return { _columns[ ItemDbInfo::EColumn::id ]._var._ulong };
    }

    /// <summary>
    /// DesignId를( 을 ) 반환한다
    /// </summary>
    ::ItemDesignId GetDesignId() const
    {
        return { _columns[ ItemDbInfo::EColumn::design_id ]._var._uint };
    }

    /// <summary>
    /// Count를( 을 ) 반환한다
    /// </summary>
    int GetCount() const
    {
        return _columns[ ItemDbInfo::EColumn::count ]._var._int;
    }

    /// <summary>
    /// Locked를( 을 ) 반환한다
    /// </summary>
    bool GetLocked() const
    {
        return _columns[ ItemDbInfo::EColumn::locked ]._var._bool;
    }

    /// <summary>
    /// OwnerId를( 을 ) 설정한다
    /// </summary>
    void SetOwnerId( u64 owner_id )
    {
        _columns[ ItemDbInfo::EColumn::owner_id ]._var._ulong = owner_id;
        SetUpdateColumn( ItemDbInfo::EColumn::owner_id );
    }
    void SetKeyOwnerId( u64 owner_id )
    {
        _columns[ ItemDbInfo::EColumn::owner_id ]._var._ulong = owner_id;
        SetUpdateAndSetupKeyColumn( ItemDbInfo::EColumn::owner_id );
    }

    /// <summary>
    /// RootOwnerId를( 을 ) 설정한다
    /// </summary>
    void SetRootOwnerId( u64 root_owner_id )
    {
        _columns[ ItemDbInfo::EColumn::root_owner_id ]._var._ulong = root_owner_id;
        SetUpdateColumn( ItemDbInfo::EColumn::root_owner_id );
    }
    void SetKeyRootOwnerId( u64 root_owner_id )
    {
        _columns[ ItemDbInfo::EColumn::root_owner_id ]._var._ulong = root_owner_id;
        SetUpdateAndSetupKeyColumn( ItemDbInfo::EColumn::root_owner_id );
    }

    /// <summary>
    /// Id를( 을 ) 설정한다
    /// </summary>
    void SetId( ::ItemId id )
    {
        _columns[ ItemDbInfo::EColumn::id ]._var._ulong = *id;
        SetUpdateColumn( ItemDbInfo::EColumn::id );
    }
    void SetKeyId( ::ItemId id )
    {
        _columns[ ItemDbInfo::EColumn::id ]._var._ulong = *id;
        SetUpdateAndSetupKeyColumn( ItemDbInfo::EColumn::id );
    }

    /// <summary>
    /// DesignId를( 을 ) 설정한다
    /// </summary>
    void SetDesignId( ::ItemDesignId design_id )
    {
        _columns[ ItemDbInfo::EColumn::design_id ]._var._uint = *design_id;
        SetUpdateColumn( ItemDbInfo::EColumn::design_id );
    }

    /// <summary>
    /// Count를( 을 ) 설정한다
    /// </summary>
    void SetCount( int count )
    {
        _columns[ ItemDbInfo::EColumn::count ]._var._int = count;
        SetUpdateColumn( ItemDbInfo::EColumn::count );
    }

    /// <summary>
    /// Locked를( 을 ) 설정한다
    /// </summary>
    void SetLocked( bool locked )
    {
        _columns[ ItemDbInfo::EColumn::locked ]._var._bool = locked;
        SetUpdateColumn( ItemDbInfo::EColumn::locked );
    }
    inline static std::shared_ptr< ItemDbModel > GetFactory()
    {
            return GItemDbTableInfo->Factory();
    }

    template< typename T >
    inline static void SetFactory( T&& factory )
    {
            GItemDbTableInfo->Factory = std::forward< T >( factory );
    }

    template< std::derived_from< ItemDbModel > T >
    static DbSelectListResult< T > SelectMany( u64 owner_id )
    {
        T forSelect;
        forSelect.BindAllColumn();
        forSelect.SetKeyOwnerId( owner_id );

        return Mala::Db::SelectMany< T >( forSelect, LDbConnection );
    }
    template< std::derived_from< ItemDbModel > T >
    static DbSelectListResult< T > SelectMany( u64 owner_id, u64 root_owner_id )
    {
        T forSelect;
        forSelect.BindAllColumn();
        forSelect.SetKeyOwnerId( owner_id );
        forSelect.SetKeyRootOwnerId( root_owner_id );

        return Mala::Db::SelectMany< T >( forSelect, LDbConnection );
    }
protected:
    Mala::Db::Column _columns[ (int)( ItemDbInfo::EColumn::max ) ];
};
