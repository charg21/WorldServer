export module EquipSlotDbModel;

/// <summary>
/// 툴에 의해 자동 생성된 코드입니다.
/// </summary>

import <string>;
import <string_view>;
import <vector>;
import <functional>;

import Mala.Core.Variant;
import Mala.Core.CoreTLS;
import Mala.Container;

import Mala.Db.DbTypes;
import Mala.Db.Table;
import Mala.Db.Column;
import Mala.Db.ColumnInfo;
import Mala.Db.IndexInfo;
import Mala.Db.DbModel;
import Mala.Db.DbModelHelper;

import CommonTypes;

using namespace std;
using namespace Mala::Container;
using namespace Mala::Db;



/// <summary>
/// Db 컬럼 메타 데이터
/// </summary>
export struct EquipSlotDbInfo
{
	enum EColumn
	{
		owner_id,
		slot,
		item_id,
		max,
	};
	inline static const char* columnStrings[ EColumn::max ]
	{
		"owner_id",
		"slot",
		"item_id",
	};
	inline static const wchar_t* columnWStrings[ EColumn::max ]
	{
		L"owner_id",
		L"slot",
		L"item_id",
	};
	inline static constexpr Mala::Core::EVarType columnTypes[ EColumn::max ]
	{
		Mala::Core::EVarType::ULong,
		Mala::Core::EVarType::UShort,
		Mala::Core::EVarType::ULong,
	};
	inline static Mala::Core::Variant columnDefaultValue[ EColumn::max ]
	{
		{ columnTypes[ owner_id ] },
		{ columnTypes[ slot ] },
		{ columnTypes[ item_id ] },
	};
	inline static bool columnInIndex[ EColumn::max ]
	{
		true,
		true,
		false,
	};
};

/// <summary>
/// equip_slot 테이블 인포 타입 정의
/// </summary>
export struct EquipSlotDbTableInfo final : public Mala::Db::TableInfo
{
	using Base = Mala::Db::TableInfo;

	Func< std::shared_ptr< struct EquipSlotDbModel > > Factory{ []{ return std::make_shared< struct EquipSlotDbModel >(); } };

	/// <summary>
	/// 생성자
	/// </summary>
	EquipSlotDbTableInfo()
	{
		_name  =  "equip_slot";
		_nameW = L"equip_slot";
		_columnInfos.reserve( EquipSlotDbInfo::max );
		for ( int n = 0; n < EquipSlotDbInfo::max; n += 1 )
		{
			auto* columnInfo{ _columnInfos.emplace_back( new ColumnInfo( *this, n ) ) };
			columnInfo->_type  = EquipSlotDbInfo::columnTypes[ n ];
			columnInfo->_name  = EquipSlotDbInfo::columnStrings[ n ];
			columnInfo->_nameW = EquipSlotDbInfo::columnWStrings[ n ];
			columnInfo->_isInPrimaryIndex   = EquipSlotDbInfo::columnInIndex[ n ];
			columnInfo->_isInSecondaryIndex = EquipSlotDbInfo::columnInIndex[ n ];
		}
		_indexs =
		{
			new Mala::Db::IndexInfo
			{
				._name {  "PRIMARY" },
				._nameW{ L"PRIMARY" },
				._mask { 0b11 },
				._columnInfos
				{
					_columnInfos[ EquipSlotDbInfo::EColumn::owner_id ],
					_columnInfos[ EquipSlotDbInfo::EColumn::slot ],
				},
			},
		};
	}
};
#define DECLARE_COLUMN( SourceFieldName, DbFieldName ) \
struct SourceFieldName final : public NullColumn \
{ \
     using DbType = DbModelType; \
\
     inline static const char* name                 { EquipSlotDbInfo::columnStrings[ DbFieldName ] };  \
     inline static const std::string_view  name_view{ name };\
     inline static const wchar_t*          nameW    { EquipSlotDbInfo::columnWStrings[ DbFieldName ] };  \
     inline static const std::wstring_view nameViewW{ nameW          };  \
     inline static constexpr DbInfoType::EColumn no{ DbFieldName };  \
};

/// <summary>
/// equip_slot테이블 메타 데이터 전역 객체
/// </summary>
export inline extern EquipSlotDbTableInfo* GEquipSlotDbTableInfo{ new EquipSlotDbTableInfo() };

/// <summary>
/// equip_slot테이블과 동기화 가능한 Db Orm 객체
/// </summary>
export struct EquipSlotDbModel : public Mala::Db::DbModel
{
    using BaseDbModel = Mala::Db::DbModel;
    using NullColumn  = Mala::Db::NullColumn;
    using DbModelType = EquipSlotDbModel;
    using DbInfoType  = EquipSlotDbInfo;

    DECLARE_COLUMN( OwnerId, EquipSlotDbInfo::EColumn::owner_id );
    DECLARE_COLUMN( Slot,    EquipSlotDbInfo::EColumn::slot );
    DECLARE_COLUMN( ItemId,  EquipSlotDbInfo::EColumn::item_id );

	struct PRIMARY final : public NullIndex
	{
		using DbType = DbModelType;
		using Tuple = std::tuple< ::PcId, u16 >;
		static constexpr DbInfoType::EColumn cols[]
		{
			OwnerId::no, Slot::no,
		};
	};

	/// <summary>
	/// 생성자
	/// </summary>
    EquipSlotDbModel()
    : BaseDbModel{ *GEquipSlotDbTableInfo }
    , _columns
        {
            GEquipSlotDbTableInfo->_columnInfos[ EquipSlotDbInfo::owner_id ],
            GEquipSlotDbTableInfo->_columnInfos[ EquipSlotDbInfo::slot ],
            GEquipSlotDbTableInfo->_columnInfos[ EquipSlotDbInfo::item_id ],
        }
    {
        _columnList.reserve( EquipSlotDbInfo::max );
        for ( int n{}; n < EquipSlotDbInfo::max; n += 1 )
            _columnList.push_back( &_columns[ n ] );
    }

	/// <summary>
	/// 소멸자
	/// </summary>
    ~EquipSlotDbModel() override = default;

	/// <summary>
	/// OwnerId를( 을 ) 반환한다
	/// </summary>
	::PcId GetOwnerId() const
	{
		return { _columns[ EquipSlotDbInfo::EColumn::owner_id ]._var._ulong };
	}

	/// <summary>
	/// Slot를( 을 ) 반환한다
	/// </summary>
    const u16 GetSlot() const
    {
        return _columns[ EquipSlotDbInfo::EColumn::slot ]._var._ushort;
    }

	/// <summary>
	/// ItemId를( 을 ) 반환한다
	/// </summary>
	::ItemId GetItemId() const
	{
		return { _columns[ EquipSlotDbInfo::EColumn::item_id ]._var._ulong };
	}

	/// <summary>
	/// OwnerId를( 을 ) 설정한다
	/// </summary>
	void SetOwnerId( ::PcId owner_id )
	{
		_columns[ EquipSlotDbInfo::EColumn::owner_id ]._var._ulong = *owner_id;
		SetUpdateColumn( EquipSlotDbInfo::EColumn::owner_id );
	}
	void SetKeyOwnerId( ::PcId owner_id )
	{
		_columns[ EquipSlotDbInfo::EColumn::owner_id ]._var._ulong = *owner_id;
		SetUpdateAndSetupKeyColumn( EquipSlotDbInfo::EColumn::owner_id );
	}

	/// <summary>
	/// Slot를( 을 ) 설정한다
	/// </summary>
    void SetSlot( const u16 slot )
    {
        _columns[ EquipSlotDbInfo::EColumn::slot ]._var._ushort = slot;
        SetUpdateColumn( EquipSlotDbInfo::EColumn::slot );
    }
    void SetKeySlot( const u16 slot )
    {
        _columns[ EquipSlotDbInfo::EColumn::slot ]._var._ushort = slot;
        SetUpdateAndSetupKeyColumn( EquipSlotDbInfo::EColumn::slot );
    }

	/// <summary>
	/// ItemId를( 을 ) 설정한다
	/// </summary>
	void SetItemId( ::ItemId item_id )
	{
		_columns[ EquipSlotDbInfo::EColumn::item_id ]._var._ulong = *item_id;
		SetUpdateColumn( EquipSlotDbInfo::EColumn::item_id );
	}
    inline static std::shared_ptr< EquipSlotDbModel > GetFactory()
    {
            return GEquipSlotDbTableInfo->Factory();
    }

    template< typename T >
    inline static void SetFactory( T&& factory )
    {
            GEquipSlotDbTableInfo->Factory = std::forward< T >( factory );
    }

    template< typename T > requires std::is_base_of_v< EquipSlotDbModel, T >
    static DbSelectListResult< T > SelectMany( ::PcId owner_id )
    {
        T forSelect;
        forSelect.BindAllColumn();
        forSelect.SetKeyOwnerId( owner_id );

        return Mala::Db::SelectMany< T >( forSelect, LDbConnection );
    }
protected:
    Mala::Db::Column _columns[ (int)( EquipSlotDbInfo::EColumn::max ) ];
};
