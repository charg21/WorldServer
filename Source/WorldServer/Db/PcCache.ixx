export module PcCache;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Core.StrongType;
import Mala.Container;
import Mala.Db.DbModelCache;
import Mala.Db.DbModelHelper;

import Pc;
import CommonTypes;
import User;

using namespace std;
using namespace Mala::Db;
using namespace Mala::Container;

EXPORT_BEGIN

/// <summary>
/// Pc의 Db 캐시
/// </summary>
class PcCache final : public PersistentDbModelCache< Pc, Pc::IX_ID >
{
    GENERATE_CLASS_TYPE_INFO( PcCache );

    using PcIdKey       = ToIndexKeyType< Pc, Pc::IX_ID >;
    using UserIdToPcIds = ConcurrentHashMultiMap< UserId, PcIdKey >;
    using PcIdList      = Vector< PcId >;
    using PcPtrList     = Vector< PcPtr >;

public:
    PcCache() = default;
    ~PcCache() final = default;

    void LoadFromDb() final;
    PcIdList GetPcIdList( UserId userId );
    PcPtrList GetPcPtrList( ::UserId userId );

private:
    /// <summary>
    /// UserId에 대한 PcId 테이블
    /// </summary>
    UserIdToPcIds _userIdToPcIds;
};

inline extern class PcCache* GPcCache{};

EXPORT_END
