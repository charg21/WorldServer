export module UserCache;

import <Macro.h>;
import <tuple>;

import Mala.Core.Types;
import Mala.Core.StrongType;
import Mala.Container;
import Mala.Container.String;
import Mala.Db.DbModelCache;
import Mala.Db.DbModelHelper;

import CommonTypes;
import User;
import UserDbModel;

using namespace std;
using namespace Mala::Core;
using namespace Mala::Db;
using namespace Mala::Container;

EXPORT_BEGIN

/// <summary>
/// User의 DB 캐시
/// </summary>
class UserCache final : public PersistentDbModelCache< User, User::PRIMARY >
{
    GENERATE_CLASS_TYPE_INFO( UserCache );

    using AccountKey = ToIndexKeyType< User, User::PRIMARY >;
    using Accounts = ConcurrentHashMap< String, AccountKey >;
    using UserPtrList = Vector< UserPtr >;

public:
    UserCache() = default;
    ~UserCache() final = default;

    void LoadFromDb() final;
    UserPtr GetUser( StringRef account );
    UserPtrList GetUsers( StringRef account );

    void AddAccountKey( UserRef user );

private:
	Accounts _accounts;
};

inline extern class UserCache* GUserCache{};


EXPORT_END
