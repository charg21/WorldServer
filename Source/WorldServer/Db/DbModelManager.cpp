import DbModelManager;

import Mala.Db.DbModelFactory;
import Mala.Memory;

import UserDbModel;
import PcDbModel;
import Item;
import StatDbModel;
import QuestDbModel;
import EquipSlot;
import User;
import Pc;
import PcCache;
import UserCache;

using namespace Mala::Db;

void DbModelManager::Init()
{
    Pc::SetFactory( []{ return MakeShared< Pc >(); } );
    User::SetFactory( []{ return MakeShared< User >(); } );
    Item::SetFactory( []{ return MakeShared< Item >(); } );
    StatDbModel::SetFactory( []{ return MakeShared< StatDbModel >(); } );
    QuestDbModel::SetFactory( []{ return MakeShared< QuestDbModel >(); } );
    EquipSlot::SetFactory( []{ return MakeShared< EquipSlot >(); } );

    GPcCache->LoadFromDb();
    GUserCache->LoadFromDb();
}
