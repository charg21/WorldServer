export module DbModelManager;


export import UserCache;
export import UserDbModel;
export import PcCache;
export import PcDbModel;

export class DbModelManager
{
public:
    DbModelManager() = default;
    ~DbModelManager() = default;

    void Init();
};
