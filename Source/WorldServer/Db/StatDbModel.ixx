export module StatDbModel;

/// <summary>
/// 툴에 의해 자동 생성된 코드입니다.
/// </summary>

import <string>;
import <string_view>;
import <vector>;
import <functional>;

import Mala.Core.Variant;
import Mala.Core.CoreTLS;
import Mala.Container;

import Mala.Db.DbTypes;
import Mala.Db.Table;
import Mala.Db.Column;
import Mala.Db.ColumnInfo;
import Mala.Db.IndexInfo;
import Mala.Db.DbModel;
import Mala.Db.DbModelHelper;

import CommonTypes;

using namespace std;
using namespace Mala::Container;
using namespace Mala::Db;



/// <summary>
/// Db 컬럼 메타 데이터
/// </summary>
export struct StatDbInfo
{
	enum EColumn
	{
		pc_id,
		type,
		value,
		max,
	};
	inline static const char* columnStrings[ EColumn::max ]
	{
		"pc_id",
		"type",
		"value",
	};
	inline static const wchar_t* columnWStrings[ EColumn::max ]
	{
		L"pc_id",
		L"type",
		L"value",
	};
	inline static constexpr Mala::Core::EVarType columnTypes[ EColumn::max ]
	{
		Mala::Core::EVarType::ULong,
		Mala::Core::EVarType::ULong,
		Mala::Core::EVarType::ULong,
	};
	inline static Mala::Core::Variant columnDefaultValue[ EColumn::max ]
	{
		{ columnTypes[ pc_id ] },
		{ columnTypes[ type ] },
		{ columnTypes[ value ] },
	};
	inline static bool columnInIndex[ EColumn::max ]
	{
		true,
		true,
		false,
	};
};

/// <summary>
/// stat 테이블 인포 타입 정의
/// </summary>
export struct StatDbTableInfo final : public Mala::Db::TableInfo
{
	using Base = Mala::Db::TableInfo;

	Func< std::shared_ptr< struct StatDbModel > > Factory{ []{ return std::make_shared< struct StatDbModel >(); } };


	/// <summary>
	/// 생성자
	/// </summary>
	StatDbTableInfo()
	{
		_name  =  "stat";
		_nameW = L"stat";
		_columnInfos.reserve( StatDbInfo::max );
		for ( int n = 0; n < StatDbInfo::max; n += 1 )
		{
			auto* columnInfo{ _columnInfos.emplace_back( new ColumnInfo( *this, n ) ) };
			columnInfo->_type  = StatDbInfo::columnTypes[ n ];
			columnInfo->_name  = StatDbInfo::columnStrings[ n ];
			columnInfo->_nameW = StatDbInfo::columnWStrings[ n ];
			columnInfo->_isInPrimaryIndex   = StatDbInfo::columnInIndex[ n ];
			columnInfo->_isInSecondaryIndex = StatDbInfo::columnInIndex[ n ];
		}
		_indexs =
		{
			new Mala::Db::IndexInfo
			{
				._name {  "PRIMARY" },
				._nameW{ L"PRIMARY" },
				._mask { 0b11 },
				._columnInfos
				{
					_columnInfos[ StatDbInfo::EColumn::pc_id ],
					_columnInfos[ StatDbInfo::EColumn::type ],
				},
			},
		};
	}
};
#define DECLARE_COLUMN( SourceFieldName, DbFieldName ) \
struct SourceFieldName final : public NullColumn \
{ \
     using DbType = DbModelType; \
\
     inline static const char* name                 { StatDbInfo::columnStrings[ DbFieldName ] };  \
     inline static const std::string_view  name_view{ name };\
     inline static const wchar_t*          nameW    { StatDbInfo::columnWStrings[ DbFieldName ] };  \
     inline static const std::wstring_view nameViewW{ nameW          };  \
     inline static constexpr DbInfoType::EColumn no{ DbFieldName };  \
};

/// <summary>
/// stat테이블 메타 데이터 전역 객체
/// </summary>
export inline extern StatDbTableInfo* GStatDbTableInfo{ new StatDbTableInfo() };

/// <summary>
/// stat테이블과 동기화 가능한 Db Orm 객체
/// </summary>
export struct StatDbModel : public Mala::Db::DbModel
{
	using BaseDbModel = Mala::Db::DbModel;
    using NullColumn  = Mala::Db::NullColumn;
    using DbModelType = StatDbModel;
    using DbInfoType  = StatDbInfo;

    DECLARE_COLUMN( PcId,  StatDbInfo::EColumn::pc_id );
    DECLARE_COLUMN( Type,  StatDbInfo::EColumn::type );
    DECLARE_COLUMN( Value, StatDbInfo::EColumn::value );

	struct PRIMARY final : public NullIndex
	{
		using DbType = DbModelType;
		using Tuple = std::tuple< ::PcId, u64 >;
		static constexpr DbInfoType::EColumn cols[]
		{
			PcId::no, Type::no,
		};
	};

	/// <summary>
	/// 생성자
	/// </summary>
    StatDbModel()
    : BaseDbModel{ *GStatDbTableInfo }
    , _columns
        {
            GStatDbTableInfo->_columnInfos[ StatDbInfo::pc_id ],
            GStatDbTableInfo->_columnInfos[ StatDbInfo::type ],
            GStatDbTableInfo->_columnInfos[ StatDbInfo::value ],
        }
    {
        _columnList.reserve( StatDbInfo::max );
        for ( int n{}; n < StatDbInfo::max; n += 1 )
            _columnList.push_back( &_columns[ n ] );
    }

	/// <summary>
	/// 소멸자
	/// </summary>
    ~StatDbModel() override = default;

	/// <summary>
	/// PcId를( 을 ) 반환한다
	/// </summary>
	::PcId GetPcId() const
	{
		return { _columns[ StatDbInfo::EColumn::pc_id ]._var._ulong };
	}

	/// <summary>
	/// Type를( 을 ) 반환한다
	/// </summary>
    const u64 GetType() const
    {
        return _columns[ StatDbInfo::EColumn::type ]._var._ulong;
    }

	/// <summary>
	/// Value를( 을 ) 반환한다
	/// </summary>
    const u64 GetValue() const
    {
        return _columns[ StatDbInfo::EColumn::value ]._var._ulong;
    }

	/// <summary>
	/// PcId를( 을 ) 설정한다
	/// </summary>
	void SetPcId( ::PcId pc_id )
	{
		_columns[ StatDbInfo::EColumn::pc_id ]._var._ulong = *pc_id;
		SetUpdateColumn( StatDbInfo::EColumn::pc_id );
	}
	void SetKeyPcId( ::PcId pc_id )
	{
		_columns[ StatDbInfo::EColumn::pc_id ]._var._ulong = *pc_id;
		SetUpdateAndSetupKeyColumn( StatDbInfo::EColumn::pc_id );
	}

	/// <summary>
	/// Type를( 을 ) 설정한다
	/// </summary>
    void SetType( const u64 type )
    {
        _columns[ StatDbInfo::EColumn::type ]._var._ulong = type;
        SetUpdateColumn( StatDbInfo::EColumn::type );
    }
    void SetKeyType( const u64 type )
    {
        _columns[ StatDbInfo::EColumn::type ]._var._ulong = type;
        SetUpdateAndSetupKeyColumn( StatDbInfo::EColumn::type );
    }

	/// <summary>
	/// Value를( 을 ) 설정한다
	/// </summary>
    void SetValue( const u64 value )
    {
        _columns[ StatDbInfo::EColumn::value ]._var._ulong = value;
        SetUpdateColumn( StatDbInfo::EColumn::value );
    }
    inline static std::shared_ptr< StatDbModel > GetFactory()
    {
            return GStatDbTableInfo->Factory();
    }

    template< typename T >
    inline static void SetFactory( T&& factory )
    {
            GStatDbTableInfo->Factory = std::forward< T >( factory );
    }

	template< std::derived_from< StatDbModel > T >
	static DbSelectListResult< T > SelectMany( ::PcId pc_id )
	{
		T forSelect;
		forSelect.BindAllColumn();
		forSelect.SetKeyPcId( pc_id );

		return Mala::Db::SelectMany< T >( forSelect, LDbConnection );
	}
protected:
    Mala::Db::Column _columns[ (int)( StatDbInfo::EColumn::max ) ];
};
