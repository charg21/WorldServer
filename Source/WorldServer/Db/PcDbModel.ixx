export module PcDbModel;

/// <summary>
/// 툴에 의해 자동 생성된 코드입니다.
/// </summary>

import <string>;
import <string_view>;
import <vector>;
import <functional>;

import Mala.Core.Variant;
import Mala.Core.CoreTLS;
import Mala.Core.StrongType;
import Mala.Container;

import Mala.Db.DbTypes;
import Mala.Db.Table;
import Mala.Db.Column;
import Mala.Db.ColumnInfo;
import Mala.Db.IndexInfo;
import Mala.Db.DbModel;
import Mala.Db.DbModelCache;
import Mala.Db.DbModelHelper;

export import CommonTypes;

using namespace std;
using namespace Mala::Container;
using namespace Mala::Db;



/// <summary>
/// Db 컬럼 메타 데이터
/// </summary>
export struct PcDbInfo
{
	enum EColumn
	{
		user_id,
		id,
		name,
		hp,
		x,
		y,
		z,
		max,
	};
	inline static const char* columnStrings[ EColumn::max ]
	{
		"user_id",
		"id",
		"name",
		"hp",
		"x",
		"y",
		"z",
	};
	inline static const wchar_t* columnWStrings[ EColumn::max ]
	{
		L"user_id",
		L"id",
		L"name",
		L"hp",
		L"x",
		L"y",
		L"z",
	};
	inline static constexpr Mala::Core::EVarType columnTypes[ EColumn::max ]
	{
		Mala::Core::EVarType::ULong,
		Mala::Core::EVarType::ULong,
		Mala::Core::EVarType::WString,
		Mala::Core::EVarType::Int,
		Mala::Core::EVarType::Single,
		Mala::Core::EVarType::Single,
		Mala::Core::EVarType::Single,
	};
	inline static Mala::Core::Variant columnDefaultValue[ EColumn::max ]
	{
		{ columnTypes[ user_id ] },
		{ columnTypes[ id ] },
		{ columnTypes[ name ] },
		{ columnTypes[ hp ] },
		{ columnTypes[ x ] },
		{ columnTypes[ y ] },
		{ columnTypes[ z ] },
	};
	inline static bool columnInIndex[ EColumn::max ]
	{
		true,
		true,
		false,
		false,
		false,
		false,
		false,
	};
	using TupleType = std::tuple< ::PcId, ::UserId, String, i32, f32, f32, f32 >;
};

/// <summary>
/// pc 테이블 인포 타입 정의
/// </summary>
export struct PcDbTableInfo final : public Mala::Db::TableInfo
{
	using Base = Mala::Db::TableInfo;

	Func< std::shared_ptr< struct PcDbModel > > Factory{ []{ return std::make_shared< struct PcDbModel >(); } };

	/// <summary>
	/// 생성자
	/// </summary>
	PcDbTableInfo()
	{
		_name = "pc";
		_nameW = L"pc";
		_columnInfos.reserve( PcDbInfo::max );
		for ( int n = 0; n < PcDbInfo::max; n += 1 )
		{
			auto* columnInfo{ _columnInfos.emplace_back( new ColumnInfo( *this, n ) ) };
			columnInfo->_type = PcDbInfo::columnTypes[ n ];
			columnInfo->_name = PcDbInfo::columnStrings[ n ];
			columnInfo->_nameW = PcDbInfo::columnWStrings[ n ];
			columnInfo->_isInPrimaryIndex = PcDbInfo::columnInIndex[ n ];
			columnInfo->_isInSecondaryIndex = PcDbInfo::columnInIndex[ n ];
		}
		_indexs =
		{
			new IndexInfo
			{
				._name {  "PRIMARY" },
				._nameW{ L"PRIMARY" },
				._mask { 0b11 },
				._columnInfos
				{
					_columnInfos[ PcDbInfo::EColumn::user_id ],
					_columnInfos[ PcDbInfo::EColumn::id ],
				},
			},
			new IndexInfo
			{
				._name {  "IX_ID" },
				._nameW{ L"IX_ID" },
				._mask { 0b10 },
				._columnInfos
				{
					_columnInfos[ PcDbInfo::EColumn::id ],
				},
			},
		};
		_dbModelCache = new Mala::Db::NullDbModelCache();
	}
};
#define DECLARE_COLUMN( SourceFieldName, DbFieldName ) \
struct SourceFieldName final : public NullColumn \
{ \
     using DbType = DbModelType; \
\
     inline static const char* name                 { PcDbInfo::columnStrings[ DbFieldName ] };  \
     inline static const std::string_view  name_view{ name };\
     inline static const wchar_t*          nameW    { PcDbInfo::columnWStrings[ DbFieldName ] };  \
     inline static const std::wstring_view nameViewW{ nameW          };  \
     inline static constexpr DbInfoType::EColumn no{ DbFieldName };  \
};

/// <summary>
/// pc테이블 메타 데이터 전역 객체
/// </summary>
export inline extern PcDbTableInfo* GPcDbTableInfo{ new PcDbTableInfo() };

/// <summary>
/// pc테이블과 동기화 가능한 Db Orm 객체
/// </summary>
export struct PcDbModel : public Mala::Db::DbModel
{
    using BaseDbModel = Mala::Db::DbModel;
    using NullColumn  = Mala::Db::NullColumn;
    using DbModelType = PcDbModel;
    using DbInfoType  = PcDbInfo;

	DECLARE_COLUMN( UserId, PcDbInfo::EColumn::user_id );
    DECLARE_COLUMN( Id,     PcDbInfo::EColumn::id );
    DECLARE_COLUMN( Name,   PcDbInfo::EColumn::name );
    DECLARE_COLUMN( Hp,     PcDbInfo::EColumn::hp );
    DECLARE_COLUMN( X,      PcDbInfo::EColumn::x );
    DECLARE_COLUMN( Y,      PcDbInfo::EColumn::y );
    DECLARE_COLUMN( Z,      PcDbInfo::EColumn::z );

	struct PRIMARY final : public NullIndex
	{
		using DbType = DbModelType;
		using Tuple = std::tuple< ::UserId, ::PcId >;
		static constexpr DbInfoType::EColumn cols[]
		{
			UserId::no, Id::no,
		};
	};
	struct IX_ID final : public NullIndex
	{
		using DbType = DbModelType;
		using Tuple = std::tuple< ::PcId >;
		static constexpr DbInfoType::EColumn cols[]
		{
			Id::no,
		};
	};

	/// <summary>
	/// 생성자
	/// </summary>
    PcDbModel()
    : BaseDbModel{ *GPcDbTableInfo }
    , _columns
        {
            GPcDbTableInfo->_columnInfos[ PcDbInfo::user_id ],
            GPcDbTableInfo->_columnInfos[ PcDbInfo::id ],
            GPcDbTableInfo->_columnInfos[ PcDbInfo::name ],
            GPcDbTableInfo->_columnInfos[ PcDbInfo::hp ],
            GPcDbTableInfo->_columnInfos[ PcDbInfo::x ],
            GPcDbTableInfo->_columnInfos[ PcDbInfo::y ],
            GPcDbTableInfo->_columnInfos[ PcDbInfo::z ],
        }
    {
        _columnList.reserve( PcDbInfo::max );
        for ( int n{}; n < PcDbInfo::max; n += 1 )
            _columnList.push_back( &_columns[ n ] );
    }

	/// <summary>
	/// 소멸자
	/// </summary>
    ~PcDbModel() override = default;

	/// <summary>
	/// UserId를( 을 ) 반환한다
	/// </summary>
	::UserId GetUserId() const
	{
		return { _columns[ PcDbInfo::EColumn::user_id ]._var._ulong };
	}

	/// <summary>
	/// Id를( 을 ) 반환한다
	/// </summary>
	::PcId GetId() const
	{
		return { _columns[ PcDbInfo::EColumn::id ]._var._ulong };
	}

	/// <summary>
	/// Name를( 을 ) 반환한다
	/// </summary>
    const String& GetName() const
    {
        return *_columns[ PcDbInfo::EColumn::name ]._var._wstring;
    }

	/// <summary>
	/// Hp를( 을 ) 반환한다
	/// </summary>
    const i32 GetHp() const
    {
		return _columns[ PcDbInfo::EColumn::hp ]._var._int;
    }

	/// <summary>
	/// X를( 을 ) 반환한다
	/// </summary>
    const float GetX() const
    {
        return _columns[ PcDbInfo::EColumn::x ]._var._single;
    }

	/// <summary>
	/// Y를( 을 ) 반환한다
	/// </summary>
    const float GetY() const
    {
        return _columns[ PcDbInfo::EColumn::y ]._var._single;
    }

	/// <summary>
	/// Z를( 을 ) 반환한다
	/// </summary>
    const float GetZ() const
    {
        return _columns[ PcDbInfo::EColumn::z ]._var._single;
    }

	/// <summary>
	/// UserId를( 을 ) 설정한다
	/// </summary>
    void SetUserId( ::UserId user_id )
    {
        _columns[ PcDbInfo::EColumn::user_id ]._var._ulong = *user_id;
        SetUpdateColumn( PcDbInfo::EColumn::user_id );
    }
    void SetKeyUserId( ::UserId user_id )
    {
        _columns[ PcDbInfo::EColumn::user_id ]._var._ulong = *user_id;
        SetUpdateAndSetupKeyColumn( PcDbInfo::EColumn::user_id );
    }

	/// <summary>
	/// Id를( 을 ) 설정한다
	/// </summary>
    void SetId( ::PcId id )
    {
        _columns[ PcDbInfo::EColumn::id ]._var._ulong = *id;
        SetUpdateColumn( PcDbInfo::EColumn::id );
    }
    void SetKeyId( ::PcId id )
    {
        _columns[ PcDbInfo::EColumn::id ]._var._ulong = *id;
        SetUpdateAndSetupKeyColumn( PcDbInfo::EColumn::id );
    }

	/// <summary>
	/// Name를( 을 ) 설정한다
	/// </summary>
    void SetName( const String& name )
    {
        *_columns[ PcDbInfo::EColumn::name ]._var._wstring = name;
        SetUpdateColumn( PcDbInfo::EColumn::name );
    }

	/// <summary>
	/// Hp를( 을 ) 설정한다
	/// </summary>
    void SetHp( const i32 hp )
    {
        _columns[ PcDbInfo::EColumn::hp ]._var._int = hp;
        SetUpdateColumn( PcDbInfo::EColumn::hp );
    }

	/// <summary>
	/// X를( 을 ) 설정한다
	/// </summary>
    void SetX( const float x )
    {
        _columns[ PcDbInfo::EColumn::x ]._var._single = x;
        SetUpdateColumn( PcDbInfo::EColumn::x );
    }

	/// <summary>
	/// Y를( 을 ) 설정한다
	/// </summary>
    void SetY( const float y )
    {
        _columns[ PcDbInfo::EColumn::y ]._var._single = y;
        SetUpdateColumn( PcDbInfo::EColumn::y );
    }

	/// <summary>
	/// Z를( 을 ) 설정한다
	/// </summary>
    void SetZ( const float z )
    {
        _columns[ PcDbInfo::EColumn::z ]._var._single = z;
        SetUpdateColumn( PcDbInfo::EColumn::z );
    }
    inline static std::shared_ptr< PcDbModel > GetFactory()
    {
            return GPcDbTableInfo->Factory();
    }

    template< typename T >
    inline static void SetFactory( T&& factory )
    {
            GPcDbTableInfo->Factory = std::forward< T >( factory );
    }

	template< std::derived_from< PcDbModel > T >
    static DbSelectListResult< T > SelectMany( ::UserId user_id )
    {
        T forSelect;
        forSelect.BindAllColumn();
        forSelect.SetKeyUserId( user_id );

        return Mala::Db::SelectMany< T >( forSelect, LDbConnection );
    }

	template< std::derived_from< PcDbModel > T >
    static DbSelectListResult< T > SelectAll()
	{
		T forSelect;
		forSelect.BindAllColumn();

		return Mala::Db::SelectAll< T >( forSelect, LDbConnection );
	}

protected:
    Mala::Db::Column _columns[ (int)( PcDbInfo::EColumn::max ) ];
};
