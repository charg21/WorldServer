#include "Macro.h"

import Mala.Core.Crash;
import Mala.Core.Types;
import Mala.Container;
import Mala.Db.DbModelHelper;

import Pc;
import PcCache;

import User;

import CommonTypes;

using namespace Mala::Core;
using namespace Mala::Db;

void PcCache::LoadFromDb()
{
    auto allPcList{ Pc::SelectAll< Pc >() };
    CRASH_IF_NULL( allPcList );

    for ( auto& pc : *allPcList )
    {
        _userIdToPcIds.insert( { pc->GetUserId(), pc->GetIndexKey< Pc::IX_ID >() } );
        Add( std::move( pc ) );
    }
}

PcCache::PcIdList PcCache::GetPcIdList( UserId userId )
{
    auto [ begin, end ]{ _userIdToPcIds.equal_range( userId ) };

    PcIdList pcIds;
    pcIds.reserve( std::distance( begin, end ) );

    for ( ; begin != end; ++begin )
    {
        pcIds.emplace_back( (u64)std::get< 0 >( begin->second ) );
    }

    return pcIds;
}

PcCache::PcPtrList PcCache::GetPcPtrList( ::UserId userId )
{
    auto [ begin, end ]{ _userIdToPcIds.equal_range( userId ) };

    PcPtrList pcPtrs;
    pcPtrs.reserve( std::distance( begin, end ) );

    for ( ; begin != end; ++begin )
    {
        pcPtrs.emplace_back( GetModel( begin->second ) );
    }

    return pcPtrs;
}
