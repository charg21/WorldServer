#include "Macro.h"

import Mala.Core.Variant;
import Mala.Core.Types;
import Mala.Container;
import Mala.Container.String;
import Mala.Db.DbModelCache;
import Mala.Db.DbModelHelper;

import User;
import UserCache;
import UserDbModel;

using namespace Mala::Core;
using namespace Mala::Db;
using namespace Mala::Container;

void UserCache::LoadFromDb()
{
    auto allUserList{ User::SelectAll< User >() };
    CRASH_IF_NULL( allUserList );

    for ( auto& user : *allUserList )
    {
        AddAccountKey( user );
        Add( std::move( user ) );
    }
}

UserPtr UserCache::GetUser( StringRef account )
{
    //auto [ begin, end ]{ _accounts.equal_range( account ) };
    //for ( auto it{ begin }; it != end; ++it )
    {
        //if ( auto user{ GetModel( it->second ) } )
        //	return std::move( user );
    }

    return {};
}

UserCache::UserPtrList UserCache::GetUsers( StringRef account )
{
    UserCache::UserPtrList userPtrList;
    auto [ begin, end ]{ _accounts.equal_range( account ) };
    for ( auto it{ begin }; it != end; ++it )
    {
        userPtrList.push_back( GetModel( it->second ) );
    }

    return std::move( userPtrList );
}

void UserCache::AddAccountKey( UserRef user )
{
    _accounts.insert( { user->GetAccount(), user->GetIndexKey< User::PRIMARY >() } );
}
