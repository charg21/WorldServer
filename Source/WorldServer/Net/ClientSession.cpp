import <Macro.h>;

import Mala.Core.Types;
import Mala.Net.PacketHeader;
import Mala.Net.PacketDispatcher;
import Mala.Log;
import Mala.Log.ConsoleLog;

import ClientSession;
import ClientPacketDispatcher;
import WorldObjectTypes;
import WorldObject;
import Pc;

using namespace Mala::Net;


void ClientSession::SetPc( PcPtr&& pc )
{
    _pc = pc;
    _pc->SetSession( GetClientSession() );
}

void ClientSession::OnRecvPacket( BYTE* buffer, i32 len )
{
    auto header = reinterpret_cast< PacketHeader* >( buffer );
    auto type = header->_type;
    ClientPacketDispatcher::Table[ type ]( this, buffer, len );
}

/// <summary>
///
/// </summary>
void ClientSession::OnDisconnected( /*reaseon*/ )
{
    PcPtr pcCopy = _pc;
    if ( !pcCopy )
        return;

    _pc->LeaveWorld();
    LOG( L"Leave WorldObject Id {}", *pcCopy->GetObjectId() );
    _pc->Post( [ pc = std::move( pcCopy ) ]()
    {
        pc->_session = nullptr;
    } );

    _pc   = nullptr;
    _user = nullptr;
}
