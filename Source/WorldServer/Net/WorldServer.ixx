export module WorldServer;

import <Macro.h>;

import <memory>;

import Mala.Core.Types;
import Mala.Container.String;
import Mala.Reflection;
import Mala.Net.EndPoint;
import Mala.Net;
import Mala.Threading.ThreadManager;

import ClientSession;

using namespace Mala::Net;

EXPORT_BEGIN

using ICore    = NetCore;
using ICorePtr = std::shared_ptr< ICore >;
using ICoreRef = const ICorePtr&;

/// <summary>
/// 게임서버 클래스
/// </summary>
class WorldServer final : public NetServerService
{
    GENERATE_CLASS_TYPE_INFO( WorldServer );

public:
    /// <summary>
    ///
    /// </summary>
    using ServerService = NetServerService;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    WorldServer(
        const EndPoint&         address,
              ICoreRef          core,
              NetSessionFactory factory,
              i32               maxSessionCount );

    virtual ~WorldServer() final;

    /// <summary>
    /// Initialize() 호출시 호출되는 콜백
    /// </summary>
    bool Initialize();

    /// <summary>
    /// Finailize() 호출시 호출되는 콜백
    /// </summary>
    void Finalize() {};

    /// <summary>
    /// Start() 호출시 호출되는 콜백
    /// </summary>
    bool Start();

    /// <summary>
    /// LoadConfig() 호출시 호출되는 콜백
    /// </summary>
    virtual bool LoadConfig( StringRef configName );
    virtual void OnError( u64 id ) {}

    /// <summary>
    /// 퍼포먼스 로그를 등록한다.
    /// </summary>
    static void RegisterPrefLog();

private:
    /// <summary>
    /// 세션을 초기화한다
    /// </summary>
    void InitializeSessions();

    /// <summary>
    /// 스레드를 초기화한다
    /// </summary>
    void InitiThreads();

    /// <summary>
    /// 월드를 초기화한다
    /// </summary>
    void InitializeWorld();

};

class WorldThreadManager final : public Mala::Threading::ThreadManager
{
    bool Init() final;
};


EXPORT_END
