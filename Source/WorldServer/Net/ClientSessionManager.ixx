export module ClientSessionManager;

import <Macro.h>;
import <memory>;

import Mala.Core.Types;
import Mala.Container;

import ClientSession;

using namespace Mala::Container;

EXPORT_BEGIN


using SendRequestList = Deque< std::shared_ptr< ClientSession > >;

/// <summary>
/// 클라이언트 세션 관리자
/// </summary>
class ClientSessionManager
{
    using ClinetSessionMap = HashMap< int, ClientSession* >;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    ClientSessionManager() = default;

    void AddSession( ClientSession* session )
    {
        _sessionMap.emplace( 1, session );
    }

    void RemoveSession( ClientSession* session )
    {
        _sessionMap.erase( 1 );
    }

private:
    ClinetSessionMap _sessionMap;
};

// inline extern std::unique_ptr< ClientSessionManager > GClientSessionManager{};

EXPORT_END
