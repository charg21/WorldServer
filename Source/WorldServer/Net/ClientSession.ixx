export module ClientSession;

import <Macro.h>;

import Mala.Container;
import Mala.Core.Types;
import Mala.Memory;
import Mala.Net;
import Mala.Net.PacketSession;
import Mala.Net.DisconnectReason;
import Mala.Net.PacketHeader;
import Mala.Threading.Job;
import Mala.Threading.JobExecutor;

import WorldObjectTypes;
import ThreadLocal;
import User;
import Pc;
import ELogicResult;

using namespace Mala::Core;
using namespace Mala::Container;
using namespace Mala::Threading;
using namespace Mala::Net;

USING_SHARED_PTR( ClientSession );

/// <summary>
/// 클라이언트 세션 객체
/// </summary>
export class ClientSession final : public PacketSession
{
public:
    using PacketSession::Send;

    ClientSession() = default;
    virtual ~ClientSession() override = default;

    template< typename S_Packet >
    void Send( ELogicResult result );

    PcPtr GetPc() { return _pc; }
    UserPtr GetUser() { return _user; }
    i64 GetId() { return _id; }
    ClientSessionPtr GetClientSession(){ return std::static_pointer_cast< ClientSession >( shared_from_this() ); }

    void SetPc( PcPtr&& pc );
    void SetUser( UserRef user ) { _user = user; }
    void SetId( i64 id ) { _id = id; }

    static ClientSessionPtr Generate()
    {
        static std::atomic< int > sessionIdIssuer = 0;
        auto session = MakeShared< ClientSession >(); // AdminSession
        session->_id = sessionIdIssuer.fetch_add( 1 );
        return std::move( session );
    }

private:
    virtual void OnRecvPacket( BYTE* buffer, i32 len ) final;
    virtual void OnDisconnected() final;

public:
    /// <summary> 식별자 </summary>
    i64 _id{};

    /// <summary> PC </summary>
    PcPtr _pc;

    /// <summary> 유저 </summary>
    UserPtr _user;
};

/// <summary>
/// 송신한다
/// </summary>
template< typename S_Packet >
void ClientSession::Send( ELogicResult result )
{
    Send( S_Packet{ ._result = result }.Write() );
}


