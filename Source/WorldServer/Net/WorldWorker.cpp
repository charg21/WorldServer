import <Macro.h>;

import Mala.Core.CoreGlobal;
import Mala.Core.CoreTLS;
import Mala.Core.Format;
import Mala.Core.TickCounter;
import Mala.Memory;
import Mala.Net;
import Mala.Threading;
import Mala.Threading.ThreadManager;
import Mala.Windows;

import WorldObjectManager;
import AoiManager;
import ClusterManager;
import Config;
import ThreadLocal;
import World;
import WorldGlobal;
import WorldServer;
import WorkerThread;
import WorkerThreadExecutor;


using namespace Mala::Core;
using namespace Mala::Threading;
using namespace Mala::Windows;

enum
{
    WORKER_TICK = 16,
};

void WorkerThread::DoInit()
{
    LThreadType = EThreadType::Worker;
    //LThreadId = GThreadManager->GenerateThreadId();
    LLastTick   = GetTick();
    LExecutor   = std::make_shared< WorkerThreadExecutor >();

    NEW( LSendRequestSessionList );
    NEW( LClusterManager );
    NEW( LAoiManager );
    NEW( LWorldObjectManager );

    LClusterManager->Init();
    LAoiManager->Init();
    LDbConnection = GDbConnectionPool->Get();
}

void WorkerThread::DoWorkerJob()
{
    auto core = GWorldServer->GetCore();
    INFO_LOG( L"[DoWorkerJob] %ws", GetName().c_str() );
    SetThisThreadDesc( GetName() );

    LOOP
    {
        LEndTickCount = GetTick() + WORKER_TICK;

        core->Dispatch( WORKER_TICK );

        LLastTick = GetTick();

        // 예약된 일감 처리
        ThreadManager::DistibuteReservedJobs();

        // 글로벌 큐 작업 분배
        ThreadManager::DoGlobalQueueWork();
        DoGlobalExecutor();
        DoSendJob();

        //if ( LMemoryCache )
        //    LMemoryCache->Handover();
    }
}

void WorkerThread::DoGlobalExecutor()
{
    GGlobalJobExecutor->Flush();
}

void WorkerThread::DoSendJob()
{
    //while ( !LSendRequestSessionList->empty() )
    //{
    //    //auto& session = LSendRequestSessionList->back();
    //    //if ( session )
    //    //{
    //    //    session->FlushSendQueue();
    //    //}

    //    //LSendRequestSessionList->pop_back();
    //};
}

String WorkerThread::GetName()
{
    return xformat( L"WorkerThread [{}]", LThreadId );
}

/// <summary>
/// Tls를 정리한다
/// </summary>
void WorkerThread::Destory()
{
    //delete LSendRequestSessionList;
    //delete LJobTimer;
    //delete LClusterManager;
}
