export module WorkerThread;

import Mala.Core;
import Mala.Container.String;


/// <summary>
/// 작업 스레드
/// </summary>
export class WorkerThread
{
public:
	static void DoInit();
	static void DoWorkerJob();
	static void DoGlobalExecutor();
	static void DoSendJob();
	static void Destory();

	static String GetName();
};