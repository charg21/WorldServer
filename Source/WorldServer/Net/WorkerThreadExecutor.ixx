export module WorkerThreadExecutor;

import Mala.Core;
import Mala.Threading.JobExecutor;

using namespace Mala::Threading;

export class WorkerThreadExecutor final
	: public std::enable_shared_from_this< WorkerThreadExecutor >
	, public Mala::Threading::JobExecutor
{
public:
	WorkerThreadExecutor() = default;
	~WorkerThreadExecutor() final = default;

	std::shared_ptr< JobExecutor > SharedFromJobExecutor() final
	{
		return shared_from_this();
	}

	std::weak_ptr< JobExecutor > WeakFromJobExecutor() final
	{
		return shared_from_this();
	}
};
