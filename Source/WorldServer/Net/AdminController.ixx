export module AdminController;

import <Macro.h>;

import Mala.Core;
import Mala.Net.HttpSession;
import Mala.Http;
import Mala.Reflection;

using namespace Mala::Core;
using namespace Mala::Net;
using namespace Mala::Http;

//USING_SHARED_PTR( AdminSession );

/// <summary>
/// 어드민 웹 세션
/// </summary>
export class AdminSession : public HttpSession
{
public:
    i32 _id{};
    i32 _port{};
};

/// <summary>
/// 어드민 웹 컨트롤러
/// </summary>
export class AdminController : public HttpController
{
    GENERATE_CLASS_TYPE_INFO( AdminController );

public:
    AdminController() = default;
    ~AdminController() override final = default;

    METHOD( DoA )
    static void DoA( HttpSessionRef httpSession, HttpRequestRef request )
    {
        ERROR_LOG( L"DoA 이게 안되네;" );
    }

    METHOD( Handle )
    void Handle( HttpSessionRef httpSession, HttpRequestRef request ) override final
    {
        ERROR_LOG( L"Handle Why.. ;;" );
    }
};


/*
AdminController controller;
controller.Initialize();
controller.GetRoutingTable()[ "DoA" ]( nullptr, "" );
controller.GetRoutingTable()[ "Handle" ]( nullptr, "" );
*/