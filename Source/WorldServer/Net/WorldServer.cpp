#include <time.h>
import <iostream>;
import <thread>;
import <memory>;
import <functional>;

import <Macro.h>;

import Mala.Windows;
import Mala.Log.ConsoleLog;
import Mala.Core.Crash;
import Mala.Core.CoreGlobal;
import Mala.Core.CoreTLS;
import Mala.Core.IniParser;
import Mala.Core.Variant;
import Mala.Core.Types;
import Mala.Core.Format;
import Mala.Db.DbConnection;
import Mala.Db.TxContext;
import Mala.Db.TxExecutor;
import Mala.Diagnostics.PerformanceCounter;
import Mala.Net;
import Mala.Memory;
import Mala.Threading.ThreadManager;
import Mala.Log;

import WorldObject;
import WorldObjectManager;
import ClusterManager;
import ClientSessionManager;
import DbThreadManager;
import DbModelManager;
import Config;
import WorldGlobal;
import ClientSession;
import Npc;
import NavMeshManager;
import ThreadLocal;
import User;
import Pc;
import World;
import WorldServer;
import WorkerThread;
import WorkerThreadExecutor;
import DesignManagerCenter;

using namespace Mala::Net;
using namespace Mala::Windows;
using namespace Mala::Threading;
using namespace Mala::Diagnostics;

WorldServer::WorldServer(
    const EndPoint&         address,
          ICoreRef          core,
          NetSessionFactory factory,
          i32               maxSessionCount )
: NetServerService{ address, core, factory, maxSessionCount }
{
}

WorldServer::~WorldServer()
{
}

bool WorldServer::Initialize()
{
    srand( time( NULL ) );
    std::locale::global( std::locale( "ko_KR" ) );

    GetCoreRef()->Initialize( Config::ConcurrentThreadCount );
    InitiThreads();
    InitializeSessions();
    LExecutor = MakeShared< WorkerThreadExecutor >();

    /// 각각 작업 비동기 처리( TEST )
    std::vector tasks
    {
        ASYNC
        (
            GDbConnectionPool->Connect( L"localhost", L"root", L"1q2w3e4r", L"test", 3306, Config::ConcurrentThreadCount );
            LDbConnection = GDbConnectionPool->Get();
            GDbModelManager->Init();
            GDbConnectionPool->Release( LDbConnection );
        ),
        ASYNC
        (
            GDesignManagerCenter->Load( L"" );
            GWorld->Initialize();
        ),
        ASYNC( ASSERT_CRASH( GNavMeshManager->Load( L"OpenWorld.bin" ) ) ),
        ASYNC( GPerfCounter->Init( L"WorldServer" ) ),
    };
    WAIT_ALL( tasks );

    LDbConnection = GDbConnectionPool->Get();

    INFO_LOG( L"World Init... OK" );
    return true;
}

void WorldServer::RegisterPrefLog()
{
    GPerfCounter->Refresh();
    INFO_LOG2( L"PerformanceCounter\n\
Cpu Usage [ProcessorTime: {:.6f}][UserTime: {:.6f}][KernelTime: {:.6f}]\n\
Memory [WorkingSet: {} MB][NonPaged: {:.4f} MB]\n\
Process [SystemCall: {} / Sec ]\n\
Net[Recv:{:.4f} kbyte/Sec][Send:{:.4f} kbyte/Sec]",
    GPerfCounter->GetPrecessorTime(),
    GPerfCounter->GetUserTime(),
    GPerfCounter->GetPrivilegedTime(),
    GPerfCounter->GetWorkingSet64AsMb(),
    GPerfCounter->GetNonpagedSystemMemorySize64AsMb(),
    GPerfCounter->GetSystemCallPerSec(),
    GPerfCounter->GetBytesRecvPerSec() / 1024.0,
    GPerfCounter->GetBytesSentPerSec() / 1024.0 );

    LExecutor->PostAfter( 1000 * 5ms, []{ WorldServer::RegisterPrefLog(); } );
}

bool WorldServer::Start()
{
    ServerService::Start();

    LThreadType = EThreadType::Main;
    LThreadId = 100;

    RegisterPrefLog();

    INFO_LOG2( L"Server Start" );

    LOOP
    {
        ThreadManager::DistibuteReservedJobs();

        LogWriter::Flush();

        sleep( 1000 );
    };

    return true;
}

bool WorldServer::LoadConfig( const String& configName )
{
    Config config;
    if( !config.Load( L"WorldServer.ini" ) )
        return false;

    Config::NpcUpdateTick = ( 1000 / Config::NpcTickPerSec );
    GThreadCount          = Config::ConcurrentThreadCount;
    config.Log();

    return true;
}

void WorldServer::InitializeSessions()
{
    GClientSessionManager = xnew< ClientSessionManager >();
}


bool WorldThreadManager::Init()
{
    for ( i32 i = 0; i < Config::ConcurrentThreadCount; i++ )
    {
        Launch( [ & ]()
        {
            WorkerThread::DoInit();
            WorkerThread::DoWorkerJob();
        } );
    }

    INFO_LOG( L"Thread Manager Init... OK" );
    return true;
}


void WorldServer::InitiThreads()
{
    for ( i32 i = 0; i < Config::ConcurrentThreadCount; i++ )
    {
        GThreadManager->Launch( [ & ]()
        {
            WorkerThread::DoInit();
            WorkerThread::DoWorkerJob();
        } );
    }

    INFO_LOG( L"Thread Manager Init... OK" );
}

void WorldServer::InitializeWorld()
{
}
