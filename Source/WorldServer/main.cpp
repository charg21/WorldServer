#include <locale.h>
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>

import Mala.Core;
import Mala.Core.CoreGlobal;
import WorldGlobal;

inline static CoreGlobal GCoreGlobal{};
inline static WorldGlobal GWorldGlobal{};

#pragma comment( lib, "F:/Dev/SeamlessServer/Executable/Release/Mala.Core.lib" )

import <Macro.h>;

import Mala.Core.Dump;
import Mala.Container;
import Mala.Db.TxContext;
import Mala.Log;
import Mala.Memory;
import Mala.Net;

import ClientSession;
import Config;
import DesignManagerCenter;
import WorldServer;
import PcManager;
import NavMeshManager;

using namespace Mala::Container;
using namespace Mala::Core;
using namespace Mala::Net;
using namespace Mala::Db;

/// <summary>
/// WorldServer 프로그램 진입접
/// </summary>
int main()
{
    _CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
    _CrtSetReportMode( _CRT_WARN, _CRTDBG_MODE_DEBUG );

    Dump::Setup();
    GNetCore = MakeShared< NetCore >();
    GWorldServer = MakeShared< WorldServer >(
        EndPoint{ L"127.0.0.1", 18888 },
        GNetCore,
        ClientSession::Generate,
        5000 );

    if ( !GWorldServer->LoadConfig( L"WorldServer.ini" ) )
    {
        ERROR_LOG( L"설정 파일을 찾을 수 없습니다." );
        return -1;
    }

    GWorldGlobal.Init();
    if ( !GWorldServer->Initialize() )
    {
        ERROR_LOG( L"초기화에 실패 하였습니다." );
        return -1;
    }

    TxContext::SetDbConnectionAllocator( []{ return GDbConnectionPool->Get(); } );

    CrashIfFalse( GWorldServer->Start() );
    GWorldServer->Finalize();

    // Call Me...
    return 1022340100;
}
