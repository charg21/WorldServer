module BehaviorTree;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;

import WorldObject;
import WorldObjectTypes;

using namespace Mala::Container;

EXPORT_BEGIN


struct BehaviorTreeNode
{
};


struct BlackBoard
{
};


/// <summary>
/// 하이
/// </summary>
struct BehaviorTree
{
	BehaviorTree( BehaviorTreeNode* root )
	: _root{ root }
	{
	}

	~BehaviorTree()
	{
	}

	//void Update()
	//{
	//	_root->Update();
	//}
	//void Reset()
	//{
	//	_root->Reset();
	//}
	void SetRoot( BehaviorTreeNode* root )
	{
		//_root = std::make_unique< BehaviorTreeNode >( root );
	}

	BehaviorTreeNode* GetRoot() const
	{
		return _root.get();
	}

public:
	std::unique_ptr< BehaviorTreeNode > _root;
	std::unique_ptr< BlackBoard > _blackBoard;
};


EXPORT_END

