import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Memory;

import WorldObject;
import WorldObjectTypes;
import Config;
import Pc;
import Npc;
import NpcFsm;
import NpcFsmDeadState;
import WorldHelper;


NpcFsmDeadState::NpcFsmDeadState( Npc& owner, NpcFsm& fsm )
: NpcFsmState{ owner }
, _fsm{ fsm }
{
}

void NpcFsmDeadState::OnTick( f32 deltaTime )
{
}
