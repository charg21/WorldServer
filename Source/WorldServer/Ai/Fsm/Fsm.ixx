export module Fsm;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;

import WorldObject;
import WorldObjectTypes;

using namespace Mala::Container;

EXPORT_BEGIN

/// <summary>
/// 유한 상태 머신
/// </summary>
template< typename TFsmState >
struct Fsm
{
public:
    using FsmStateKey = typename TFsmState::State;
    using FsmStatePtr = std::unique_ptr< TFsmState >;
    using FsmStateMap = Map< FsmStateKey, FsmStatePtr >;

public:
    virtual ~Fsm() = default;

    void ChangeState( FsmStateKey state )
    {
        if ( _currentState )
            _currentState->OnEnd();

        SetState( state );

        if ( _currentState )
            _currentState->OnBegin();
    }

    void Add( FsmStateKey stateType, FsmStatePtr&& state )
    {
        _stateMap[ stateType ] = std::move( state );
    }

	virtual void OnTick( f32 deltaTime )
	{
        if( _currentState )
            _currentState->OnTick( deltaTime );
	}

	void SetState( FsmStateKey state )
	{
		auto iter = _stateMap.find( state );
		if ( iter != _stateMap.end() )
			_currentState = iter->second.get();
	}

    void Reset()
    {
		if ( _currentState )
			_currentState->OnEnd();

		_currentState = nullptr;
    }

    TFsmState* _currentState{};
    FsmStateMap _stateMap;
};

template< typename TState >
class FsmState
{
public:
	using State = TState;

public:
	virtual ~FsmState() = default;

    virtual void OnBegin()
    {
    }

    virtual void OnTick( f32 deltaTime )
    {
    }

    virtual void OnEnd()
    {
    }

};

EXPORT_END

