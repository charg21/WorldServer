export module NpcFsm;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Memory;

import WorldObject;
import WorldObjectTypes;

import Fsm;
import NpcFsmState;
import NpcAiEvent;

using namespace Mala::Container;

EXPORT_BEGIN

/// <summary>
/// NPC 유한 상태 머신
/// </summary>
class NpcFsm : public Fsm< NpcFsmState >
{
public:
    NpcFsm();

    void Init( Npc& owner );
    void OnRecvEvent( NpcAiEventRef event );

    Position _fromPos;
};

EXPORT_END

