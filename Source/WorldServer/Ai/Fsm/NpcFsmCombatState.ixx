export module NpcFsmCombatState;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Memory;

import WorldObject;
import WorldObjectTypes;
import Fsm;
import NpcFsmState;

using namespace Mala::Container;

EXPORT_BEGIN

class NpcFsm;

class NpcFsmCombatState final : public NpcFsmState
{
public:
    NpcFsmCombatState( Npc& owner, NpcFsm& fsm )
    : NpcFsmState{ owner }
    , _fsm{ fsm }
    {}

    ~NpcFsmCombatState() final = default;

    void OnTick( f32 deltaTime ) final{};
    class NpcSkillListComponent& GetSkillList() const;

private:
    NpcFsm& _fsm;
};

EXPORT_END

