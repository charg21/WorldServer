import <Macro.h>;

import Mala.Core.Types;
import Mala.Core.Random;
import Mala.Container;
import Mala.Memory;

import WorldObject;
import WorldObjectTypes;
import Fsm;
import Npc;
import NpcFsmState;
import NpcFsmReturnState;
import MovementComponent;
import MovementComponent;
import WorldHelper;
import Config;
import NavMeshManager;


using namespace Mala::Container;
using namespace Mala::Core;


NpcFsmReturnState::NpcFsmReturnState( Npc& owner, NpcFsm& fsm )
: NpcFsmState{ owner }
, _fsm{ fsm }
{
}

void NpcFsmReturnState::OnTick( f32 deltaTime )
{
    // 귀환 위치에 도착했다면 정찰 상태로 전환
    if ( GetNpcMovement().IsArrived() )
        return _fsm.ChangeState( ENpcFsmState::Patrol );

    auto [ pos, dir ]{ WorldHelper::GetNextPos(
        GetNpc().GetPos(),
        GetNpc().GetToPos(),
        deltaTime,
        Config::NpcMoveSpeed ) };

    GetNpcMovement().MoveTo( pos, dir );
}

void NpcFsmReturnState::OnBegin()
{
    GetNpcMovement()._toPos = GetReturnPos();
}

NpcMovementComponent& NpcFsmReturnState::GetNpcMovement()
{
    return GetNpc()._npcMovement;
}

Position NpcFsmReturnState::GetReturnPos()
{
    return GetNpcMovement()._returnPos;
}

Position NpcFsmReturnState::GetPos()
{
    return GetNpcMovement()._pos;
}

