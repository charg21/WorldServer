export module NpcFsmChaseState;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Memory;

import WorldObject;
import WorldObjectTypes;
import Fsm;
import NpcFsmState;

using namespace Mala::Container;

EXPORT_BEGIN

class NpcFsm;

class NpcFsmChaseState final : public NpcFsmState
{
public:
    NpcFsmChaseState( Npc& owner, NpcFsm& fsm );
    ~NpcFsmChaseState() final = default;

    void OnTick( f32 deltaTime ) final;
    void MoveToTarget( f32 deltaTime );

private:
    NpcFsm& _fsm;
};

EXPORT_END
