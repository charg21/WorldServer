export module NpcFsmDeadState;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Memory;

import WorldObject;
import WorldObjectTypes;
import Fsm;
import NpcFsmState;

using namespace Mala::Container;

EXPORT_BEGIN

class NpcFsm;

class NpcFsmDeadState final : public NpcFsmState
{
public:
    NpcFsmDeadState( Npc& owner, NpcFsm& fsm );
    ~NpcFsmDeadState() final = default;
    void OnTick( f32 deltaTime ) final;

private:
    NpcFsm& _fsm;
};

EXPORT_END
