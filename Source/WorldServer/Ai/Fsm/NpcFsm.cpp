import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Memory;

import WorldObject;
import WorldObjectTypes;

import NpcFsm;
import NpcFsmState;
import NpcFsmChaseState;
import NpcFsmCombatState;
import NpcFsmDeadState;
import NpcFsmReturnState;
import NpcFsmPatrolState;

using namespace Mala::Container;


NpcFsm::NpcFsm()
: Fsm< NpcFsmState >()
{
}

/// <summary>
/// 데이터로 초기화 해야할듯...
/// </summary>
void NpcFsm::Init( Npc& owner )
{
    //Add( ENpcFsmState::Normal, std::make_unique< NpcFsmNormalState >( owner, *this ) );
    Add( ENpcFsmState::Patrol, std::make_unique< NpcFsmPatrolState >( owner, *this ) );
    //Add( ENpcFsmState::Caution, std::make_unique< NpcFsmCautionState >( owner, *this ) );
    Add( ENpcFsmState::Chase, std::make_unique< NpcFsmChaseState >( owner, *this ) );
    Add( ENpcFsmState::Return, std::make_unique< NpcFsmReturnState >( owner, *this ) );
    Add( ENpcFsmState::Combat, std::make_unique< NpcFsmCombatState >( owner, *this ) );
    Add( ENpcFsmState::Dead, std::make_unique< NpcFsmDeadState >( owner, *this ) );

    ChangeState( ENpcFsmState::Patrol );
}

void NpcFsm::OnRecvEvent( NpcAiEventRef event )
{
    if ( _currentState )
        _currentState->OnRecvEvent( event );
}
