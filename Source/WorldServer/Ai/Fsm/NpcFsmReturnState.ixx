export module NpcFsmReturnState;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Memory;

import WorldObject;
import WorldObjectTypes;
import Fsm;
import NpcFsmState;

using namespace Mala::Container;

EXPORT_BEGIN

class NpcFsm;
class NpcMovementComponent;

class NpcFsmReturnState final : public NpcFsmState
{
public:
    NpcFsmReturnState( Npc& owner, NpcFsm& fsm );
    ~NpcFsmReturnState() final = default;

    void OnBegin() final;
    void OnTick( f32 deltaTime ) final;

    NpcMovementComponent& GetNpcMovement();
    Position GetReturnPos();
    Position GetPos();

private:
    NpcFsm& _fsm;
};

EXPORT_END
