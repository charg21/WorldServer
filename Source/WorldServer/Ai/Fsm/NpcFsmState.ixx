export module NpcFsmState;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Memory;

import WorldObject;
import WorldObjectTypes;
import Fsm;
import NpcAiEvent;

using namespace Mala::Container;

EXPORT_BEGIN

/// <summary>
/// NPC FSM 상태
/// </summary>
enum class ENpcFsmState
{
    /// <summary>
    /// 일반 상태
    /// </summary>
    Normal,

    /// <summary>
    /// 정찰 상태
    /// </summary>
    Patrol,

    /// <summary>
    /// 경계 상태
    /// </summary>
    Caution,

    /// <summary>
    /// 추적 상태
    /// </summary>
    Chase,

    /// <summary>
    /// 전투 상태
    /// </summary>
    Combat,

    /// <summary>
    /// 복귀 상태
    /// </summary>
    Return,

    /// <summary>
    /// 사망 상태
    /// </summary>
    Dead,
};

class NpcFsmState : public FsmState< ENpcFsmState >
{
public:
    NpcFsmState( Npc& npc )
    : _npc{ npc }
    {}
    ~NpcFsmState() override = default;

    void OnTick( f32 deltaTime ) override {};
    virtual void OnRecvEvent( NpcAiEventRef event ){};

    Npc& GetNpc() { return _npc; }

    Npc& _npc;
};


EXPORT_END

