export module NpcFsmPatrolState;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Memory;

import WorldObject;
import WorldObjectTypes;
import Fsm;
import NpcFsmState;

using namespace Mala::Container;

EXPORT_BEGIN

class NpcFsm;

/// <summary>
/// NPC 정찰 상태
/// </summary>
class NpcFsmPatrolState final : public NpcFsmState
{
public:
    NpcFsmPatrolState( Npc& owner, NpcFsm& fsm );
    ~NpcFsmPatrolState() final = default;

    void OnTick( f32 deltaTime ) final;
    Position GetNextPos();

private:
    NpcFsm& _fsm;
};

EXPORT_END

