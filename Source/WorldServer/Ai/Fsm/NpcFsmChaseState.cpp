import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Memory;

import WorldObject;
import WorldObjectTypes;
import Config;
import Pc;
import Npc;
import NpcFsm;
import NpcFsmChaseState;
import WorldHelper;
import CombatComponent;


NpcFsmChaseState::NpcFsmChaseState( Npc& owner, NpcFsm& fsm )
: NpcFsmState{ owner }
, _fsm{ fsm }
{
}

void NpcFsmChaseState::OnTick( f32 deltaTime )
{
    /// NPC가 타겟을 찾지 못하면( 시야 범위 밖에 있는 경우 ) 추격 상태를 해제
    /// 경계 상태로 전환, 일정 시간동안 다른 대상을 찾지 못한다면
    /// 리턴 상태로 전환, 이후 다시 정찰 상태로 전환
    PcPtr target{ GetNpc().GetTarget() };
    if ( !target || !target->_combat->IsAlive() )
        return _fsm.ChangeState( ENpcFsmState::Patrol );

    MoveToTarget( deltaTime );
}

void NpcFsmChaseState::MoveToTarget( f32 deltaTime )
{
    auto* movement{ GetNpc()._movement };
    auto target{ GetNpc().GetTarget() };

    auto [ pos, dir ] = WorldHelper::GetNextPos(
        GetNpc().GetPos(),
        target->GetPos(),
        deltaTime,
        Config::NpcMoveSpeed );

    movement->MoveTo( pos, dir );
}
