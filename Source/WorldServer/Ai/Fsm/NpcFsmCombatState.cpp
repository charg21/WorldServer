import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Memory;

import Npc;
import Fsm;
import NpcFsmState;
import NpcFsmCombatState;
import SkillListComponent;

using namespace Mala::Container;

class NpcFsm;

NpcSkillListComponent& NpcFsmCombatState::GetSkillList() const
{
    return _npc._skillList;
}

