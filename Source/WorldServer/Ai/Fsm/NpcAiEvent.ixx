export module NpcAiEvent;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Memory;

EXPORT_BEGIN

struct NpcAiEvent{};
using NpcAiEventPtr = std::shared_ptr< NpcAiEvent >;
using NpcAiEventRef = const NpcAiEventPtr&;

EXPORT_END

