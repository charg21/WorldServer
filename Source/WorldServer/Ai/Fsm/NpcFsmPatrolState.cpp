import <Macro.h>;

import Mala.Core.Types;
import Mala.Core.Random;
import Mala.Container;
import Mala.Memory;

import WorldObject;
import WorldObjectTypes;
import Fsm;
import Npc;
import NpcFsmState;
import NpcFsmPatrolState;
import MovementComponent;
import WorldHelper;
import Config;
import NavMeshManager;


using namespace Mala::Container;
using namespace Mala::Core;


NpcFsmPatrolState::NpcFsmPatrolState( Npc& owner, NpcFsm& fsm )
: NpcFsmState{ owner }
, _fsm{ fsm }
{
}

void NpcFsmPatrolState::OnTick( f32 deltaTime )
{
    auto* movement{ GetNpc()._movement };
    if ( movement->IsArrived() )
        movement->SetToPos( GetNextPos() );

    auto [ pos, dir ] = WorldHelper::GetNextPos(
        movement->GetPos(),
        movement->GetToPos(),
        deltaTime,
        Config::NpcMoveSpeed );

    movement->MoveTo( pos, dir );
}

Position NpcFsmPatrolState::GetNextPos()
{
    return WorldHelper::GetNextPos( GetNpc().GetPos(), (EDirection)( FastRand() % 4 ) );
}


