export module Cheat;

import <Macro.h>;
import Mala.Core.Types;
import Mala.Container.String;
import Mala.Reflection;

EXPORT_BEGIN

/// <summary>
/// 치트
/// </summary>
struct Cheat
{
	GENERATE_CLASS_TYPE_INFO( Cheat );

public:
	Field( _command )
	String _command;

	Cheat( const String& cmd );

protected:
	virtual void Execute( class ClientSession* ) abstract;
};

EXPORT_END
