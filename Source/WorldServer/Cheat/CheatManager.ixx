export module CheatManager;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;

import Cheat;
import CheatWorldObject;

using namespace Mala::Core;
using namespace Mala::Container;

EXPORT_BEGIN

/// <summary>
/// 치트 관리자
/// </summary>
class CheatManager
{
    /// <summary>
    /// 치트 컨테이너 타입 정의
    /// </summary>
    using Cheats = HashMap< String, Cheat* >;

public:
    Cheats _cheats;

    void Initialize()
    {
        auto cheat = new CheatWorldObject();
        _cheats[ cheat->_command ] = cheat;
    }

    Cheat* GetCheat( StringRef cmd )
    {
        auto iter = _cheats.find( cmd );
        if ( iter == _cheats.end() )
            return nullptr;

        return iter->second;
    }
};


EXPORT_END
