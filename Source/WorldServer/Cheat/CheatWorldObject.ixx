export module CheatWorldObject;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Reflection;
import Cheat;

EXPORT_BEGIN

/// <summary>
/// 치트 - 월드 오브젝트
/// </summary>
struct CheatWorldObject final : public Cheat
{
    GENERATE_CLASS_TYPE_INFO( CheatWorldObject );

public:
    CheatWorldObject();
    ~CheatWorldObject() = default;

protected:
    void Execute( ClientSession* ) final;
};

EXPORT_END
