
#include "Mala.Core/CoreMacro.h"
#include "Mala.Core/Reflection/FieldMacro.h"
#include "Mala.Core/Reflection/TypeInfoMacro.h"
#include "Mala.Core/Reflection/MethodMacro.h"

#define NEW( Var ) { Var = new std::remove_pointer_t< decltype( Var ) >(); }

import Mala.Core.DateTime;
import Mala.Log.ConsoleLog;
import Mala.Log;

#define WIDEN(x)      L ## x
#define WIDEN2(x)     WIDEN(x)
#define __WFILE__     WIDEN2(__FILE__)
#define __WFUNCTION__ WIDEN2(__FUNCTION__)

#define NOW_STR Mala::Core::DateTime::Now().ToString< Mala::Core::EDateFormat::HmS >().c_str()
#define TIME_WITH_SRC_LOCATION NOW_STR, __WFUNCTION__, __LINE__
#define CONSOLE_LOG( Color, Text, ... ) Mala::Log::WriteLog( Mala::Log::EColor::Color, Text __VA_OPT__( , ) __VA_ARGS__ );
#define WARN_LOG( FMT, ... )   CONSOLE_LOG( Yellow, L"[%ws][WARN][%ws:%d]" ## FMT ## L"\n", TIME_WITH_SRC_LOCATION __VA_OPT__( , ) __VA_ARGS__ );
#define ERROR_LOG( FMT, ... )  CONSOLE_LOG( Red,    L"[%ws][ERRO][%ws:%d]" ## FMT ## L"\n", TIME_WITH_SRC_LOCATION __VA_OPT__( , ) __VA_ARGS__ );
#define INFO_LOG( FMT, ... )   CONSOLE_LOG( White,  L"[%ws][INFO][%ws:%d]" ## FMT ## L"\n", TIME_WITH_SRC_LOCATION __VA_OPT__( , ) __VA_ARGS__ );
#define CONFIG_LOG( FMT, ... ) CONSOLE_LOG( Green, std::format( L"[{}][CONFIG]" ## FMT ## L"\n", NOW_STR __VA_OPT__( , ) __VA_ARGS__ ).c_str() );
#define DEBUG_MODE
#ifdef DEBUG_MODE
#define DEBUG_LOG( FMT, ... ) LOG( Mint, L"[%ws][DEBUG][%ws:%d]"##FMT##L"\n", TIME_WITH_SRC_LOCATION __VA_OPT__( , ) __VA_ARGS__ );
#else
#define DEBUG_LOG( FMT, ... ) {}
#endif


#define LOG( FMT, ... ) LogWriter::Write( L"[{}][{}:{}]"##FMT##L"\n", TIME_WITH_SRC_LOCATION __VA_OPT__( , ) __VA_ARGS__ );
#define INFO_LOG2( FMT, ... ) LogWriter::Write( L"[{}][INFO][{}:{}]"##FMT##L"\n", TIME_WITH_SRC_LOCATION __VA_OPT__( , ) __VA_ARGS__ );

/// <summary>
/// Smart Pointer
/// </summary>
#define DECLARE_SHARED_PTR( CLASS )                \
class CLASS;                                     \
using CLASS##Ptr     = std::shared_ptr< CLASS >; \
using CLASS##Ref     = const CLASS##Ptr&;        \
using CLASS##WeakPtr = std::weak_ptr< CLASS >;   \
using CLASS##WeakRef = const CLASS##WeakPtr&;

/// <summary>
/// Memory Trace
/// </summary>
#define XNEW( Type, ... ) xnew2< Type, __LINE__ >( SourceLocation::Here(), __VA_ARGS__ );
#define XDELETE( Type, Obj ) xdelete< Type >( Obj );
