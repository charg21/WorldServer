module;

import Mala.Core.CoreGlobal;

export module Config;

import <Macro.h>;
import <memory>;
import <iostream>;

import Mala.Core.Format;
import Mala.Core.Types;
import Mala.Core.IniParser;
import Mala.Core.Variant;
import Mala.Log;
import Mala.Log.ConsoleLog;
import Mala.Container.String;
import Mala.Reflection;

using namespace Mala::Db;
using namespace Mala::Core;

EXPORT_BEGIN


struct Config
{
    GENERATE_CLASS_TYPE_INFO( Config );

public:
    bool Load( const String& configFile );
    void Log();

public:
    Field( ConcurrentThreadCount )
    inline static i32 ConcurrentThreadCount{ 22 };

    Field( NpcTickPerSec )
    inline static i32 NpcTickPerSec{ 10 };

    Field( NpcUpdateTick )
    inline static i32 NpcUpdateTick{ 1000 / NpcTickPerSec };

    Field( NpcMoveSpeed )
    inline static f32 NpcMoveSpeed{ 3.f * 4 };

    Field( NpcBaseId )
    inline static i32 NpcBaseId{ 30'000 };

    Field( NpcCount )
    inline static i32 NpcCount{ 40'000 };

    Field( DbHost )
    inline static String DbHost{ L"127.0.0.1" };

    Field( DbUser )
    inline static String DbUser{ L"root" };

    Field( DbPass )
    inline static String DbPass{ L"test" };

    Field( DbSchema )
    inline static String DbSchema{ L"test" };

    Field( DbPort )
    inline static i32 DbPort{};

    Field( DbConnectionCount )
    inline static i32 DbConnectionCount{};

    Field( DbThreadCount )
    inline static i32 DbThreadCount{};

    Field( UseDbForeginKeyCheck )
    inline static bool UseDbForeginKeyCheck{ false };

};

void Config::Log()
{
    auto& typeInfo = GetTypeInfo();
    for ( const auto* field : typeInfo.GetFields() )
    {
        auto& fieldTypeInfo = field->GetTypeInfo();
        if ( fieldTypeInfo.IsA< bool >() )
        {
            CONFIG_LOG( L"{} = {}", field->GetNameW(), field->Get< Config, bool >( this ) );
        }
        else if ( fieldTypeInfo.IsA< int >() )
        {
            CONFIG_LOG( L"{} = {}", field->GetNameW(), field->Get< Config, int >( this ) );
        }
        else if ( fieldTypeInfo.IsA< double >() )
        {
            CONFIG_LOG( L"{} = {}", field->GetNameW(), field->Get< Config, double >( this ) );
        }
        else if ( fieldTypeInfo.IsA< float >() )
        {
            CONFIG_LOG( L"{} = {}", field->GetNameW(), field->Get< Config, float >( this ) );
        }
        else if ( fieldTypeInfo.IsA< String >() )
        {
            CONFIG_LOG( L"{} = {}", field->GetNameW(), field->Get< Config, String >( this ) );
        }
        else if ( fieldTypeInfo.IsA< Millisecond >() )
        {
            CONFIG_LOG( L"{} = {}ms", field->GetNameW(), field->Get< Config, int >( this ) );
        }
    }
}

template< typename T >
bool SetConfig( Config* self, IniParser& parser, const Field*& field, T defaultValue )
{
    auto& fieldTypeInfo = field->GetTypeInfo();
    if ( fieldTypeInfo.IsA< T >() )
    {
        T outValue{};
        if ( parser.TryGet< T >( field->GetNameW(), outValue, defaultValue ) )
            field->Set( self, outValue );

        return false;
    }

    return true;
}

bool Config::Load( const String& configFile )
{
    Mala::Core::IniParser parser;
    if( !parser.LoadFile( configFile ) )
        return false;

    auto& typeInfo = GetTypeInfo();
    for( auto* field : typeInfo.GetFields() )
    {
        if ( SetConfig< bool >( this, parser, field, false ) ) continue;
        if ( SetConfig< int >( this, parser, field, 0 ) )      continue;
        if ( SetConfig< double >( this, parser, field, 0.0 ) ) continue;
        if ( SetConfig< float >( this, parser, field, 0.0f ) ) continue;
        if ( SetConfig< String >( this, parser, field, L"" ) ) continue;

        CRASH();
    }

    return true;
}

EXPORT_END
