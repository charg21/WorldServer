export module Grid;

import <Macro.h>;
import <type_traits>;

import Mala.Core.Types;
import Mala.Core.TypeTraits;
import Mala.Container;
import Mala.Math;

using namespace Mala::Container;
using namespace Mala::Math;

EXPORT_BEGIN

template< typename T, size_t Y, size_t X >
class Grid
{
public:
    using ValueType      = T;
    using SizeType       = usize;
    using Pointer        = T*;
    using Reference      = T&;
    using ConstPointer   = const T*;
    using ConstReference = const T&;
    using Array2dType    = Array< Array< T, Y >, X >;
    using IndexType      = Vector2Int;

    inline static constexpr size_t Height{ Y };
    inline static constexpr size_t Width { X };

    /// <summary>
    /// 특정 인덱스의 원소를 반환한다
    /// </summary>
    Array2dType& Get()
    {
        return _elems;
    }

    /// <summary>
    /// 특정 인덱스의 값을 반환한다
    /// </summary>
    ConstReference GetValue( int y, int x ) const
    {
        return _elems[ y ][ x ];
    }

    /// <summary>
    /// 특정 인덱스의 값을 반환한다
    /// </summary>
    Reference GetValue( int y, int x )
    {
        return _elems[ y ][ x ];
    }

    auto begin()
    {
        return _elems.begin();
    }

    auto end()
    {
        return _elems.end();
    }

    auto cbegin() const
    {
        return _elems.cbegin();
    }

    auto cend() const
    {
        return _elems.cend();
    }

    constexpr bool Empty() const noexcept
    {
        return false;
    }

    constexpr bool empty() const noexcept
    {
        return false;
    }

    constexpr SizeType Size() const noexcept
    {
        return Height * Width;
    }

    constexpr SizeType size() const noexcept
    {
        return Height * Width;
    }

    constexpr SizeType max_size() const noexcept
    {
        return Height * Width;
    }

protected:
    Array2dType _elems;
};

template< typename T, size_t Y >
class Grid< T, Y, 0 >
{
public:
    using ValueType      = T;
    using SizeType       = usize;
    using Pointer        = T*;
    using Reference      = T&;
    using ConstPointer   = const T*;
    using ConstReference = const T&;

    inline static constexpr size_t Height{ 0 };
    inline static constexpr size_t Width { 0 };

    using Array2DType = Array< Array< T, Height >, Width >;
    using IndexType   = Vector2Int;

public:

    constexpr bool empty() const noexcept
    {
        return true;
    }

    constexpr SizeType size() const noexcept
    {
        return 0;
    }

    constexpr SizeType max_size() const noexcept
    {
        return 0;
    }
};

template< typename T >
class Grid< T, 0, 0 >
{
public:
    using ValueType      = T;
    using SizeType       = usize;
    using Pointer        = T*;
    using Reference      = T&;
    using ConstPointer   = const T*;
    using ConstReference = const T&;

    inline static constexpr size_t Height{ 0 };
    inline static constexpr size_t Width { 0 };

    using Array2DType = Array< Array< T, Height >, Width >;
    using IndexType   = Vector2Int;
public:

    constexpr bool empty() const noexcept
    {
        return true;
    }

    constexpr SizeType size() const noexcept
    {
        return 0;
    }

    constexpr SizeType max_size() const noexcept
    {
        return 0;
    }
};

EXPORT_END