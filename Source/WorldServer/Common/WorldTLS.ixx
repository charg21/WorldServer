export module ThreadLocal;

import <Macro.h>;
import <vector>;
import <functional>;
import <memory>;

import Mala.Core.Types;
import Mala.Threading;
import Mala.Container;

import WorkerThreadExecutor;

using namespace Mala::Container;
using namespace Mala::Threading;
using namespace Mala::Net;

EXPORT_BEGIN

enum class EThreadType
{
    None,
    Worker,
    Db,
    Main,
};

class ClusterManager;
class AoiManager;

using SendRequestSession     = std::shared_ptr< class ClientSession >;
using SendRequestSessionList = std::vector< SendRequestSession >;
//using SendRequestSessionList = Deque< SendRequestSession >;

inline thread_local EThreadType             LThreadType             { EThreadType::None };
inline thread_local i64                     LLastTick               { -1                };
inline thread_local SendRequestSessionList* LSendRequestSessionList {};
inline thread_local JobExecutorPtr          LExecutor               {};
inline thread_local ClusterManager*         LClusterManager         {};
inline thread_local AoiManager*             LAoiManager             {};

EXPORT_END
