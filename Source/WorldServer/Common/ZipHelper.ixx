export module ZipHelper;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Net.SendBuffer;

using namespace Mala::Net;


EXPORT_BEGIN

/// <summary>
/// 패킷 압축 헬퍼
/// </summary>
struct ZipHelper
{
    /// <summary>
    /// 패킷을 압축한다.
    /// </summary>
    static SendBufferPtr DoZip( void* inBuffer, int inLen );
    bool IsZipped( void* inBuffer, int inLen ){ return true; }
};

EXPORT_END