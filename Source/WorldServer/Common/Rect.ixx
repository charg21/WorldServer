export module Rect;

import <Macro.h>;

EXPORT_BEGIN

/// <summary>
/// 직사각형을 나타내는 객체
/// </summary>
template< typename T >
struct TRectBase
{
    union
    {
        T _left;
        T _x;
        T _xMin;
    };

    union
    {
        T _right;
        T _width;
        T _xMax;
    };

    union
    {
        T _bottom;
        T _y;
        T _yMin;
    };

    union
    {
        T _top;
        T _height;
        T _yMax;
    };

    /// <summary>
    /// AABB, 다른 직사각형과 겹치는지( 충돌 ) 여부를 반환한다
    /// </summary>
    bool IsOverlapped( const TRectBase& rhs )
    {
        return IsOverlapped( *this, rhs );
    }

    /// <summary>
    /// AABB, 두 직사각형이 겹치는지( 충돌 ) 여부를 반환한다
    /// </summary>
    static bool IsOverlapped( const TRectBase& lhs, const TRectBase& rhs )
    {
        if ( lhs._left > rhs._right )
            return false;

        if ( lhs._right < rhs._left )
            return false;

        if ( lhs._top < rhs._bottom )
            return false;

        if ( lhs._bottom > rhs._top )
            return false;

        return true;
    }
};

/// <summary>
/// 정수형 직사각형 타입
/// </summary>
struct Rect final : public TRectBase< int >
{
};

using Box = TRectBase< float >;

EXPORT_END