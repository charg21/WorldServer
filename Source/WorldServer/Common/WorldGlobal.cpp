import <Macro.h>;

import Mala.Core;
import Mala.Core.CoreGlobal;
import Mala.Core.Format;
import Mala.Core.Types;
import Mala.Core.IniParser;
import Mala.Core.Variant;
import Mala.Db.DbConnection;
import Mala.Db.TxContext;
import Mala.Log;
import Mala.Log.ConsoleLog;
import Mala.Container.String;
import Mala.Reflection;
import Mala.Memory;

import WorldObjectManager;
import Config;
import CheatManager;
import ChannelManager;
import DbThreadManager;
import DbModelManager;
import DesignManagerCenter;
import PcManager;
import PcCache;
import UserCache;
import WorldGlobal;
import World;
import ThreadLocal;
import WorldServer;
import NavMeshManager;
import WorkerThreadExecutor;


using namespace Mala::Db;
using namespace Mala::Core;
using namespace Mala::Net;
using namespace Mala::Threading;

WorldGlobal::WorldGlobal()
{
    GLogQueue            = new LogRecordQueue< 16 >();
    GDesignManagerCenter = xnew< DesignManagerCenter >();
    GChannelManager      = xnew< ChannelManager >();
    GCheatManager        = xnew< CheatManager >();
    GPcManagerPtr        = MakeShared< PcManager >();
    GPcManager           = GPcManagerPtr.get();
    GPcCache             = xnew< PcCache >();
    GDbConnectionPool    = xnew< DbConnectionPool >();
    GDbModelManager      = xnew< DbModelManager >();
    GUserCache           = xnew< UserCache >();
    GNavMeshManager      = xnew< NavMeshManager >();
    GWorld               = xnew< World >();
}

WorldGlobal::~WorldGlobal()
{
    xdelete( GWorld );
    GPcManagerPtr.reset();
    xdelete( GUserCache );
    xdelete( GPcCache );
    xdelete( GDbModelManager );
    xdelete( GDbConnectionPool );
    xdelete( GNavMeshManager );
    xdelete( GChannelManager );
    xdelete( GCheatManager );
    xdelete( GDesignManagerCenter );
}

void WorldGlobal::Load()
{
}

void WorldGlobal::Init()
{
}
