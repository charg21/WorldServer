export module IdHelper;

import <Macro.h>;

import Mala.Core.Types;

EXPORT_BEGIN

using namespace Mala::Core;

/// <summary>
/// ULID기반 ID 생성기
/// YearOffset(3) Month(4) Day(5)
/// Hour(5) Minute(6) Second(6) Millisec(10)
/// ThreadId(6) 46비트 + 난수 or Sequence 19비트
/// ref: https://github.com/sator-imaging/Half-Ulid + ThreadId 개념 추가
/// </summary>
class IdHelper
{
    inline static constexpr u64 YearMax { 1 << 3 };  // 2 ^ 3  = 8   ( 8년 )
    inline static constexpr u64 MonthMax{ 1 << 4 };  // 2 ^ 4  = 16  ( 16개월 )
    inline static constexpr u64 DayMax  { 1 << 5 };  // 2 ^ 5  = 32  ( 32일 )

    inline static constexpr u64 HourMax    { 1 << 5 };  // 2 ^ 5  = 32  ( 32시간 )
    inline static constexpr u64 MinuteMax  { 1 << 6 };  // 2 ^ 6  = 64  ( 64분 )
    inline static constexpr u64 SecondMax  { 1 << 6 };  // 2 ^ 6  = 64  ( 64초 )
    inline static constexpr u64 MillisecMax{ 1 << 9 };  // 2 ^ 9  = 512 ( 1024 / 2 = 512 밀리초 )

    inline static constexpr u64 ThreadIdMax{ 1 << 7 };  // 2 ^ 7  = 128 ( 128 스레드 )
    inline static constexpr u64 IdMax      { 1 << 19 }; // 2 ^ 19 = 524288 ( 랜덤 or 시퀀스 )

    inline static constexpr u64 YearOrigin{ 2024 };

public:
    IdHelper() = delete;
    ~IdHelper() = delete;

public:
    /// <summary>
    /// 다음 원소를 반환합니다.
    /// 타입별로 ID 생성기를 별도로 두기 위해 템플릿 사용
    /// </summary>
    template< typename T = u64 >
    static T Next()
    {
        thread_local static i32 _idIssuer{};

        u64 id = NextBase();
        id |= ( _idIssuer++ ) % IdMax;

        return (T)( id );
    }

    /// <summary>
    /// 공용으로 사용되는
    /// </summary>
    static u64 NextBase()
    {
        auto [ year, month, day, hour, min, sec, tick ] = DateTime::Now().ToTuple();
        u64 id = (u64)( ( year - YearOrigin ) % YearMax ) << 61; // 상위 3
        id |= (u64)month << 57; // 4
        id |= (u64)day << 52; // 5
        id |= (u64)hour << 47; // 5
        id |= (u64)min << 41; // 6
        id |= (u64)sec << 35; // 6
        id |= (u64)tick << 29; // 10
        id |= ( GetThreadId() % ThreadIdMax ) << 19; // 7

        return id;
    }

    inline thread_local static i32 _threadId{ -1 };

    static u64 GetThreadId()
    {
        static std::atomic< i32 > _threadIdIssuer{};

        if ( _threadId == -1 )
            _threadId = _threadIdIssuer++;

        return _threadId;
    }

    static DateTime ToDateTimeFromId( u64 id )
    {
        i32 year = id >> 61;
        i32 month = ( id >> 57 ) & 0xF;
        i32 day = ( id >> 52 ) & 0x1F;
        i32 hour = ( id >> 47 ) & 0x1F;
        i32 min = ( id >> 41 ) & 0x3F;
        i32 sec = ( id >> 35 ) & 0x3F;
        i32 tick = ( id >> 29 ) & 0x3FF;

        return DateTime
        {
            year + (i32)YearOrigin,
            month,
            day,
            hour,
            min,
            sec,
            tick
        };
    }
};

EXPORT_END

