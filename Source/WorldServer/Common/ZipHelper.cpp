#include "lz4/lz4.h"

import Mala.Core.Types;
import Mala.Core.CoreGlobal;
import Mala.Net.SendBuffer;

import ZipHelper;

using namespace Mala::Net;

/// <summary>
/// 패킷을 압축한다.
/// </summary>
SendBufferPtr ZipHelper::DoZip( void* inBuffer, int inLen )
{
    /// 최대 크기 선 계산
    int maxZipLen{ LZ4_compressBound( inLen ) };
    auto zipBuffer{ GSendBufferManager->Open( maxZipLen ) };

    int zipLen = LZ4_compress_default(
        static_cast< const char* >( inBuffer ),
        reinterpret_cast< char* >( zipBuffer->Buffer() ),
        inLen,
        maxZipLen );
    if ( zipLen <= 0 )
        return {};

    zipBuffer->Close( zipLen );
    return std::move( zipBuffer );
}

