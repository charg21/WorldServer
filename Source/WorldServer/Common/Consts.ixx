export module Consts;

import <Macro.h>;

import Rect;
import Mala.Core.Types;
import Mala.Math.Vector2Int;

EXPORT_BEGIN


using namespace Mala::Math;

inline constexpr size_t Multiple{ 5 };

namespace Consts::World
{
    inline constexpr size_t Height{ 800 * Multiple };
    inline constexpr size_t Width { 800 * Multiple };
    inline constexpr Rect   Range { 0, Height, 0, Width };

    inline constexpr size_t DefaultGatherListCapacity{ 1024 };
}

namespace Consts::View
{
    inline constexpr size_t FieldOfView{ 19 * Multiple   };
    inline constexpr size_t HalfFov    { FieldOfView / 2 };
    inline constexpr size_t TwoFov     { FieldOfView * 2 };
    inline constexpr size_t Height     { FieldOfView     };
    inline constexpr size_t Width      { FieldOfView     };

    inline constexpr Rect   Range      { 0, Height, 0, Width };
}

namespace Consts::Aoi
{
    inline constexpr i32 Height = Consts::View::Height;
    inline constexpr i32 Width  = Consts::View::Width;
    inline constexpr i32 MaxNearCount = 9;
    inline constexpr i32 MaxRow = ( World::Height / Height ) + ( ( World::Height % Height > 0 ) ? 1 : 0 );
    inline constexpr i32 MaxCol = ( World::Width / Width ) + ( ( World::Width % Width > 0 ) ? 1 : 0 );
    inline constexpr i32 Capacity = MaxRow * MaxCol;
    inline constexpr i32 DefaultWorldObjectMapCapacity = 1024;

    // inline constexpr Vector2Int DefaultWorldObjectMapCapacity2{ MaxCol, MaxRow };
}

namespace Consts::Cluster
{
    inline constexpr size_t Height      { View::Height * 2       };
    inline constexpr size_t Width       { View::Width  * 2       };
    inline constexpr size_t MaxNearCount{ 4                      };
    inline constexpr size_t MaxRow      { World::Height / Height };
    inline constexpr size_t MaxCol      { World::Width  / Width  };

    inline constexpr Rect   Range       { 0, Height, 0, Width    };
}

EXPORT_END