export module JsonHelper;

import <Macro.h>;
import <tuple>;

import "document.h";
import "writer.h";
import "stringbuffer.h";
import "rapidjson.h";

import Mala.Container.String;
import Mala.Io.File;
import Mala.Text;

using namespace Mala::Io;
using namespace Mala::Text::Utf8;
//using namespace Mala::Text::Utf16;


EXPORT_BEGIN

/// <summary>
/// Json 헬퍼 클래스
/// </summary>
namespace JsonHelper
{
    /// <summary>
    /// Json 문자열을 Json 문서로 변환합니다.
    /// </summary>
    rapidjson::Document Parse( const std::string& json )
    {
        rapidjson::Document document;
        document.Parse( json.c_str() );

        return document;
    }

    /// <summary>
    /// Json 경로를 통해 불러오고 Json 문서로 변환합니다.
    /// </summary>
    rapidjson::Document LoadAndParse( StringRef jsonPath )
    {
        auto utf8Path = Mala::Text::Utf16::ToUtf8( jsonPath );
        auto jsonStr = File::ReadAllText( utf8Path );

        return Parse( jsonStr.c_str() );
    }

};

EXPORT_END
