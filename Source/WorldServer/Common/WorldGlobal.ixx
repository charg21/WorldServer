export import Mala.Core.CoreGlobal;

export module WorldGlobal;

import <Macro.h>;
import <memory>;
import <iostream>;

import Mala.Core.Format;
import Mala.Core.Types;
import Mala.Core.IniParser;
import Mala.Core.Variant;
import Mala.Log;
import Mala.Log.ConsoleLog;
import Mala.Container.String;
import Mala.Db.DbConnection;
import Mala.Reflection;
import WorldObjectManager;

using namespace Mala::Db;
using namespace Mala::Core;

EXPORT_BEGIN

inline extern std::shared_ptr< class WorldServer > GWorldServer{};

inline extern       DbConnectionPool*      GDbConnectionPool{};
inline extern class DbModelManager*        GDbModelManager{};
inline extern class World*                 GWorld{};
inline extern class ClientSessionManager*  GClientSessionManager{};
inline extern class ChannelManager*        GChannelManager{};
inline extern class CheatManager*          GCheatManager{};
inline extern class NavMeshManager*        GNavMeshManager{};
//inline extern class PcCache*               GPcCache{};
inline extern class PcManager*             GPcManager{};


struct WorldGlobal
{
    WorldGlobal();
    ~WorldGlobal();

    void Load();
    void Init();
};

EXPORT_END
