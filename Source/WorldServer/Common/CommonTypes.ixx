export module CommonTypes;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Core.StrongType;


EXPORT_BEGIN

/// <summary>
/// 커스텀 기획 식별자 정의
/// </summary>
USING_STRONG_VALUE_TYPE( BuffDesignId,      u32 );
USING_STRONG_VALUE_TYPE( ClassDesignId,     u32 );
USING_STRONG_VALUE_TYPE( ItemDesignId,      u32 );
USING_STRONG_VALUE_TYPE( NpcDesignId,       u32 );
USING_STRONG_VALUE_TYPE( QuestDesignId,     u32 );
USING_STRONG_VALUE_TYPE( QuestStepDesignId, u32 );
USING_STRONG_VALUE_TYPE( SkillDesignId,     u32 );


/// <summary>
/// 커스텀 오브젝트 식별자 정의
/// </summary>
USING_STRONG_VALUE_TYPE( WorldObjectId, u64 );
USING_STRONG_VALUE_TYPE( UserId,        u64 );
USING_STRONG_VALUE_TYPE( PcId,          u64 );
USING_STRONG_VALUE_TYPE( NpcId,         u64 );
USING_STRONG_VALUE_TYPE( ItemId,        u64 );


EXPORT_END
