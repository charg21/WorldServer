import Mala.Core;
import Mala.Container;

import <iostream>;
import WorldObject;
import WorldObjectTypes;
import BroadCastHelper;
import ELogicResult;
import DesignTypes;
import Pc;
import SkillHelper;
import SkillDesign;
import SkillDesignPtr;
import SkillLogic;
import PacketExporter;

import CombatComponent;

using namespace Mala::Container;


import CombatPacket.C_StartSkill;
import CombatPacket.S_StartSkill;
import CombatPacket.S_Hit;


ELogicResult SkillLogic::Start( PcRef pc, C_StartSkill& startSkill )
{
    startSkill._skillDesignId = 1;  // 임시로 1번 스킬로 고정
    auto [ result, skillDesign ] = CheckPreCondition( pc, startSkill );
    if ( result != ELogicResult::Success )
        return result;

    pc->PostAfter( 16ms, [ pc, skillDesign, targetId = WorldObjectId{ startSkill._targetId } ]() mutable
    {
        auto result = CheckCondition( pc, skillDesign );
        if ( result != ELogicResult::Success )
            return pc->Send< S_StartSkill >( result );

        auto* combat = pc->Get< CombatComponent >();
        combat->_doingCasting = true;

        /// 아래 타겟과 별개로 공격 모션 시작을 주변에 브로드 캐스팅
        S_StartSkill startSkill;
        PacketExporter::ExportToS_StartSkill( skillDesign, pc, startSkill );
        BroadCastHelper::Send( pc, startSkill.Write() );

        // 스킬 정보에 해당하는 오브젝트 리스트 수집 TLS접근
        auto args = _CreateSkillArgs( pc, skillDesign );
        auto& targetList = SkillHelper::GatherTargetList( pc, skillDesign, targetId );
        for ( auto* target : targetList )
            FireSkill( pc, target, args );

        combat->_doingCasting = false;
    } );

    return ELogicResult::Pending;
}

SkillLogic::CheckResult SkillLogic::CheckPreCondition( PcRef pc, C_StartSkill& startSkil )
{
    /// 스킬 기획 데이터를 확인한다
    auto skillDesign = SkillDesignPtr( { startSkil._skillDesignId } );
    if ( !skillDesign )
        return { ELogicResult::InvalidDesignId, nullptr };

    return { ELogicResult::Success, skillDesign._ptr };
}

/// <summary>
/// 조건을 체크한다.
/// </summary>
ELogicResult SkillLogic::CheckCondition( PcRef pc, SkillDesignRef design )
{
    /// 생존 체크
    auto* combat = pc->Get< CombatComponent >();
    if ( !combat->IsAlive() )
        return ELogicResult::AlreadyDeadPc;

    /// 캐스팅 여부 체크
    if ( combat->DoingCast() )
        return ELogicResult::AlreadyDoingCast;

    /// 쿨타임
    return ELogicResult::Success;
}

/// <summary>
/// 스킬 파라미터를 만든다.
/// </summary>
SkillStartArgs SkillLogic::_CreateSkillArgs(
    PcRef          pc,
    SkillDesignRef design )
{
    //auto* targetCombat = pc->Get< CombatComponent >();

    return SkillStartArgs
    {
        .skillDesign = design,
        .casterPos   = pc->GetPos(),
        //.casterStatCache = std::make_shared< StatCache >( pc )
    };
}

void SkillLogic::FireSkill(
    PcRef           caster,
    WorldObject*          target,
    SkillStartArgs& args )
{
    /// 대상에게 스킬 처리를 실행한다.
    target->Post( [ args, caster, target = target->SharedFromWorldObject() ]
    {
        auto  targetCombat{ target->_combat };
        auto* skillDesign{ args.skillDesign };
        auto  damage{ (i32)skillDesign->_damage };
        auto  curHp = 0;// pcCombat.DecreaseHp( skillDesign->_damage );

        //     std::wcout << xformat( L"Damaged: {}, Caster: {} => Target: {}",
        //         damage,
                 //*caster->GetObjectId(),
        //         *target->GetObjectId() ) << "\n";

        targetCombat->OnDamage( caster, damage );

        S_Hit hit;
        PacketExporter::ExportToS_Hit( skillDesign, curHp, caster, hit );
        BroadCastHelper::Send( target, hit.Write() );

        if ( curHp <= 0 )
            targetCombat->OnDead( caster );
    } );
}
