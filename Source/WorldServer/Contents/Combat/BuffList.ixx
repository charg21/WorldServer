module;

import <Macro.h>;

export module BuffList;
import Mala.Core.Types;
import Mala.Container;
import DesignTypes;

using namespace Mala::Container;


/// <summary>
/// Buff Id가 4바이트이면
/// [BuffId][BuffId]
/// </summary>
export class BuffList
{
public:
	inline static constexpr size_t Capacity{ 32 };

public:
	bool Has( BuffDesignId buffDesignId )
	{
		for ( BuffDesignId currBuffDesginId : _buffDesignIds )
		{
			if ( currBuffDesginId == buffDesignId )
				return true;
		}

		return false;
	}

	bool CanAdd()
	{
		return true;
	}

    void AddBuff( BuffDesignId buffDesignId )
    {
        if ( _Has( buffDesignId ) )
            return;

        //i16 index = -1;
        //if ( _emptyIndexList.size() > 0 )
        //{
        //	index = _emptyIndexList.back();
        //	_emptyIndexList.pop_back();
        //}
        //else
        //{
        //	index = _buffDesignIds.size();
        //	_buffDesignIds.push_back( buffDesignId );
        //}

        //_buffs.insert( buffDesignId, index );

    }

    void RemoveBuff( BuffDesignId buffDesignId )
    {
    }

	//bool HasFast( i32 buffDesignId )
	//{
	//	u64 buffDesignId2 = ( buffDesignId << 32 ) + buffDesignId;
	//	for ( i64 buffDesginId : _buffDesignIds )
	//	{
	//		if ( buffDesignId2 & buffDesignId )
	//			return true;
	//	}
	//	return false;
	//}

private:
	bool _Has( BuffDesignId buffDesignId )
	{
		return _buffs.contains( buffDesignId );
	}

	bool TryPopEmptyIndex( i16& outIndex )
	{
		//if ( _emptyIndexList.size() > 0 )
		//{
		//	i16 index = _emptyIndexList.back();
		//	_emptyIndexList.pop_back();
		//	return index;
		//}
		//else
		//{
		//	return -1;
		//}

		return false;
	}

private:
	/// 뷰
	Array< BuffDesignId, 64 > _buffDesignIds;

	/// 버프 목록
	HashMap< BuffDesignId, i64 > _buffs;

	/// 빈 인덱스 목록
	Stack< i16 > _emptyIndexList;
};
