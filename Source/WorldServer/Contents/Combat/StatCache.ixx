export module StatCache;


import Mala.Math.Vector3;

import WorldObjectTypes;
//import SkillDesign;

export
{

class WorldObject;

enum class EStat
{
    Hp = 0,
    Power = 1,
    Max = 2,
};

/// <summary>
/// 스킬 캐스터의 정보 캐시
/// </summary>
class StatCache
{
public:
    /// <summary>
    ///
    /// </summary>
    StatCache( WorldObject& object );

    i32 _stats[ (i32)EStat::Max ];

    /// <summary>
    /// 오브젝트의 위치
    /// </summary>
    Position _pos;

    ///// <summary>
    ///// 스킬 기획 데이터
    ///// </summary>
    //const SkillDesign* _skillDesign{};
};

}
