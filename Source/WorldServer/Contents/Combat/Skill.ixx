export module Combat.Skill;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Core.StrongType;
import Mala.Container;

import WorldObject;
import WorldObjectTypes;
import WorldHelper;
import SkillHelper;
import DesignTypes;
import SkillDesign;

using namespace Mala::Container;

EXPORT_BEGIN

class Skill
{
public:
    Skill( WorldObjectRef caster );
    ~Skill() = default;

    void Do();

public:
    /// <summary>
    /// 캐스터
    /// </summary>
    WorldObjectWeakPtr _caster;

    /// <summary>
    /// 스킬 디자인
    /// </summary>
    SkillDesignRef _design{};

    /// <summary>
    /// 이미 적중된 대상 식별자 집합
    /// </summary>
    HashSet< WorldObjectId > _alreadyHitTargeIdSet;
};

EXPORT_END
