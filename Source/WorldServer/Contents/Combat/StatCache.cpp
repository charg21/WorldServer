import Mala.Core;

import WorldObject;
import StatCache;

using namespace Mala::Core;

StatCache::StatCache( WorldObject& object )
{
    /// 스텟을 접근하는 행위는 현재 스레드에서 처리중인 오브젝트여야 함
    CrashIfFalse( object.IsCurrentExecutor() );

    _stats[ (i32)EStat::Hp ] = 10;

    _pos = object.GetPos();
}
