module;

import <Macro.h>;

export module Effect;

import Mala.Core.Types;
import Mala.Core.StrongType;
import Mala.Container;

import WorldObjectTypes;
import DesignTypes;

using namespace Mala::Container;

EXPORT_BEGIN

class Effect
{
public:
    Effect() = default;
    virtual ~Effect() = default;

    Effect( const Effect& rhs ) = delete;
    Effect( Effect&& rhs ) noexcept = delete;
};


EXPORT_END
