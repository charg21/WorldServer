module;
export module SkillLogic;

import <Macro.h>;

import Mala.Core;
import Mala.Container;

import WorldObject;
import WorldObjectTypes;
import DesignTypes;
import ELogicResult;
import SkillHelper;
import WorldHelper;

using namespace Mala::Container;

EXPORT_BEGIN

class C_StartSkill;


class StatCache
{
public:
    /// <summary>
    ///
    /// </summary>
    StatCache( WorldObject* object );
};

struct SkillStartArgs // Pooling?
{
    /// <summary> </summary>
    SkillDesignRef skillDesign{};

    /// <summary> </summary>
    Position casterPos;

    /// <summary> </summary>
    Millisecond startTime;

    /// <summary> </summary>
    std::shared_ptr< StatCache > casterStatCache;
};

class SkillLogic
{
public:
    /// <summary>
    ///
    /// </summary>
    using CheckResult = Tuple< ELogicResult, const SkillDesign* >;

public:
    /// <summary>
    ///
    /// </summary>
    static ELogicResult Start( PcRef pc, C_StartSkill& startSkil );

private:
    /// <summary>
    ///
    /// </summary>
    static CheckResult CheckPreCondition( PcRef pc, C_StartSkill& packet );

    /// <summary>
    ///
    /// </summary>
    static ELogicResult CheckCondition( PcRef pc, SkillDesignRef design );

    /// <summary>
    ///
    /// </summary>
    static SkillStartArgs _CreateSkillArgs( PcRef pc, SkillDesignRef design );

    /// <summary>
    ///
    /// </summary>
    static void FireSkill(
        PcRef           caster,
        WorldObject*    target,
        SkillStartArgs& args );

    /// <summary>
    ///
    /// </summary>
    static void _BroadcastSkillStart(
        PcRef           caster,
        WorldObject*    target,
        SkillStartArgs& args );

};


EXPORT_END
