module;

import <Macro.h>;

export module SkillHelper;

import Mala.Core.Types;
import Mala.Container;
import ThreadLocal;
import ClusterManager;
import AoiManager;
import WorldObject;
import WorldObjectTypes;
import WorldHelper;
import DesignTypes;
import SkillDesign;

import CombatComponent;
import MovementComponent;


using namespace Mala::Container;

EXPORT_BEGIN

namespace SkillHelper
{
//public:
    using TargetList = Vector< WorldObject* >;

    using SkillDesignRef = const SkillDesign*;

//public:
    /*static */TargetList& GetTargetList()
    {
        static thread_local TargetList targetList{ 1024 };
        targetList.clear();

        return targetList;
    }

    TargetList& GatherTargetList(
        WorldObjectRef       caster,
        SkillDesignRef skillDesign,
        WorldObjectId        targetId )
    {
        auto& targetList = GetTargetList();

        /// 러프하게 AABB 클러스터와 스킬 타켓팅 범위( Circle )에 포함되는 클러스터 목록을 가져온다.
        auto& clusterList = WorldHelper::GatherWorldObjectList(
            caster->_movement->_pos,
            skillDesign->_radius );

        /// 클러스터 목록을 순회하면서, 조건에 부합하는 오브젝트들을 타겟 리스트에 추가한다.
        for( auto* object : clusterList )
        {
            auto* combat = object->Get< CombatComponent >();
            if ( !combat )
                continue;

            /// 대상의 생존 여부 체크
            if ( !combat->IsAlive() )
                continue;

            /// 대상의 거리를 체크
            targetList.push_back( object );
        }

        return targetList;
    }
};

EXPORT_END
