module;

import <Macro.h>;

export module SaveLoad.SavePcTx;

import Mala.Core.Types;
import Mala.Core.StrongType;
import Mala.Db.TxContext;

import WorldObject;
import Pc;
import User;
import WorldObjectTypes;
import ELogicResult;

using namespace Mala::Container;
using namespace Mala::Db;


EXPORT_BEGIN

enum class ESaveType
{
    Expired,
    Logout,
};

class SavePcTx
{
public:
    /// <summary>
    /// 조건 검사 결과
    /// </summary>
    using CheckResult = Tuple< ELogicResult, int >;

public:
    /// <summary>
    /// 실행한다.
    /// </summary>
    static ELogicResult Save( PcRef pc, ESaveType saveType, TxContextRef tx = nullptr );

};

EXPORT_END
