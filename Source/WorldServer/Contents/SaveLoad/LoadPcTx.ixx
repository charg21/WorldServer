module;

import <Macro.h>;

export module SaveLoad.LoadPcTx;

import Mala.Core.Types;
import Mala.Core.StrongType;
import Mala.Db.TxContext;

import WorldObject;
import User;
import WorldObjectTypes;
import ELogicResult;

using namespace Mala::Container;
using namespace Mala::Db;

EXPORT_BEGIN

class C_Login;

class LoadPcTx
{
public:
    /// <summary>
    /// 조건 검사 결과
    /// </summary>
    using CheckResult = Tuple< ELogicResult, i32 >;

public:
    /// <summary>
    /// 실행한다.
    /// </summary>
    static ELogicResult Load( PcRef pc, PcId pcId, TxContextRef tx = nullptr );

    static void SendResult();
    static void ExportToLoadPc();
};

EXPORT_END
