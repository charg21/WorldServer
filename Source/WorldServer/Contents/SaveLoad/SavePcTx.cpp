import Mala.Core.Types;
import Mala.Core.StrongType;
import Mala.Db.TxContext;

import Pc;
import User;
import SaveLoad.SavePcTx;
import ELogicResult;

using namespace Mala::Db;

ELogicResult SavePcTx::Save( PcRef pc, ESaveType saveType, TxContextRef tx )
{
    auto localTx = tx;
    if ( !localTx )
        localTx = TxContext::New();

    pc->Post( [ pc, saveType ]
	{
		/*
		if( saveType == ESaveType::Save )
		{
			pc->Save();
		}
		*/
	} );

    if ( !tx )
        pc->PostTx( std::move( localTx ) );

    return ELogicResult::Pending;
}

