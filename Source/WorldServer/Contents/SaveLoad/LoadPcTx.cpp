import Mala.Core.Types;
import Mala.Db.TxContext;
import Mala.Db.DbModelHelper;

import ClientSession;
import Pc;
import PcDbModel;
import PcCache;
import User;
import ItemDbModel;
import WorldGlobal;

import SaveLoad.LoadPcTx;
import ELogicResult;
import AuthPacket.S_LoadPc;

using namespace Mala::Db;

ELogicResult LoadPcTx::Load( PcRef pc, PcId pcId, TxContextRef tx )
{
    auto localTx = tx;
    if ( !localTx )
        localTx = TxContext::New();

    auto cachePc{ GPcCache->GetModel( pc->GetIndexKey< Pc::IX_ID >() ) };
    if ( !cachePc )
        return ELogicResult::NullPc;

    localTx->AddDbJob( [ pc = pc.get(), tx, cachePc ]( auto* dbConn )
    {
        /// Pc 캐싱 내역 반영
        cachePc->CopyTo( *pc );
        /// DB 컴포넌트 로딩
        for ( auto* dbLoadComponent : pc->GetDbLoadComponentList() )
            dbLoadComponent->LoadComponentFromDb( tx );

        return true;
    } );

    localTx->SetResultJob( [ pc ]( bool txResult )
    {
        S_LoadPc sLoadPc{ ._result = !txResult ? ELogicResult::DbFail : ELogicResult::Success };
        if ( txResult )
        {
            pc->_pcBag.ExportToItemList( sLoadPc._items );
            pc->_equip.ExportToEquipList( sLoadPc._equips );
            pc->_pcStat.ExportToStatList( sLoadPc._stats );
        }

        pc->Send( sLoadPc.Write() );
    } );

    if ( !tx )
        pc->PostTx( std::move( localTx ) );

    return ELogicResult::Pending;
}
