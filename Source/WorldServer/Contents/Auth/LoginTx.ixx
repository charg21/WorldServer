module;
export module Auth.LoginTx;

import <Macro.h>;

import Mala.Memory;
import Mala.Core.Types;
import Mala.Core.StrongType;
import Mala.Container;

import WorldObject;
import WorldObjectTypes;
import ContentsDbTypes;
import User;
import UserCache;
import PcCache;
import DesignTypes;
import ELogicResult;
import SenderComponent;

using namespace Mala::Container;
using namespace Mala::Db;

EXPORT_BEGIN

class C_Login;

namespace Mala::Db
{
    class DbConnection;
}

class LoginTx
{
public:
    /// <summary>
    /// 조건 검사 결과
    /// </summary>
    using CheckResult = Tuple< ELogicResult, int >;

    /// <summary>
    /// DB 조회 결과
    /// </summary>
    using SelectResult = Tuple< ELogicResult, UserDbModelPtr, Vector< PcDbModelPtr > >;

public:
    /// <summary>
    /// 로그인 한다.
    /// </summary>
    static ELogicResult Login( UserRef user, C_Login& login, TxContextRef tx = nullptr );

private:
    /// <summary>
    /// 사전 조건을 체크한다.( 메모리 )
    /// </summary>
    static CheckResult CheckPreCondition( UserRef user, C_Login& login );

    /// <summary>
    /// 정보를 조회한다.
    /// </summary>
    static SelectResult SelectFromCache( UserRef user, C_Login& login );
    static SelectResult SelectFromDb( UserRef user, C_Login& login, DbConnection* dbConnection );

};

class RegisterUserTx
{
public:
    static ELogicResult Register( UserRef userRef, String& account, String& password, TxContextRef tx = nullptr );
};

EXPORT_END
