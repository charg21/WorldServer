#include "Macro.h"

import Mala.Memory;
import Mala.Core.Types;
import Mala.Core.StrongType;
import Mala.Container;
import Mala.Container.String;
import Mala.Db.DbModel;

import Mala.Db.DbConnection;
import Mala.Db.DbModelCache;
import Mala.Db.DbModelHelper;
import Mala.Db.TxContext;
import Mala.Db.LeftJoin;

import DbModelManager;
import Auth.LoginTx;
import WorldObject;
import Character;
import ClientSession;
import ELogicResult;
import DesignTypes;
import Pc;
import PcCache;
import PcDbModel;
import User;
import UserDbModel;
import UserCache;

import WorldGlobal;
import IdHelper;

using namespace Mala::Container;
using namespace Mala::Db;

import AuthPacket.C_Login;
import AuthPacket.S_Login;
import CombatPacket.S_StartSkill;

ELogicResult LoginTx::Login( UserRef user, C_Login& login, TxContextRef tx )
{
    auto localTx = tx;
    if ( !localTx )
        localTx = TxContext::New();

    auto [ result, userId ] = CheckPreCondition( user, login );
    if ( result != ELogicResult::Success )
        return result;

    localTx->AddDbJob( [ tx = localTx.get(), user, login ]( auto* dbCon ) mutable
    {
        //auto [ result, dbUser, pcList ] = SelectFromDb( user, login, dbCon );
        auto [ result, dbUser, pcList ] = SelectFromCache( user, login );
        if ( result != ELogicResult::Success )
        {
            user->Send( S_Login{ ._result = result }.Write() );
            return false;
        }

		tx->EndWith( [ MOVE( user ), MOVE( dbUser ), MOVE( pcList ) ]
        {
            dbUser->CopyTo( *user );
            S_Login sLogin{ ._userId = *dbUser->GetId() };
			for ( auto& pc : pcList )
				sLogin._pcIds.push_back( *pc->GetId() );

            user->Send( sLogin.Write() );
        } );

        return true;
	} );

    if ( !tx )
		user->PostTx( std::move( localTx ) );

    return ELogicResult::Pending;
}

LoginTx::CheckResult LoginTx::CheckPreCondition( UserRef user, C_Login& login )
{
    if( user->_loginState != ELoginState::Init )
        return { ELogicResult::AlreadyDeadPc, 0 };

    return { ELogicResult::Success, 0 };
}

LoginTx::SelectResult LoginTx::SelectFromCache( UserRef user, C_Login& login )
{
    IDbModelCache* dbModelCache{};
    auto cacheUser{ User::GetUser( login._account ) };
	if ( !cacheUser )
	    return { ELogicResult::NullUser, {}, {} };

    auto pcPtrList{ Pc::GetPcList( ::UserId{} ) };
    return { ELogicResult::Success, std::move( cacheUser ), { pcPtrList.begin(), pcPtrList.end() } };
};

LoginTx::SelectResult LoginTx::SelectFromDb( UserRef user, C_Login& login, DbConnection* dbConnection )
{
    LeftJoin< UserDbModel, PcDbModel > leftJoin;
    leftJoin._connection = dbConnection;
    leftJoin.BindAllColumn();
    auto result = leftJoin.On().Equal< UserDbModel::Id, PcDbModel::UserId >()
            .Where().Equal< UserDbModel::Account >( login._account )
            .Limit( 3 )
		    .Select();
    if ( result == EDbResult::QueryFailure )
        return { ELogicResult::DbFail, {}, {} };
    if ( result == EDbResult::NoData )
        return { ELogicResult::NullUser, {}, {} };

    auto [ userList, pcList ] = leftJoin.ToList< UserDbModel::PRIMARY, PcDbModel::PRIMARY >();
    return { ELogicResult::Success, std::move( userList[ 0 ] ), std::move( pcList ) };
}

ELogicResult RegisterUserTx::Register( UserRef user, String& account, String& password, TxContextRef tx )
{
    auto localTx = tx;
    if ( !localTx )
        localTx = TxContext::New();

    auto newUser = tx->Create< User >();
    newUser->SetKeyId( IdHelper::Next< UserId >() );
    newUser->SetKeyAccount( account );
    newUser->SetPcId( IdHelper::Next< ::PcId >() );
    newUser->SetPassword( password );

    auto newPc = tx->Create< Pc >();
    newPc->SetKeyId( newUser->GetPcId() );
    newPc->SetKeyUserId( newUser->GetId() );

    tx->EndWith( [ user, /*tx = tx.get(), */newUser, newPc ]
    {
        auto newUserPtr{ std::static_pointer_cast< User >( User::GetFactory() ) };
        newUser->CopyTo( *newUserPtr );
        //GUserCache->Register( newUserPtr );

        // 작업성공시 반영
        newUser->CopyTo( *user );
    } );

    if ( !tx )
        user->PostTx( std::move( localTx ) );

    return ELogicResult::Pending;
}
