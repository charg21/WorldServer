import Mala.Core.Types;
import Mala.Core.StrongType;
import Mala.Container;

import <iostream>;
import WorldObject;
import Pc;
import User;
import DesignTypes;
import QuestDesign;
import Quest.QuestJob;
import ELogicResult;

using namespace Mala::Container;

import AuthPacket.C_Login;
import CombatPacket.S_StartSkill;

ELogicResult QuestJob::Do( PcRef pc, C_Login& login )
{
    auto [ result, questDesign ] = CheckCondition( pc, login );
    if ( result != ELogicResult::Success )
        return result;

    //pc->Post( [ pc, questDesign, targetId = startSkil._targetId ]() mutable
    //{
    //} );

    return ELogicResult::Pending;
}

QuestJob::CheckResult QuestJob::CheckCondition( PcRef pc, C_Login& login )
{
    //if( !pc->IsAlive() )
    //    return { ELogicResult::AlreadyDeadPc, 0 };

    /////
    //auto questDesign = QuestDesignPtr( QuestDesignId{ ( int )startSkil._questDesignId } );
    //if( !questDesign )
    //    return { ELogicResult::InvalidDesignId, 0 };

    return { ELogicResult::Success, 0 };
}
