module;

export module Quest.QuestJob;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Core.StrongType;
import Mala.Container;

import WorldObject;
import Pc;
import WorldObjectTypes;
import DesignTypes;
import ELogicResult;

using namespace Mala::Container;

EXPORT_BEGIN

class C_Login;

class QuestJob
{
public:
	/// <summary>
	/// 검사 결과
	/// </summary>
	using CheckResult = Tuple< ELogicResult, int >;

public:
	/// <summary>
	///
	/// </summary>
	static ELogicResult Do( PcRef pc, C_Login& login );

private:
	/// <summary>
	/// 조건을 검사한다.
	/// </summary>
	static CheckResult CheckCondition( PcRef pc, C_Login& packet );

};

EXPORT_END
