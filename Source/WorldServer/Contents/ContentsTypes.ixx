export module ContentsTypes;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
//import Mala.Reflection;

using namespace Mala::Container;

EXPORT_BEGIN

USING_SHARED_PTR( IRootMotionManager );
USING_SHARED_PTR( RootMotionManager );
USING_SHARED_PTR( SKill );
USING_SHARED_PTR( EquipSlot );
USING_SHARED_PTR( Item );

using ItemPtrVector = Vector< ItemPtr >;

EXPORT_END
