module;

import <Macro.h>;

export module BroadCastHelper;

import Mala.Core.Types;
import Mala.Core.TypeCast;
import Mala.Net.SendBuffer;
import Mala.Container;
import ThreadLocal;
import ClusterManager;
import AoiManager;
import WorldObject;
import Pc;
import MovementComponent;
import WorldObjectTypes;
import WorldHelper;
import DesignTypes;
import WorldHelper;

using namespace Mala::Core;
using namespace Mala::Container;
using namespace Mala::Net;

EXPORT_BEGIN

namespace BroadCastHelper
{

void Send( WorldObjectRef caster, SendBufferRef sendBuffer )
{
    auto& objectList = WorldHelper::GatherWorldObjectList(
        caster->_movement->_pos,
        100,
        EWorldObjectType::Pc );

    for ( WorldObject* object : objectList )
    {
        auto* pc = TypeCast< Pc >( object );
        if ( !pc )
            continue;

        pc->Send( sendBuffer );
    }
}


};

EXPORT_END
