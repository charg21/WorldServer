export module SkillArgs;

import <Macro.h>;

import <memory>;
import Mala.Core.Types;
import WorldObjectTypes;
import DesignTypes;

struct Cluster;

using namespace Mala::Net;

EXPORT_BEGIN


struct SkillArgs
{
    /// <summary>
    /// 스킬 식별자
    /// </summary>
    SkillDesignId skillDesignId;

    /// <summary>
    /// 타겟 식별자
    /// </summary>
    WorldObjectId targetId;
};


EXPORT_END