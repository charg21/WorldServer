import <Macro.h>;

import Mala.Core.Types;
import Mala.Core.StrongType;
import Mala.Db.TxContext;

import Item.EquipItemTx;

import WorldObject;
import Pc;
import User;
import WorldObjectTypes;
import ELogicResult;

using namespace Mala::Container;
using namespace Mala::Db;


/// <summary>
/// 아이템 장착 TX
/// </summary>
ELogicResult EquipItemTx::Equip( PcRef pc, ItemId itemId, TxContextRef tx )
{
    auto localTx = tx;
    if ( !localTx )
        localTx = TxContext::New();

    if ( !tx )
        pc->PostTx( std::move( localTx ) );

    return ELogicResult::Pending;
}
