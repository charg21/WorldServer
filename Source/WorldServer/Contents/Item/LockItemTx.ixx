module;

import <Macro.h>;

export module Item.LockItemTx;

import Mala.Core.Types;
import Mala.Core.StrongType;
import Mala.Db.TxContext;

import User;
import WorldObjectTypes;
import ELogicResult;

using namespace Mala::Container;
using namespace Mala::Db;

EXPORT_BEGIN

/// <summary>
/// 아이템 잠금 TX
/// </summary>
class LockItemTx
{
public:
    /// <summary>
    /// 검사 결과
    /// </summary>
    using CheckResult = Tuple< ELogicResult, i32 >;

public:
    /// <summary>
    /// 잠금을 해제한다.
    /// </summary>
    static ELogicResult Run( PcRef pc, ItemId itemId, bool lock, TxContextRef tx = nullptr );
};

EXPORT_END
