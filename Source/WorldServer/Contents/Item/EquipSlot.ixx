export module EquipSlot;

import <string>;
import <string_view>;
import <vector>;
import <functional>;

import Mala.Core.Variant;
import Mala.Core.CoreTLS;

import ContentsTypes;
import ContentsDbTypes;
import EquipSlotDbModel;
import PktEquip;
import Item;

using namespace std;

/// <summary>
/// 장비 슬롯
/// </summary>
export struct EquipSlot final : public EquipSlotDbModel
{
    ItemWeakPtr _item;

    ~EquipSlot() final = default;

    void ExportToEquip( PktEquip& pktEquip )
    {
        pktEquip._itemId = *GetItemId();
        pktEquip._slot   = GetSlot();
    }

    void SetItem( ItemRef item )
    {
        _item = item;
    }

    virtual void Sync()
    {
    }
};
