module;

import <Macro.h>;

export module Item.EquipItemTx;

import Mala.Core.Types;
import Mala.Core.StrongType;
import Mala.Db.TxContext;

import User;
import WorldObjectTypes;
import ELogicResult;

using namespace Mala::Container;
using namespace Mala::Db;

EXPORT_BEGIN

/// <summary>
/// 아이템 장착 TX
/// </summary>
class EquipItemTx
{
public:
    /// <summary>
    /// 검사 결과
    /// </summary>
    using CheckResult = Tuple< ELogicResult, int >;

public:
    /// <summary>
    /// 장착
    /// </summary>
    static ELogicResult Equip( PcRef pc, ItemId itemId, TxContextRef tx = nullptr );
};

EXPORT_END
