export module Item;

import <string>;
import <string_view>;
import <vector>;
import <functional>;

import Mala.Core.Variant;
import Mala.Core.CoreTLS;

import ItemDbModel;
import PktItem;

using namespace std;

/// <summary>
/// 아이템
/// </summary>
export struct Item final : public ItemDbModel
{
    ~Item() final = default;

    void ExportToItem( PktItem& pktItem )
    {
        pktItem._id = *GetId();
        pktItem._designId = *GetDesignId();
        pktItem._count = GetCount();
    }
};
