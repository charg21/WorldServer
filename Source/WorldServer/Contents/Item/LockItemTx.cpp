import <Macro.h>;

import Mala.Core.Types;
import Mala.Core.StrongType;
import Mala.Db.TxContext;

import Item.LockItemTx;

import WorldObject;
import Pc;
import User;
import Item;
import WorldObjectTypes;
import ELogicResult;
import BagComponent;

using namespace Mala::Container;
using namespace Mala::Db;

/// <summary>
/// 잠근다.
/// </summary>
ELogicResult LockItemTx::Run( PcRef pc, ItemId itemId, bool lock, TxContextRef tx )
{
    auto localTx = tx;
    if ( !localTx )
        localTx = TxContext::New();

    localTx->AddReadyJob( [ weakPc = pc->weak_from_this(), itemId, lock ]( auto& tx )
    {
        auto pc{ weakPc.lock() };
        if ( !pc )
            return false;

        ItemPtr item;
        if ( !pc->_pcBag.TryGet( itemId, item ) )
            return false;

        if ( item->GetLocked() == lock )
            return false;

        auto planItem{ tx->Update< Item >( item ) };
        planItem->SetLocked( lock );

        return true;
    } );

    if ( !tx )
        pc->PostTx( std::move( localTx ) );

    return ELogicResult::Pending;
}
