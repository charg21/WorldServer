import Mala.Core.Types;
import Mala.Container.String;
import Mala.Core.Format;

import WorldObjectTypes;
import WorldObject;
import AoiManager;
import Consts;
import ClusterManager;
import ThreadLocal;
import WorldHelper;
import IComponent;
import AggroComponent;
import ViewComponent;
import Npc;
import Pc;


AggroComponent::AggroComponent( WorldObject& owner )
: IComponent{ owner }
{
    _typeId = (int)EComponentType::Aggro;
}

NpcAggroComponent::NpcAggroComponent( Npc& owner )
: AggroComponent{ owner }
{
}

Npc& NpcAggroComponent::GetNpc()
{
    return static_cast<Npc&>( _owner );
}

NpcViewComponent& NpcAggroComponent::GetNpcView()
{
    return GetNpc()._npcView;
}

void NpcAggroComponent::OnDamage( WorldObjectRef object, int damage )
{
    /// 현재는 단순 누적 대미지 기반 가중치를 줌
    Add( object->GetObjectId(), damage );
}

void NpcAggroComponent::OnKill( WorldObjectRef object )
{
    /// 일단 어그로에서 제거
    Remove( object->GetObjectId() );
}

void NpcAggroComponent::Add( WorldObjectId objectId, int aggro )
{
    _aggroMap[ objectId ] += aggro;
}

void NpcAggroComponent::Remove( WorldObjectId objectId )
{
    _aggroMap.erase( objectId );
}

void NpcAggroComponent::Reset()
{
    _aggroMap.clear();
}

WorldObjectPtr NpcAggroComponent::GetTaget()
{
    if ( _aggroMap.empty() )
        return {};

    auto [ actorId, _ ] = *std::max_element(
        _aggroMap.begin(),
        _aggroMap.end(),
        []( const auto& lhs, const auto& rhs )
        {
            return lhs.second < rhs.second;
        } );

    return GetNpcView().GetObjectPtr( actorId );
}
