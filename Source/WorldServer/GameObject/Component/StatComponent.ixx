export module StatComponent;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;


import WorldObject;
import WorldObjectTypes;

import IComponent;
import Consts;

import WorldTypes;
import EWorldObjectType;
import ComponentType;
import EStat;
import PktStat;
import StatDbModel;


using namespace Mala::Container;
using namespace Mala::Db;
using namespace Mala::Math;


export enum class EStatCategory
{
    /// <summary>
    /// 전체
    /// </summary>
    Total = 0,

    /// <summary>
    /// 영구 스탯
    /// </summary>
    Permanent = 1,

    /// <summary>
    /// 장비 스탯
    /// </summary>
    Equip = 2,

    Max
};


export using StatList = Array< i64, (i32)( EStat::Max ) >;
using StatList2D = Array< StatList, (i32)( EStatCategory::Max ) >;

/// <summary>
/// 스탯 컴포넌트
/// </summary>
export class StatComponent : public IComponent
{
public:
    GENERATE_CLASS_TYPE_INFO( StatComponent );

public:
    inline static EComponentType Type{ EComponentType::Stat };

public:
    /// <summary>
    /// 생성자
    /// </summary>
    StatComponent( WorldObject& owner, StatList& statList );
    ~StatComponent() override = default;

    /// <summary>
    /// 스레드 세이프하게 스탯 접근을 위한 뷰
    /// </summary>
    StatList& GetTotalStats();
    void ChangeStat( EStat stat, i64 value, EStatCategory category = EStatCategory::Permanent );

protected:
    /// <summary>
    /// 전체 스탯( 다른 스레드에서 스레드세이프하게 접근 가능함 )
    /// </summary>
    StatList& _totalStats;

    /// <summary>
    /// 현재 체력
    /// </summary>
    i32 _hp{};
};

/// <summary>
/// Pc 스탯 컴포넌트
/// </summary>
export class PcStatComponent final : public StatComponent, public IDbLoadComponent
{
    using StatDbModelList = Vector< std::shared_ptr< StatDbModel > >;

public:
    GENERATE_CLASS_TYPE_INFO( PcStatComponent );

public:
    /// <summary>
    /// 생성자
    /// </summary>
    PcStatComponent( Pc& owner );
    ~PcStatComponent() final = default;

    Pc& GetPc();
    PcId GetPcId();
    void LoadComponentFromDb( TxContextRef tx ) final;
    void ExportToStatList( Vector< PktStat >& stats );
    void ImportFromStatList( StatDbModelList&& stats );
    void Reset();

    void OnTick( f32 deltaTime ) final;

private:
    StatList2D _stats2d;
};

/// <summary>
/// Npc 스탯 컴포넌트
/// </summary>
export class NpcStatComponent final : public StatComponent
{
public:
    GENERATE_CLASS_TYPE_INFO( NpcStatComponent );

public:
    using StatMap = Map< EStat, i64 >;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    NpcStatComponent( Npc& owner );
    ~NpcStatComponent() final = default;

    Npc& GetNpc();
    bool LoadFromDesign();

private:
    StatList _totalStats;
    StatMap _stats;
    class NpcDesign* _design;
};
