import <ranges>;
import Mala.Core;
import Mala.Core.CoreGlobal;
import Mala.Core.TypeCast;
import Mala.Container;
import Consts;
import ThreadLocal;
import WorldHelper;
import WorldObject;
import WorldObjectTypes;
import Pc;

import IComponent;
import QuestComponent;
import PacketExporter;


using namespace Mala::Container;
using namespace Mala::Threading;
using namespace Mala::Math;


QuestComponent::QuestComponent( Pc& owner )
: IComponent( owner )
{
}

Pc& QuestComponent::GetPc()
{
	return static_cast< Pc& >( _owner );
}

void QuestComponent::ExportToQuestList( Vector< PktQuest >& questList )
{
    for ( auto& [_, quest] : _questMap )
    {
        PktQuest& pktQuest{ questList.emplace_back() };
        pktQuest._designId = quest->GetDesignId();
    }
}

QuestPtr QuestComponent::GetQuest( QuestDesignId questDesignId )
{
    auto iter = _questMap.find( questDesignId );
    if ( iter == _questMap.end() )
        return nullptr;

    return iter->second;
}

QuestRef QuestComponent::GetQuestRef( QuestDesignId questDesignId )
{
    auto iter = _questMap.find( questDesignId );
    if ( iter == _questMap.end() )
        return nullptr;

    return iter->second;
}

