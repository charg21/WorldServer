import Mala.Core.Types;

import WorldObjectTypes;
import WorldObject;
import AoiManager;
import ClusterManager;
import ThreadLocal;
import WorldHelper;


import IComponent;
import CombatComponent;
import StatComponent;
import Pc;
import Npc;
import NpcDesign;


/// <summary>
/// 생성자
/// </summary>
CombatComponent::CombatComponent( WorldObject& owner )
: IComponent{ owner }
{
    _typeId = (int)EComponentType::Combat;
    //INIT_TL( Combat );
}

void CombatComponent::Update()
{
}

void CombatComponent::OnEnterWorld()
{
}

void CombatComponent::OnLeaveWorld()
{
}

void CombatComponent::OnFlush()
{

}

void CombatComponent::StartSkill( const SkillArgs& param )
{
}

/// <summary>
/// 생존 여부를 반환한다
/// </summary>
bool CombatComponent::IsAlive() const
{
    return true;
}

StatComponent& CombatComponent::GetStat()
{
    return *_owner._stat;
}

PcCombatComponent::PcCombatComponent( Pc& pc )
: CombatComponent{ pc }
{
}

Pc& PcCombatComponent::GetPc()
{
    return static_cast< Pc& >( _owner );
}

PcStatComponent& PcCombatComponent::GetPcStat()
{
    return GetPc()._pcStat;
}

NpcCombatComponent::NpcCombatComponent( Npc& npc )
: CombatComponent{ npc }
{
}

Npc& NpcCombatComponent::GetNpc()
{
    return static_cast< Npc& >( _owner );
}

Npc& NpcCombatComponent::GetNpc() const
{
    return static_cast< Npc& >( _owner );
}

NpcAggroComponent& NpcCombatComponent::GetAggro()
{
    return GetNpc()._aggro;
}

NpcStatComponent& NpcCombatComponent::GetNpcStat()
{
    return GetNpc()._npcStat;
}

void NpcCombatComponent::OnHit( WorldObjectRef target, i32 damage )
{

}

void NpcCombatComponent::OnDamage( WorldObjectRef attacker, i32 damage )
{
    GetAggro().OnDamage( attacker, damage );
}

void NpcCombatComponent::OnDead( WorldObjectRef killer )
{
    //GetNpc();
    GetAggro().Reset();
}

void NpcCombatComponent::OnKill( WorldObjectRef victim )
{
    GetAggro().OnKill( victim );
}

bool NpcCombatComponent::IsPeace()
{
    if ( !GetNpc()._design )
        return true; // 기본은 평화몹( 후공임 )

    return GetNpc()._design->_isPeace;
}

bool NpcCombatComponent::IsAggressive()
{
    return !IsPeace();
}

NpcAiComponent& NpcCombatComponent::GetAi()
{
    return GetNpc()._ai;
}
