export module IComponentOwner;

import <Macro.h>;

import Mala.Container;
import Mala.Container.EnumFlagMap;
import Mala.Core.EnumHelper;
import Mala.Core.TypeTraits;
import Mala.Memory;
import Mala.Reflection;

import IComponent;
import ComponentType;

using namespace Mala;
using namespace Mala::Container;

constexpr bool USING_ENUM_FLAG_MAP = true;

export class IComponentOwner
{
	using ComponentHashMap = HashMap< EComponentType, IComponent* >;
    using ComponentEnumMap = EnumFlagMap< EComponentType, IComponent* >;

    using ComponentMap = ConditionalT< USING_ENUM_FLAG_MAP, ComponentEnumMap, ComponentHashMap >;

public:
    IComponentOwner( class WorldObject& owner );
    virtual ~IComponentOwner();

    template< std::derived_from< IComponent > T >
    bool Has()
    {
		return EnumHelper< EComponentType >::HasFlag( _ownFlags, T::Type );
    }

    template< std::derived_from< IComponent > T >
    void Add()
    {
        if ( Has< T >() )
            return;

        _components.emplace( T::Type, (IComponent*)xnew< T >( _owner ) );
        EnumHelper< EComponentType >::SetFlag( _ownFlags, T::Type );
    }

    template< std::derived_from< IComponent > T >
    void Add( T* component )
    {
        if ( Has< T >() )
            return;

        _components.emplace( T::Type, (IComponent*)component );
        EnumHelper< EComponentType >::SetFlag( _ownFlags, T::Type );
    }

    template< std::derived_from< IComponent > T >
    T* Get()
    {
        if ( !Has< T >() )
            return nullptr;

        return reinterpret_cast< T* >( _components[ T::Type ] );
    }

    template< std::derived_from< IComponent > T >
    T* Get() const
    {
        if ( !Has< T >() )
            return nullptr;

        return reinterpret_cast< T* >( _components[ T::Type ] );
    }

    template< std::derived_from< IComponent > T >
    T& Ensure()
    {
        Add< T >();
        return *Get< T >();
    }

	template< std::invocable< IComponent* > TJob >
	void ForEach( TJob job )
	{
		/*for ( auto& [ _, component ] : _components2 )
            job( component );*/

        for ( auto component : _components )
            job( component );
	}

    template< std::derived_from< IComponent > T >
    void Remove()
    {
        _components.erase( T::Type );
    }

    template< std::derived_from< IComponent > T >
    auto Extract()
    {
        return _components.extract( T::Type );
    }

private:
    EComponentType _ownFlags{ EComponentType::None };

    ComponentMap _components;

    class WorldObject& _owner;
};
