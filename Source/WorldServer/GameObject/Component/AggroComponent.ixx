export module AggroComponent;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Reflection;

import WorldObject;
import WorldObjectTypes;
import IComponent;
import Consts;
import ComponentType;

using namespace Mala::Container;
using namespace Mala::Math;


/// <summary>
/// 어그로 컴포넌트
/// </summary>
export class AggroComponent : public IComponent
{
public:
    GENERATE_CLASS_TYPE_INFO( AggroComponent );

public:
    inline static EComponentType Type{ EComponentType::Aggro };

public:
	/// <summary>
	/// 생성자
	/// </summary>
	AggroComponent( WorldObject& owner );
	~AggroComponent() override = default;

};


/// <summary>
/// Npc 어그로 컴포넌트
/// </summary>
export class NpcAggroComponent final : public AggroComponent
{
public:
	GENERATE_CLASS_TYPE_INFO( NpcAggroComponent );

public:
	friend class Npc;
	using AggroMap = HashMap< WorldObjectId, i64/* AggroValue */ >;

public:
	/// <summary>
	/// 생성자
	/// </summary>
	NpcAggroComponent( Npc& owner );
	~NpcAggroComponent() final = default;

	Npc& GetNpc();
	class NpcViewComponent& GetNpcView();
	void OnDamage( WorldObjectRef object, int damage );
	void OnKill( WorldObjectRef object );
	void Add( WorldObjectId actorId, int aggro = 1 );
	void Remove( WorldObjectId actorId );
	void Reset();

	WorldObjectPtr GetTaget();

private:
	AggroMap _aggroMap;
};
