import <Macro.h>;

import Mala.Container;
import Mala.Core.EnumHelper;
import Mala.Memory;

import WorldObject;
import ComponentType;
import IComponent;
import IComponentOwner;

/// <summary>
/// 생성자
/// </summary>
IComponentOwner::IComponentOwner( WorldObject& owner )
: _owner{ owner }
{
}

/// <summary>
/// 소멸자
/// </summary>
IComponentOwner::~IComponentOwner()
{
    //for ( auto* component : _components )
    //{
    //	xdelete( component );
    //}
}
