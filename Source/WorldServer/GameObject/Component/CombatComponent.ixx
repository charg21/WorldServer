export module CombatComponent;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Reflection;

import WorldObject;
import WorldObjectTypes;
import Consts;
import ComponentType;
import EWorldObjectType;
import WorldTypes;

import IComponent;
import DesignTypes;
import SkillArgs;

using namespace Mala::Container;
using namespace Mala::Math;

export class BuffList
{
};

struct CombatContext
{
};

/// <summary>
/// 전투 컴포넌트
/// </summary>
export class CombatComponent : public IComponent
{
    GENERATE_CLASS_TYPE_INFO( CombatComponent );

public:
    inline static EComponentType Type{ EComponentType::Combat };

public:
    CombatComponent( WorldObject& owner );
    ~CombatComponent() override = default;

    /// <summary>
    /// 갱신한다.
    /// </summary>
    void Update();

    void OnLeaveWorld() final;
    void OnEnterWorld() final;

    virtual void OnHit( WorldObjectRef target, i32 damage ) = 0;
    virtual void OnDamage( WorldObjectRef attacker, i32 damage ) = 0;
    virtual void OnRevive(){};
    virtual void OnDead( WorldObjectRef killer ) = 0;
    virtual void OnKill( WorldObjectRef victim ) = 0;

    void OnFlush() final;
    void StartSkill( const SkillArgs& param );
    virtual bool IsAlive() const;

    bool DoingCast() const { return _doingCasting; }
    void SetDoingCast( bool doingCast ) { _doingCasting = doingCast; }
    i64 CalcDamage() { return 1; }

    class StatComponent& GetStat();
    Circle GetHitCollision(){ return _collision; }

    // 캐스팅 여부
    bool _doingCasting{};

    Circle _collision;

    Field( _buffs )
    BuffList _buffs;
};


export class PcCombatComponent final : public CombatComponent
{
    GENERATE_CLASS_TYPE_INFO( PcCombatComponent );

public:
    PcCombatComponent( Pc& pc );
    ~PcCombatComponent() final = default;

    class Pc& GetPc();
    class PcStatComponent& GetPcStat();

    void OnHit( WorldObjectRef target, i32 damage ) final{};
    void OnDamage( WorldObjectRef attacker, i32 damage ) final{};
    void OnDead( WorldObjectRef killer ) final{};
    void OnKill( WorldObjectRef victim ) final{};
};


export class NpcCombatComponent final : public CombatComponent
{
    GENERATE_CLASS_TYPE_INFO( NpcCombatComponent );

public:
    NpcCombatComponent( Npc& pc );
    ~NpcCombatComponent() final = default;

    class Npc& GetNpc();
    class Npc& GetNpc() const;
    class NpcAiComponent& GetAi();
    class NpcAggroComponent& GetAggro();
    class NpcStatComponent& GetNpcStat();

    void OnHit( WorldObjectRef target, i32 damage ) final;
    void OnDamage( WorldObjectRef attacker, i32 damage ) final;
    void OnDead( WorldObjectRef killer ) final;
    void OnKill( WorldObjectRef victim ) final;

    bool IsPeace();
    bool IsAggressive();
};

