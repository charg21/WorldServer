import Mala.Core.Types;

import WorldObjectTypes;
import WorldObject;
import AoiManager;
import ClusterManager;
import ThreadLocal;
import WorldHelper;

import IComponent;
import ChatComponent;
import Pc;

import ChatPacket.S_Chat;

ChatComponent::ChatComponent( WorldObject& owner )
: IComponent{ owner }
{
}


PcChatComponent::PcChatComponent( Pc& pc )
: ChatComponent{ pc }
{
}

void PcChatComponent::Write( String&& message )
{
    S_Chat chat
    {
        ._message = std::move( message )
    };
    //chat._name = _owner.GetName();
    chat._message = std::move( message );
}

void PcChatComponent::Listen( const String& message )
{
}
