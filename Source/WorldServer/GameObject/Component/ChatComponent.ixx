﻿export module ChatComponent;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Reflection;

import IComponent;
import WorldObject;
import WorldObjectTypes;
import Consts;
import WorldTypes;
import EWorldObjectType;
import ComponentType;

using namespace Mala::Container;
using namespace Mala::Math;

/// <summary>
/// 채팅 컴포넌트
/// </summary>
export class ChatComponent : public IComponent
{
    GENERATE_CLASS_TYPE_INFO( ChatComponent );

public:
    inline static EComponentType Type{ EComponentType::Chat };

public:
    ChatComponent( WorldObject& owner );
	~ChatComponent() override = default;
};

export class PcChatComponent final : public ChatComponent
{
	GENERATE_CLASS_TYPE_INFO( PcChatComponent );

public:
    PcChatComponent( Pc& pc );
    ~PcChatComponent() final = default;

    void Write( String&& message );
    void Listen( const String& message );
};
