export module SkillListComponent;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Reflection;


import WorldObject;
import WorldObjectTypes;

import IComponent;
import Consts;
import ComponentType;
import SkillDesign;


using namespace Mala::Container;
using namespace Mala::Math;


/// <summary>
/// 스킬 목록 컴포넌트
/// </summary>
export class SkillListComponent : public IComponent
{
public:
    GENERATE_CLASS_TYPE_INFO( SkillListComponent );

public:
    inline static EComponentType Type{ EComponentType::SkillList };

    using SkillList = Vector< const SkillDesign* >;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    SkillListComponent( WorldObject& owner );
    ~SkillListComponent() override = default;

};

/// <summary>
/// Pc 스킬 목록 컴포넌트
/// </summary>
export class PcSkillListComponent final : public SkillListComponent
{
public:
    GENERATE_CLASS_TYPE_INFO( PcSkillListComponent );

public:
    /// <summary>
    /// 생성자
    /// </summary>
    PcSkillListComponent( Pc& owner );
    ~PcSkillListComponent() final = default;
};

/// <summary>
/// Npc 스킬 목록 컴포넌트
/// </summary>
export class NpcSkillListComponent final : public SkillListComponent
{
public:
    GENERATE_CLASS_TYPE_INFO( NpcSkillListComponent );

public:
    /// <summary>
    /// 생성자
    /// </summary>
    NpcSkillListComponent( Npc& owner );
    ~NpcSkillListComponent() final = default;
};
