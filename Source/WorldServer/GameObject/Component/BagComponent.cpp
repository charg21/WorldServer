#include "Macro.h"

import <ranges>;
import Mala.Core.Types;
import Mala.Core.CoreTLS;
import Mala.Container;
import Mala.Container.String;
import Mala.Core.Format;
import Mala.Db.DbModelHelper;

import WorldObjectTypes;
import WorldObject;
import AoiManager;
import Consts;
import ContentsDbTypes;
import ContentsTypes;
import ClusterManager;
import ThreadLocal;
import WorldHelper;
import Pc;
import Npc;
import ItemDbModel;
import IdHelper;

import IComponent;
import BagComponent;
import Item;
import PktItem;
import ELogicResult;

using namespace Mala::Db;
using namespace Mala::Container;

BagComponent::BagComponent( WorldObject& owner )
: IComponent{ owner }
{
    _typeId = (int)EComponentType::Bag;
}

void BagComponent::OnFlush()
{
}

ELogicResult BagComponent::Add( ItemRef item )
{
    if ( _items.emplace( item->GetId(), item ).second )
        return ELogicResult::Success;
    else
        return ELogicResult::AlreadyAddedItem;
}

ELogicResult BagComponent::Add( ItemPtr&& item )
{
	if ( _items.emplace( item->GetId(), std::move( item ) ).second )
        return ELogicResult::Success;
    else
        return ELogicResult::AlreadyAddedItem;
}

bool BagComponent::Remove( ItemId itemId )
{
    return _items.erase( itemId );
}

bool BagComponent::Contains( ItemId itemId )
{
    return _items.contains( itemId );
}

ItemPtr BagComponent::Get( ItemId itemId )
{
	auto iter = _items.find( itemId );
	if ( iter != _items.end() )
		return iter->second;

    return {};
}

ItemPtr BagComponent::GetByDesignId( ItemDesignId itemDesignId )
{
    for ( auto& item : _items | std::views::values )
    {
        if ( item->GetDesignId() == itemDesignId )
            return item;
    }

    return {};
}

bool BagComponent::TryGet( ItemId itemId, ItemPtr& outItem )
{
    auto iter = _items.find( itemId );
    if ( iter != _items.end() )
    {
        outItem = iter->second;
        return true;
    }

    return false;
}

bool BagComponent::TryGetByDesignId( ItemDesignId itemDesignId, ItemPtr& outItem )
{
    for ( auto& item : _items | std::views::values )
    {
        if ( item->GetDesignId() == itemDesignId )
        {
            outItem = item;
            return true;
        }
    }

    return false;
}

bool BagComponent::Empty()
{
    return _items.empty();
}

auto BagComponent::begin()
{
	return _items.begin();
}

auto BagComponent::end()
{
    return _items.end();
}

PcBagComponent::PcBagComponent( Pc& owner )
: BagComponent{ owner }
{
}

Pc& PcBagComponent::GetPc()
{
	return static_cast< Pc& >( _owner );
}

void PcBagComponent::LoadComponentFromDb( TxContextRef tx )
{
    tx->AddDbJob( [ tx = tx.get(), this ]( auto* dbConn )
    {
        auto itemList{ Item::SelectMany< Item >( *GetPc().GetObjectId() ) };
        if ( !itemList.has_value() )
            return false;

        if ( itemList->empty() )
            DoTestGiveItem( dbConn, itemList.value() ); /// Item 지금 테스트

        tx->EndWith( [ this, itemList{ std::move( itemList.value() ) } ] mutable
        {
            Reset();
            ImportFromDbList( std::move( itemList ) );
        } );

        return true;
    } );
}

void PcBagComponent::ImportFromDbList( ItemPtrVector&& itemList )
{
    _items.reserve( itemList.size() );

    for ( auto& item : itemList )
        Add( std::move( item ) );
}

void PcBagComponent::Reset()
{
	_items.clear();
}

void PcBagComponent::DoTestGiveItem( DbConnection* dbConn, Vector< ItemPtr >& items )
{
	auto itemPtr{ std::static_pointer_cast< Item >( Item::GetFactory() ) };
	itemPtr->SetKeyOwnerId    ( *GetPc().GetObjectId()     );
	itemPtr->SetKeyRootOwnerId( *GetPc().GetObjectId()     );
	itemPtr->SetDesignId      ( { 1 }                      );
	itemPtr->SetId            ( IdHelper::Next< ItemId >() );
	itemPtr->SetCount         ( 1                          );
	itemPtr->SetLocked        ( false                      );
	if ( !itemPtr->Insert( dbConn ) )
		return ERROR_LOG( L"Item Insert Fail" );

	items.emplace_back( std::move( itemPtr ) );
}

void PcBagComponent::ExportToItem( ItemId itemId, PktItem& outItem )
{
}

void PcBagComponent::ExportToItemList( Vector< PktItem >& itemList )
{
    for ( auto& [ _, item ] : _items )
        item->ExportToItem( itemList.emplace_back() );
}

NpcBagComponent::NpcBagComponent( Npc& owner )
: BagComponent{ owner }
{
}

Npc& NpcBagComponent::GetNpc()
{
    return static_cast< Npc& >( _owner );
}
