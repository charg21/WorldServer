export module AiComponent;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;

import WorldObject;
import WorldObjectTypes;
import IComponent;
import ComponentType;
import NpcFsm;

using namespace Mala::Net;
using namespace Mala::Container;

EXPORT_BEGIN

enum class EWakeUpState : u8
{
    Sleep,
    Awaken
};

/// <summary>
/// 인공능지
/// <summary
class AiComponent : public IComponent
{
public:
    GENERATE_CLASS_TYPE_INFO( AiComponent );

public:
    /// <summary>
    /// 생성자
    /// </summary>
    AiComponent( WorldObject& owner ) : IComponent{ owner } {};
    ~AiComponent() override = default;

    void Tick( f32 deltaTime )
    {
        OnTick( deltaTime );
    }

    /// 갱신한다
    virtual void OnTick( f32 deltaTime ) abstract;
};

/// <summary>
///  Npc 인공 능지
/// </summary>
struct NpcAiComponent : public AiComponent
{
public:
    GENERATE_CLASS_TYPE_INFO( NpcAiComponent );

public:
    /// <summary>
    /// 생성자
    /// </summary>
    NpcAiComponent( Npc& owner );
    NpcAiComponent( const NpcAiComponent& rhs ) = default;
	~NpcAiComponent() override = default;

    /// <summary>
    /// 갱신한다
    /// </summary>
    void OnTick( f32 deltaTime ) override;
    virtual void OnDead() = 0;
    virtual void Init(){}// = 0;

    struct NpcAggroComponent& GetAggro();
    struct NpcMovementComponent& GetMovement();

protected:
    class Npc& _owner;
};

/// <summary>
///  Npc Fsm 인공 능지
/// </summary>
struct NpcFsmAiComponent final : public NpcAiComponent
{
public:
    GENERATE_CLASS_TYPE_INFO( NpcFsmAiComponent );

public:
    /// <summary>
    /// 생성자
    /// </summary>
    NpcFsmAiComponent( Npc& owner );
    NpcFsmAiComponent( const NpcFsmAiComponent& rhs ) = delete;
    ~NpcFsmAiComponent() final = default;

    /// <summary>
    /// 갱신한다
    /// </summary>
    void OnTick( f32 deltaTime ) final{ _fsm.OnTick( deltaTime ); }
    void OnDead() final;
    void Init() final { _fsm.Init( _owner ); }
    NpcFsm& GetFsm(){ return _fsm; }

protected:
    NpcFsm _fsm;
};

/// <summary>
///  Npc Fsm 행동 트리 인공 능지
/// </summary>
struct NpcBehaviorTreeAiComponent final : public NpcAiComponent
{
public:
    GENERATE_CLASS_TYPE_INFO( NpcBehaviorTreeAiComponent );

public:
    /// <summary>
    /// 생성자
    /// </summary>
    NpcBehaviorTreeAiComponent( Npc& owner );
    NpcBehaviorTreeAiComponent( const NpcBehaviorTreeAiComponent& rhs ) = delete;
    ~NpcBehaviorTreeAiComponent() final = default;

    /// <summary>
    /// 갱신한다
    /// </summary>
    void OnTick( f32 deltaTime ) final{}
    void Init() final {}
    void OnDead() final{};
};



EXPORT_END

