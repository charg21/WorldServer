export module QuestComponent;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container.MapHelper;
import Mala.Reflection;


import WorldObject;
import WorldObjectTypes;

import IComponent;
import Consts;
import WorldTypes;
import EWorldObjectType;
import ComponentType;
import QuestDbModel;
import PktQuest;


using namespace Mala::Container;
using namespace Mala::Math;



export class Quest : public QuestDbModel
{
    GENERATE_CLASS_TYPE_INFO( Quest );

    bool IsCompleted(){ return false; }
    bool IsProgresse(){ return false; }
};

export using QuestPtr = std::shared_ptr< Quest >;
export using QuestRef = const QuestPtr&;

/// <summary>
/// 퀘스트 컴포넌트
/// </summary>
export class QuestComponent : public IComponent
{
    using QuestMap = HashMap< QuestDesignId, QuestPtr >;

public:
    GENERATE_CLASS_TYPE_INFO( QuestComponent );

public:
    inline static EComponentType Type{ EComponentType::Quest };

public:
    /// <summary>
    /// 생성자
    /// </summary>
    QuestComponent( Pc& owner );
    ~QuestComponent() override = default;

    Pc& GetPc();

    bool IsProgress( QuestDesignId questDesignId );
    bool IsProgress( QuestStepDesignId questStepDesignId );
    bool IsCompleted( QuestDesignId questDesignId );
    bool IsCompleted( QuestStepDesignId questStepDesignId );

    void ExportToQuestList( Vector< PktQuest >& questList );

    QuestPtr GetQuest( QuestDesignId questDesignId );
    QuestRef GetQuestRef( QuestDesignId questDesignId );

private:
    QuestMap _questMap;
};
