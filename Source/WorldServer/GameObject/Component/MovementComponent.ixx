export module MovementComponent;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Reflection;

import WorldObject;
import WorldObjectTypes;
import Consts;
import ComponentType;
import EWorldObjectType;
import IComponent;
import WorldTypes;
import PktVector3;


using namespace Mala::Container;
using namespace Mala::Math;


/// <summary>
/// 이동 컴포넌트
/// </summary>
export class MovementComponent : public IComponent
{
public:
    GENERATE_CLASS_TYPE_INFO( MovementComponent );

public:
    inline static EComponentType Type{ EComponentType::Movement };

public:
    /// <summary>
    /// 생성자
    /// </summary>
    MovementComponent( WorldObject& owner );
    ~MovementComponent() override = default;

    /// <summary>
    /// 좌표를 반환한다
    /// </summary>
    PktVector3 GetPktPos() const;
    const Position& GetPos() const;
    const Position& GetFromPos() const;
    const Position& GetToPos() const;
    Direction GetDir() const;
    float GetDirYaw() const;

    virtual void SetPos( const Position& pos ) abstract;
    void SetToPos( const Position& pos );
    void SetPosOnly( const Position& pos );
    bool IsArrived() const;

    /// <summary>
    /// 이동한다
    /// </summary>
    void MoveTo( const Position& pos, const Direction& dir );

    class StatComponent& GetStat();
    class ViewComponent& GetView();
    virtual void OnMove(){};

public:
    Field( _pos )
    Position _pos{};

    Field( _fromPos )
    Position _fromPos{};

    Field( _toPos )
    Position _toPos{};

    Field( _dir )
    Direction _dir{};
};

/// <summary>
/// Pc 이동 컴포넌트
/// </summary>
export class PcMovementComponent final : public MovementComponent
{
public:
    GENERATE_CLASS_TYPE_INFO( PcMovementComponent );

public:
    /// <summary>
    /// 생성자
    /// </summary>
    PcMovementComponent( Pc& owner );
    ~PcMovementComponent() final = default;

    Pc& GetPc();
    void SetPos( const Position& pos ) final{};
    void OnMove() final;
};

/// <summary>
/// Npc 이동 컴포넌트
/// </summary>
export class NpcMovementComponent final : public MovementComponent
{
public:
    GENERATE_CLASS_TYPE_INFO( NpcMovementComponent );

public:
    /// <summary>
    /// 생성자
    /// </summary>
    NpcMovementComponent( Npc& owner );
    ~NpcMovementComponent() final = default;

    /// <summary>
    /// Npc를 반환한다.
    /// </summary>
    Npc& GetNpc();

    /// <summary>
    /// 좌표를 설정한다.
    /// </summary>
    void SetPos( const Position& pos ) final{};

    /// <summary>
    /// 복귀 위치
    /// </summary>
    Position _returnPos;

};
