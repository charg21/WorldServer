import <Macro.h>;

import Mala.Core.Types;
import Mala.Reflection;


import WorldObject;
import WorldObjectTypes;

import IComponent;
import Consts;
import Pc;
import PacketExporter;
import Npc;
import WorldTypes;
import EWorldObjectType;
import ComponentType;
import ViewComponent;
import MovementComponent;

import WorldPacket.S_Move;

MovementComponent::MovementComponent( WorldObject& owner )
: IComponent{ owner }
{
}

PktVector3 MovementComponent::GetPktPos() const
{
    return { _pos.X, _pos.Y, _pos.Z };
}

const Position& MovementComponent::GetPos() const
{
    return _pos;
}

const Position& MovementComponent::GetFromPos() const
{
    return _fromPos;
}

const Position& MovementComponent::GetToPos() const
{
    return _toPos;
}

Direction MovementComponent::GetDir() const
{
    return _dir;
}

float MovementComponent::GetDirYaw() const
{
    return Vector2ToYaw( _dir );
}

void MovementComponent::SetToPos( const Position& toPos )
{
    _toPos = toPos;
}

void MovementComponent::SetPosOnly( const Position& pos )
{
    _pos = pos;
}

void MovementComponent::MoveTo( const Position& pos, const Direction& dir )
{
    _pos = pos;
    _dir = dir;

    OnMove();
    GetView().OnMove();
}

bool MovementComponent::IsArrived() const
{
    return _pos == _toPos;
}

StatComponent& MovementComponent::GetStat()
{
    return *_owner._stat;
}

ViewComponent& MovementComponent::GetView()
{
    return *_owner._view;
}

PcMovementComponent::PcMovementComponent( Pc& owner )
: MovementComponent{ owner }
{
}

Pc& PcMovementComponent::GetPc()
{
    return static_cast< Pc& >( _owner );
}

void PcMovementComponent::OnMove()
{
    S_Move move;
    GetPc().ExportTo( move );
    GetPc().Send( move.Write() );
}

NpcMovementComponent::NpcMovementComponent( Npc& owner )
: MovementComponent{ owner }
{
}

Npc& NpcMovementComponent::GetNpc()
{
    return static_cast< Npc& >( _owner );
}
