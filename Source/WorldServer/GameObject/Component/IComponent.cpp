import <Macro.h>;

import Mala.Container;
import Mala.Core.EnumHelper;
import Mala.Memory;
import Mala.Reflection;

import WorldObject;
import ComponentType;
import IComponent;

/// <summary>
/// 생성자
/// </summary>
IComponent::IComponent( WorldObject& owner )
: _owner{ owner }
{
}

WorldObjectId IComponent::GetOwnerId()
{
    return _owner.GetObjectId();
}

