#include "Macro.h"

import Mala.Core.Types;
import Mala.Core.Format;
import Mala.Container.String;
import Mala.Db.TxContext;
import Mala.Db.DbModelHelper;
import WorldObjectTypes;
import WorldObject;
import Consts;
import ThreadLocal;
import Pc;
import Npc;

import IComponent;
import BagComponent;
import EquipComponent;
import EquipSlot;
import PktEquip;
import Item;

using namespace Mala::Db;
using namespace Mala::Threading;
using namespace Mala::Container;


EquipComponent::EquipComponent( WorldObject& owner )
: IComponent{ owner }
{
    _typeId = (int)EComponentType::Stat;
}

PcEquipComponent::PcEquipComponent( Pc& owner )
: EquipComponent{ owner }
{
}

NpcEquipComponent::NpcEquipComponent( Npc& owner )
: EquipComponent{ owner }
{
}

Npc& NpcEquipComponent::GetNpc()
{
    return static_cast< Npc& >( _owner );
}

Pc& PcEquipComponent::GetPc()
{
    return static_cast< Pc& >( _owner );
}

PcId PcEquipComponent::GetPcId()
{
    return GetPc().GetId();
}

PcBagComponent& PcEquipComponent::GetBag()
{
    return GetPc()._pcBag;
}

PcStatComponent& PcEquipComponent::GetStat()
{
    return GetPc()._pcStat;
}

void PcEquipComponent::LoadComponentFromDb( TxContextRef tx )
{
    tx->AddDbJob( [ tx = tx.get(), this ]( auto* dbCon )
    {
        auto equipList{ EquipSlot::SelectMany< EquipSlot >( GetPcId() ) };
        if ( !equipList.has_value() )
            return false;

        EnsureEquipSlot( dbCon, equipList.value() );

        tx->EndWith( [ this, equipList{ std::move( equipList.value() ) } ] mutable
        {
            Reset();
            ImportFromDbEquipList( std::move( equipList ) );
        } );

        return true;
    } );
}

void PcEquipComponent::ImportFromDbEquipList( Vector< EquipSlotPtr >&& equipList )
{
    for ( auto& equipSlot : equipList )
    {
        CRASH_IF_NULL( equipSlot );
        _equipSlots[ equipSlot->GetSlot() ] = std::move( equipSlot );
    }

    for ( auto& equipSlot : _equipSlots )
    {
        auto item = GetBag().Get( { equipSlot->GetItemId() } );
        CRASH_IF_NULL( item );
        equipSlot->SetItem( item );
    }
}

bool PcEquipComponent::Equip( EEquipSlot slot, ItemRef item )
{
    return false;
}

bool PcEquipComponent::EnsureEquipSlot( DbConnection* dbCon, Vector<EquipSlotPtr>& equipList )
{
    if ( equipList.size() == (int)EEquipSlot::Max )
        return true;

    /*if ( auto item{ GetBag().GetByDesignId( { 1 } ) }; item )
    {
        auto tx{ TxContext::New() };
        auto slot{ tx->Create< EquipSlot >() };
        slot->SetKeyOwnerId( GetPc().GetId() );
        slot->SetKeySlot( (i32)( EEquipSlot::Weapon ) );
        slot->SetItemId( item->GetId() );
        GetPc().PostTx( std::move( tx ) );
    }*/

    return true;
}

void PcEquipComponent::Unequip( EEquipSlot slot, ItemRef item )
{
}

void PcEquipComponent::ExportToEquip( EEquipSlot slot, PktEquip& outEquip )
{
}

void PcEquipComponent::ExportToEquipList( Vector< PktEquip >& equipList )
{
    equipList.reserve( _equipSlots.max_size() );
    for ( auto& equip : _equipSlots )
    {
        if ( !equip )
            continue;

        equip->ExportToEquip( equipList.emplace_back() );
    }
}

