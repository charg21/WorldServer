import <Macro.h>;

import Mala.Core.Random;
import Mala.Math;

import AggroComponent;
import AiComponent;
import Npc;
import World;
import WorldHelper;
import Config;

/// <summary>
/// 생성자
/// </summary>
NpcAiComponent::NpcAiComponent( Npc& owner )
: AiComponent{ owner }
, _owner{ owner }
{
}

/// <summary>
/// 갱신한다
/// </summary>
void NpcAiComponent::OnTick( f32 deltaTime )
{
	auto& movement = _owner._movement;
    if ( _owner.GetPos() == movement->_toPos )
        movement->_toPos = WorldHelper::GetNextPos( _owner.GetPos(), (EDirection)( Mala::Core::FastRand() % 4 ) );

    auto [ pos, dir ] = WorldHelper::GetNextPos(
        _owner.GetPos(),
        _owner.GetToPos(),
        deltaTime,
        Config::NpcMoveSpeed );

    GetMovement().MoveTo(pos, dir);
}

void NpcAiComponent::OnDead()
{
}

NpcAggroComponent& NpcAiComponent::GetAggro()
{
	return _owner._aggro;
}

NpcMovementComponent& NpcAiComponent::GetMovement()
{
    return _owner._npcMovement;
}

NpcFsmAiComponent::NpcFsmAiComponent( Npc& owner )
: NpcAiComponent{ owner }
{
}

void NpcFsmAiComponent::OnDead()
{
	GetFsm().ChangeState( ENpcFsmState::Dead );
}
