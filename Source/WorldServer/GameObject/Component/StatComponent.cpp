#include "Macro.h"

import Mala.Core.Types;
import Mala.Core.Format;
import Mala.Container;
import Mala.Container.String;
import Mala.Db.TxContext;
import Mala.Db.DbModelHelper;
import WorldObjectTypes;
import WorldObject;
import Consts;
import ThreadLocal;
import Pc;
import Npc;
import NpcDesign;

import IComponent;
import StatComponent;
import StatDbModel;
import CommonTypes;
import EStat;

using namespace Mala::Container;
using namespace Mala::Db;
using namespace Mala::Threading;


/// <summary>
/// 생성자
/// </summary>
StatComponent::StatComponent( WorldObject& owner, StatList& statList )
: IComponent{ owner }
, _totalStats{ statList }
{
	_typeId = (int)EComponentType::Stat;
}

StatList& StatComponent::GetTotalStats()
{
    return _totalStats;
}

void StatComponent::ChangeStat( EStat stat, i64 value, EStatCategory category )
{
    _totalStats[ (i32)stat ] += value;
}

PcStatComponent::PcStatComponent( Pc& owner )
: StatComponent{ owner, _stats2d[ (i32)( EStatCategory::Total ) ] }
{
}

NpcStatComponent::NpcStatComponent( Npc& owner )
: StatComponent{ owner, _totalStats }
{
}

Npc& NpcStatComponent::GetNpc()
{
    return static_cast< Npc& >( _owner );
}

bool NpcStatComponent::LoadFromDesign()
{
	auto* design = GetNpc()._design;
    if ( !design )
        return false;

    ChangeStat( EStat::Hp, design->_hp );
    ChangeStat( EStat::Power, design->_power );

    return true;
}

Pc& PcStatComponent::GetPc()
{
    return static_cast< Pc& >( _owner );
}

PcId PcStatComponent::GetPcId()
{
    return GetPc().GetId();
}

void PcStatComponent::LoadComponentFromDb( TxContextRef tx )
{
    tx->AddDbJob( [ tx = tx.get(), this ]( auto* dbCon )
    {
        auto statDbList{ StatDbModel::SelectMany< StatDbModel >( GetPcId() ) };
        if ( !statDbList.has_value() )
            return false;

        if ( statDbList->empty() ) // 기본 스탯 세팅
            return true;

        tx->EndWith( [ this, statDbList = std::move( statDbList.value() ) ] mutable
        {
            Reset();
            ImportFromStatList( std::move( statDbList ) );
        } );

        return true;
    } );
}

void PcStatComponent::ExportToStatList( Vector< PktStat >& stats )
{
    stats.reserve( _totalStats.size() );
    for ( auto statType : EStatList )
    {
        auto statValue = _totalStats[ (i32)statType ];
        if ( statValue == 0 )
            continue;

        stats.emplace_back(
            PktStat{
                ._type = statType,
                ._value = (i32)statValue } );
    }
}

void PcStatComponent::ImportFromStatList( StatDbModelList&& statDbList )
{
    for ( auto& stat : statDbList )
        ChangeStat( (EStat)( stat->GetType() ), stat->GetValue() );
}

void PcStatComponent::Reset()
{
    for ( auto& statList : _stats2d )
    {
        statList.fill( 0 );
    }
}

void PcStatComponent::OnTick( f32 deltaTime )
{
}
