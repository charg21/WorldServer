export module EquipComponent;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Reflection;


import WorldObject;
import WorldObjectTypes;
import IComponent;
import Consts;
import ContentsTypes;
import ContentsDbTypes;

import WorldTypes;
import EWorldObjectType;
import ComponentType;
import EquipSlotDbModel;
import EEquipSlot;

using namespace Mala::Container;
using namespace Mala::Db;
using namespace Mala::Math;


/// <summary>
/// 장비 장착 컴포넌트
/// </summary>
export class EquipComponent : public IComponent
{
public:
    GENERATE_CLASS_TYPE_INFO( EquipComponent );

public:
    inline static EComponentType Type{ EComponentType::Equip };

public:
    /// <summary>
    /// 생성자
    /// </summary>
    EquipComponent( WorldObject& owner );
    ~EquipComponent() override = default;

    /// <summary>
    /// 장착/장착 해제 한다.
    /// </summary>
    virtual bool Equip( EEquipSlot slot, ItemRef item ) = 0;
    virtual void Unequip( EEquipSlot slot, ItemRef item ) = 0;

};


export namespace Mala::Db
{
    class DbConnection;
};


/// <summary>
/// Pc 장비 장착 컴포넌트
/// </summary>
export class PcEquipComponent final : public EquipComponent, public IDbLoadComponent
{
public:
    GENERATE_CLASS_TYPE_INFO( PcEquipComponent );

public:
    using EquipSlots = Array< EquipSlotPtr, (i32)( EEquipSlot::Max ) >;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    PcEquipComponent( Pc& owner );
    ~PcEquipComponent() final = default;

    /// <summary>
    /// Pc를 반환한다.
    /// </summary>
    Pc& GetPc();
    PcId GetPcId();
    class PcBagComponent& GetBag();
    class PcStatComponent& GetStat();

    void Reset(){};
    void LoadComponentFromDb( TxContextRef tx ) final;
    void ImportFromDbEquipList( Vector< EquipSlotPtr >&& equipList );
    bool Equip( EEquipSlot slot, ItemRef item ) final;
    void Unequip( EEquipSlot slot, ItemRef item ) final;

    /// <summary>
    /// 정보를 내보낸다.
    /// </summary>
    void ExportToEquip( EEquipSlot slot, class PktEquip& outEquip );
    void ExportToEquipList( Vector< class PktEquip >& equipList );

    bool EnsureEquipSlot( DbConnection* dbCon, Vector< EquipSlotPtr >& equipList );

private:
    // 무기 방어구
    EquipSlots _equipSlots;

};

/// <summary>
/// Npc 장비 컴포넌트
/// </summary>
export class NpcEquipComponent final : public EquipComponent
{
public:
    GENERATE_CLASS_TYPE_INFO( NpcEquipComponent );

public:
    /// <summary>
    /// 생성자
    /// </summary>
    NpcEquipComponent( Npc& owner );
    ~NpcEquipComponent() final = default;

    /// <summary>
    /// Npc를 반환한다.
    /// </summary>
    Npc& GetNpc();

};
