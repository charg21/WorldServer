export module BagComponent;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Db.DbConnection;
import Mala.Reflection;

import WorldObject;
import WorldObjectTypes;
import IComponent;
import Consts;
import ComponentType;
import ContentsTypes;
import ContentsDbTypes;
import ItemDbModel;
import ELogicResult;
import DesignTypes;

using namespace Mala::Container;
using namespace Mala::Math;
using namespace Mala::Db;
using namespace std;


/// <summary>
/// 가방 컴포넌트
/// </summary>
export class BagComponent : public IComponent
{
public:
    GENERATE_CLASS_TYPE_INFO( BagComponent );

public:
    inline static EComponentType Type{ EComponentType::Bag };

    using ItemMap = HashMap< ItemId, ItemPtr >;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    BagComponent( WorldObject& owner );
    ~BagComponent() override = default;

    ELogicResult Add( ItemRef item );
    ELogicResult Add( ItemPtr&& item );
    bool Remove( ItemId itemId );
    bool Contains( ItemId itemId );
    ItemPtr Get( ItemId itemId );
    ItemPtr GetByDesignId( ItemDesignId itemDesignId );
    bool TryGet( ItemId itemId, ItemPtr& outItem );
    bool TryGetByDesignId( ItemDesignId itemDesignId, ItemPtr& outItem );
    bool Empty();

    auto begin();
    auto end();

public:
    /// <summary>
    /// 잡큐를 비운 후 호출되는 콜백
    /// </summary>
    void OnFlush() final;

protected:
    ItemMap _items;
};

/// <summary>
/// Pc 가방 컴포넌트
/// </summary>
export class PcBagComponent final : public IDbLoadComponent, public BagComponent
{
public:
    GENERATE_CLASS_TYPE_INFO( PcBagComponent );

public:
    /// <summary>
    /// 생성자
    /// </summary>
    PcBagComponent( Pc& owner );
    ~PcBagComponent() final = default;

    void LoadComponentFromDb( TxContextRef tx ) final;
    void ImportFromDbList( ItemPtrVector&& itemList );
    Pc& GetPc();
    void Reset();

    void DoTestGiveItem( DbConnection* dbConn, Vector< ItemPtr >& items );
    void ExportToItem( ItemId itemId, class PktItem& outItem );
    void ExportToItemList( Vector< class PktItem >& itemList );
};

/// <summary>
/// Npc 가방 컴포넌트
/// </summary>
export class NpcBagComponent final : public BagComponent
{
    GENERATE_CLASS_TYPE_INFO( NpcBagComponent );

public:
    /// <summary>
    /// 생성자
    /// </summary>
    NpcBagComponent( Npc& owner );
    ~NpcBagComponent() final = default;

    /// <summary>
    /// Npc를 반환한다.
    /// </summary>
    Npc& GetNpc();
};
