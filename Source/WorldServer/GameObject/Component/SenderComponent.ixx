export module SenderComponent;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container.MapHelper;
import Mala.Reflection;

import WorldObject;
import WorldObjectTypes;
import Consts;
import ComponentType;
import EWorldObjectType;
import IComponent;
import WorldTypes;

import ELogicResult;

using namespace Mala::Container;
using namespace Mala::Net;

export using ClientSessionPtr = std::shared_ptr< class ClientSession >;
export using ClientSessionRef = const ClientSessionPtr&;

/// <summary>
/// PC 송신 컴포넌트
/// </summary>
export class SenderComponent : public IComponent
{
public:
    GENERATE_CLASS_TYPE_INFO( SenderComponent );

public:
    inline static EComponentType Type{ EComponentType::Sender };

public:
    SenderComponent( Pc& owner );
    ~SenderComponent() final = default;

    /// <summary>
    /// 송신한다
    /// </summary>
    template< typename S_Packet >
    void Send( ELogicResult result );
    void Send( SendBufferPtr&& snedBuffer );
    void Send( SendBufferRef snedBuffer );

    class Pc& GetPc(){ return reinterpret_cast< Pc& >( _owner ); }
};


/// <summary>
/// 송신한다
/// </summary>
template< typename S_Packet >
inline void SenderComponent::Send( ELogicResult result )
{
    Send( S_Packet{ ._result = result }.Write() );
}
