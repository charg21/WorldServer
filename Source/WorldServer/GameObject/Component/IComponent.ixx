export module IComponent;

import <Macro.h>;

import Mala.Container;
import Mala.Core.Types;
import Mala.Core.EnumHelper;
import Mala.Reflection;

import WorldObjectTypes;
import ComponentType;

using namespace Mala::Db;

export class IComponent
{
public:
    GENERATE_CLASS_TYPE_INFO( IComponent );

public:
    DECLARE_TL( ComponentTypeList );

public:
    /// <summary>
    /// 생성자
    /// </summary>
    IComponent( class WorldObject& owner );
    virtual ~IComponent() = default;

    /// <summary>
    /// 붙인다/떼낸다
    /// </summary>
    virtual void Attach(){}
    virtual void Detach(){}

    virtual void OnTick( f32 deltaTime ){}
    virtual void OnPreEnterWorld(){}
    virtual void OnEnterWorld(){}
    virtual void OnLeaveWorld(){}
    virtual void OnFlush(){}

    WorldObjectId GetOwnerId();

protected:
    class WorldObject& _owner;
};

export class IDbLoadComponent
{
public:
    GENERATE_CLASS_TYPE_INFO( IDbLoadComponent );

public:
    virtual void LoadComponentFromDb( TxContextRef tx ) = 0;
};
