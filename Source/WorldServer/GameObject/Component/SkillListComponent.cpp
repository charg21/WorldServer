import Mala.Core.Types;
import Mala.Container.String;
import Mala.Core.Format;

import WorldObjectTypes;
import WorldObject;
import AoiManager;
import Consts;
import ClusterManager;
import ThreadLocal;
import Pc;
import Npc;
import WorldHelper;


import IComponent;
import SkillListComponent;


SkillListComponent::SkillListComponent( WorldObject& owner )
: IComponent{ owner }
{
    _typeId = (int)EComponentType::SkillList;
}

PcSkillListComponent::PcSkillListComponent( Pc& owner )
: SkillListComponent{ owner }
{
}


NpcSkillListComponent::NpcSkillListComponent( Npc& owner )
: SkillListComponent{ owner }
{
}
