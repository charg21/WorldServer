export module ComponentType;

import Mala.Core.Types;
import Mala.Core.TypeList;
import Mala.Core.TypeCast;
import Mala.Core.EnumHelper;

using namespace Mala;

/// <summary>
/// 컴포넌트 타입 타입 리스트 정의
/// </summary>
export using ComponentTypeList = TypeList
<
    class ViewComponent,
    class CombatComponent,
    class MovementComponent,
    class BagComponent,
    class AiComponent,
    class SkillListComponent,
    class AggroComponent,
    class ChatComponent,
    class StatComponent,
    class EquipComponent,
    class SenderComponent,
    class QuestComponent
>;

/// <summary>
/// 컴포넌트 타입 열거형
/// </summary>
export enum class EComponentType : unsigned long long
{
    None      = 0,
    View      = EnumFlag::ToValue< ComponentTypeList, ViewComponent >(),
    Combat    = EnumFlag::ToValue< ComponentTypeList, CombatComponent >(),
    Movement  = EnumFlag::ToValue< ComponentTypeList, MovementComponent >(),
    Bag       = EnumFlag::ToValue< ComponentTypeList, BagComponent >(),
    Ai        = EnumFlag::ToValue< ComponentTypeList, AiComponent >(),
    SkillList = EnumFlag::ToValue< ComponentTypeList, SkillListComponent >(),
    Aggro     = EnumFlag::ToValue< ComponentTypeList, AggroComponent >(),
    Chat      = EnumFlag::ToValue< ComponentTypeList, ChatComponent >(),
    Stat      = EnumFlag::ToValue< ComponentTypeList, StatComponent >(),
    Equip     = EnumFlag::ToValue< ComponentTypeList, EquipComponent >(),
    Sender    = EnumFlag::ToValue< ComponentTypeList, SenderComponent >(),
    Quest     = EnumFlag::ToValue< ComponentTypeList, QuestComponent >(),

    PcComponet  = View | Combat | Stat | Movement | SkillList | Equip | Bag | Chat | Sender | Quest,
    NpcComponet = View | Combat | Stat | Movement | SkillList | Ai | Aggro,

    All = ~0ULL,
};


