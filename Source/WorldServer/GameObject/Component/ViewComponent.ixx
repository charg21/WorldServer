export module ViewComponent;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container.MapHelper;
import Mala.Reflection;


import WorldObject;
import WorldObjectTypes;

import IComponent;
import Consts;
import WorldTypes;
import EWorldObjectType;
import ComponentType;


using namespace Mala::Container;
using namespace Mala::Math;


/// <summary>
/// 시야 컴포넌트
/// </summary>
export class ViewComponent : public IComponent
{
public:
    GENERATE_CLASS_TYPE_INFO( ViewComponent );

public:
    inline static EComponentType Type{ EComponentType::View };

public:
    /// <summary>
    /// 생성자
    /// </summary>
    ViewComponent( WorldObject& owner, EWorldObjectType viewType = EWorldObjectType::Max );
    ~ViewComponent() override = default;

    /// <summary>
    /// 현재 시야 자료구조의 사본을 가져온다
    /// </summary>
    WorldObjectMap2& GetViewSnapshot();

    /// <summary>
    /// 지정 좌표 기준 시야내 오브젝트를 모은다
    /// </summary>
    WorldObjectMaps2& GatherWorldObjectList();
    static WorldObjectMaps2& GetGatherList();
    static WorldObjectMap2& GetSnapshotList();

    bool Add( WorldObjectPtr&& otherPtr );
    bool Add( WorldObjectRef otherRef );
    void Remove( WorldObjectId actorId );
    bool TryRemove( WorldObjectId actorId );
    Npc* GetNpc( WorldObjectId actorId );
    Pc* GetPc( WorldObjectId actorId );
    WorldObjectPtr GetObjectPtr( WorldObjectId actorId );
    NpcPtr GetNpcPtr( WorldObjectId actorId );
    PcPtr GetPcPtr( WorldObjectId actorId );
    bool Contains( WorldObjectId actorId );
    bool Empty();
    auto begin();
    auto end();

    static void Enter( Pc& pc );
    static void Enter( Npc& npc );
    static void UpdatePos( WorldObject& actor, const Position& fromPos, const Position& toPos );

public:
    /// <summary>
    /// 월드 입장/퇴장시 호출되는 콜백
    /// </summary>
    void OnLeaveWorld() final;
    void OnPreEnterWorld() final;
    void OnEnterWorld() final;

    void OnMove();
    void OnMoved( WorldObject* other );
    virtual void OnEnter( WorldObject* other ) {};
    virtual void OnLeave( WorldObject* other ) {};
    bool ChangeClusterIndex( Index toIndex );

    /// <summary>
    /// 잡큐를 비운 후 호출되는 콜백
    /// </summary>
    void OnFlush() final;

    /// <summary>
    /// 이동 컴포넌트
    /// </summary>
    MovementComponent& GetMovement();

private:
    /// <summary>
    /// 시야 내 객체 컨테이너
    /// </summary>
    //Field( _view )
    ViewWorldObjectList _view{ 512 };

    /// <summary>
    /// 이전 AOI 인덱스
    /// </summary>
    //Field( _fromAoiIndex )
    Index _fromAoiIndex{};

    /// <summary>
    /// AOI 인덱스
    /// </summary>
    //Field( _aoiIndex )
    Index _aoiIndex{};

    /// <summary>
    /// 시야 타입
    /// </summary>
    EWorldObjectType _viewType{};
};

/// <summary>
/// Pc 시야 컴포넌트
/// </summary>
export class PcViewComponent final : public ViewComponent
{
public:
    GENERATE_CLASS_TYPE_INFO( PcViewComponent );

public:
    /// <summary>
    /// 생성자
    /// </summary>
    PcViewComponent( Pc& owner );
    ~PcViewComponent() final = default;

    /// <summary>
    /// Pc를 반환한다.
    /// </summary>
    Pc& GetPc();
    PcPtr GetPcPtr();
    void Attach() final;
    void OnEnter( WorldObject* other ) final;
    void OnLeave( WorldObject* other ) final;
};

/// <summary>
/// Npc 시야 컴포넌트
/// </summary>
export class NpcViewComponent final : public ViewComponent
{
public:
    GENERATE_CLASS_TYPE_INFO( NpcViewComponent );

public:
    /// <summary>
    /// 생성자
    /// </summary>
    NpcViewComponent( Npc& owner );
    ~NpcViewComponent() final = default;

    /// <summary>
    /// Npc를 반환한다.
    /// </summary>
    Npc& GetNpc();
    class NpcAggroComponent& GetAggro();
    class NpcCombatComponent& GetCombat();
    NpcPtr GetNpcPtr();
    void Attach() final;
    void OnEnter( WorldObject* other ) final;
    void OnLeave( WorldObject* other ) final;
};


class ViewList2
{
public:
    /// <summary>
    /// 오브젝트의 인덱스 딕셔너리
    /// </summary>
    HashMap< WorldObjectId, i32 > _objectIndexDict;
    Vector< WorldObjectPtr > _objects;

    /// <summary>
    /// 생성자
    /// </summary>
    ViewList2( i32 capacity )
    : _objectIndexDict( capacity )
    , _objects{}
    {
        _objects.reserve( capacity );
    }

    /// <summary>
    /// 오브젝트를 추가한다.
    /// </summary>
    bool TryAdd( WorldObjectRef object )
    {
        if ( _objectIndexDict.emplace( object->GetObjectId(), _objects.size() ).second )
        {
            _objects.emplace_back( object );
            return true;
        }

        /// 이미 맵에 존재
        return false;
    }

    bool TryRemove( WorldObjectId objectId )
    {
        auto node = _objectIndexDict.extract( objectId );
        if ( !node )
            return false;

        auto index = node.mapped();
        if ( index != _objects.size() - 1 )
        {
            _objects[ index ] = std::move( _objects.back() );
            _objectIndexDict[ _objects[ index ]->GetObjectId() ] = index;
        }

        _objects.pop_back();
        return true;
    }


    bool TryGetValue( WorldObjectId objectId, WorldObjectPtr& outObject )
    {
        i32 index{};
        if ( MapHelper::TryGetValue( _objectIndexDict, index, objectId ) )
        {
            outObject = _objects[ index ];
            return true;
        }

        outObject = {};
        return false;
    }

    void Clear()
    {
        _objects.clear();
        _objectIndexDict.clear();
    }
};
