import <ranges>;
import Mala.Core;
import Mala.Core.CoreGlobal;
import Mala.Core.TypeCast;
import Mala.Math.Vector2Int;
import Mala.Threading.GlobalJobExecutor;
import WorldObjectTypes;
import WorldObject;
import AoiManager;
import Consts;
import ClusterManager;
import ThreadLocal;
import WorldHelper;
import Pc;
import Npc;

import IComponent;
import AggroComponent;
import ViewComponent;
import MovementComponent;
import PacketExporter;

import WorldPacket.S_Spawn;
import WorldPacket.S_Move;
import WorldPacket.S_Despawn;


using namespace Mala::Threading;
using namespace Mala::Math;


/// <summary>
/// 생성자
/// </summary>
ViewComponent::ViewComponent( WorldObject& owner, EWorldObjectType viewType )
: IComponent{ owner }
, _viewType{ viewType }
{
    _typeId = (int)EComponentType::View;
	_view.reserve( 1024 );
}

WorldObjectMap2& ViewComponent::GetViewSnapshot()
{
    auto& copyList = ViewComponent::GetSnapshotList();
    for ( auto& [ id, ptr ] : _view )
    {
        copyList.emplace( id, ptr.get() );
    }

    return copyList;
}

WorldObjectMaps2& ViewComponent::GatherWorldObjectList()
{
    auto& gatherList = LAoiManager->GatherWorldObject(
        _owner.GetPos(),
        _viewType );

    return gatherList;
}

WorldObjectMaps2& ViewComponent::GetGatherList()
{
    static thread_local WorldObjectMaps2 gatherList( Consts::Aoi::MaxNearCount );
    gatherList.clear();

    return gatherList;
}

WorldObjectMap2& ViewComponent::GetSnapshotList()
{
    static thread_local WorldObjectMap2 copyList( 1024 );
    copyList.clear();

    return copyList;
}

/// <summary>
/// 갱신한다
/// Ref. https://www.ripublication.com/ijaer17/ijaerv12n19_24.pdf
/// </summary>
void ViewComponent::OnMove()
{
    auto& oldViewList    = GetViewSnapshot();
    auto& newViewMapList = GatherWorldObjectList();

    auto myWorldObjectId = _owner.GetObjectId();
    // 이동 및 시야에 등장한 플레이어 처리
    for ( auto& newViewMap : newViewMapList )
    {
        for ( auto& [ id, other ] : *newViewMap )
        {
            if ( oldViewList.erase( id ) )
            {
                OnMoved( other ); // 기존 현재 다 있음
            }
            else // 기존 시야에 없던 경우
            {
                if ( myWorldObjectId != id )
                    OnEnter( other );
            }
        }
    }

    //시야에서 벗어난 플레이어 처리
    for ( auto* other : oldViewList | std::views::values )
        OnLeave( other );
}

void ViewComponent::OnEnterWorld()
{
}

void ViewComponent::OnMoved( WorldObject* other )
{
    auto pc = TypeCast< Pc >( other );
    if ( !pc )
        return;

    // 내시야에 이미 있던 경우 이동 패킷을 보내준다.
    pc->Post(
    [
        pc         = pc,
        myObjectId = GetOwnerId(),
        pos        = _owner.GetPos(),
        yaw        = Vector2ToYaw( _owner.GetDir() ),
        destPos    = _owner.GetToPos()
    ]
    {
        pc->Send( PacketWriter::WriteS_Move( myObjectId, pos, destPos, yaw ) );
    } );
}

bool ViewComponent::ChangeClusterIndex( Index toIndex )
{
    _fromAoiIndex = _aoiIndex;
    _aoiIndex = toIndex;

    return _fromAoiIndex != toIndex;
}

void ViewComponent::OnLeaveWorld()
{
    auto viewWorldObjectList = std::move( _view );
    auto viewObjectListView = viewWorldObjectList | std::views::values;

    std::ranges::for_each( viewObjectListView, [ this ]( auto& otherRef ){ OnLeave( otherRef.get() ); } );
}

void ViewComponent::OnPreEnterWorld()
{
    _aoiIndex     = WorldHelper::GetAoiIndex( _owner.GetPos() );
    _fromAoiIndex = _aoiIndex;
}

void ViewComponent::OnFlush()
{
    auto& movement = GetMovement();
    if ( movement._fromPos != movement._pos )
    {
		auto toAoiIndex = WorldHelper::GetAoiIndex( movement._pos );
        if ( ChangeClusterIndex( toAoiIndex ) )
            ViewComponent::UpdatePos( _owner, movement._fromPos, _owner.GetPos() );
    }

    movement._fromPos = _owner.GetPos();
}

MovementComponent& ViewComponent::GetMovement()
{
	return *_owner._movement;
}

bool ViewComponent::Add( WorldObjectPtr&& otherPtr )
{
    return _view.emplace( otherPtr->GetObjectId(), std::move( otherPtr ) ).second;
}

bool ViewComponent::Add( WorldObjectRef otherRef )
{
    return _view.emplace( otherRef->GetObjectId(), otherRef ).second;
}

void ViewComponent::Remove( WorldObjectId objectId )
{
    _view.erase( objectId );
}

bool ViewComponent::TryRemove( WorldObjectId objectId )
{
    return _view.erase( objectId );
}

Npc* ViewComponent::GetNpc( WorldObjectId otherId )
{
    return nullptr;
}

Pc* ViewComponent::GetPc( WorldObjectId otherId )
{
    return nullptr;
}

WorldObjectPtr ViewComponent::GetObjectPtr( WorldObjectId otherId )
{
    auto iter = _view.find( otherId );
    if ( iter == _view.end() )
        return nullptr;

    return iter->second;
}

PcPtr ViewComponent::GetPcPtr( WorldObjectId otherId )
{
    return std::static_pointer_cast< Pc >( GetObjectPtr( otherId ) );
}

bool ViewComponent::Contains( WorldObjectId otherId )
{
    return _view.contains( otherId );
}

bool ViewComponent::Empty()
{
    return _view.empty();
}

auto ViewComponent::begin()
{
    return _view.begin();
}

auto ViewComponent::end()
{
    return _view.end();
}

void ViewComponent::Enter( Npc& npc )
{
    GGlobalJobExecutor->Post( [ npc = npc.shared_from_this() ]()
    {
        LAoiManager->OnEnter( npc.get() );
    } );
}

void ViewComponent::Enter( Pc& pc )
{
    GGlobalJobExecutor->Post( [ pc = pc.shared_from_this() ]()
    {
        LAoiManager->OnEnter( pc.get() );
    } );
}

void PcViewComponent::Attach()
{
    ViewComponent::Enter( GetPc() );
}

void NpcViewComponent::Attach()
{
    ViewComponent::Enter( GetNpc() );
}

void ViewComponent::UpdatePos( WorldObject& object, const Position& fromPos, const Position& toPos )
{
    GGlobalJobExecutor->Post( [ fromPos, toPos, object = object.SharedFromWorldObject() ]() mutable
    {
        LAoiManager->OnMove( fromPos, toPos, std::static_pointer_cast< Character >( object ) );
    } );
}

/// <summary>
/// 생성자
/// </summary>
PcViewComponent::PcViewComponent( Pc& owner )
: ViewComponent{ owner, EWorldObjectType::Max }
{
}

Pc& PcViewComponent::GetPc()
{
	return static_cast< Pc& >( _owner );
}

PcPtr PcViewComponent::GetPcPtr()
{
    return GetPc().shared_from_this();
}

void PcViewComponent::OnEnter( WorldObject* other )
{
    /// 내 시야에 추가
    Add( other->SharedFromWorldObject() );

    /// 상대 정보 획득
    S_Spawn spawn;
    PacketExporter::ExportToWorldObject( *other, spawn._other );
    GetPc().Send( spawn.Write() );  // 타인 정보를 내게 보냄

    // 상대 시야에 내 정보 추가 메시징 처리
    other->Post( [ other, my = GetPcPtr(), myPos = GetPc().GetPos() ]
    {
        /// 내 시야에 추가
        if ( auto* otherView = other->_view; otherView )
            otherView->Add( my );

        if ( Pc* pc = TypeCast< Pc >( other ) )
        {
            S_Spawn spawn;
            PacketExporter::ExportToWorldObject( *my, spawn._other );
            pc->Send( spawn.Write() );
        }
        else
        {
            other->RegisterTick();
        }
    } );
}

void PcViewComponent::OnLeave( WorldObject* other )
{
    // 내 시야에서 상대 제거
    TryRemove( other->GetObjectId() );

    S_Despawn despawn{ ._objectId = *other->GetObjectId() };
    GetPc().Send( despawn.Write() );

    other->Post( [ myObjectId = GetOwnerId(), other ]
    {
        auto* otherView = other->_view;
        if ( !otherView )
            return;

        if ( !otherView->TryRemove( myObjectId ) )
            return;

		if ( auto* otherPc = TypeCast< Pc >( other ) )
            otherPc->Send( S_Despawn{ ._objectId = *myObjectId }.Write() );
    } );
}

/// <summary>
/// 생성자
/// </summary>
NpcViewComponent::NpcViewComponent( Npc& owner )
: ViewComponent{ owner, EWorldObjectType::Pc }
{
}

Npc& NpcViewComponent::GetNpc()
{
    return static_cast< Npc& >( _owner );
}

NpcAggroComponent& NpcViewComponent::GetAggro()
{
	return GetNpc()._aggro;
}

NpcCombatComponent& NpcViewComponent::GetCombat()
{
    return GetNpc()._npcCombat;
}

NpcPtr NpcViewComponent::GetNpcPtr()
{
    return GetNpc().shared_from_this();
}

void NpcViewComponent::OnEnter( WorldObject* other )
{
    auto otherPc = TypeCast< Pc >( other );
    if ( !otherPc )
        return;

    /// NPC 시야에 상대를 추가
    //if ( !otherPc.->IsAlive() )
    {
        Add( otherPc->SharedFromWorldObject() );
        if ( GetCombat().IsAggressive() )
            GetAggro().Add( otherPc->GetObjectId(), 1 );
    }

    // 상대 시야에 나 추가 메시징 처리
    // 좌표는 복사후 보내야할거 같음
    otherPc->Post( [ otherPc, my = GetNpcPtr() ]
    {
        /// 시야 처리 체크
        auto* otherPcView = otherPc->_view;
        if ( !otherPcView )
            return;

        S_Spawn spawn;
        PacketExporter::ExportToS_Spawn( *my, spawn );
        otherPcView->Add( std::move( my ) );
        otherPc->Send( spawn.Write() );
    } );
}

void NpcViewComponent::OnLeave( WorldObject* other )
{
    auto otherPc = TypeCast< Pc >( other );
    if ( !otherPc )
        return;

    // 내 시야에서 상대 제거
    TryRemove( otherPc->GetObjectId() );

    otherPc->Post( [ myObjectId = GetOwnerId(), otherPc ]
    {
        // 퇴장 처리 후 패킷을 보내준다.
        auto* otherPcView = otherPc->_view;
        if ( !otherPcView )
            return;

        if ( otherPcView->TryRemove( myObjectId ) )
            otherPc->Send( S_Despawn{ ._objectId = *myObjectId }.Write() );
    } );
}
