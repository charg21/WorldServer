import <Macro.h>;

import Mala.Core.CoreTLS;
import Mala.Core.CoreGlobal;
import Mala.Core.TypeCast;
import Mala.Net.PacketHeader;
import Mala.Net.SendBuffer;
import Mala.Math;
import Mala.Math.Vector2;
import Mala.Threading;
import Mala.Windows;

import WorldObject;
import WorldObjectTypes;
import Consts;
import ClusterJob;
import ThreadLocal;
import World;
import WorldHelper;
import Pc;
import IComponent;
import IComponentOwner;
import ViewComponent;
import MovementComponent;

import PktObject;

using namespace Mala::Math;
using namespace Mala::Net;
using namespace Mala::Threading;


WorldObject::WorldObject( EWorldObjectType objectType )
: IComponentOwner{ *this }
{
    INIT_TL( WorldObject );
    _objectType = objectType;
}

const Position& WorldObject::GetPos() const
{
    return _movement->_pos;
}

const Position& WorldObject::GetFromPos() const
{
    return _movement->_fromPos;
}

const Position& WorldObject::GetToPos() const
{
    return _movement->_toPos;
}

void WorldObject::SetPos( const Position& position )
{
    _movement->_pos = position;
}

void WorldObject::SetFromPos( const Position& position )
{
    _movement->_fromPos = position;
}

void WorldObject::SetToPos( const Position& position )
{
    _movement->_toPos = position;
}

void WorldObject::SetPos( f32 x, f32 y, f32 z )
{
    _movement->_pos = Position{ x, y, z };
}

void WorldObject::SetIndex( Index index )
{
    _index = index;
}

const Direction& WorldObject::GetDir() const
{
    return _movement->_dir;
}

Direction WorldObject::GetDir()
{
    return _movement->_dir;
}

float WorldObject::GetDirYaw() const
{
    return _movement->GetDirYaw();
}

void WorldObject::SetDir( const Direction& dir )
{
    _movement->_dir = dir;
}

void WorldObject::SetDir( f32 yawDegree )
{
    SetDir( Direction::FromYaw( yawDegree ) );
}

inline bool WorldObject::operator==( const WorldObject& object ) const
{
    return GetObjectId() == object.GetObjectId();
}

inline bool WorldObject::operator!=( const WorldObject& object ) const
{
    return GetObjectId() != object.GetObjectId();
}

bool WorldObject::EnterWorld()
{
    OnPreEnterWorld();
    if ( auto* view = Get< ViewComponent >() )
        view->OnPreEnterWorld();

    GGlobalJobExecutor->Post( ClusterEnterJob::Make( SharedFromWorldObject(), EClusterChangeReason::EnterWorld ) );
    OnEnterWorld();

    if ( auto* view = Get< ViewComponent >() )
    {
        view->OnEnterWorld();
        view->OnMove();
    }

    OnSpawnComplete();

    return true;
}

bool WorldObject::LeaveWorld()
{
    Post( [ this, self = SharedFromWorldObject() ]
    {
        OnLeaveWorld();
        GGlobalJobExecutor->Post( ClusterLeaveJob::Make( SharedFromWorldObject() ) );

        if ( auto* view = Get< ViewComponent >() )
        {
            view->OnLeaveWorld();
            view->Detach();
            Remove< ViewComponent >();
            _view = nullptr;
        }
    } );

    return true;
}

ClusterIndex WorldObject::GetClusterIndex()
{
    return _index;
}

void WorldObject::ChangeCluster()
{
    //const auto fromIndex{ GetClusterIndex() };
    //auto toIndex{ WorldHelper::GetClusterIndex( GetPos() ) };
    //
    //// 같은 인덱스의 클러스터도 TLS별로 달라, 인덱스로 비교
    //if ( fromIndex != toIndex )
    //{
    //    SetClusterIndex( toIndex );
    //    //GGlobalJobExecutor->Post( ClusterSwitchJob::Make( fromIndex, toIndex, SharedFromWorldObject() ) );
    //}
}

void WorldObject::SetClusterIndex( ClusterIndex clusterIndex )
{
    _index = clusterIndex;
}

void WorldObject::Tick()
{
    _tickRegistered = false;

    f32 deltaTime = GetDeltaTime();
    OnTick( deltaTime );
    _lastTick = std::max( LLastTick, _lastTick );
}

void WorldObject::RegisterTick()
{
    if ( _tickRegistered )
        return;

    // 내부에서 레퍼 카운트 증감을 한다.
    PostAfter( 50ms, [ this ]
    {
        Tick();
        if ( CanTick() )
            RegisterTick();
    } );

    _tickRegistered = true;
}

WorldObjectPtr WorldObject::GetRootWorldObject()
{
    return _rootWorldObject.lock();
}

WorldObjectPtr WorldObject::GetTopRootWorldObject()
{
    if ( auto root = GetRootWorldObject() )
    {
        while ( auto topRoot = root->GetRootWorldObject() )
            root = topRoot;

        return root;
    }

    return {};
}

inline WorldObjectPtr WorldObject::SharedFromWorldObject()
{
    return std::static_pointer_cast< WorldObject >( SharedFromJobExecutor() );
}

inline WorldObjectWeakPtr WorldObject::WeakFromWorldObject()
{
    return SharedFromWorldObject();
}

void WorldObject::OnFlush()
{
    if ( _view )
        _view->OnFlush();
}

i64 WorldObject::GetDeltaTick()
{
    return LLastTick - _lastTick;
}

f32 WorldObject::GetDeltaTime()
{
    return GetDeltaTick() / 1000.f;
}

void Send( WorldObject& object, SendBufferPtr&& buffer )
{
    if ( auto* pc = TypeCast< Pc >( &object ) )
        pc->Send( std::move( buffer ) );
}

void Send( WorldObjectRef object, SendBufferPtr&& buffer )
{
    if ( auto* pc = TypeCast< Pc >( object.get() ) )
        pc->Send( std::move( buffer ) );
}
