export module Npc;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Math;
import Mala.Reflection;
import Mala.Threading.JobExecutor;

import WorldObject;
import WorldObjectTypes;
import AggroComponent;
import Character;

import AiComponent;
import CombatComponent;
import ViewComponent;
import MovementComponent;
import StatComponent;
import SkillListComponent;

import ClientSession;
import PktVector3;

using namespace Mala::Net;
using namespace Mala::Container;
using namespace Mala::Threading;

EXPORT_BEGIN

class NpcDesign;

/// <summary>
/// 논 플레이어블 캐릭터
/// </summary>
class Npc final
    : public Character
    , public std::enable_shared_from_this< Npc >
{
    GENERATE_CLASS_TYPE_INFO( Npc );

public:
    using WorldObjectBase = Character;

public:
    using WorldObject::SetPos;
    using Character::SetPos;

    friend class NpcAggroComponent;
    friend struct NpcAiComponent;

public:
    Npc();
    ~Npc() final = default;

    /// <summary>
    /// 활성 상태
    /// </summary>
    bool IsBusy();

    EWakeUpState GetWakeUpState();

    /// <summary>
    ///
    /// </summary>
    void OnTick( f32 deltaTime ) final;
    bool CanTick() final;
    PcPtr GetTarget(){ return _target.lock(); }

public:
    /// <summary>
    /// 월드 콜백
    /// </summary>
    void OnEnterWorld() final;

public:
    JobExecutorPtr SharedFromJobExecutor() final;
    JobExecutorWeakPtr WeakFromJobExecutor() final;

public:
    /// <summary>
    /// 활성 상태
    /// </summary>
    Field( _wakeUpState )
    EWakeUpState _wakeUpState{ EWakeUpState::Sleep };

    /// <summary>
    /// 기획 데이터
    /// </summary>
    const NpcDesign* _design{};

    /// <summary>
    /// 대상
    /// </summary>
    PcWeakPtr _target;

    /// <summary>
    /// 컴포넌트
    /// </summary>
    NpcViewComponent _npcView;
    NpcMovementComponent _npcMovement;
    NpcCombatComponent _npcCombat;
    NpcFsmAiComponent _ai;
    NpcSkillListComponent _skillList;
    NpcAggroComponent _aggro;
    NpcStatComponent _npcStat;

};

EXPORT_END
