import <Macro.h>;

import Mala.Core.TypeCast;
import Mala.Core.TypeList;
import Mala.Container;
import Mala.Reflection;
import Mala.Db.TxExecutor;
import Mala.Threading.JobExecutor;
import Mala.Memory;

import WorldObject;
import Consts;
import ClusterManager;
import Pc;
import PcCache;
import Npc;
import ThreadLocal;
import Rect;

import CombatComponent;
import ViewComponent;
import IComponent;
import IComponentOwner;

import EWorldObjectType;
import PktObject;
import WorldPacket.S_Spawn;
import WorldPacket.S_EnterWorld;
import WorldPacket.S_Move;
import WorldPacket.S_Despawn;
import WorldPacket.S_LeaveWorld;

import PacketExporter;

using namespace Mala::Core;
using namespace Mala::Container;
using namespace Mala::Db;
using namespace Mala::Threading;

Pc::Pc()
: Character  { EWorldObjectType::Pc }
, TxExecutor { *( dynamic_cast< JobExecutor* >( this ) ) }
, _pcCombat  { *this }
, _pcView    { *this }
, _pcMovement{ *this }
, _pcBag     { *this }
, _chat      { *this }
, _pcStat    { *this }
, _sender    { *this }
, _equip     { *this }
, _quest     { *this }
{
    INIT_TL( Pc );
    //Add< View >( &_pcView );
    _view = &_pcView;
    Add< CombatComponent >( &_pcCombat );
    _combat = &_pcCombat;
    _movement = &_pcMovement;
    _stat = &_pcStat;
}

Pc::~Pc()
{
}

bool Pc::CanTick()
{
    if ( _session )
        return true;

    /// 추가 검사가 필요한 내용

    return false;
}

StaticVector< IDbLoadComponent*, 64 > Pc::GetDbLoadComponentList()
{
    // 현재 리플렉션 구조상 다중 상속 받은 부모 클래스는 체크는 파악이 힘듬
    //Vector< IDbLoadComponent* > vec;
    //auto& typeInfo = GetTypeInfo();
    //auto& fieldInfos = typeInfo.GetFields();
    //for ( auto& field : fieldInfos )
    //{
    //  if ( !field->GetTypeInfo().IsChildOf< IDbLoadComponent >() )
    //      continue;
    //  auto& f = field->Get< Pc, IDbLoadComponent >( this );
    //  vec.push_back( &f );
    //}

    // 단순 처리
    return { &_pcBag, &_equip, &_pcStat };
}

void Pc::OnPreEnterWorld()
{
    Add< ViewComponent >( &_pcView );
    _view = &_pcView;
    _view->Attach();
}

void Pc::OnEnterWorld()
{
    Send( PacketWriter::WriteS_EnterWorld( *this ) );

    RegisterTick();
}

void Pc::OnTick( f32 deltaTime )
{
    _pcStat.OnTick( deltaTime );
}

void Pc::OnLeaveWorld()
{
    Send( PacketWriter::WriteS_LeaveWorld( *this ) );
}

JobExecutorPtr Pc::SharedFromJobExecutor()
{
    return shared_from_this();
}

JobExecutorWeakPtr Pc::WeakFromJobExecutor()
{
    return SharedFromJobExecutor();
}

TxExecutorPtr Pc::SharedFromTxExecutor()
{
    return shared_from_this();
}

TxExecutorWeakPtr Pc::WeakFromTxExecutor()
{
    return shared_from_this();
}

/// <summary>
/// 동기화 한다.
/// </summary>
inline void Pc::Sync( EQueryType queryType )
{
    switch ( queryType )
    {
    case EQueryType::Insert:
        break;
    case EQueryType::Update:
        break;
    default:
        break;
    }
}

Pc::PcPtrList Pc::GetPcList( ::UserId userId )
{
    return GPcCache->GetPcPtrList( userId );
}
