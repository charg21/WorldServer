export module WorldObject;

import <Macro.h>;

import Mala.Core;
import Mala.Container;
import Mala.Net;
import Mala.Threading;
import Mala.Reflection;

import Consts;
import WorldTypes;
import WorldObjectTypes;
import IComponent;
import IComponentOwner;

import EWorldObjectType;

using namespace Mala::Container;
using namespace Mala::Threading;
using namespace Mala::Net;

EXPORT_BEGIN

#undef _stat

/// <summary>
/// 월드에 배치되어 상호작용을 할 수 있는 객체
/// </summary>
class WorldObject : public JobExecutor, public IComponentOwner
{
    friend class PacketExporter;

    GENERATE_CLASS_TYPE_INFO( WorldObject );

public:
    DECLARE_TL( AllWorldObjectTypeList );

public:
    WorldObject( EWorldObjectType objectType );
    ~WorldObject() override = default;

    bool EnterWorld();
    bool LeaveWorld();
    virtual void OnPreEnterWorld() {};
    virtual void OnEnterWorld() {};
    virtual void OnLeaveWorld() {};

    const Position& GetPos() const;
    const Position& GetFromPos() const;
    const Position& GetToPos() const;
    const Direction& GetDir() const;
    Direction GetDir();
    float GetDirYaw() const;
    ClusterIndex GetClusterIndex();
    WorldObjectId GetObjectId() const { return _objectId; }
    EWorldObjectType GetWorldObjectType() { return _objectType; }

    void SetPos( f32 x, f32 y, f32 z );
    void SetPos( const Position& position );
    void SetFromPos( const Position& position );
    void SetToPos( const Position& position );
    void SetIndex( Index index );
    void SetClusterIndex( ClusterIndex clusterIndex );
    void SetDir( const Direction& position );
    void SetDir( f32 yawDegree );
    void SetWorldObjectId( WorldObjectId id ) { _objectId = id; }

    void Tick();
    void RegisterTick();
    virtual void OnTick( f32 deltaTime ) abstract;
    virtual bool CanTick() { return false; }
    virtual void OnSpawnComplete() {};

    void ChangeCluster();
    WorldObjectPtr GetRootWorldObject();
    WorldObjectPtr GetTopRootWorldObject();
    WorldObjectPtr SharedFromWorldObject();
    WorldObjectWeakPtr WeakFromWorldObject();

public:
    inline bool operator==( const WorldObject& object ) const;
    inline bool operator!=( const WorldObject& object ) const;

    void OnFlush() final;
    i64 GetDeltaTick();
    f32 GetDeltaTime();

public:
    EWorldObjectType _objectType{ EWorldObjectType::Max };

    Field( _tickRegistered )
    bool _tickRegistered{};

    Field( _view )
    class ViewComponent* _view{};

    Field( _movement )
    class MovementComponent* _movement{};

    Field( _combat )
    class CombatComponent* _combat{};

    Field( _bag )
    class BagComponent* _bag{};

    Field( _stat )
    class StatComponent* _stat{};

    /// <summary> 클러스터 인덱스 </summary>
    Field( _index )
    Index _index{};
    Index _wideClusterIndex;
    Index _clusterIndex;

    /// <summary> 마지막 틱 </summary>
    Field( _lastTick )
    i64 _lastTick{};

    WorldObjectId _objectId{};

    /// <summary> 루트 오브젝트 </summary>
    WorldObjectWeakPtr _rootWorldObject{};
};

inline void Send( WorldObject& actor, SendBufferPtr&& buffer );
inline void Send( WorldObjectRef object, SendBufferPtr&& buffer );

EXPORT_END
