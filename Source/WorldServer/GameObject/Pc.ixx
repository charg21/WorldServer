export module Pc;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Core.Concepts;
import Mala.Container;
import Mala.Db.TxExecutor;
import Mala.Math;
import Mala.Reflection;

import WorldObject;
import WorldObjectTypes;
import Character;
import PcDbModel;

import BagComponent;
import CombatComponent;
import ChatComponent;
import EquipComponent;
import MovementComponent;
import QuestComponent;
import SenderComponent;
import StatComponent;
import ViewComponent;

import ELogicResult;

using namespace Mala::Net;
using namespace Mala::Db;
using namespace Mala::Threading;
using namespace Mala::Container;

EXPORT_BEGIN

/// <summary>
/// 플레이어 캐릭터
/// </summary>
class Pc final
    : public Character
    , public TxExecutor
    , public PcDbModel
    , public std::enable_shared_from_this< Pc >
{
    GENERATE_CLASS_TYPE_INFO( Pc );

public:
    using BaseDbModel = PcDbModel;
    using Character::Send;
    using PcPtrList = Vector< PcPtr >;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    Pc();
    ~Pc() final;
    void OnTick( f32 deltaTime ) final;

    /// <summary>
    /// 송신한다.
    /// </summary>
    template< typename S_Packet >
    void Send( ELogicResult result );

public:
    /// <summary>
    /// 월드 콜백
    /// </summary>
    void OnEnterWorld() final;
    void OnPreEnterWorld() final;
    void OnLeaveWorld() final;

protected:
    bool CanTick() final;

public:
    StaticVector< IDbLoadComponent*, 64 > GetDbLoadComponentList();

public:
    JobExecutorPtr SharedFromJobExecutor() final;
    JobExecutorWeakPtr WeakFromJobExecutor() final;
    TxExecutorPtr SharedFromTxExecutor() final;
    TxExecutorWeakPtr WeakFromTxExecutor() final;

    /// <summary>
    /// 동기화 한다
    /// </summary>
    void Sync( EQueryType queryType ) final;

    static PcPtrList GetPcList( ::UserId userId );

public:
    /// <summary>
    /// 컴포넌트
    /// </summary>
    Field( _pcView )
    PcViewComponent _pcView;

    Field( _pcMovement )
    PcMovementComponent _pcMovement;

    Field( _pcCombat )
    PcCombatComponent _pcCombat;

    Field( _pcBag )
    PcBagComponent _pcBag;

    Field( _chat )
    PcChatComponent _chat;

    Field( _pcStat )
    PcStatComponent _pcStat;

    Field( _equip )
    PcEquipComponent _equip;

    Field( _sender )
    SenderComponent _sender;

    Field( _quest )
    QuestComponent _quest;
};

EXPORT_END

/// <summary>
/// 송신한다.
/// </summary>
template< typename S_Packet >
inline void Pc::Send( ELogicResult result )
{
    Send( S_Packet{ ._result = result }.Write() );
}
