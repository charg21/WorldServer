import <Macro.h>;

import Mala.Core.Crash;
import Mala.Core.TypeCast;
import Mala.Core.TypeList;
import Mala.Math;
import Mala.Threading.Lock;

import Character;
import World;
import ClusterManager;
import Npc;
import Pc;
import ClientSession;
import ThreadLocal;
import MovementComponent;

import PacketExporter;
import EWorldObjectType;
import PktObject;
import WorldPacket.S_Spawn;
import WorldPacket.S_EnterWorld;
import WorldPacket.S_Move;
import WorldPacket.S_Despawn;
import WorldPacket.S_LeaveWorld;

using namespace Mala::Core;
using namespace Mala::Math;
using namespace Mala::Net;

Character::Character( EWorldObjectType objectType )
: WorldObject( objectType )
{
    INIT_TL( Character );
}

void Character::Send( SendBufferRef sendBuffer )
{
    if ( !_session )
        return;

    _session->Send( sendBuffer );
}

void Character::Send( SendBufferPtr&& sendBuffer )
{
    if ( !_session )
        return;

    _session->Send( std::move( sendBuffer ) );
}

void Character::SetSession( ClientSessionRef session )
{
    _session = session;
}

void Character::SetPos( const PktVector3& pos )
{
    _movement->_pos._x = pos._x;
    _movement->_pos._y = pos._y;
    _movement->_pos._z = pos._z;
}

void Character::ExportTo( S_Despawn& notify )
{
    notify._objectId = *GetObjectId();
}

void Character::ExportTo( S_Move& move )
{
	move._objectId = *GetObjectId();
    move._pos._y = GetPos()._y;
    move._pos._x = GetPos()._x;
    move._pos._z = GetPos()._z;

    move._yaw = Vector2ToYaw( _movement->_dir );

    move._toPos._x = _movement->_toPos._x;
    move._toPos._y = _movement->_toPos._y;
    move._toPos._z = _movement->_toPos._z;
}

void Character::ExportTo( S_LeaveWorld& ack )
{
    ack._objectId = *GetObjectId();
}
