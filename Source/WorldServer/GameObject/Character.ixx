export module Character;

import <Macro.h>;

import <memory>;

import Mala.Core.Types;
import Mala.Math;
import Mala.Reflection;

import WorldObject;
import WorldObjectTypes;
import PktVector3;

using namespace Mala::Net;
using namespace Mala::Threading;

EXPORT_BEGIN

using ClientSessionPtr = std::shared_ptr< class ClientSession >;
using ClientSessionRef = const ClientSessionPtr&;

/// <summary>
/// 캐릭터 클래스
/// </summary>
class Character : public WorldObject
{
    friend class PacketExporter;

    using WorldObjectBase = WorldObject;

    GENERATE_CLASS_TYPE_INFO( Character );

    friend class ClientSession;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    Character( EWorldObjectType actorType );
    ~Character() override = default;

    /// <summary>
    /// 송신한다
    /// </summary>
    void Send( SendBufferRef sendBuffer );
    void Send( SendBufferPtr&& sendBuffer );

    /// <summary>
    /// 세션을 설정한다
    /// </summary>
    void SetSession( ClientSessionRef session );

    /// <summary>
    /// 좌표를 설정한다
    /// </summary>
    void SetPos( const PktVector3& pos );
    using WorldObject::SetPos;

    void ExportTo( class S_Despawn& despawn );
    void ExportTo( class S_Move& move );
    void ExportTo( class S_LeaveWorld& leave );

public:
    ClientSessionPtr _session{};
};

EXPORT_END
