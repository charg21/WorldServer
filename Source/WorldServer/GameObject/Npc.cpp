import <Macro.h>;

import Mala.Core.Types;
import Mala.Core.Random;
import Mala.Core.CoreTLS;
import Mala.Core.Variant;
import Mala.Math;
import Mala.Reflection;
import Mala.Threading.JobExecutor;

import WorldObject;
import <Macro.h>;

import ClusterManager;
import Npc;
import World;
import Config;
import ThreadLocal;
import IComponent;
import ViewComponent;
import CombatComponent;
import SkillListComponent;

import EWorldObjectType;

import PktObject;
import PacketExporter;

import WorldPacket.S_Spawn;
import WorldPacket.S_Move;
import WorldPacket.S_Despawn;

using namespace Mala::Threading;

/// <summary>
/// 생성자
/// </summary>
Npc::Npc()
: Character( EWorldObjectType::Npc )
, _npcCombat  { *this }
, _npcView    { *this }
, _npcMovement{ *this }
, _ai         { *this }
, _aggro      { *this }
, _npcStat    { *this }
, _skillList  { *this }
{
    INIT_TL( Npc );
    Add< ViewComponent >( &_npcView );
    Add< CombatComponent >( &_npcCombat );
    Add< NpcMovementComponent >( &_npcMovement );
    _view = &_npcView;
    _combat = &_npcCombat;
    _movement = &_npcMovement;
    //_ai.Init( *this );
    //auto& typeInfo = GetTypeInfo();
    //auto* field = typeInfo.GetField( "_activeState" );
    //field->Set( this, Mala::Core::Variant{ ENpcState::Busy } );
}

void Npc::OnEnterWorld()
{
    _view->Attach();
    _lastTick               = LLastTick;
    _npcMovement._toPos     = GetPos();
    _npcMovement._returnPos = GetPos();
    _ai.Init();
}

JobExecutorPtr Npc::SharedFromJobExecutor()
{
    return shared_from_this();
}

JobExecutorWeakPtr Npc::WeakFromJobExecutor()
{
    return shared_from_this();
}

bool Npc::CanTick()
{
    // 주변에 플레이어가 없다면 갱신을 안함.
    return !_view->Empty();
}

void Npc::OnTick( f32 deltaTime )
{
    _ai.Tick( deltaTime );
}

/// <summary>
/// 활성 상태
/// </summary>
EWakeUpState Npc::GetWakeUpState()
{
    return _wakeUpState;
}

/// <summary>
/// 바쁜 상태 여부
/// </summary>
bool Npc::IsBusy()
{
    return _wakeUpState == EWakeUpState::Awaken;
}
