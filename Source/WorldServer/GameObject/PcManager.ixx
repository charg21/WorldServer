export module PcManager;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Core.StrongType;
import Mala.Db.DbModelCache;
import Mala.Threading.JobExecutor;
import Mala.Reflection;

import WorldObject;
import WorldObjectTypes;
import WorldObjectManager;
import Pc;

using namespace std;
using namespace Mala::Net;
using namespace Mala::Db;
using namespace Mala::Threading;

EXPORT_BEGIN

/// <summary>
/// PC 관리자
/// </summary>
class PcManager
    : public TWorldObjectManager< Pc >
    , public JobExecutor
    , public std::enable_shared_from_this< PcManager >
{
    GENERATE_CLASS_TYPE_INFO( PcManager );

public:
    PcManager() = default;
    ~PcManager() = default;

    template< typename S_Packet >
    void BroadCast( ELogicResult result );

    JobExecutorPtr SharedFromJobExecutor() final { return shared_from_this(); }
    JobExecutorWeakPtr WeakFromJobExecutor() final { return SharedFromJobExecutor(); }
};

inline extern std::shared_ptr< PcManager > GPcManagerPtr{};

EXPORT_END

template< typename S_Packet >
void PcManager::BroadCast( ELogicResult result )
{
    //for ( auto& [ actorId, pc ] : _objects )
    //{
    //	//pc->Send< S_Packet >( result );
    //}
}
