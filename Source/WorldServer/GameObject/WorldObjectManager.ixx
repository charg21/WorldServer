export module WorldObjectManager;

import <Macro.h>;

import Mala.Core.TypeCast;
import Mala.Core.Types;
import Mala.Container;
import Mala.Threading.Lock;
import Mala.Memory;

import WorldObject;
import WorldObjectTypes;
import Pc;

using namespace Mala::Container;
using namespace Mala::Core;
using namespace Mala::Threading;

EXPORT_BEGIN

/// <summary>
/// 오브젝트 관리자 클래스
/// </summary>
template< typename TLock, WorldObjectType TWorldObject >
class TLockWorldObjectManager
{
    /// <summary>
    /// 오브젝트 포인터 타입 정의
    /// </summary>
    using TWorldObjectPtr = std::shared_ptr< TWorldObject >;

    /// <summary>
    /// 오브젝트 포인터의 레퍼런스 타입 정의
    /// </summary>
    using TWorldObjectRef = const TWorldObjectPtr&;

    /// <summary>
    /// 오브젝트 맵 타입 정의
    /// </summary>
    using TWorldObjectMap = HashMap< WorldObjectId, TWorldObjectPtr >;

    /// <summary>
    /// 오브젝트 맵 이터페이터 타입 정의
    /// </summary>
    using TWorldObjectMapIterator = typename TWorldObjectMap::iterator;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    TLockWorldObjectManager() = default;

    /// <summary>
    /// 소멸자
    /// </summary>
    ~TLockWorldObjectManager() = default;

    /// <summary>
    /// 등록한다
    /// </summary>
    bool Register( WorldObjectId id, TWorldObjectRef object )
    {
        {
            EXCLUSIVE_LOCK;

            return _objectMap.emplace( id, object ).second;
        }
    }

    /// <summary>
    /// 등록해제한다
    /// </summary>
    bool Unregister( WorldObjectId id )
    {
        {
            EXCLUSIVE_LOCK;

            return _objectMap.erase( id );
        }
    }

    /// <summary>
    /// 획득한다.
    /// </summary>
    TWorldObjectPtr GetWorldObject( WorldObjectId id )
    {
        {
            SHARED_LOCK;

            if ( auto findResult = _objectMap.find( id ); findResult != _objectMap.end() )
                return findResult->second;
        }

        return nullptr;
    }

    /// <summary>
    /// 획득한다.
    /// </summary>
    TWorldObjectRef GetWorldObjectRef( WorldObjectId id )
    {
        {
            SHARED_LOCK;

            if ( auto findResult = _objectMap.find( id ); findResult != _objectMap.end() )
                return findResult->second;
        }

        return nullptr;
    }

    /// <summary>
    /// 오브젝트를 반환한다.
    /// 오브젝트가 없으면 생성 및 등록후 반환한다
    /// </summary>
    template< WorldObjectType TTWorldObject >
    std::shared_ptr< TTWorldObject > GetOrDefault( WorldObjectId id )
    {
        TWorldObjectPtr actorPtr;

        {
            EXCLUSIVE_LOCK;

            // 레퍼런스 카운트 증가
            actorPtr = _objectMap[ id ];
            if ( !actorPtr )
            {
                actorPtr = MakeShared< TTWorldObject >();
                actorPtr->SetWorldObjectId( id );

                _objectMap[ id ] = actorPtr;
            }
        }

        return TypeCast< TTWorldObject >( std::move( actorPtr ) );
    }

    bool Contains( WorldObjectId id )
    {
        {
            SHARED_LOCK;

            return _objectMap.contains( id );
        }
    }

protected:
    TLock     _lock;
    TWorldObjectMap _objectMap;
};

/// <summary>
/// 오브젝트 관리자 클래스
/// </summary>
template< std::derived_from< WorldObject > TWorldObject >
class TWorldObjectManager
{
    /// <summary>
    /// 오브젝트 포인터 타입 정의
    /// </summary>
    using TWorldObjectPtr = std::shared_ptr< TWorldObject >;

    /// <summary>
    /// 오브젝트 포인터의 레퍼런스 타입 정의
    /// </summary>
    using TWorldObjectRef = const TWorldObjectPtr&;

    /// <summary>
    /// 오브젝트 맵 타입 정의
    /// </summary>
    using TWorldObjectMap = ConcurrentHashMap< WorldObjectId, TWorldObjectPtr >;

    /// <summary>
    /// 오브젝트 맵 이터페이터 타입 정의
    /// </summary>
    using TWorldObjectMapIterator = typename TWorldObjectMap::iterator;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    TWorldObjectManager() = default;

    /// <summary>
    /// 소멸자
    /// </summary>
    ~TWorldObjectManager() = default;

    /// <summary>
    /// 등록한다
    /// </summary>
    bool Register( WorldObjectId id, TWorldObjectRef object )
    {
        _objectMap.insert( { id, object } );
        return true;
    }

    /// <summary>
    /// 등록한다
    /// </summary>
    void Register( WorldObjectId id, TWorldObjectPtr&& object )
    {
        _objectMap.insert( { id, std::move( object ) } );
    }

    /// <summary>
    /// 획득한다.
    /// </summary>
    TWorldObjectPtr GetWorldObject( WorldObjectId id )
    {
        return _objectMap.find( id )->second;
    }

    /// <summary>
    /// 획득한다.
    /// </summary>
    TWorldObjectRef GetWorldObjectRef( WorldObjectId id )
    {
        if ( auto findResult = _objectMap.find( id ); findResult != _objectMap.end() )
            return findResult->second;

        return nullptr;
    }

    /// <summary>
    /// 오브젝트를 반환한다.
    /// 오브젝트가 없으면 생성 및 등록후 반환한다
    /// </summary>
    template< WorldObjectType TTWorldObject >
    std::shared_ptr< TTWorldObject > GetOrDefault( WorldObjectId id )
    {
        TWorldObjectPtr actorPtr;

        {
            // 레퍼런스 카운트 증가
            actorPtr = _objectMap[ id ];
            if ( !actorPtr )
            {
                actorPtr = MakeShared< TTWorldObject >();
                actorPtr->SetWorldObjectId( id );

                _objectMap[ id ] = actorPtr;
            }
        }

        return TypeCast< TTWorldObject >( std::move( actorPtr ) );
    }

    /// <summary>
    /// 보유 여부를 반환한다.
    /// </summary>
    bool Contains( WorldObjectId id )
    {
        return _objectMap.contains( id );
    }

protected:
    TWorldObjectMap _objectMap;
};

using WorldObjectManager      = TLockWorldObjectManager< Lock, WorldObject >;
using LocalWorldObjectManager = TLockWorldObjectManager< NullLock, WorldObject >;

inline extern WorldObjectManager* GWorldObjectManager{};
inline thread_local extern LocalWorldObjectManager* LWorldObjectManager{};

EXPORT_END
