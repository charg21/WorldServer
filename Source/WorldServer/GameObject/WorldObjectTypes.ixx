export module WorldObjectTypes;

import <Macro.h>;

export import <memory>;
export import Mala.Core.Types;
export import Mala.Core.TypeList;
export import Mala.Core.StrongType;
export import Mala.Container;
export import Mala.Math;

export import CommonTypes;

using namespace Mala::Container;
using namespace Mala::Core;
using namespace Mala::Math;

EXPORT_BEGIN

/// <summary>
/// 오브젝트 관련 타입 정의
/// </summary>
using Position  = Vector3;
using Direction = Vector2;
using Index     = Vector2Int;

/// <summary>
/// 오브젝트 포인터 타입 정의
/// </summary>
DECLARE_SHARED_PTR( WorldObject );
DECLARE_SHARED_PTR( Character   );
DECLARE_SHARED_PTR( Pc          );
DECLARE_SHARED_PTR( Npc         );
DECLARE_SHARED_PTR( Projectile  );
DECLARE_SHARED_PTR( Gadget      );
DECLARE_SHARED_PTR( StatCache   );

/// <summary>
/// 오브젝트 타입 타입 리스트 정의
/// </summary>
using AllWorldObjectTypeList = TypeList< WorldObject, Character, Pc, Npc >;
using WorldObjectTypeList    = TypeList< Pc, Npc >;
using WorldObjectTL          = AllWorldObjectTypeList;
using WorldObjectPtrMap      = HashMap< WorldObjectId, WorldObjectPtr >;
using WorldObjectMap         = HashMap< WorldObjectId, WorldObject* >;
using WorldObjectMap2        = FlatHashMap< WorldObjectId, WorldObject* >;
using WorldObjectList        = Vector< WorldObject* >;
using WorldObjectMaps        = Vector< WorldObjectMap* >;
using WorldObjectMaps2       = Vector< WorldObjectMap* >;
using PcPtrMap               = HashMap< WorldObjectId, PcPtr >;
using PcPtrVector            = Vector< PcPtr >;
using PcMap                  = HashMap< WorldObjectId, Pc* >;
using NpcPtrMap              = HashMap< WorldObjectId, NpcPtr >;
using NpcMap                 = HashMap< WorldObjectId, Npc* >;
using NpcPtrVector           = Vector< NpcPtr >;

/// <summary>
/// 시야 처리 자료 구조
/// </summary>
using ViewList                 = HashSet< WorldObjectId >;
using ViewWorldObjectList      = HashMap< WorldObjectId, WorldObjectPtr >;
//using ViewWorldObjectList2     = FlatHashMap< WorldObjectId, WorldObjectPtr >;
using LocalViewWorldObjectList = HashMap< WorldObjectId, WorldObject* >;

template< typename T >
concept WorldObjectType = std::is_base_of_v< WorldObject, T >;

EXPORT_END
