export module EEquipSlot;

export enum class EEquipSlot : unsigned char
{
    Weapon = 0, /// 
    Armor  = 1, /// 
    Max    = 2, /// 
};

export constexpr EEquipSlot EEquipSlotList[]
{
    EEquipSlot::Weapon,
    EEquipSlot::Armor,
    EEquipSlot::Max,
};

