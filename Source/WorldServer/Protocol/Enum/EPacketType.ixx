export module EPacketType;

export enum class EPacketType : unsigned short
{
    C_EnterWorld  = 0, /// 
    S_EnterWorld  = 1, /// 
    S_Spawn       = 2, /// 
    C_LeaveWorld  = 3, /// 
    S_LeaveWorld  = 4, /// 
    S_Despawn     = 5, /// 
    C_Move        = 6, /// 
    S_Move        = 7, /// 
    S_SkillList   = 8, /// 
    C_StartSkill  = 9, /// 
    S_StartSkill  = 10, /// 
    S_Hit         = 11, /// 
    C_Login       = 12, /// 
    S_Login       = 13, /// 
    S_Ping        = 14, /// 
    C_Pong        = 15, /// 
    C_Chat        = 16, /// 
    S_Chat        = 17, /// 
    C_LoadPc      = 18, /// 
    S_LoadPc      = 19, /// 
    C_Cheat       = 20, /// 
    S_Cheat       = 21, /// 
    S_Message     = 22, /// 
    C_ListChannel = 23, /// 
    S_ListChannel = 24, /// 
    C_UseItem     = 25, /// 
    S_UseItem     = 26, /// 
    S_ListItem    = 27, /// 
    C_LockItem    = 28, /// 
    S_LockItem    = 29, /// 
    C_UnLockItem  = 30, /// 
    S_UnLockItem  = 31, /// 
    Max           = 32, /// 이 열거형의 최대값
};

export constexpr EPacketType EPacketTypeList[]
{
    EPacketType::C_EnterWorld,
    EPacketType::S_EnterWorld,
    EPacketType::S_Spawn,
    EPacketType::C_LeaveWorld,
    EPacketType::S_LeaveWorld,
    EPacketType::S_Despawn,
    EPacketType::C_Move,
    EPacketType::S_Move,
    EPacketType::S_SkillList,
    EPacketType::C_StartSkill,
    EPacketType::S_StartSkill,
    EPacketType::S_Hit,
    EPacketType::C_Login,
    EPacketType::S_Login,
    EPacketType::S_Ping,
    EPacketType::C_Pong,
    EPacketType::C_Chat,
    EPacketType::S_Chat,
    EPacketType::C_LoadPc,
    EPacketType::S_LoadPc,
    EPacketType::C_Cheat,
    EPacketType::S_Cheat,
    EPacketType::S_Message,
    EPacketType::C_ListChannel,
    EPacketType::S_ListChannel,
    EPacketType::C_UseItem,
    EPacketType::S_UseItem,
    EPacketType::S_ListItem,
    EPacketType::C_LockItem,
    EPacketType::S_LockItem,
    EPacketType::C_UnLockItem,
    EPacketType::S_UnLockItem,
    EPacketType::Max,
};

