export module EQuestState;

export enum class EQuestState : unsigned char
{
    NotStarted = 0, /// 
    Progress   = 1, /// 
    Complete   = 2, /// 
};

export constexpr EQuestState EQuestStateList[]
{
    EQuestState::NotStarted,
    EQuestState::Progress,
    EQuestState::Complete,
};

