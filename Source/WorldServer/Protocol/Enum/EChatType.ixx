export module EChatType;

export enum class EChatType : short
{
    World  = 0, /// 
    Global = 1, /// 
};

export constexpr EChatType EChatTypeList[]
{
    EChatType::World,
    EChatType::Global,
};

