export module EStat;

export enum class EStat : short
{
    Exp      = 0, ///
    Hp       = 1, ///
    Power    = 2, ///
    Speed    = 3, ///
    Critical = 4, ///
    Max      = 5, ///
};

export constexpr EStat EStatList[]
{
    EStat::Exp,
    EStat::Hp,
    EStat::Power,
    EStat::Speed,
    EStat::Critical,
    EStat::Max,
};

