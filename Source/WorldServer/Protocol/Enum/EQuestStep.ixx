export module EQuestStep;

export enum class EQuestStep : unsigned char
{
    Hunt    = 0, /// 
    Meet    = 1, /// 
    Collect = 2, /// 
};

export constexpr EQuestStep EQuestStepList[]
{
    EQuestStep::Hunt,
    EQuestStep::Meet,
    EQuestStep::Collect,
};

