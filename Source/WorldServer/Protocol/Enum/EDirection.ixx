export module EDirection;

export enum class EDirection : unsigned char
{
    Left  = 0, /// 
    Right = 1, /// 
    Up    = 2, /// 
    Down  = 3, /// 
    Max   = 4, /// 
};

export constexpr EDirection EDirectionList[]
{
    EDirection::Left,
    EDirection::Right,
    EDirection::Up,
    EDirection::Down,
    EDirection::Max,
};

