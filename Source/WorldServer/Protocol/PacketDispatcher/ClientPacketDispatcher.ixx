export module ClientPacketDispatcher;

/// <summary>
/// 툴에 의해 자동 생성된 코드입니다.
/// </summary>

import Mala.Net.Buffer;
import Mala.Net.PacketDispatcher;

import EPacketType;

import WorldPacket.C_EnterWorld;
import WorldPacket.C_EnterWorldHandler;
import WorldPacket.C_LeaveWorld;
import WorldPacket.C_LeaveWorldHandler;
import WorldPacket.C_Move;
import WorldPacket.C_MoveHandler;
import CombatPacket.C_StartSkill;
import CombatPacket.C_StartSkillHandler;
import AuthPacket.C_LoadPc;
import AuthPacket.C_LoadPcHandler;
import AuthPacket.C_Login;
import AuthPacket.C_LoginHandler;
import PlayPacket.C_Pong;
import PlayPacket.C_PongHandler;
import ChatPacket.C_Chat;
import ChatPacket.C_ChatHandler;
import CheatPacket.C_Cheat;
import CheatPacket.C_CheatHandler;
import ChannelPacket.C_ListChannel;
import ChannelPacket.C_ListChannelHandler;
import ItemPacket.C_UseItem;
import ItemPacket.C_UseItemHandler;
import ItemPacket.C_LockItem;
import ItemPacket.C_LockItemHandler;
import ItemPacket.C_UnLockItem;
import ItemPacket.C_UnLockItemHandler;

using namespace std;
using namespace Mala::Net;

#define REGISTER_HANDLER( PKT_TYPE ) \
Base::Table[ (size_t)( EPacketType::PKT_TYPE ) ] = HandlerJob< PKT_TYPE >;

export class ClientPacketDispatcher : public Mala::Net::PacketDispatcher
{
    using Base = Mala::Net::PacketDispatcher;
public:
    using Mala::Net::PacketDispatcher::Table;

public:
    ClientPacketDispatcher()
    {
        Base::SetCapacity( (int)( EPacketType::Max) );

        REGISTER_HANDLER( C_EnterWorld );
        REGISTER_HANDLER( C_LeaveWorld );
        REGISTER_HANDLER( C_Move );
        REGISTER_HANDLER( C_StartSkill );
        REGISTER_HANDLER( C_Login );
        REGISTER_HANDLER( C_Pong );
        REGISTER_HANDLER( C_Chat );
        REGISTER_HANDLER( C_LoadPc );
        REGISTER_HANDLER( C_Cheat );
        REGISTER_HANDLER( C_ListChannel );
        REGISTER_HANDLER( C_UseItem );
        REGISTER_HANDLER( C_LockItem );
        REGISTER_HANDLER( C_UnLockItem );

    }
public:
    static packet_HandlerJob GetHandler( int n )
    {
        return ClientPacketDispatcher::Table[ n ];
    }


};

inline static ClientPacketDispatcher initOnce{};
