export module AuthPacket.C_LoginHandler;

import Mala.Net.PacketSession;
import Mala.Net.PacketDispatcher;
import Mala.Memory;

import ClientSession;
import Pc;
import User;

import AuthPacket.C_Login;
import AuthPacket.S_Login;
import Auth.LoginTx;

using namespace Mala::Net;

template<>
void Mala::Net::Handler< C_Login >( PacketSession* session, C_Login& packet )
{
    auto* clientSession{ reinterpret_cast< ClientSession* >( session ) };
    auto& user = clientSession->_user;
    if ( !user )
    {
        user = MakeShared< User >();
        user->_session = clientSession;
    }

    auto result = LoginTx::Login( user, packet );
    if ( result != ELogicResult::Pending )
        clientSession->Send< S_Login >( result );
};

