export module ItemPacket.C_LockItemHandler;


import Mala.Net.PacketSession;
import Mala.Net.PacketDispatcher;

import ItemPacket.C_LockItem;

import Pc;
import ClientSession;
import Item.LockItemTx;

using namespace Mala::Net;

template<>
void Mala::Net::Handler< C_LockItem >( PacketSession* session, C_LockItem& packet )
{
    auto clientSession = reinterpret_cast< ClientSession* >( session );

    auto& pc = clientSession->_pc;
    if ( !pc )
        return;

    LockItemTx::Run( pc, { packet._itemId }, true );
};

