export module WorldPacket.C_LeaveWorldHandler;


import Mala.Net.PacketSession;
import Mala.Net.PacketDispatcher;

// Contents
import Pc;
import ClientSession;
import ThreadLocal;
import World;
import ClusterManager;

import WorldPacket.C_LeaveWorld;
import WorldPacket.S_LeaveWorld;
import WorldPacket.S_Despawn;

using namespace Mala::Net;

template<>
void Mala::Net::Handler< C_LeaveWorld >( PacketSession* session, C_LeaveWorld& packet )
{
    auto clientSession = reinterpret_cast< ClientSession* >( session );
    auto& pc = clientSession->_pc;
    // dor액터 아이디, 좌표,
    pc->LeaveWorld();
    // LClusterManager->Leave( pc->GetObjectId(), pc->GetWorldObjectType() );

    clientSession->_pc = nullptr;
};
