export module AuthPacket.C_LoadPcHandler;


import Mala.Net.PacketSession;
import Mala.Net.PacketDispatcher;
import Mala.Memory;
import Mala.Db.TxContext;

import AuthPacket.C_LoadPc;
import AuthPacket.S_LoadPc;
import SaveLoad.LoadPcTx;

import ClientSession;
import CommonTypes;
import Pc;

using namespace Mala::Net;
using namespace Mala::Db;


template<>
void Mala::Net::Handler< C_LoadPc >( PacketSession* session, C_LoadPc& packet )
{
    auto& loadPc       { reinterpret_cast< C_LoadPc& >( packet )        };
    auto& clientSession{ reinterpret_cast< ClientSession& >( *session ) };
    auto& pc           { clientSession._pc                              };
    if ( !pc )
    {
        clientSession.SetPc( MakeShared< Pc >() );
        pc->SetWorldObjectId( { loadPc._pcId } );
    }

    auto loadResult = LoadPcTx::Load( pc, { loadPc._pcId } );
    if ( loadResult != ELogicResult::Pending )
        clientSession.Send( S_LoadPc{ ._result = loadResult }.Write() );
};

