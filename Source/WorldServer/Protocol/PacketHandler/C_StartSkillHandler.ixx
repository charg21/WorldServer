export module CombatPacket.C_StartSkillHandler;

import Mala.Net.PacketSession;
import Mala.Net.PacketDispatcher;
import Mala.Container;

import WorldObjectTypes;
import ClientSession;
import ELogicResult;
import Pc;
import SkillLogic;

import CombatPacket.C_StartSkill;
import CombatPacket.S_StartSkill;

using namespace Mala::Net;

template<>
void Mala::Net::Handler< C_StartSkill >( PacketSession* session, C_StartSkill& packet )
{
    auto clientSession = reinterpret_cast< ClientSession* >( session );
    auto& pcRef{ clientSession->_pc };

    auto result = SkillLogic::Start( pcRef, packet );
    if ( result != ELogicResult::Pending )
        return clientSession->Send< S_StartSkill >( result );
};
