export module WorldPacket.C_EnterWorldHandler;

import <Macro.h>;

import Mala.Core.Random;
import Mala.Core.StrongType;
import Mala.Log.ConsoleLog;
import Mala.Net.PacketSession;
import Mala.Net.PacketDispatcher;
import Mala.Math;
import Mala.Memory;
import Mala.Log;

// Contents
import WorldObjectTypes;
import WorldObjectManager;
import Consts;
import World;
import WorldHelper;
import Character;
import Pc;
import ClientSession;
import ClusterManager;
import ThreadLocal;

// Protocol
import PktVector3;
import PktObject;
import WorldPacket.C_EnterWorld;

using namespace Mala::Core;
using namespace Mala::Net;
using namespace Mala::Math;

template<>
void Mala::Net::Handler< C_EnterWorld >( PacketSession* session, C_EnterWorld& packet )
{
    auto clientSession = reinterpret_cast< ClientSession* >( session );

    LOG( L"Connected... Id: {}", clientSession->_id );

    PktVector3 position{ FastRand() % ( 800 * Multiple ), 0.0f, FastRand() % ( 800 * Multiple ) };
    //PktPosition position{ 0.0f, 0.0f, 0.0f };

    auto& pc = clientSession->_pc;
    if ( !pc )
    {
        pc = MakeShared< Pc >();
        pc->SetWorldObjectId( WorldObjectId( (u64)( clientSession->_id ) ) );
        pc->SetSession( clientSession->GetClientSession() );
        pc->SetPos( position );
        pc->SetFromPos( pc->GetPos() );
        pc->SetIndex( WorldHelper::GetClusterIndex( pc->GetPos() ) );
    }

    pc->Post( [ pc = pc.get() ]()
    {
    	pc->EnterWorld();
    } );
};
