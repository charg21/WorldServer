export module WorldPacket.C_MoveHandler;

import <Macro.h>;

import Mala.Log.ConsoleLog;
import Mala.Math.Vector2;
import Mala.Net.PacketSession;
import Mala.Net.PacketDispatcher;

// Contents
import WorldObject;
import WorldObjectTypes;
import Pc;
import Character;
import ClientSession;
import MovementComponent;

import PktVector3;
import WorldPacket.C_Move;

using namespace Mala::Net;
using namespace Mala::Math;

template<>
void Mala::Net::Handler< C_Move >( PacketSession* session, C_Move& packet )
{
    auto clientSession{ reinterpret_cast< ClientSession* >( session ) };
    auto pc{ clientSession->GetPc() };

    pc->Post(
    [
        pcMovement = &pc->_pcMovement,
        pktPos = packet._pos,
        pktToPos = packet._toPos,
        dirYaw = packet._yaw ]
    {
        auto dir{ Direction::FromYaw( dirYaw ) };
        Position pos{ pktPos._x, pktPos._y, pktPos._z };

        pcMovement->MoveTo( pos, dir );
    } );
};

