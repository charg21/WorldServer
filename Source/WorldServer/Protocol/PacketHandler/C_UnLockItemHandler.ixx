export module ItemPacket.C_UnLockItemHandler;


import Mala.Net.PacketSession;
import Mala.Net.PacketDispatcher;

import ItemPacket.C_UnLockItem;

import Pc;
import ClientSession;
import Item.LockItemTx;

using namespace Mala::Net;

template<>
void Mala::Net::Handler< C_UnLockItem >( PacketSession* session, C_UnLockItem& packet )
{
    auto clientSession = reinterpret_cast< ClientSession* >( session );

    auto& pc = clientSession->_pc;
    if ( !pc )
        return;

    LockItemTx::Run( pc, { packet._itemId }, false );
};

