import PacketExporter;

import Mala.Core.CoreGlobal;
import Mala.Core.Types;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;
import Mala.Net.PacketHeader;
import Mala.Math;
import EPacketType;

import <string>;

import EWorldObjectType;
import Character;
import WorldObject;
import Pc;
import WorldObjectTypes;
import DesignTypes;
import SkillDesign;
import MovementComponent;

import PktObject;
import PktPc;
import PktVector3;
import CombatPacket.S_Hit;
import CombatPacket.S_StartSkill;
import WorldPacket.S_EnterWorld;
import WorldPacket.S_LeaveWorld;
import WorldPacket.S_Move;
import WorldPacket.S_Spawn;

using namespace std;
using namespace Mala::Net;
using namespace Mala::Math;

PktVector3 ToPktPos( const Position& pos )
{
    return { pos.X, pos.Y, pos.Z };
}

void PacketExporter::ExportToWorldObject( WorldObject& actor, PktObject& pktWorldObject )
{
    auto& movement{ actor._movement };

    pktWorldObject._objectId = actor._objectId._value;
    pktWorldObject._pos = ToPktPos( movement->GetPos() );
    pktWorldObject._objectType = actor._objectType;
    pktWorldObject._yaw = Vector2ToYaw( movement->_dir );
}

void PacketExporter::ExportToWorldObject( Character& character, PktObject& pktWorldObject )
{
    auto& movement{ character._movement };

    pktWorldObject._objectId = *( character.GetObjectId() );
    pktWorldObject._pos = ToPktPos( movement->GetPos() );
    pktWorldObject._yaw = Vector2ToYaw( movement->_dir );
    pktWorldObject._objectType = character._objectType;
}

void PacketExporter::ExportToPc( Pc& pc, PktPc& pktPc )
{
    auto& movement{ pc._movement };

    pktPc._objectId = *pc.GetObjectId();
    pktPc._pos = ToPktPos( movement->GetPos() );
    pktPc._yaw = Vector2ToYaw( movement->_dir );
    //pc._name = character._name;
}

void PacketExporter::ExportToS_StartSkill(
    SkillDesignRef skillDesign,
    PcRef          caster,
    S_StartSkill&  startSkill )
{
    startSkill._result = ELogicResult::Success;
    startSkill._skillDesignId = *skillDesign->_id;
    startSkill._casterId = *caster->GetId();
}

void PacketExporter::ExportToS_Hit(
    SkillDesignRef skillDesign,
    i64            curHp,
    PcRef          caster,
    S_Hit&         hit )
{
    hit._damage        = skillDesign->_damage;
    hit._curHp         = curHp;
    hit._skillDesignId = *skillDesign->_id;
    hit._casterId      = *caster->GetObjectId();
}

void PacketExporter::ExportToS_Spawn( Character& character, S_Spawn& spawn )
{
    ExportToWorldObject( character, spawn._other );
}

void PacketExporter::ExportToS_Move( WorldObjectId moverId, const Position& fromPos, f32 yaw, S_Move& moveNoti )
{
    moveNoti._objectId = *moverId;
    moveNoti._pos = ToPktPos( fromPos );
    moveNoti._yaw = yaw;
}

S_Move PacketExporter::ExportToS_Move(
          WorldObjectId moverId,
    const Position&     fromPos,
    const Position&     toPos,
          f32           yaw )
{
    return
    {
        *moverId,
        ToPktPos( fromPos ),
        ToPktPos( toPos ),
        yaw
    };
}

SendBufferPtr PacketWriter::WriteS_Move( WorldObjectId moverId, const Position& fromPos, const Position& toPos, f32 yaw )
{
    return PacketExporter::ExportToS_Move( moverId, fromPos, toPos, yaw ).Write();
}

SendBufferPtr PacketWriter::WriteS_EnterWorld( Pc& pc )
{
    S_EnterWorld enter;
    PacketExporter::ExportToPc( pc, enter._my );
    return enter.Write();
}

SendBufferPtr PacketWriter::WriteS_LeaveWorld( Pc& pc )
{
    S_LeaveWorld leave;
    pc.ExportTo( leave );
    return leave.Write();
}
