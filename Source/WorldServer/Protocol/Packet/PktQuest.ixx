export module PktQuest;

import Mala.Core.Types;
import Mala.Core.TypeTraits;
import Mala.Container.String;
import Mala.Text;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;
import Mala.Net.PacketHeader;


using namespace std;
using namespace Mala::Net;

#pragma pack( push, 1 )

export class PktQuest
{
public:
    int _designId; ///
    int _step; ///

public:
    bool Write( SendBufferRef s, int& count )
    {
        if constexpr ( IsPod< PktQuest >::value )
        {
            std::memcpy( s->Buffer() + count, this, sizeof( PktQuest ) );
            count += sizeof( PktQuest );
            return true;
        }
        bool success = true;
        success &= BitConverter::TryWriteBytes( s, count, _designId );
        count += sizeof( int );
        success &= BitConverter::TryWriteBytes( s, count, _step );
        count += sizeof( int );
        return success;
    }

    void Read( BYTE* buffer, int& count )
    {
        if constexpr ( IsPod< PktQuest >::value )
        {
            std::memcpy( this, buffer + count, sizeof( PktQuest ) );
            count += sizeof( PktQuest );
            return;
        }
        _designId = BitConverter::ToInt32( buffer, count );
        count += sizeof( int );
        _step = BitConverter::ToInt32( buffer, count );
        count += sizeof( int );
    }

}
;
#pragma pack( pop )
