export module PktSkill;

import Mala.Core.Types;
import Mala.Core.TypeTraits;
import Mala.Container.String;
import Mala.Text;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;
import Mala.Net.PacketHeader;


using namespace std;
using namespace Mala::Net;

#pragma pack( push, 1 )

export class PktSkill
{
public:
    unsigned int _skillDesignId; /// 
    unsigned long long _lastUseTick; /// 

public:
    bool Write( SendBufferRef s, int& count )
    {
        if constexpr ( IsPod< PktSkill >::value )
        {
            std::memcpy( s->Buffer() + count, this, sizeof( PktSkill ) );
            count += sizeof( PktSkill );
            return true;
        }
        else
        {
            bool success = true;
            success &= BitConverter::TryWriteBytes( s, count, _skillDesignId );
            count   += sizeof( unsigned int );
            success &= BitConverter::TryWriteBytes( s, count, _lastUseTick );
            count   += sizeof( unsigned long long );
            return success;
        }
    }

    void Read( BYTE* buffer, int& count )
    {
        if constexpr ( IsPod< PktSkill >::value )
        {
            std::memcpy( this, buffer + count, sizeof( PktSkill ) );
            count += sizeof( PktSkill );
            return;
        }
        else
        {
            _skillDesignId = BitConverter::ToUInt32( buffer, count );
            count   += sizeof( unsigned int );
            _lastUseTick = BitConverter::ToUInt64( buffer, count );
            count   += sizeof( unsigned long long );
        }
    }

}
;
#pragma pack( pop )
