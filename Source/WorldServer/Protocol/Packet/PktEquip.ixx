export module PktEquip;

import Mala.Core.Types;
import Mala.Core.TypeTraits;
import Mala.Container.String;
import Mala.Text;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;
import Mala.Net.PacketHeader;


using namespace std;
using namespace Mala::Net;

#pragma pack( push, 1 )

export class PktEquip
{
public:
    unsigned long long _itemId; /// 
    unsigned short _slot; /// 

public:
    bool Write( SendBufferRef s, int& count )
    {
        if constexpr ( IsPod< PktEquip >::value )
        {
            std::memcpy( s->Buffer() + count, this, sizeof( PktEquip ) );
            count += sizeof( PktEquip );
            return true;
        }
        else
        {
            bool success = true;
            success &= BitConverter::TryWriteBytes( s, count, _itemId );
            count   += sizeof( unsigned long long );
            success &= BitConverter::TryWriteBytes( s, count, _slot );
            count   += sizeof( unsigned short );
            return success;
        }
    }

    void Read( BYTE* buffer, int& count )
    {
        if constexpr ( IsPod< PktEquip >::value )
        {
            std::memcpy( this, buffer + count, sizeof( PktEquip ) );
            count += sizeof( PktEquip );
            return;
        }
        else
        {
            _itemId = BitConverter::ToUInt64( buffer, count );
            count   += sizeof( unsigned long long );
            _slot = BitConverter::ToUInt16( buffer, count );
            count   += sizeof( unsigned short );
        }
    }

}
;
#pragma pack( pop )
