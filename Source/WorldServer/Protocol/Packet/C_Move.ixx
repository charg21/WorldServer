export module WorldPacket.C_Move;

import Mala.Core.CoreGlobal;
import Mala.Core.Types;
import Mala.Container;
import Mala.Container.String;
import Mala.Text;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;
import Mala.Net.PacketHeader;
import EPacketType;

import PktVector3;

using namespace std;
using namespace Mala::Container;
using namespace Mala::Net;

#pragma pack( push, 1 )
export class C_Move
{
public:
    PktVector3 _pos; /// 
    PktVector3 _toPos; /// 
    float _yaw; /// 

public:
    SendBufferPtr Write()
    {
        auto s = GSendBufferManager->Open( 1024 );
        bool success = BitConverter::TryWriteBytes( s, sizeof( u16 ), (u16)( EPacketType::C_Move ) );
        i32 count{ sizeof( u32 ) };

        success &= _pos.Write( s, count );
        success &= _toPos.Write( s, count );
        success &= BitConverter::TryWriteBytes( s, count, _yaw );
        count   += sizeof( float );
        BitConverter::TryWriteBytes( s, 0, u16( count ) );
        s->Close( count );
        return std::move( s );
    }


    void Read( BYTE* buffer, int len )
    {
        i32 count{ 4 };

        _pos.Read( buffer, count );
        _toPos.Read( buffer, count );
        _yaw = BitConverter::ToSingle( buffer, count );
        count   += sizeof( float );
    }

}

;
#pragma pack( pop )
