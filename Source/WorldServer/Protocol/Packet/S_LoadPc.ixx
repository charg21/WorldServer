export module AuthPacket.S_LoadPc;

import Mala.Core.CoreGlobal;
import Mala.Core.Types;
import Mala.Container;
import Mala.Container.String;
import Mala.Text;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;
import Mala.Net.PacketHeader;
import EPacketType;

import <Vector>;
import ELogicResult;
import PktPc;
import PktItem;
import PktEquip;
import PktSkill;
import PktStat;
import PktQuest;

using namespace std;
using namespace Mala::Container;
using namespace Mala::Net;

#pragma pack( push, 1 )
export class S_LoadPc
{
public:
    ELogicResult _result; /// 
    PktPc _pktPc; /// 
    Vector< PktItem > _items; /// 보유 아이템 목록
    Vector< PktEquip > _equips; /// 장착 장비 목록
    Vector< PktSkill > _skills; /// 보유 스킬 목록
    Vector< PktStat > _stats; /// 스탯 목록
    Vector< PktQuest > _quests; /// 퀘스트 목록

public:
    SendBufferPtr Write()
    {
        auto s = GSendBufferManager->Open( 1024 );
        bool success = BitConverter::TryWriteBytes( s, sizeof( u16 ), (u16)( EPacketType::S_LoadPc ) );
        i32 count{ sizeof( u32 ) };

        success &= BitConverter::TryWriteBytes( s, count, ( std::underlying_type_t< decltype( _result ) > )_result );
        count   += sizeof( ELogicResult );
        success &= _pktPc.Write( s, count );
        success &= BitConverter::TryWriteBytes( s, count, (u16)( _items.size() ) );
        count += sizeof( u16 );
        for ( auto& item : _items )
        {
            success &= item.Write( s, count );
        }
        success &= BitConverter::TryWriteBytes( s, count, (u16)( _equips.size() ) );
        count += sizeof( u16 );
        for ( auto& item : _equips )
        {
            success &= item.Write( s, count );
        }
        success &= BitConverter::TryWriteBytes( s, count, (u16)( _skills.size() ) );
        count += sizeof( u16 );
        for ( auto& item : _skills )
        {
            success &= item.Write( s, count );
        }
        success &= BitConverter::TryWriteBytes( s, count, (u16)( _stats.size() ) );
        count += sizeof( u16 );
        for ( auto& item : _stats )
        {
            success &= item.Write( s, count );
        }
        success &= BitConverter::TryWriteBytes( s, count, (u16)( _quests.size() ) );
        count += sizeof( u16 );
        for ( auto& item : _quests )
        {
            success &= item.Write( s, count );
        }
        BitConverter::TryWriteBytes( s, 0, u16( count ) );
        s->Close( count );
        return std::move( s );
    }


    void Read( BYTE* buffer, int len )
    {
        i32 count{ 4 };

        _result = (ELogicResult)BitConverter::ToInt16( buffer, count );
        count += sizeof( ELogicResult );
        _pktPc.Read( buffer, count );
        u16 _itemsLength = BitConverter::ToUInt16( buffer, count );
        _items.resize( _itemsLength );
        for ( int i = 0; i < _itemsLength; i += 1 )
        {
            auto& _each_ = _items.emplace_back();
            _each_.Read( buffer, count );
        }
        u16 _equipsLength = BitConverter::ToUInt16( buffer, count );
        _equips.resize( _equipsLength );
        for ( int i = 0; i < _equipsLength; i += 1 )
        {
            auto& _each_ = _equips.emplace_back();
            _each_.Read( buffer, count );
        }
        u16 _skillsLength = BitConverter::ToUInt16( buffer, count );
        _skills.resize( _skillsLength );
        for ( int i = 0; i < _skillsLength; i += 1 )
        {
            auto& _each_ = _skills.emplace_back();
            _each_.Read( buffer, count );
        }
        u16 _statsLength = BitConverter::ToUInt16( buffer, count );
        _stats.resize( _statsLength );
        for ( int i = 0; i < _statsLength; i += 1 )
        {
            auto& _each_ = _stats.emplace_back();
            _each_.Read( buffer, count );
        }
        u16 _questsLength = BitConverter::ToUInt16( buffer, count );
        _quests.resize( _questsLength );
        for ( int i = 0; i < _questsLength; i += 1 )
        {
            auto& _each_ = _quests.emplace_back();
            _each_.Read( buffer, count );
        }
    }

}

;
#pragma pack( pop )
