export module PktItem;

import Mala.Core.Types;
import Mala.Core.TypeTraits;
import Mala.Container.String;
import Mala.Text;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;
import Mala.Net.PacketHeader;


using namespace std;
using namespace Mala::Net;

#pragma pack( push, 1 )

export class PktItem
{
public:
    unsigned long long _id; /// 
    unsigned int _designId; /// 
    int _count; /// 

public:
    bool Write( SendBufferRef s, int& count )
    {
        if constexpr ( IsPod< PktItem >::value )
        {
            std::memcpy( s->Buffer() + count, this, sizeof( PktItem ) );
            count += sizeof( PktItem );
            return true;
        }
        else
        {
            bool success = true;
            success &= BitConverter::TryWriteBytes( s, count, _id );
            count   += sizeof( unsigned long long );
            success &= BitConverter::TryWriteBytes( s, count, _designId );
            count   += sizeof( unsigned int );
            success &= BitConverter::TryWriteBytes( s, count, _count );
            count   += sizeof( int );
            return success;
        }
    }

    void Read( BYTE* buffer, int& count )
    {
        if constexpr ( IsPod< PktItem >::value )
        {
            std::memcpy( this, buffer + count, sizeof( PktItem ) );
            count += sizeof( PktItem );
            return;
        }
        else
        {
            _id = BitConverter::ToUInt64( buffer, count );
            count   += sizeof( unsigned long long );
            _designId = BitConverter::ToUInt32( buffer, count );
            count   += sizeof( unsigned int );
            _count = BitConverter::ToInt32( buffer, count );
            count   += sizeof( int );
        }
    }

}
;
#pragma pack( pop )
