export module AuthPacket.S_Login;

import Mala.Core.CoreGlobal;
import Mala.Core.Types;
import Mala.Container;
import Mala.Container.String;
import Mala.Text;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;
import Mala.Net.PacketHeader;
import EPacketType;

import <Vector>;
import ELogicResult;

using namespace std;
using namespace Mala::Container;
using namespace Mala::Net;

#pragma pack( push, 1 )
export class S_Login
{
public:
    ELogicResult _result; /// 
    unsigned long long _userId; /// User Id
    Vector< unsigned long long > _pcIds; /// Pc Id 목록

public:
    SendBufferPtr Write()
    {
        auto s = GSendBufferManager->Open( 1024 );
        bool success = BitConverter::TryWriteBytes( s, sizeof( u16 ), (u16)( EPacketType::S_Login ) );
        i32 count{ sizeof( u32 ) };

        success &= BitConverter::TryWriteBytes( s, count, ( std::underlying_type_t< decltype( _result ) > )_result );
        count   += sizeof( ELogicResult );
        success &= BitConverter::TryWriteBytes( s, count, _userId );
        count   += sizeof( unsigned long long );
        success &= BitConverter::TryWriteBytes( s, count, (u16)( _pcIds.size() ) );
        count += sizeof( u16 );
        for ( auto item : _pcIds )
        {
            success &= BitConverter::TryWriteBytes( s, count, item );
            count   += sizeof( item );
        }
        BitConverter::TryWriteBytes( s, 0, u16( count ) );
        s->Close( count );
        return std::move( s );
    }


    void Read( BYTE* buffer, int len )
    {
        i32 count{ 4 };

        _result = (ELogicResult)BitConverter::ToInt16( buffer, count );
        count += sizeof( ELogicResult );
        _userId = BitConverter::ToUInt64( buffer, count );
        count   += sizeof( unsigned long long );
        u16 _pcIdsLength = BitConverter::ToUInt16( buffer, count );
        _pcIds.resize( _pcIdsLength );
        for ( int i = 0; i < _pcIdsLength; i += 1 )
        {
            auto& _each_ = _pcIds.emplace_back();
            BufferReader::TryRead( buffer, count, _each_ );
            count += sizeof( _each_ );
        }
    }

}

;
#pragma pack( pop )
