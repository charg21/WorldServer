export module CombatPacket.S_Hit;

import Mala.Core.CoreGlobal;
import Mala.Core.Types;
import Mala.Container;
import Mala.Container.String;
import Mala.Text;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;
import Mala.Net.PacketHeader;
import EPacketType;


using namespace std;
using namespace Mala::Container;
using namespace Mala::Net;

#pragma pack( push, 1 )
export class S_Hit
{
public:
    unsigned long long _targetId; /// 대상 식별자
    long _damage; /// 피해량
    long _curHp; /// 
    unsigned int _skillDesignId; /// 스킬 디자인 식별자
    unsigned long long _casterId; /// 캐스터 식별자

public:
    SendBufferPtr Write()
    {
        auto s = GSendBufferManager->Open( 1024 );
        bool success = BitConverter::TryWriteBytes( s, sizeof( u16 ), (u16)( EPacketType::S_Hit ) );
        i32 count{ sizeof( u32 ) };

        success &= BitConverter::TryWriteBytes( s, count, _targetId );
        count   += sizeof( unsigned long long );
        success &= BitConverter::TryWriteBytes( s, count, _damage );
        count   += sizeof( long );
        success &= BitConverter::TryWriteBytes( s, count, _curHp );
        count   += sizeof( long );
        success &= BitConverter::TryWriteBytes( s, count, _skillDesignId );
        count   += sizeof( unsigned int );
        success &= BitConverter::TryWriteBytes( s, count, _casterId );
        count   += sizeof( unsigned long long );
        BitConverter::TryWriteBytes( s, 0, u16( count ) );
        s->Close( count );
        return std::move( s );
    }


    void Read( BYTE* buffer, int len )
    {
        i32 count{ 4 };

        _targetId = BitConverter::ToUInt64( buffer, count );
        count   += sizeof( unsigned long long );
        _damage = BitConverter::ToInt64( buffer, count );
        count   += sizeof( long );
        _curHp = BitConverter::ToInt64( buffer, count );
        count   += sizeof( long );
        _skillDesignId = BitConverter::ToUInt32( buffer, count );
        count   += sizeof( unsigned int );
        _casterId = BitConverter::ToUInt64( buffer, count );
        count   += sizeof( unsigned long long );
    }

}

;
#pragma pack( pop )
