export module ChannelPacket.S_ListChannel;

import Mala.Core.CoreGlobal;
import Mala.Core.Types;
import Mala.Container;
import Mala.Container.String;
import Mala.Text;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;
import Mala.Net.PacketHeader;
import EPacketType;

import <Vector>;

using namespace std;
using namespace Mala::Container;
using namespace Mala::Net;

#pragma pack( push, 1 )
export class S_ListChannel
{
public:
    Vector< int > _channels; /// 

public:
    SendBufferPtr Write()
    {
        auto s = GSendBufferManager->Open( 1024 );
        bool success = BitConverter::TryWriteBytes( s, sizeof( u16 ), (u16)( EPacketType::S_ListChannel ) );
        i32 count{ sizeof( u32 ) };

        success &= BitConverter::TryWriteBytes( s, count, (u16)( _channels.size() ) );
        count += sizeof( u16 );
        for ( auto item : _channels )
        {
            success &= BitConverter::TryWriteBytes( s, count, item );
            count   += sizeof( item );
        }
        BitConverter::TryWriteBytes( s, 0, u16( count ) );
        s->Close( count );
        return std::move( s );
    }


    void Read( BYTE* buffer, int len )
    {
        i32 count{ 4 };

        u16 _channelsLength = BitConverter::ToUInt16( buffer, count );
        _channels.resize( _channelsLength );
        for ( int i = 0; i < _channelsLength; i += 1 )
        {
            auto& _each_ = _channels.emplace_back();
            BufferReader::TryRead( buffer, count, _each_ );
            count += sizeof( _each_ );
        }
    }

}

;
#pragma pack( pop )
