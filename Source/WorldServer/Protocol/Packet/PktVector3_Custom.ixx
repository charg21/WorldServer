export module PktVector3:Custom;

import Mala.Core.Types;
import Mala.Core.TypeTraits;
import Mala.Container.String;
import Mala.Text;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;
import Mala.Net.PacketHeader;

import PktVector3;

using namespace std;
using namespace Mala::Net;

bool PktVector3::Write( SendBufferRef s, i32& count )
{
    bool success = true;

    success &= BitConverter::TryWriteBytes( s, count, (u16)( _x * 10 ) );
    count += sizeof( u16 );
    success &= BitConverter::TryWriteBytes( s, count, (u16)( _y * 10 ) );
    count += sizeof( u16 );
    success &= BitConverter::TryWriteBytes( s, count, (u16)( _z * 10 ) );
    count += sizeof( u16 );
    return success;
}

void PktVector3::Read( BYTE* buffer, i32& count )
{
    _x = BitConverter::ToUInt16( buffer, count ) / 10.f;
    count += sizeof( u16 );
    _y = BitConverter::ToUInt16( buffer, count ) / 10.f;
    count += sizeof( u16 );
    _z = BitConverter::ToUInt16( buffer, count ) / 10.f;
    count += sizeof( u16 );
}
