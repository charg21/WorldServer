export module PktVector3;

import Mala.Core.Types;
import Mala.Core.TypeTraits;
import Mala.Container.String;
import Mala.Text;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;
import Mala.Net.PacketHeader;


using namespace std;
using namespace Mala::Net;

#pragma pack( push, 1 )

export class PktVector3
{
public:
    float _x; ///
    float _y; ///
    float _z; ///

public:
    bool Write( SendBufferRef s, i32& count );

    void Read( BYTE* buffer, i32& count );

}
;
#pragma pack( pop )
