export module CombatPacket.S_SkillList;

import Mala.Core.CoreGlobal;
import Mala.Core.Types;
import Mala.Container;
import Mala.Container.String;
import Mala.Text;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;
import Mala.Net.PacketHeader;
import EPacketType;

import <Vector>;

using namespace std;
using namespace Mala::Container;
using namespace Mala::Net;

#pragma pack( push, 1 )
export class S_SkillList
{
public:
    Vector< unsigned int > _skillDesignIdList; /// 스킬 디자인 식별자 목록

public:
    SendBufferPtr Write()
    {
        auto s = GSendBufferManager->Open( 1024 );
        bool success = BitConverter::TryWriteBytes( s, sizeof( u16 ), (u16)( EPacketType::S_SkillList ) );
        i32 count{ sizeof( u32 ) };

        success &= BitConverter::TryWriteBytes( s, count, (u16)( _skillDesignIdList.size() ) );
        count += sizeof( u16 );
        for ( auto item : _skillDesignIdList )
        {
            success &= BitConverter::TryWriteBytes( s, count, item );
            count   += sizeof( item );
        }
        BitConverter::TryWriteBytes( s, 0, u16( count ) );
        s->Close( count );
        return std::move( s );
    }


    void Read( BYTE* buffer, int len )
    {
        i32 count{ 4 };

        u16 _skillDesignIdListLength = BitConverter::ToUInt16( buffer, count );
        _skillDesignIdList.resize( _skillDesignIdListLength );
        for ( int i = 0; i < _skillDesignIdListLength; i += 1 )
        {
            auto& _each_ = _skillDesignIdList.emplace_back();
            BufferReader::TryRead( buffer, count, _each_ );
            count += sizeof( _each_ );
        }
    }

}

;
#pragma pack( pop )
