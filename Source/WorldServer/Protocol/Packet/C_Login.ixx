export module AuthPacket.C_Login;

import Mala.Core.CoreGlobal;
import Mala.Core.Types;
import Mala.Container;
import Mala.Container.String;
import Mala.Text;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;
import Mala.Net.PacketHeader;
import EPacketType;

import <string>;

using namespace std;
using namespace Mala::Container;
using namespace Mala::Net;

#pragma pack( push, 1 )
export class C_Login
{
public:
    String _account; /// 계정
    String _password; /// 비밀번호

public:
    SendBufferPtr Write()
    {
        auto s = GSendBufferManager->Open( 1024 );
        bool success = BitConverter::TryWriteBytes( s, sizeof( u16 ), (u16)( EPacketType::C_Login ) );
        i32 count{ sizeof( u32 ) };

        u16 _accountLen = (u16)BitConverter::TryWriteBytes( s, count, _account );
        success &= BitConverter::TryWriteBytes( s, count, _accountLen );
        count += sizeof( u16 );
        count += _accountLen;
        u16 _passwordLen = (u16)BitConverter::TryWriteBytes( s, count, _password );
        success &= BitConverter::TryWriteBytes( s, count, _passwordLen );
        count += sizeof( u16 );
        count += _passwordLen;
        BitConverter::TryWriteBytes( s, 0, u16( count ) );
        s->Close( count );
        return std::move( s );
    }


    void Read( BYTE* buffer, int len )
    {
        i32 count{ 4 };

        u16 _accountByteLen = BitConverter::ToUInt16( buffer, count );
        count += sizeof( u16 ); 
        _account = Encoding::Unicode::GetString( buffer, count, _accountByteLen ); 
        count += _accountByteLen; 
        u16 _passwordByteLen = BitConverter::ToUInt16( buffer, count );
        count += sizeof( u16 ); 
        _password = Encoding::Unicode::GetString( buffer, count, _passwordByteLen ); 
        count += _passwordByteLen; 
    }

}

;
#pragma pack( pop )
