export module PktPc;

import Mala.Core.Types;
import Mala.Core.TypeTraits;
import Mala.Container.String;
import Mala.Text;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;
import Mala.Net.PacketHeader;

import <string>;
import PktVector3;

using namespace std;
using namespace Mala::Net;

#pragma pack( push, 1 )

export class PktPc
{
public:
    unsigned long long _objectId; /// 오브젝트 식별자
    PktVector3 _pos; /// 좌표
    float _yaw; ///
    String _name; /// 이름

public:
    bool Write( SendBufferRef s, int& count )
    {
        bool success = true;
        success &= BitConverter::TryWriteBytes( s, count, _objectId );
        count += sizeof( unsigned long long );
        success &= _pos.Write( s, count );
        success &= BitConverter::TryWriteBytes( s, count, _yaw );
        count += sizeof( float );
        u16 _nameLen = (u16)BitConverter::TryWriteBytes( s, count, _name );
        success &= BitConverter::TryWriteBytes( s, count, _nameLen );
        count += sizeof( u16 );
        count += _nameLen;
        return success;
    }

    void Read( BYTE* buffer, int& count )
    {
        _objectId = BitConverter::ToUInt64( buffer, count );
        count += sizeof( unsigned long long );
        _pos.Read( buffer, count );
        _yaw = BitConverter::ToSingle( buffer, count );
        count += sizeof( float );
        u16 _nameByteLen = BitConverter::ToUInt16( buffer, count );
        count += sizeof( u16 );
        _name = Encoding::Unicode::GetString( buffer, count, _nameByteLen );
        count += _nameByteLen;
    }

}
;
#pragma pack( pop )
