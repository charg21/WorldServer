export module AuthPacket.C_LoadPc;

import Mala.Core.CoreGlobal;
import Mala.Core.Types;
import Mala.Container;
import Mala.Container.String;
import Mala.Text;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;
import Mala.Net.PacketHeader;
import EPacketType;

import <string>;

using namespace std;
using namespace Mala::Container;
using namespace Mala::Net;

#pragma pack( push, 1 )
export class C_LoadPc
{
public:
    unsigned long long _pcId; /// 
    String _token; /// 

public:
    SendBufferPtr Write()
    {
        auto s = GSendBufferManager->Open( 1024 );
        bool success = BitConverter::TryWriteBytes( s, sizeof( u16 ), (u16)( EPacketType::C_LoadPc ) );
        i32 count{ sizeof( u32 ) };

        success &= BitConverter::TryWriteBytes( s, count, _pcId );
        count   += sizeof( unsigned long long );
        u16 _tokenLen = (u16)BitConverter::TryWriteBytes( s, count, _token );
        success &= BitConverter::TryWriteBytes( s, count, _tokenLen );
        count += sizeof( u16 );
        count += _tokenLen;
        BitConverter::TryWriteBytes( s, 0, u16( count ) );
        s->Close( count );
        return std::move( s );
    }


    void Read( BYTE* buffer, int len )
    {
        i32 count{ 4 };

        _pcId = BitConverter::ToUInt64( buffer, count );
        count   += sizeof( unsigned long long );
        u16 _tokenByteLen = BitConverter::ToUInt16( buffer, count );
        count += sizeof( u16 ); 
        _token = Encoding::Unicode::GetString( buffer, count, _tokenByteLen ); 
        count += _tokenByteLen; 
    }

}

;
#pragma pack( pop )
