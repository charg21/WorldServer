export module ItemPacket.S_ListItem;

import Mala.Core.CoreGlobal;
import Mala.Core.Types;
import Mala.Container;
import Mala.Container.String;
import Mala.Text;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;
import Mala.Net.PacketHeader;
import EPacketType;

import <Vector>;
import PktItem;

using namespace std;
using namespace Mala::Container;
using namespace Mala::Net;

#pragma pack( push, 1 )
export class S_ListItem
{
public:
    Vector< PktItem > _items; /// 

public:
    SendBufferPtr Write()
    {
        auto s = GSendBufferManager->Open( 1024 );
        bool success = BitConverter::TryWriteBytes( s, sizeof( u16 ), (u16)( EPacketType::S_ListItem ) );
        i32 count{ sizeof( u32 ) };

        success &= BitConverter::TryWriteBytes( s, count, (u16)( _items.size() ) );
        count += sizeof( u16 );
        for ( auto& item : _items )
        {
            success &= item.Write( s, count );
        }
        BitConverter::TryWriteBytes( s, 0, u16( count ) );
        s->Close( count );
        return std::move( s );
    }


    void Read( BYTE* buffer, int len )
    {
        i32 count{ 4 };

        u16 _itemsLength = BitConverter::ToUInt16( buffer, count );
        _items.resize( _itemsLength );
        for ( int i = 0; i < _itemsLength; i += 1 )
        {
            auto& _each_ = _items.emplace_back();
            _each_.Read( buffer, count );
        }
    }

}

;
#pragma pack( pop )
