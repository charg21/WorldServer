export module PktStat;

import Mala.Core.Types;
import Mala.Core.TypeTraits;
import Mala.Container.String;
import Mala.Text;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;
import Mala.Net.PacketHeader;

import EStat;

using namespace std;
using namespace Mala::Net;

#pragma pack( push, 1 )

export class PktStat
{
public:
    EStat _type; ///
    int _value; ///

public:
    bool Write( SendBufferRef s, int& count )
    {
        if constexpr ( IsPod< PktStat >::value )
        {
            std::memcpy( s->Buffer() + count, this, sizeof( PktStat ) );
            count += sizeof( PktStat );
            return true;
        }
        else
        {
            bool success = true;
            success &= BitConverter::TryWriteBytes( s, count, ( std::underlying_type_t< decltype( _type ) > )_type );
            count   += sizeof( EStat );
            success &= BitConverter::TryWriteBytes( s, count, _value );
            count   += sizeof( int );
            return success;
        }
    }

    void Read( BYTE* buffer, int& count )
    {
        if constexpr ( IsPod< PktStat >::value )
        {
            std::memcpy( this, buffer + count, sizeof( PktStat ) );
            count += sizeof( PktStat );
            return;
        }
        else
        {
            _type = (EStat)BitConverter::ToInt16( buffer, count );
            count += sizeof( EStat );
            _value = BitConverter::ToInt32( buffer, count );
            count   += sizeof( int );
        }
    }

}
;
#pragma pack( pop )
