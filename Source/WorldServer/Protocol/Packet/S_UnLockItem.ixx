export module ItemPacket.S_UnLockItem;

import Mala.Core.CoreGlobal;
import Mala.Core.Types;
import Mala.Container;
import Mala.Container.String;
import Mala.Text;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;
import Mala.Net.PacketHeader;
import EPacketType;

import ELogicResult;

using namespace std;
using namespace Mala::Container;
using namespace Mala::Net;

#pragma pack( push, 1 )
export class S_UnLockItem
{
public:
    ELogicResult _result; /// 
    unsigned long long _itemId; /// 

public:
    SendBufferPtr Write()
    {
        auto s = GSendBufferManager->Open( 1024 );
        bool success = BitConverter::TryWriteBytes( s, sizeof( u16 ), (u16)( EPacketType::S_UnLockItem ) );
        i32 count{ sizeof( u32 ) };

        success &= BitConverter::TryWriteBytes( s, count, ( std::underlying_type_t< decltype( _result ) > )_result );
        count   += sizeof( ELogicResult );
        success &= BitConverter::TryWriteBytes( s, count, _itemId );
        count   += sizeof( unsigned long long );
        BitConverter::TryWriteBytes( s, 0, u16( count ) );
        s->Close( count );
        return std::move( s );
    }


    void Read( BYTE* buffer, int len )
    {
        i32 count{ 4 };

        _result = (ELogicResult)BitConverter::ToInt16( buffer, count );
        count += sizeof( ELogicResult );
        _itemId = BitConverter::ToUInt64( buffer, count );
        count   += sizeof( unsigned long long );
    }

}

;
#pragma pack( pop )
