export module WorldPacket.S_Move;

import Mala.Core.CoreGlobal;
import Mala.Core.Types;
import Mala.Container;
import Mala.Container.String;
import Mala.Text;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;
import Mala.Net.PacketHeader;
import EPacketType;

import PktVector3;

using namespace std;
using namespace Mala::Container;
using namespace Mala::Net;

#pragma pack( push, 1 )
export class S_Move
{
public:
    unsigned long long _objectId; /// 
    PktVector3 _pos; /// 
    PktVector3 _toPos; /// 
    float _yaw; /// 

public:
    SendBufferPtr Write()
    {
        auto s = GSendBufferManager->Open( 1024 );
        bool success = BitConverter::TryWriteBytes( s, sizeof( u16 ), (u16)( EPacketType::S_Move ) );
        i32 count{ sizeof( u32 ) };

        success &= BitConverter::TryWriteBytes( s, count, _objectId );
        count   += sizeof( unsigned long long );
        success &= _pos.Write( s, count );
        success &= _toPos.Write( s, count );
        success &= BitConverter::TryWriteBytes( s, count, _yaw );
        count   += sizeof( float );
        BitConverter::TryWriteBytes( s, 0, u16( count ) );
        s->Close( count );
        return std::move( s );
    }


    void Read( BYTE* buffer, int len )
    {
        i32 count{ 4 };

        _objectId = BitConverter::ToUInt64( buffer, count );
        count   += sizeof( unsigned long long );
        _pos.Read( buffer, count );
        _toPos.Read( buffer, count );
        _yaw = BitConverter::ToSingle( buffer, count );
        count   += sizeof( float );
    }

}

;
#pragma pack( pop )
