export module PktVector2;

import Mala.Core.Types;
import Mala.Core.TypeTraits;
import Mala.Container.String;
import Mala.Text;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;
import Mala.Net.PacketHeader;


using namespace std;
using namespace Mala::Net;

#pragma pack( push, 1 )

export class PktVector2
{
public:
	float _x; ///
	float _y; ///

public:
	bool Write( SendBufferRef s, i32& count )
	{
		if constexpr ( IsPod< PktVector2 >::value )
		{
			std::memcpy( s->Buffer() + count, this, sizeof( PktVector2 ) );
			count += sizeof( PktVector2 );
			return true;
		}
		bool success = true;
		success &= BitConverter::TryWriteBytes( s, count, _x );
		count   += sizeof( float );
		success &= BitConverter::TryWriteBytes( s, count, _y );
		count   += sizeof( float );
		return success;
	}

	void Read( BYTE* buffer, i32& count )
	{
		if constexpr ( IsPod< PktVector2 >::value )
		{
			std::memcpy( this, buffer + count, sizeof( PktVector2 ) );
			count += sizeof( PktVector2 );
			return;
		}
		_x = BitConverter::ToSingle( buffer, count );
		count   += sizeof( float );
		_y = BitConverter::ToSingle( buffer, count );
		count   += sizeof( float );
	}

}
;
#pragma pack( pop )
