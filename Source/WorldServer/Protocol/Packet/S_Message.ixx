export module CheatPacket.S_Message;

import Mala.Core.CoreGlobal;
import Mala.Core.Types;
import Mala.Container;
import Mala.Container.String;
import Mala.Text;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;
import Mala.Net.PacketHeader;
import EPacketType;

import <string>;

using namespace std;
using namespace Mala::Container;
using namespace Mala::Net;

#pragma pack( push, 1 )
export class S_Message
{
public:
    String _message; /// 

public:
    SendBufferPtr Write()
    {
        auto s = GSendBufferManager->Open( 1024 );
        bool success = BitConverter::TryWriteBytes( s, sizeof( u16 ), (u16)( EPacketType::S_Message ) );
        i32 count{ sizeof( u32 ) };

        u16 _messageLen = (u16)BitConverter::TryWriteBytes( s, count, _message );
        success &= BitConverter::TryWriteBytes( s, count, _messageLen );
        count += sizeof( u16 );
        count += _messageLen;
        BitConverter::TryWriteBytes( s, 0, u16( count ) );
        s->Close( count );
        return std::move( s );
    }


    void Read( BYTE* buffer, int len )
    {
        i32 count{ 4 };

        u16 _messageByteLen = BitConverter::ToUInt16( buffer, count );
        count += sizeof( u16 ); 
        _message = Encoding::Unicode::GetString( buffer, count, _messageByteLen ); 
        count += _messageByteLen; 
    }

}

;
#pragma pack( pop )
