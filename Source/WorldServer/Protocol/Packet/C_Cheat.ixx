export module CheatPacket.C_Cheat;

import Mala.Core.CoreGlobal;
import Mala.Core.Types;
import Mala.Container;
import Mala.Container.String;
import Mala.Text;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;
import Mala.Net.PacketHeader;
import EPacketType;

import <string>;

using namespace std;
using namespace Mala::Container;
using namespace Mala::Net;

#pragma pack( push, 1 )
export class C_Cheat
{
public:
    String _command; /// 

public:
    SendBufferPtr Write()
    {
        auto s = GSendBufferManager->Open( 1024 );
        bool success = BitConverter::TryWriteBytes( s, sizeof( u16 ), (u16)( EPacketType::C_Cheat ) );
        i32 count{ sizeof( u32 ) };

        u16 _commandLen = (u16)BitConverter::TryWriteBytes( s, count, _command );
        success &= BitConverter::TryWriteBytes( s, count, _commandLen );
        count += sizeof( u16 );
        count += _commandLen;
        BitConverter::TryWriteBytes( s, 0, u16( count ) );
        s->Close( count );
        return std::move( s );
    }


    void Read( BYTE* buffer, int len )
    {
        i32 count{ 4 };

        u16 _commandByteLen = BitConverter::ToUInt16( buffer, count );
        count += sizeof( u16 ); 
        _command = Encoding::Unicode::GetString( buffer, count, _commandByteLen ); 
        count += _commandByteLen; 
    }

}

;
#pragma pack( pop )
