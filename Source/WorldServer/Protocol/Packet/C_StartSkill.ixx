export module CombatPacket.C_StartSkill;

import Mala.Core.CoreGlobal;
import Mala.Core.Types;
import Mala.Container;
import Mala.Container.String;
import Mala.Text;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;
import Mala.Net.PacketHeader;
import EPacketType;

import PktVector3;

using namespace std;
using namespace Mala::Container;
using namespace Mala::Net;

#pragma pack( push, 1 )
export class C_StartSkill
{
public:
    unsigned long long _targetId; /// 스킬 대상 식별자( 논 타겟인 경우 0 가능 )
    unsigned int _skillDesignId; /// 스킬 디자인 식별자
    float _yaw; /// 스킬 방향
    PktVector3 _castPos; /// 시전 위치

public:
    SendBufferPtr Write()
    {
        auto s = GSendBufferManager->Open( 1024 );
        bool success = BitConverter::TryWriteBytes( s, sizeof( u16 ), (u16)( EPacketType::C_StartSkill ) );
        i32 count{ sizeof( u32 ) };

        success &= BitConverter::TryWriteBytes( s, count, _targetId );
        count   += sizeof( unsigned long long );
        success &= BitConverter::TryWriteBytes( s, count, _skillDesignId );
        count   += sizeof( unsigned int );
        success &= BitConverter::TryWriteBytes( s, count, _yaw );
        count   += sizeof( float );
        success &= _castPos.Write( s, count );
        BitConverter::TryWriteBytes( s, 0, u16( count ) );
        s->Close( count );
        return std::move( s );
    }


    void Read( BYTE* buffer, int len )
    {
        i32 count{ 4 };

        _targetId = BitConverter::ToUInt64( buffer, count );
        count   += sizeof( unsigned long long );
        _skillDesignId = BitConverter::ToUInt32( buffer, count );
        count   += sizeof( unsigned int );
        _yaw = BitConverter::ToSingle( buffer, count );
        count   += sizeof( float );
        _castPos.Read( buffer, count );
    }

}

;
#pragma pack( pop )
