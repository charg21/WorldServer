export module WorldPacket.S_EnterWorld;

import Mala.Core.CoreGlobal;
import Mala.Core.Types;
import Mala.Container;
import Mala.Container.String;
import Mala.Text;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;
import Mala.Net.PacketHeader;
import EPacketType;

import <Vector>;
import PktPc;
import PktNpc;

using namespace std;
using namespace Mala::Container;
using namespace Mala::Net;

#pragma pack( push, 1 )
export class S_EnterWorld
{
public:
    PktPc _my; /// 
    Vector< PktPc > _pcs; /// 
    Vector< PktNpc > _npcs; /// 

public:
    SendBufferPtr Write()
    {
        auto s = GSendBufferManager->Open( 1024 );
        bool success = BitConverter::TryWriteBytes( s, sizeof( u16 ), (u16)( EPacketType::S_EnterWorld ) );
        i32 count{ sizeof( u32 ) };

        success &= _my.Write( s, count );
        success &= BitConverter::TryWriteBytes( s, count, (u16)( _pcs.size() ) );
        count += sizeof( u16 );
        for ( auto& item : _pcs )
        {
            success &= item.Write( s, count );
        }
        success &= BitConverter::TryWriteBytes( s, count, (u16)( _npcs.size() ) );
        count += sizeof( u16 );
        for ( auto& item : _npcs )
        {
            success &= item.Write( s, count );
        }
        BitConverter::TryWriteBytes( s, 0, u16( count ) );
        s->Close( count );
        return std::move( s );
    }


    void Read( BYTE* buffer, int len )
    {
        i32 count{ 4 };

        _my.Read( buffer, count );
        u16 _pcsLength = BitConverter::ToUInt16( buffer, count );
        _pcs.resize( _pcsLength );
        for ( int i = 0; i < _pcsLength; i += 1 )
        {
            auto& _each_ = _pcs.emplace_back();
            _each_.Read( buffer, count );
        }
        u16 _npcsLength = BitConverter::ToUInt16( buffer, count );
        _npcs.resize( _npcsLength );
        for ( int i = 0; i < _npcsLength; i += 1 )
        {
            auto& _each_ = _npcs.emplace_back();
            _each_.Read( buffer, count );
        }
    }

}

;
#pragma pack( pop )
