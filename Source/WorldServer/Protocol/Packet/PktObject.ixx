export module PktObject;

import Mala.Core.Types;
import Mala.Core.TypeTraits;
import Mala.Container.String;
import Mala.Text;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;
import Mala.Net.PacketHeader;

import PktVector3;
import EWorldObjectType;

using namespace std;
using namespace Mala::Net;

#pragma pack( push, 1 )

export class PktObject
{
public:
    unsigned long long _objectId; /// 오브젝트 식별자
    PktVector3 _pos; /// 좌표
    float _yaw; ///
    EWorldObjectType _objectType; ///

public:
    bool Write( SendBufferRef s, int& count )
    {
        bool success = true;
        success &= BitConverter::TryWriteBytes( s, count, _objectId );
        count += sizeof( unsigned long long );
        success &= _pos.Write( s, count );
        success &= BitConverter::TryWriteBytes( s, count, _yaw );
        count += sizeof( float );
        success &= BitConverter::TryWriteBytes( s, count, ( std::underlying_type_t< decltype( _objectType ) > )_objectType );
        count += sizeof( EWorldObjectType );
        return success;
    }

    void Read( BYTE* buffer, int& count )
    {
        _objectId = BitConverter::ToUInt64( buffer, count );
        count += sizeof( unsigned long long );
        _pos.Read( buffer, count );
        _yaw = BitConverter::ToSingle( buffer, count );
        count += sizeof( float );
        _objectType = (EWorldObjectType)BitConverter::ToInt16( buffer, count );
        count += sizeof( EWorldObjectType );
    }

}
;
#pragma pack( pop )
