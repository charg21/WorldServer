export module PacketExporter;

import Mala.Core.CoreGlobal;
import Mala.Core.Types;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;
import Mala.Net.PacketHeader;
import Mala.Math;
import EPacketType;

import <string>;

import EWorldObjectType;
import WorldObjectTypes;
import DesignTypes;

using namespace std;
using namespace Mala::Net;
using namespace Mala::Math;

export
{
class PktObject;
class PktPc;
class S_Hit;
class S_StartSkill;
class S_Spawn;
class S_Move;
}

export class PacketExporter
{
public:
    static void ExportToWorldObject( WorldObject& actor, PktObject& pktWorldObject );
    static void ExportToWorldObject( Character& character, PktObject& pktWorldObject );
    static void ExportToPc( Pc& pc, PktPc& pktPc );

#pragma region CombatPacket
    static void ExportToS_StartSkill( SkillDesignRef skillDesign, PcRef caster, S_StartSkill& startSkill );
    static void ExportToS_Hit( SkillDesignRef skillDesign, i64 curHp, PcRef caster, S_Hit& hit );
#pragma endregion

#pragma region WorldPacket
    static void ExportToS_Spawn( Character& character, S_Spawn& spawn );
    static void ExportToS_Move( WorldObjectId moverId, const Position& fromPos, f32 yaw, S_Move& move );
    static S_Move ExportToS_Move( WorldObjectId moverId, const Position& fromPos, const Position& toPos, f32 yaw );
#pragma endregion

};

export struct PacketWriter
{
static SendBufferPtr WriteS_Move( WorldObjectId moverId, const Position& fromPos, const Position& toPos, f32 yaw );
static SendBufferPtr WriteS_EnterWorld( Pc& pc );
static SendBufferPtr WriteS_LeaveWorld( Pc& pc );

};

