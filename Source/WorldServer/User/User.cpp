import User;

import Mala.Core.Types;
import Mala.Core.CoreTLS;
import Mala.Core.Variant;
import Mala.Container;
import Mala.Db.DbModel;
import Mala.Db.DbModelCache;
import Mala.Db.DbModelHelper;
import Mala.Db.DbConnection;
import Mala.Db.Join;
import Mala.Db.LeftJoin;
import Mala.Db.TxExecutor;
import Mala.Memory;
import Mala.Net.PacketHeader;
import Mala.Net.SendBuffer;
import Mala.Threading.JobExecutor;

import WorldObjectTypes;
import ClientSession;
import ELogicResult;
import ThreadLocal;
import Pc;
import PcDbModel;
import UserDbModel;
import DbThreadManager;
import IdHelper;
import WorldGlobal;
import UserCache;

import AuthPacket.S_Login;

using namespace std;
using namespace Mala::Core;
using namespace Mala::Container;
using namespace Mala::Db;
using namespace Mala::Net;
using namespace Mala::Threading;


User::User()
: TxExecutor{ *( dynamic_cast< JobExecutor* >( this ) ) }
{
}

User::~User()
{
}

void User::Send( SendBufferRef sendBuffer )
{
    if ( !_session )
        return;

    _session->Send( sendBuffer );
}

void User::Send( SendBufferPtr&& sendBuffer )
{
    if ( !_session )
        return;

    _session->Send( std::move( sendBuffer ) );
}

ELogicResult User::Login( String&& account, String&& password )
{
    ///// 스레드 풀에 작업을 던집니다.
    //GDbThreadManager->ExecuteDb(
    //[
    //    this,
    //    self     = shared_from_this(),
    //    account  = std::move( account ),
    //    password = std::move( password )
    //]( DbConnection* connection )
    //{
    //    ELogicResult result = Login( account, password, connection );
    //    if ( result == ELogicResult::Success )
    //        return false;

    //    JobExecutor::Post( [ this, self = std::move( self ), result ]()
    //    {
    //        S_Login login{ ._result = result };
    //        Send( login.Write() );
    //    } );

    //    return true;
    //} );

    return ELogicResult::Pending;
}

import <string>;
import <iostream>;
import ItemDbModel;

/// <summary>
///
/// </summary>
ELogicResult User::DoTest()
{
    if constexpr ( true )
    {
        GUserCache->Contains( GetIndexKey< User::PRIMARY >() );
        GUserCache->GetModel( { *GetId() } );
        GUserCache->GetUser( { {} } );
    }

    /*
    User와 Pc의 모든 컬럼을 바인딩
    */
    {
        LeftJoin< UserDbModel, PcDbModel, ItemDbModel > leftJoin;
        leftJoin._connection = LDbConnection;
        leftJoin.BindAllColumn();
        leftJoin.On().Equal< UserDbModel::PcId, PcDbModel::Id >()
                .On().Equal< UserDbModel::PcId, ItemDbModel::OwnerId >()
                .Where().Like< UserDbModel::Account >( L"Bot%" )
                .Limit( 10 );
        leftJoin.Select();
        auto [ userList, pcList, itemList ] = leftJoin.ToList< UserDbModel::PRIMARY, PcDbModel::PRIMARY, ItemDbModel::PRIMARY >();
        std::cout << userList.size() << std::endl;
        std::cout << pcList.size() << std::endl;
        std::cout << itemList.size() << std::endl;
    }

    {
        LeftJoin< UserDbModel, PcDbModel, ItemDbModel > leftJoin;
        leftJoin._connection = LDbConnection;
        leftJoin.On().Equal< UserDbModel::PcId, PcDbModel::Id >()
                .On().Equal< UserDbModel::PcId, ItemDbModel::OwnerId >()
                .Where().Equal< UserDbModel::PcId >( 12345 )
                .Delete( LDbConnection );
        //auto& userDbModel = leftJoin.Get< UserDbModel >();
        //auto key1 = userDbModel.GetIndexKey< UserDbModel::PRIMARY >();
        //auto key2 = userDbModel.GetIndexKey< UserDbModel::account >();
        //HashMap< std::decay_t< decltype( key ) >, int > userPtrMap;
        //HashMap< decltype( key1 ), int > userPtrMap1;
        //HashMap< decltype( key2 ), int > userPtrMap2;
        //userPtrMap1.insert( { key1, 1 } );
        //userPtrMap2.insert( { key2, 2 } );
    }

 //   do
 //   {
 //       auto& pcDbModel = leftJoin.Get< PcDbModel >();
 //       pcDbModel.GetColumn< PcDbModel::Id >();
 //       auto& itemDbModel = leftJoin.Get< ItemDbModel >();
 //       itemDbModel.GetColumns< ItemDbModel::PRIMARY >();
 //       //if ( indexMap.contains(
 //       //    {
 //       //        leftJoin.Get< UserDbModel >().GetId(),
 //       //        leftJoin.Get< UserDbModel >().GetAccount(),
 //       //        leftJoin.Get< PcDbModel >().GetId()
 //       //    } ) )

 //       n += 1;
    //} while ( leftJoin.FetchRow() );

    //GDbThreadManager->ExecuteDb(
    //    [
    //        this,
    //        account  = std::move( account ),
    //        password = std::move( password )
    //    ]( DbConnection* connection )
    //    {
    //        ELogicResult result = Register( account, password, connection );
    //        if ( result == ELogicResult::Success )
    //            return false;

    //        return true;
    //    } );

    return ELogicResult::Pending;
}

/// <summary>
///
/// </summary>
ELogicResult User::Login(
    const String&       account,
    const String&       password,
          DbConnection* connection )
{
    // User테이블과 Pc 테이블을 조인
    Join< UserDbModel, PcDbModel > join;

    // User와 Pc의 모든 컬럼을 바인딩
    join.Get< UserDbModel >().BindAllColumn();
    join.Get< PcDbModel >().BindAllColumn();

    // SELECT *
    // FROM user as t1
    // LEFT JOIN pc as t2
    // ON t1.pc_id = t2.id
    // WHERE t1.account = account
    // AND t1.password = passwrd
    // LIMIT 1;
    join.On().Equal< UserDbModel::PcId, PcDbModel::Id >()
        .Where().Equal< UserDbModel::Account >( account )
        .And().Equal< UserDbModel::Password >( password )
        .Limit( 1 );

    if ( !join.Select( connection ) )
        return ELogicResult::DbFail;

    if ( !join.FetchRow() )
        return ELogicResult::NullUser;

    //if ( !join.IsNull< Pc >() )
    //    return ELogicResult::NullPc;

    // 로직 성공시 Sync + Send()
    join.Get< UserDbModel >().CopyTo( *this );

    return ELogicResult::Success;
}

/// <summary>
///
/// </summary>
ELogicResult User::Register(
    const String&       account,
    const String&       password,
          DbConnection* connection )
{
    if ( !connection->BeginTransaction() )
        return ELogicResult::DbFail;

    SetKeyId     ( { IdHelper::Next() } );
    SetKeyAccount( account );
    SetPassword  ( password );
    if ( !Insert( connection ) )
    {
        connection->Rollback();

        return ELogicResult::DbFail;
    }

    if ( !connection->Commit() )
        return ELogicResult::DbFail;

    return ELogicResult::Success;
}

void User::PostTx( TxContextPtr&& tx )
{
    TxExecutor::PostTx( std::move( tx ), []( bool ) {} );
}

/// <summary>
/// 동기화 한다
/// </summary>
inline void User::Sync( EQueryType queryType )
{
    switch ( queryType )
    {
        // Most Case
    [[ likely ]] case EQueryType::Update:
        //GetId();
        break;

    [[ unlikely ]] case EQueryType::Insert:
    {
    }

        break;
    default:
        break;
    }
}

JobExecutorPtr User::SharedFromJobExecutor()
{
    return shared_from_this();
}

JobExecutorWeakPtr User::WeakFromJobExecutor()
{
    return shared_from_this();
}

TxExecutorPtr User::SharedFromTxExecutor()
{
    return shared_from_this();
}

TxExecutorWeakPtr User::WeakFromTxExecutor()
{
    return shared_from_this();
}

UserPtr User::GetUser( StringRef account )
{
    return GUserCache->GetUser( account );
}
