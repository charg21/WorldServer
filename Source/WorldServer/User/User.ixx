export module User;

import <Macro.h>;
import <iostream>;

import Mala.Core.Types;
import Mala.Db.DbConnection;
import Mala.Db.TxExecutor;
import Mala.Db.TxContext;
import Mala.Db.DbTypes;
import Mala.Net.DisconnectReason;
import Mala.Threading.Job;
import Mala.Threading.JobExecutor;

import WorldObjectTypes;
import Pc;
import PcDbModel;
import UserDbModel;
import DbThreadManager;
import ContentsDbTypes;
import CommonTypes;
import ELogicResult;

using namespace Mala;
using namespace Mala::Core;
using namespace Mala::Db;
using namespace Mala::Threading;
using namespace Mala::Net;

EXPORT_BEGIN

USING_SHARED_PTR( User );

enum class ELoginState
{
    Init,
    Processing,
    Success,
    Fail
};

/// <summary>
/// 유저 객체
/// </summary>
class User final
    : public JobExecutor
    , public TxExecutor
    , public UserDbModel
    , public std::enable_shared_from_this< User >
{
public:
    using BaseDbModel = UserDbModel;
    using UserDbModel::PRIMARY;

public:
    class ClientSession* _session{};
    u64 _sessionId;

public:
    User();
    ~User() final;

    /// <summary>
    /// 전송한다
    /// </summary>
    void Send( SendBufferRef sendBuffer );
    void Send( SendBufferPtr&& sendBuffer );
    template< typename S_Packet >
    void Send( ELogicResult result );

    /// <summary>
    /// 등록한다
    /// </summary>
    ELogicResult DoTest();

    /// <summary>
    /// 로그인 한다
    /// </summary>
    ELogicResult Login( String&& account, String&& password );
    ELogicResult Login(
        const String&       account,
        const String&       password,
              DbConnection* connection );

    /// <summary>
    /// 가입한다
    /// </summary>
    ELogicResult Register( StringRef account, StringRef password, DbConnection* connection );

    /// <summary>
    /// 트랜잭션을 실행한다
    /// </summary>
    void PostTx( TxContextPtr&& tx );

    /// <summary>
    /// 동기화 한다
    /// </summary>
    void Sync( EQueryType queryType ) final;

    static UserPtr GetUser( StringRef account );

    JobExecutorPtr SharedFromJobExecutor() final;
    JobExecutorWeakPtr WeakFromJobExecutor() final;
    TxExecutorPtr SharedFromTxExecutor() final;
    TxExecutorWeakPtr WeakFromTxExecutor() final;

    ELoginState _loginState{ ELoginState::Init };
};

EXPORT_END

/// <summary>
/// 송신한다
/// </summary>
template< typename S_Packet >
inline void User::Send( ELogicResult result )
{
    Send( S_Packet{ ._result = result }.Write() );
}
