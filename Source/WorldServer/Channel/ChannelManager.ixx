export module ChannelManager;

import <Macro.h>;

import <functional>;
import Mala.Container.StaticVector;

import Channel;


EXPORT_BEGIN

/// <summary>
/// 월드의 공간 분할을 위한 객체
/// </summary>
class ChannelManager
{
    using Channels = Mala::Container::StaticVector< Channel*, 8 >;
    using ChannelJob = std::function< void( Channel* ) >;

public:
    Channels _channels;

    void Initialize()
    {
        for ( int channelId = 0; channelId < 1/*Consts.Channel.DefaultCount*/; channelId += 1 )
        {
            auto channel = CreateChannel();
            channel->_world = nullptr;// new World( channel );
        }
    }

    Channel* CreateChannel()
    {
        auto channel = new Channel();
        channel->_channelId = 1;
        _channels.push_back( channel );

        return channel;
    }

    Channel* GetChannel( int channelId )
    {
        for ( int n = 0; n < 8; n += 1 )
        {
            auto channel = _channels[ n ];
            if ( channel != nullptr && channel->_channelId == channelId )
                return channel;
        }

        return nullptr;
    }

    void ForEach( const ChannelJob& channelJob )
    {
        for ( int n = 0; n < 8; n += 1 )
        {
            auto channel = _channels[ n ];
            if ( channel == nullptr )
                return;

            channelJob( channel );
        }
    }

};


EXPORT_END