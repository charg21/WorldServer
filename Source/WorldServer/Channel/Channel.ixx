export module Channel;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Math;

import WorldObject;
import WorldObjectTypes;
import Consts;
import WorldTypes;


EXPORT_BEGIN

/// <summary>
/// 월드의 공간 분할을 위한 객체
/// </summary>
struct Channel
{
public:
    /// <summary>
    /// 채널 식별자
    /// </summary>
    i32 _channelId{};

    /// <summary>
    /// 월드
    /// </summary>
    WorldPtr _world;
};


EXPORT_END
