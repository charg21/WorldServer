export module ClassDesign;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;

import DesignTypes;
import IDesign;
import ClassDesignTemplate;

using namespace Mala;
using namespace Mala::Core;
using namespace Mala::Container;

EXPORT_BEGIN

/// <summary>
/// Class 기획 데이터 클래스
/// </summary>
class ClassDesign final : public ClassDesignTemplate
{
	GENERATE_CLASS_TYPE_INFO( ClassDesign );

public:
	/// <summary>
	/// 데이터를 초기화한다.
	/// </summary>
	bool Initialize() override final{ return true; }

public:
	/// <summary>
	/// 생성자
	/// </summary>
	ClassDesign() = default;
	ClassDesign( ClassDesignId id, const String& name, bool gender, const String& race )
	: ClassDesignTemplate{ id, name, gender, race }{}
};

EXPORT_END
