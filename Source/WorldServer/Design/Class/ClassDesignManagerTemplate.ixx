export module ClassDesignManagerTemplate;

import <Macro.h>;
import "document.h";
import "rapidjson.h";

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;
import Mala.Io.File;
import Mala.Text;

import DesignTypes;
import IDesignManager;
import IDesign;
import ClassDesign;
import JsonHelper;

using namespace Mala::Container;
using namespace Mala::Text::Utf8;

EXPORT_BEGIN

class ClassDesignManagerTemplate : public IDesignManager
{
	GENERATE_CLASS_TYPE_INFO( ClassDesignManagerTemplate );

public:
	/// <summary>
	/// 디자인 포인터 맵 타입 정의
	/// </summary>
	using ClassDesignMap = HashMap< ClassDesignId, const ClassDesign* >;

	/// <summary>
	/// 디자인 데이터 목록 타입 정의
	/// </summary>
	using ClassDesignList = Vector< ClassDesign >;

public:
	/// <summary>
	/// 데이터를 로드한다
	/// </summary>
	bool Load( StringRef path ) override final
	{
		auto doc = JsonHelper::LoadAndParse( path );
		if ( doc.HasParseError() )
			return false;

		_path = path;
		rapidjson::Value& v = doc[ "classList" ];
		_classDesignList.reserve( doc.Size() );
		for ( decltype( v.Size() ) i{}; i < v.Size(); i += 1 )
		{
			auto& _class = v[ i ];
			auto& classDesign = _classDesignList.emplace_back
			(
				ClassDesignId{ _class[ "Id" ].GetUint() },
				ToUtf16( _class[ "Name" ].GetString() ),
				_class[ "Gender" ].GetBool(),
				ToUtf16( _class[ "Race" ].GetString() )
			);
			_classDesignMap.emplace( classDesign._id, &classDesign );
		}

		_classDesignMap.reserve( _classDesignList.size() );
		for ( decltype( v.Size() ) i{}; i < v.Size(); i += 1 )
			_classDesignMap.emplace( _classDesignList[ i ]._id, &_classDesignList[ i ] );

		return true;
	}

	/// <summary>
	/// 초기화 한다
	/// </summary>
	bool Initialize() override final
	{
		for ( auto& design : _classDesignList )
		{
			if ( !design.Initialize() )
			    return false;

			design._manager = this;
		}

		for ( auto& design : _classDesignList )
		{
			_classDesignMap.emplace( design._id, &design );
		}

		return true;
	}

	/// <summary>
	/// 기획 데이터를 획득한다
	/// </summary>
	const ClassDesign* GetClassDesign( ClassDesignId id ) const
	{
		auto iter = _classDesignMap.find( id );
		if ( iter != _classDesignMap.end() )
		   return iter->second;

		return nullptr;
	}

	/// <summary>
	/// 기획 시트명을 반환한다
	/// </summary>
	const StringView& GetSheetName() const override final { return _sheetName; }

	/// <summary>
	/// 경로 반환한다
	/// </summary>
	const String& GetPath() const { return _path; }


public:
	/// <summary>
	/// 맵 컨테이너
	/// </summary>
	ClassDesignMap _classDesignMap;

	/// <summary>
	/// 리스트 컨테이너
	/// </summary>
	ClassDesignList _classDesignList;

	/// <summary>
	/// 시트 명
	/// </summary>
	Field( _sheetName )
	StringView _sheetName{ L"Class" };

	/// <summary>
	/// 경로
	/// </summary>
	Field( _path )
	String _path;

};

EXPORT_END
