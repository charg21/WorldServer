export module ClassDesignTemplate;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;

import DesignTypes;
import IDesign;

using namespace Mala::Container;

EXPORT_BEGIN

class ClassDesignManagerTemplate;

class ClassDesignTemplate : public IDesign
{
	GENERATE_CLASS_TYPE_INFO( ClassDesignTemplate );

public:
	/// <summary>
	/// 생성자
	/// </summary>
	ClassDesignTemplate() = default;
	ClassDesignTemplate( ClassDesignId id, const String& name, bool gender, const String& race )
	: _id{ id },
	_name{ name },
	_gender{ gender },
	_race{ race }
	{}
public:
	/// <summary>
	/// 식별자
	/// </summary>
	Field( _id )
	ClassDesignId _id{};

	/// <summary>
	/// 이름
	/// </summary>
	Field( _name )
	String _name{};

	/// <summary>
	/// 성별
	/// </summary>
	Field( _gender )
	bool _gender{};

	/// <summary>
	/// 종족
	/// </summary>
	Field( _race )
	String _race{};

	/// <summary>
	/// 매니저
	/// </summary>
	Field( _manager )
	const ClassDesignManagerTemplate* _manager{ nullptr };

};

EXPORT_END
