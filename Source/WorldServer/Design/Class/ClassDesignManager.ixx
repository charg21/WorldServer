export module ClassDesignManager;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;

import DesignTypes;
import IDesignManager;
import IDesign;

import ClassDesignManagerTemplate;

EXPORT_BEGIN

class ClassDesignManager final : public ClassDesignManagerTemplate
{
	GENERATE_CLASS_TYPE_INFO( ClassDesignManager );

};

extern inline ClassDesignManager* GClassDesignManager{};

EXPORT_END
