export module ClassDesignPtr;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;

import DesignTypes;
import ClassDesignManager;


EXPORT_BEGIN

class ClassDesign;

class ClassDesignPtr
{

public:
    /// <summary>
    /// ClassDesignPtr 생성자
    /// </summary>
    ClassDesignPtr( ClassDesignId id )
    : _ptr{ GClassDesignManager->GetClassDesign( id ) }
    {
    }

    const ClassDesign* operator->() const { return _ptr; }

    operator bool() const { return _ptr; }


    const ClassDesign* _ptr{};

};

EXPORT_END
