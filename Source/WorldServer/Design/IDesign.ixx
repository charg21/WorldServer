export module IDesign;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Reflection;


EXPORT_BEGIN


/// <summary>
/// 디자인 인터페이스
/// </summary>
class IDesign
{
public:
	GENERATE_CLASS_TYPE_INFO( IDesign );

public:
	//virtual bool Load() { return true; }
	virtual bool Initialize() { return true; }
};

EXPORT_END
