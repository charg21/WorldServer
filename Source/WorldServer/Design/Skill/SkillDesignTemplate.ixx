export module SkillDesignTemplate;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Container.String;
import Mala.Reflection;

import DesignTypes;
import IDesign;

EXPORT_BEGIN

class SkillDesignManagerTemplate;

class SkillDesignTemplate : public IDesign
{
	GENERATE_CLASS_TYPE_INFO( SkillDesignTemplate );

public:
	/// <summary>
	/// 생성자
	/// </summary>
	SkillDesignTemplate() = default;
	SkillDesignTemplate( SkillDesignId id, const String& name, f32 radius, int damage )
	: _id{ id },
	_name{ name },
	_radius{ radius },
	_damage{ damage }
	{}
public:
	/// <summary>
	/// 식별자
	/// </summary>
	Field( _id )
	SkillDesignId _id{};

	/// <summary>
	/// 이름
	/// </summary>
	Field( _name )
	String _name{};

	/// <summary>
	/// 범위
	/// </summary>
	Field( _radius )
	f32 _radius{};

	/// <summary>
	/// 데미지
	/// </summary>
	Field( _damage )
	int _damage{};

	/// <summary>
	/// 매니저
	/// </summary>
	Field( _manager )
	const SkillDesignManagerTemplate* _manager{ nullptr };

};

EXPORT_END
