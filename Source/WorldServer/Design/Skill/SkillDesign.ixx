export module SkillDesign;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;
import Mala.Math.Vector3;

import DesignTypes;
import IDesign;
import SkillDesignTemplate;

using namespace Mala;
using namespace Mala::Core;
using namespace Mala::Container;
using namespace Mala::Math;

EXPORT_BEGIN


/// <summary>
/// 스킬 타겟팅 타입
/// </summary>
enum class ESkillTargetType
{
	Target,
	Area,
};

enum class EAreaType
{
	Box,
	Cylinder,
};

/// <summary>
/// 타겟팅 데이터
/// </summary>
struct TargetingSequence
{
	i64       _time;
	EAreaType _areaType;
};

using TargetingSequenceList = Vector< TargetingSequence >;

/// <summary>
/// Skill 기획 데이터 클래스
/// </summary>
struct SkillDesign final : SkillDesignTemplate
{
public:
    GENERATE_CLASS_TYPE_INFO( SkillDesign );

public:
    /// <summary>
    /// 생성자
    /// </summary>
    SkillDesign() = default;
    SkillDesign( SkillDesignId id, const String& name, f32 radius, int damage )
    : SkillDesignTemplate{ id, name, radius, damage }
    {
    }

//  Field( _targetingType )
//  ESkillTargetType _targetingType;

//  Field( _targetingList )
//  TargetingSequenceList _targetingList;
};


// /// <summary>
// /// 생성자
// /// </summary>
// SkillDesignPtr() = default;
// SkillDesignPtr( const SkillDesignPtr& rhs )
// : _ptr{ rhs._ptr }
// {
// }
// SkillDesignPtr( const SkillDesign* rhs )
// : _ptr{ rhs }
// {
// }

EXPORT_END
