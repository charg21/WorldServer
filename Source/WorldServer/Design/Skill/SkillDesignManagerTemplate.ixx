module;

export module SkillDesignManagerTemplate;

import <Macro.h>;
import "document.h";
import "rapidjson.h";

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;
import Mala.Io.File;
import Mala.Text;

import DesignTypes;
import IDesignManager;
import IDesign;
import SkillDesign;
import JsonHelper;

using namespace Mala::Container;
using namespace Mala::Text::Utf8;

EXPORT_BEGIN

class SkillDesignManagerTemplate : public IDesignManager
{
	GENERATE_CLASS_TYPE_INFO( SkillDesignManagerTemplate );

public:
	/// <summary>
	/// 디자인 포인터 맵 타입 정의
	/// </summary>
	using SkillDesignMap = HashMap< SkillDesignId, const SkillDesign* >;

	/// <summary>
	/// 디자인 데이터 목록 타입 정의
	/// </summary>
	using SkillDesignList = Vector< SkillDesign >;

public:
	/// <summary>
	/// 데이터를 로드한다
	/// </summary>
	bool Load( StringRef path ) override final
	{
		auto doc = JsonHelper::LoadAndParse( path );
		if ( doc.HasParseError() )
			return false;

		_path = path;
		rapidjson::Value& v = doc[ "skillList" ];
		_skillDesignList.reserve( doc.Size() );
		for ( decltype( v.Size() ) i{}; i < v.Size(); i += 1 )
		{
			auto& _skill = v[ i ];
			_skillDesignList.emplace_back
			(
				SkillDesignId{ _skill[ "Id" ].GetUint() },
				ToUtf16( _skill[ "Name" ].GetString() ),
				_skill[ "Radius" ].GetFloat(),
				_skill[ "Damage" ].GetInt()
			);
		}

		_skillDesignMap.reserve( _skillDesignList.size() );
		for ( auto& design : _skillDesignList )
			_skillDesignMap.emplace( design._id, &design );

		return true;
	}

	/// <summary>
	/// 초기화 한다
	/// </summary>
	bool Initialize() override final
	{
		for ( auto& design : _skillDesignList )
		{
			if ( !design.Initialize() )
			    return false;

			design._manager = this;
		}

		for ( auto& design : _skillDesignList )
		{
			_skillDesignMap.emplace( design._id, &design );
		}

		return true;
	}

	/// <summary>
	/// 기획 데이터를 획득한다
	/// </summary>
	const SkillDesign* GetSkillDesign( SkillDesignId id ) const
	{
		auto iter = _skillDesignMap.find( id );
		if ( iter != _skillDesignMap.end() )
		   return iter->second;

		return nullptr;
	}

	/// <summary>
	/// 기획 시트명을 반환한다
	/// </summary>
	const StringView& GetSheetName() const override final { return _sheetName; }

	/// <summary>
	/// 경로 반환한다
	/// </summary>
	const String& GetPath() const { return _path; }


public:
	/// <summary>
	/// 맵 컨테이너
	/// </summary>
	SkillDesignMap _skillDesignMap;

	/// <summary>
	/// 리스트 컨테이너
	/// </summary>
	SkillDesignList _skillDesignList;

	/// <summary>
	/// 시트 명
	/// </summary>
	Field( _sheetName )
	StringView _sheetName{ L"Skill" };

	/// <summary>
	/// 경로
	/// </summary>
	Field( _path )
	String _path;

};

EXPORT_END
