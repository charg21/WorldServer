export module SkillDesignPtr;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;

import DesignTypes;
import SkillDesignManager;


EXPORT_BEGIN

class SkillDesign;

class SkillDesignPtr
{
public:
	/// <summary>
	/// SkillDesignPtr 생성자
	/// </summary>
	SkillDesignPtr( SkillDesignId id )
    : _ptr{ GSkillDesignManager->GetSkillDesign( id ) }
    {
    }

	const SkillDesign* operator->() const { return _ptr; }

	operator bool() const { return _ptr; }


public:
	const SkillDesign* _ptr{};

};

EXPORT_END
