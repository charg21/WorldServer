export module SkillDesignManager;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;

import DesignTypes;
import IDesignManager;
import IDesign;

import SkillDesignManagerTemplate;

EXPORT_BEGIN

class SkillDesignManager final : public SkillDesignManagerTemplate
{
	GENERATE_CLASS_TYPE_INFO( SkillDesignManager );

};

extern inline SkillDesignManager* GSkillDesignManager{};

EXPORT_END
