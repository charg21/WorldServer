export module QuestStepDesignManagerTemplate;

import <Macro.h>;
import "document.h";
import "rapidjson.h";

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;
import Mala.Io.File;
import Mala.Text;

import DesignTypes;
import IDesignManager;
import IDesign;
import QuestStepDesign;
import JsonHelper;


using namespace Mala::Container;
using namespace Mala::Text::Utf8;

EXPORT_BEGIN

class QuestStepDesignManagerTemplate : public IDesignManager
{
	GENERATE_CLASS_TYPE_INFO( QuestStepDesignManagerTemplate );

public:
	/// <summary>
	/// 디자인 포인터 맵 타입 정의
	/// </summary>
	using QuestStepDesignMap = HashMap< QuestStepDesignId, const QuestStepDesign* >;

	/// <summary>
	/// 디자인 데이터 목록 타입 정의
	/// </summary>
	using QuestStepDesignList = Vector< QuestStepDesign >;

	/// <summary>
	/// 디자인 데이터 그룹 타입 정의
	/// </summary>
	using QuestStepDesignGroup = Vector< const QuestStepDesign* >;

	/// <summary>
	/// 디자인 데이터 그룹 맵 타입 정의
	/// </summary>
	using QuestStepDesignGroupMap = HashMap< QuestDesignId, QuestStepDesignGroup >;

public:
	/// <summary>
	/// 데이터를 로드한다
	/// </summary>
	bool Load( StringRef path ) override final
	{
		auto doc = JsonHelper::LoadAndParse( path );
		if ( doc.HasParseError() )
			return false;

		_path = path;
		rapidjson::Value& v = doc[ "questTaskList" ];
		_questTaskDesignList.reserve( doc.Size() );
		for ( decltype( v.Size() ) i{}; i < v.Size(); i += 1 )
		{
			auto& _questTask = v[ i ];
			auto& questTaskDesign = _questTaskDesignList.emplace_back
			(
				QuestStepDesignId{ _questTask[ "Id" ].GetUint() },
				ToUtf16( _questTask[ "Name" ].GetString() ),
				ToUtf16( _questTask[ "Type" ].GetString() ),
				QuestDesignId{ _questTask[ "QuestId" ].GetUint() },
				_questTask[ "Index" ].GetInt()
			);
		}

		_questTaskDesignMap.reserve( _questTaskDesignList.size() );
		for ( auto& design : _questTaskDesignList )
			_questTaskDesignMap.emplace( design._id, &design );

		return true;
	}

	/// <summary>
	/// 초기화 한다
	/// </summary>
	bool Initialize() override final
	{
		for ( auto& design : _questTaskDesignList )
		{
			if ( !design.Initialize() )
			    return false;

			design._manager = this;
		}

		for ( auto& design : _questTaskDesignList )
		{
			_questTaskDesignMap.emplace( design._id, &design );
		}

		for ( auto& design : _questTaskDesignList )
		{
			_questTaskDesignGroupMap[ design._questId ].emplace_back( &design );
		}

		return true;
	}

	/// <summary>
	/// 기획 데이터를 획득한다
	/// </summary>
	const QuestStepDesign* GetQuestStepDesign( QuestStepDesignId id ) const
	{
		auto iter = _questTaskDesignMap.find( id );
		if ( iter != _questTaskDesignMap.end() )
		   return iter->second;

		return nullptr;
	}

	/// <summary>
	/// 기획 데이터를 획득한다
	/// </summary>
	const QuestStepDesignGroup& GetQuestStepDesignGroup( QuestDesignId id ) const
	{
		auto iter = _questTaskDesignGroupMap.find( id );
		if ( iter != _questTaskDesignGroupMap.end() )
		   return iter->second;

		static const QuestStepDesignGroup s_questTaskDesignGroup;
		return s_questTaskDesignGroup;
	}

	/// <summary>
	/// 기획 시트명을 반환한다
	/// </summary>
	const StringView& GetSheetName() const override final { return _sheetName; }

	/// <summary>
	/// 경로 반환한다
	/// </summary>
	const String& GetPath() const { return _path; }


public:
	/// <summary>
	/// 맵 컨테이너
	/// </summary>
	QuestStepDesignMap _questTaskDesignMap;

	/// <summary>
	/// 리스트 컨테이너
	/// </summary>
	QuestStepDesignList _questTaskDesignList;

	/// <summary>
	/// 그룹 맵 컨테이너
	/// </summary>
	QuestStepDesignGroupMap _questTaskDesignGroupMap;

	/// <summary>
	/// 시트 명
	/// </summary>
	Field( _sheetName )
	StringView _sheetName{ L"QuestStep" };

	/// <summary>
	/// 경로
	/// </summary>
	Field( _path )
	String _path;

};

EXPORT_END
