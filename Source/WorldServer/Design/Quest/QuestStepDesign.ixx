export module QuestStepDesign;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;

import DesignTypes;
import IDesign;
import QuestStepDesignTemplate;

EXPORT_BEGIN

/// <summary>
/// QuestStep 기획 데이터 클래스
/// </summary>
class QuestStepDesign final : public QuestStepDesignTemplate
{
	GENERATE_CLASS_TYPE_INFO( QuestStepDesign );

public:
	/// <summary>
	/// 데이터를 초기화한다.
	/// </summary>
	bool Initialize() override final{ return true; }

public:
	/// <summary>
	/// 생성자
	/// </summary>
	QuestStepDesign() = default;
	QuestStepDesign( QuestStepDesignId id, const String& name, const String& type, QuestDesignId questId, int index )
	: QuestStepDesignTemplate{ id, name, type, questId, index }{}

};

EXPORT_END
