export module QuestStepDesignPtr;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;

import DesignTypes;
import QuestStepDesignManager;


EXPORT_BEGIN

class QuestStepDesign;

class QuestStepDesignPtr
{

public:
    /// <summary>
    /// QuestStepDesignPtr 생성자
    /// </summary>
    QuestStepDesignPtr( QuestStepDesignId id )
    : _ptr{ GQuestStepDesignManager->GetQuestStepDesign( id ) }
    {
    }

    const QuestStepDesign* operator->() const { return _ptr; }

    operator bool() const { return _ptr; }


    const QuestStepDesign* _ptr{};

};

EXPORT_END
