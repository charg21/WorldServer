﻿export module QuestDesignPtr;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;

import DesignTypes;
import QuestDesignManager;


EXPORT_BEGIN

class QuestDesign;

class QuestDesignPtr
{

	/// <summary>
	/// QuestDesignPtr 생성자
	/// </summary>
	QuestDesignPtr( QuestDesignId id )
	: _ptr{ GQuestDesignManager->GetQuestDesign( id ) }
	{}

	const QuestDesign* operator->() const { return _ptr; }

	operator bool() const { return _ptr; }

	const QuestDesign* _ptr{};

};

EXPORT_END
