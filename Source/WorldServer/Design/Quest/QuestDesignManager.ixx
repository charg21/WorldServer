export module QuestDesignManager;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;

import DesignTypes;
import IDesignManager;
import IDesign;

import QuestDesignManagerTemplate;

EXPORT_BEGIN

class QuestDesignManager final : public QuestDesignManagerTemplate
{
    GENERATE_CLASS_TYPE_INFO( QuestDesignManager );

};

extern inline QuestDesignManager* GQuestDesignManager{};

EXPORT_END
