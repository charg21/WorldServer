export module QuestDesignTemplate;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;

import DesignTypes;
import IDesign;

using namespace Mala::Container;

EXPORT_BEGIN

class QuestDesignManagerTemplate;

class QuestDesignTemplate : public IDesign
{
	GENERATE_CLASS_TYPE_INFO( QuestDesignTemplate );

public:
	/// <summary>
	/// 생성자
	/// </summary>
	QuestDesignTemplate() = default;
	QuestDesignTemplate( QuestDesignId id, const String& name )
	: _id{ id },
	_name{ name }
	{}
public:
	/// <summary>
	/// 식별자
	/// </summary>
	Field( _id )
	QuestDesignId _id{};

	/// <summary>
	/// 이름
	/// </summary>
	Field( _name )
	String _name{};

	/// <summary>
	/// 매니저
	/// </summary>
	Field( _manager )
	const QuestDesignManagerTemplate* _manager{ nullptr };

};

EXPORT_END
