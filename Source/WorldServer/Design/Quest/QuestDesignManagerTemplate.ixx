export module QuestDesignManagerTemplate;

import <Macro.h>;
import "document.h";
import "rapidjson.h";

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;
import Mala.Io.File;
import Mala.Text;

import DesignTypes;
import IDesignManager;
import IDesign;
import QuestDesign;
import JsonHelper;

using namespace Mala::Container;
using namespace Mala::Text::Utf8;

EXPORT_BEGIN

class QuestDesignManagerTemplate : public IDesignManager
{
    GENERATE_CLASS_TYPE_INFO( QuestDesignManagerTemplate );

public:
    /// <summary>
    /// 디자인 포인터 맵 타입 정의
    /// </summary>
    using QuestDesignMap = HashMap< QuestDesignId, const QuestDesign* >;

    /// <summary>
    /// 디자인 데이터 목록 타입 정의
    /// </summary>
    using QuestDesignList = Vector< QuestDesign >;

public:
    /// <summary>
    /// 데이터를 로드한다
    /// </summary>
    bool Load( StringRef path ) override final
    {
        auto doc = JsonHelper::LoadAndParse( path );
        if ( doc.HasParseError() )
            return false;

		_path = path;
		rapidjson::Value& v = doc[ "questList" ];
		_questDesignList.reserve( doc.Size() );
		for ( decltype( v.Size() ) i{}; i < v.Size(); i += 1 )
		{
			auto& _quest = v[ i ];
			_questDesignList.emplace_back
			(
				QuestDesignId{ _quest[ "Id" ].GetUint() },
				ToUtf16( _quest[ "Name" ].GetString() )
			);
		}

		_questDesignMap.reserve( _questDesignList.size() );
		for ( decltype( v.Size() ) i{}; i < v.Size(); i += 1 )
			_questDesignMap.emplace( _questDesignList[ i ]._id, &_questDesignList[ i ] );

		return true;
	}

    /// <summary>
    /// 초기화 한다
    /// </summary>
    bool Initialize() override final
    {
        for ( auto& design : _questDesignList )
        {
            if ( !design.Initialize() )
                return false;

            design._manager = this;
        }

        for ( auto& design : _questDesignList )
        {
            _questDesignMap.emplace( design._id, &design );
        }

        return true;
    }

    /// <summary>
    /// 기획 데이터를 획득한다
    /// </summary>
    const QuestDesign* GetQuestDesign( QuestDesignId id ) const
    {
        auto iter = _questDesignMap.find( id );
        if ( iter != _questDesignMap.end() )
            return iter->second;

        return nullptr;
    }

    /// <summary>
    /// 기획 시트명을 반환한다
    /// </summary>
    const StringView& GetSheetName() const override final { return _sheetName; }

    /// <summary>
    /// 경로 반환한다
    /// </summary>
    const String& GetPath() const { return _path; }

public:
    /// <summary>
    /// 맵 컨테이너
    /// </summary>
    QuestDesignMap _questDesignMap;

    /// <summary>
    /// 리스트 컨테이너
    /// </summary>
    QuestDesignList _questDesignList;

    /// <summary>
    /// 시트 명
    /// </summary>
    Field( _sheetName )
    StringView _sheetName{ L"Quest" };

    /// <summary>
    /// 경로
    /// </summary>
    Field( _path )
    String _path;

};

EXPORT_END
