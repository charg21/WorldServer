export module QuestStepDesignGroupPtr;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;

import DesignTypes;
import QuestStepDesignManager;


EXPORT_BEGIN

class QuestStepDesignGroupPtr
{
	GENERATE_CLASS_TYPE_INFO( QuestStepDesignGroupPtr );

public:
	///// <summary>
	///// 생성자
	///// </summary>
	QuestStepDesignGroupPtr( QuestDesignId id )
	: _ptrs{ GQuestStepDesignManager->GetQuestStepDesignGroup( id ) }
	{}

	auto begin() const { return _ptrs.begin(); }
	auto end() const { return _ptrs.end(); }

	/// <summary>
	/// 기획 데이터 포인터 목록
	/// </summary>
	const QuestStepDesignManager::QuestStepDesignGroup& _ptrs;

};

EXPORT_END
