export module QuestStepDesignTemplate;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;

import DesignTypes;
import IDesign;

using namespace Mala::Container;

EXPORT_BEGIN

class QuestStepDesignManagerTemplate;

class QuestStepDesignTemplate : public IDesign
{
    GENERATE_CLASS_TYPE_INFO( QuestStepDesignTemplate );

public:
    /// <summary>
    /// 생성자
    /// </summary>
    QuestStepDesignTemplate() = default;
    QuestStepDesignTemplate( QuestStepDesignId id, const String& name, const String& type, QuestDesignId questId, int index )
    : _id{ id },
    _name{ name },
    _type{ type },
    _questId{ questId },
    _index{ index }
    {
    }
public:
    /// <summary>
    /// 식별자
    /// </summary>
    Field( _id )
    QuestStepDesignId _id{};

    /// <summary>
    /// 이름
    /// </summary>
    Field( _name )
    String _name{};

    /// <summary>
    /// 종류
    /// </summary>
    Field( _type )
    String _type{};

    /// <summary>
    /// 퀘스트 식별자
    /// </summary>
    Field( _questId )
    QuestDesignId _questId{};

    /// <summary>
    /// 순서
    /// </summary>
    Field( _index )
    int _index{};

    /// <summary>
    /// 매니저
    /// </summary>
    Field( _manager )
    const QuestStepDesignManagerTemplate* _manager{};

};

EXPORT_END
