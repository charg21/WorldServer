export module QuestDesign;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;

import DesignTypes;
import IDesign;
import QuestDesignTemplate;
import QuestStepDesign;
import QuestStepDesignGroupPtr;
import QuestStepDesignManager;

using namespace Mala::Container;

EXPORT_BEGIN

class QuestDesign final : public QuestDesignTemplate
{
	GENERATE_CLASS_TYPE_INFO( QuestDesign );

public:
	/// <summary>
	/// 데이터를 초기화한다.
	/// </summary>
	using QuestStepDesignList = Vector< const QuestStepDesign* >;

public:
	/// <summary>
	/// 데이터를 초기화한다.
	/// </summary>
	bool Initialize() override final
	{ 
		auto taskDesignGroup = QuestStepDesignGroupPtr( _id );
		for( const auto& taskDesign : taskDesignGroup )
			_taskDesignList.push_back( taskDesign );

		return true; 
	}

public:
	/// <summary>
	/// 생성자
	/// </summary>
	QuestDesign() = default;
	QuestDesign( QuestDesignId id, const String& name )
	: QuestDesignTemplate{ id, name }{}

	QuestStepDesignList _taskDesignList;
};

EXPORT_END
