export module QuestStepDesignManager;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;

import DesignTypes;
import IDesignManager;
import IDesign;

import QuestStepDesignManagerTemplate;

EXPORT_BEGIN

class QuestStepDesignManager final : public QuestStepDesignManagerTemplate
{
    GENERATE_CLASS_TYPE_INFO( QuestStepDesignManager );
};

extern inline QuestStepDesignManager* GQuestStepDesignManager{};

EXPORT_END
