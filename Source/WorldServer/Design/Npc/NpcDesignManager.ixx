export module NpcDesignManager;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;

import DesignTypes;
import NpcDesign;
import NpcDesignManagerTemplate;


EXPORT_BEGIN

class NpcDesignManager final : public NpcDesignManagerTemplate
{
    GENERATE_CLASS_TYPE_INFO( NpcDesignManager );
};

extern inline NpcDesignManager* GNpcDesignManager{};


class NpcDesignGroupPtr
{
    //NpcDesignGroupPtr( NpcDesignId id )
    //	: _ptrs{ GNpcDesignManager->GetNpcDesignGroup( id ) }
    //{}

    //const NpcDesignManager::NpcDesignGroup& _ptrs;
};

EXPORT_END
