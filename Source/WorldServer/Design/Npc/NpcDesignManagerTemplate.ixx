export module NpcDesignManagerTemplate;

import <Macro.h>;
import "document.h";
import "rapidjson.h";

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;
import Mala.Io.File;
import Mala.Text;

import DesignTypes;
import IDesignManager;
import IDesign;
import NpcDesign;
import JsonHelper;

using namespace Mala::Container;
using namespace Mala::Text::Utf8;

EXPORT_BEGIN

class NpcDesignManagerTemplate : public IDesignManager
{
    GENERATE_CLASS_TYPE_INFO( NpcDesignManagerTemplate );

public:
    /// <summary>
    /// 디자인 포인터 맵 타입 정의
    /// </summary>
    using NpcDesignMap = HashMap< NpcDesignId, const NpcDesign* >;

    /// <summary>
    /// 디자인 데이터 목록 타입 정의
    /// </summary>
    using NpcDesignList = Vector< NpcDesign >;

public:
    /// <summary>
    /// <summary>
    /// 데이터를 로드한다
    /// </summary>
    bool Load( StringRef path ) override final
    {
        auto doc = JsonHelper::LoadAndParse( path );
        if ( doc.HasParseError() )
            return false;

        _path = path;
        rapidjson::Value& v = doc[ "npcList" ];
        _npcDesignList.reserve( doc.Size() );
        for ( decltype( v.Size() ) i{}; i < v.Size(); i += 1 )
        {
            auto& _npc = v[ i ];
            _npcDesignList.emplace_back
            (
                NpcDesignId{ _npc[ "Id" ].GetUint() },
                ToUtf16( _npc[ "Name" ].GetString() ),
                _npc[ "Level" ].GetInt(),
                _npc[ "Exp" ].GetInt(),
                _npc[ "Hp" ].GetInt(),
                _npc[ "Damage" ].GetInt(),
                _npc[ "WalkSpeed" ].GetFloat(),
                _npc[ "RunSpeed" ].GetFloat(),
                _npc[ "IsPeace" ].GetBool()
            );
        }

        _npcDesignMap.reserve( _npcDesignList.size() );
        for ( auto& design : _npcDesignList )
            _npcDesignMap.emplace( design._id, &design );

        return true;
    }

    /// <summary>
    /// 초기화 한다
    /// </summary>
    bool Initialize() override final
    {
        for ( auto& design : _npcDesignList )
        {
            if ( !design.Initialize() )
                return false;

            design._manager = this;
        }

        for ( auto& design : _npcDesignList )
        {
            _npcDesignMap.emplace( design._id, &design );
        }

        return true;
    }

    /// <summary>
    /// 기획 데이터를 획득한다
    /// </summary>
    const NpcDesign* GetNpcDesign( NpcDesignId id ) const
    {
        auto iter = _npcDesignMap.find( id );
        if ( iter != _npcDesignMap.end() )
            return iter->second;

        return nullptr;
    }

    /// <summary>
    /// 기획 시트명을 반환한다
    /// </summary>
    const StringView& GetSheetName() const override final { return _sheetName; }

    /// <summary>
    /// 경로 반환한다
    /// </summary>
    const String& GetPath() const { return _path; }


public:
    /// <summary>
    /// 맵 컨테이너
    /// </summary>
    NpcDesignMap _npcDesignMap;

    /// <summary>
    /// 리스트 컨테이너
    /// </summary>
    NpcDesignList _npcDesignList;

    /// <summary>
    /// 시트 명
    /// </summary>
    Field( _sheetName )
    StringView _sheetName{ L"Npc" };

    /// <summary>
    /// 경로
    /// </summary>
    Field( _path )
    String _path;

};

//	/// <summary>
//	/// 기획 클래스 데이터 맵 타입 정의
//	/// </summary>
//	using NpcDesignGroup = Vector< const NpcDesign* >;
//
//	/// <summary>
//	/// 기획 클래스 데이터 맵 타입 정의
//	/// </summary>
//	using NpcDesignGroupMap = HashMap< NpcDesignId, NpcDesignGroup >;
//

//	/// 클래스 정보 데이터 맵
//	Field( _npcDesignGroupMap )
//	NpcDesignGroupMap _npcDesignGroupMap;

//	/// <summary>
//	/// 기획 클래스 데이터를 획득한다.
//	/// </summary>
//	const NpcDesignGroup& GetNpcDesignGroup( NpcDesignId npcDesignId ) const
//	{
//		auto iter = _npcDesignGroupMap.find( npcDesignId );
//		if ( iter != _npcDesignGroupMap.end() )
//			return iter->second;
//
//		static NpcDesignGroup s_npcDesignGroup;
//		return s_npcDesignGroup;
//	}
//};

EXPORT_END
