export module NpcDesignTemplate;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;

import DesignTypes;
import IDesign;

EXPORT_BEGIN

class NpcDesignManagerTemplate;

class NpcDesignTemplate : public IDesign
{
	GENERATE_CLASS_TYPE_INFO( NpcDesignTemplate );

public:
    NpcDesignTemplate() = default;
    NpcDesignTemplate( NpcDesignId id, String name, int level, int exp, int hp, int power, float walkSpeed, float runSpeed, bool isPeace )
    : _id{ id },
    _name{ name },
    _level{ level },
    _exp{ exp },
    _hp{ hp },
    _power{ power },
    _walkSpeed{ walkSpeed },
    _runSpeed{ runSpeed },
    _isPeace{ isPeace }
    {
    }

    NpcDesignTemplate( NpcDesignId id, StringRef name )
    : _id{ id }
    , _name{ name }
    {
    }

public:
    /// <summary>
    /// 식별자
    /// </summary>
    Field( _id )
    NpcDesignId _id{};

    /// <summary>
    /// 이름
    /// </summary>
    Field( _name )
    String _name{};

    /// <summary>
    /// 레벨
    /// </summary>
    Field( _level )
    int _level{};

    /// <summary>
    /// 경험치
    /// </summary>
    Field( _exp )
    int _exp{};

    /// <summary>
    /// 체력
    /// </summary>
    Field( _hp )
    int _hp{};

    /// <summary>
    /// 힘
    /// </summary>
    Field( _power )
    int _power{};

    /// <summary>
    /// 달리기 속도
    /// </summary>
    Field( _runSpeed )
    float _runSpeed{};

    /// <summary>
    /// 걷기 속도
    /// </summary>
    Field( _walkSpeed )
    float _walkSpeed{};

    /// <summary>
    /// 선공몹 여부
    /// </summary>
    Field( _isPeace )
    bool _isPeace{};

    /// <summary>
    /// 매니저
    /// </summary>
    Field( _manager )
    const NpcDesignManagerTemplate* _manager{ nullptr };
};

EXPORT_END
