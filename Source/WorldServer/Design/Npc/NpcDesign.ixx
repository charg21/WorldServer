export module NpcDesign;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;

import DesignTypes;
import IDesign;
import NpcDesignTemplate;

EXPORT_BEGIN

/// <summary>
/// Npc 기획 데이터 클래스
/// </summary>
class NpcDesign : public NpcDesignTemplate
{
public:
    GENERATE_CLASS_TYPE_INFO( NpcDesign );

public:
    /// <summary>
    /// 데이터를 초기화한다.
    /// </summary>
    bool Initialize() override { return true; }

public:
    NpcDesign() = default;
    NpcDesign( NpcDesignId id, const String& name, int level, int exp, int hp, int power, float walkSpeed, float runSpeed, bool isPeace )
    : NpcDesignTemplate{ id, name, level, exp, hp, power, walkSpeed, runSpeed, isPeace }{
    }
    NpcDesign( NpcDesignId id, StringRef name )
    : NpcDesignTemplate( id, name )
    {
    }
};


EXPORT_END
