export module NpcDesignPtr;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;

import DesignTypes;
import NpcDesignManager;


EXPORT_BEGIN

class NpcDesign;

class NpcDesignPtr
{
public:
    /// <summary>
    /// NpcDesignPtr 생성자
    /// </summary>
    NpcDesignPtr( NpcDesignId id )
    : _ptr{ GNpcDesignManager->GetNpcDesign( id ) }
    {
    }

    const NpcDesign* operator->() const { return _ptr; }
    operator bool() const { return _ptr; }
    operator const NpcDesign* ( ) const { return _ptr; }

    const NpcDesign* _ptr{};
};

EXPORT_END
