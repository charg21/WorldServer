module;

import <future>;

export module IDesignManager;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container.String;
import Mala.Reflection;

EXPORT_BEGIN

/// <summary>
/// 디자인 매니저 인터페이스
/// </summary>
class IDesignManager
{
public:
	GENERATE_CLASS_TYPE_INFO( IDesignManager );

public:
	virtual bool Load( StringRef path ) { return true; }
	virtual std::future< bool > LoadAsync( StringRef path )
	{
		return std::async( [ this, path ]()
		{
			if ( !Load( path ) )
			{
				ERROR_LOG( L"%ws Load Failed...", GetSheetName().data() );

				return false;
			}

			WARN_LOG( L"%ws Load Ok...", GetSheetName().data() );

			return true;
		} );
	}

	virtual bool Initialize() { return true; }
	virtual const StringView& GetSheetName() const { return L""; }
};


EXPORT_END
