export module DesignManagerCenter;

import <Macro.h>;

import <iostream>;
import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;

import DesignTypes;
import IDesignManager;

using namespace Mala;
using namespace Mala::Core;
using namespace Mala::Container;


EXPORT_BEGIN

/// <summary>
/// 기획 데이터 센터
/// </summary>
struct DesignManagerCenter
{
	GENERATE_CLASS_TYPE_INFO( DesignManagerCenter );

	/// <summary>
	/// 기획 데이터 매니저 포인터 목록 정의
	/// </summary>
	using DesignManagerList = Vector< IDesignManager* >;

public:
	/// <summary>
	/// 기획 데이터 매니저 목록
	/// </summary>
	Field( _designManagers )
	DesignManagerList _designManagers;

public:
	/// <summary>
	/// 준비한다.
	/// </summary>
	void Prepare();

	/// <summary>
	/// 데이터를 로드한다.
	/// </summary>
	bool Load( StringRef path );
	bool LoadAsync( StringRef path );

	/// <summary>
	/// 초기화한다.
	/// </summary>
	bool Initialize();
};

extern inline DesignManagerCenter* GDesignManagerCenter{};

EXPORT_END
