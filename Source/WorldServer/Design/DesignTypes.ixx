export module DesignTypes;

import <Macro.h>;
export import <memory>;
export import Mala.Core.StrongType;

export import CommonTypes;

using namespace Mala::Core;

EXPORT_BEGIN

USING_REF( BuffDesign  );
USING_REF( ClassDesign );
USING_REF( ItemDesign  );
USING_REF( NpcDesign   );
USING_REF( QuestDesign );
USING_REF( SkillDesign );

EXPORT_END
