export module ItemDesignManager;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;

import DesignTypes;
import IDesignManager;
import IDesign;

import ItemDesignManagerTemplate;

EXPORT_BEGIN

class ItemDesignManager final : public ItemDesignManagerTemplate
{
    GENERATE_CLASS_TYPE_INFO( ItemDesignManager );

    METHOD( EnsureInstance )
    static void EnsureInstance()
    {
        //if ( !GItemDesignManager )
        //	GItemDesignManager = new ItemDesignManager();
    }
};

extern inline ItemDesignManager* GItemDesignManager{};


//class ItemDesignGroupPtr
//{
//	GENERATE_CLASS_TYPE_INFO( ItemDesignGroupPtr );
//
//	ItemDesignGroupPtr( ItemDesignId id )
//	: _ptrs{ GItemDesignManager->GetItemDesignGroup( id ) }
//	{}
//
//	const ItemDesignManager::ItemDesignGroup& _ptrs;
//};

EXPORT_END
