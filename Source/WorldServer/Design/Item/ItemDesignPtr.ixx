export module ItemDesignPtr;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;

import DesignTypes;
import ItemDesignManager;


EXPORT_BEGIN

class ItemDesign;

class ItemDesignPtr
{

    /// <summary>
    /// ItemDesign 생성자
    /// </summary>
    ItemDesignPtr( ItemDesignId id )
    : _ptr{ GItemDesignManager->GetItemDesign( id ) }
    {
    }

    const ItemDesign* operator->() const { return _ptr; }

    operator bool() const { return _ptr; }

    const ItemDesign* _ptr{};

};

EXPORT_END
