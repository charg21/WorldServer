export module ItemDesignTemplate;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;

import DesignTypes;
import IDesign;

EXPORT_BEGIN

class ItemDesignManagerTemplate;

class ItemDesignTemplate : public IDesign
{
    GENERATE_CLASS_TYPE_INFO( ItemDesignTemplate );

public:
    ItemDesignTemplate() = default;
    ItemDesignTemplate( ItemDesignId id, const String& name, int price )
    : _id{ id },
    _name{ name },
    _price{ price }
    {
    }

public:
    /// <summary>
    /// 식별자
    /// </summary>
    Field( _id )
    ItemDesignId _id{};

    /// <summary>
    /// 이름
    /// </summary>
    Field( _name )
    String _name{};

    /// <summary>
    /// 가격
    /// </summary>
    Field( _price )
    int _price{};

    /// <summary>
    /// 매니저
    /// </summary>
    Field( _manager )
    const ItemDesignManagerTemplate* _manager{ nullptr };

};

EXPORT_END
