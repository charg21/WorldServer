export module ItemDesign;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;

import DesignTypes;
import IDesign;
import ItemDesignTemplate;

EXPORT_BEGIN

/// <summary>
/// Item 기획 데이터 클래스
/// </summary>
class ItemDesign final : public ItemDesignTemplate
{
public:
    GENERATE_CLASS_TYPE_INFO( ItemDesign );

public:
    ItemDesign() = default;
    ItemDesign( ItemDesignId id, const String& name, int price )
    : ItemDesignTemplate{ id, name, price }
    {
    }

public:
    /// <summary>
    /// 데이터를 초기화한다.
    /// </summary>
    bool Initialize() override final{ return true; }
};



EXPORT_END
