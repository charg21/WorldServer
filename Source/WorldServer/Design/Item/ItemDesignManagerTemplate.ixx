export module ItemDesignManagerTemplate;

import <Macro.h>;
import "document.h";
import "rapidjson.h";

import Mala.Core.Types;
import Mala.Container;
import Mala.Reflection;
import Mala.Io.File;
import Mala.Text;

import DesignTypes;
import IDesignManager;
import IDesign;
import ItemDesign;
import JsonHelper;

using namespace Mala::Container;
using namespace Mala::Text::Utf8;

EXPORT_BEGIN

class ItemDesignManagerTemplate : public IDesignManager
{
    GENERATE_CLASS_TYPE_INFO( ItemDesignManagerTemplate );

public:
    /// <summary>
    /// 디자인 포인터 맵 타입 정의
    /// </summary>
    using ItemDesignMap = HashMap< ItemDesignId, const ItemDesign* >;

    /// <summary>
    /// 디자인 데이터 목록 타입 정의
    /// </summary>
    using ItemDesignList = Vector< ItemDesign >;

    /*/// <summary>
    /// 기획 클래스 데이터 맵 타입 정의
    /// </summary>
    using ItemDesignGroup = Vector< const ItemDesign* >;*/

    /*/// <summary>
    /// 기획 클래스 데이터 맵 타입 정의
    /// </summary>
    using ItemDesignGroupMap = HashMap< ItemDesignId, ItemDesignGroup >;*/

public:
    /// <summary>
    /// 데이터를 로드한다
    /// </summary>
    bool Load( StringRef path ) override final
    {
        auto doc = JsonHelper::LoadAndParse( path );
        if ( doc.HasParseError() )
            return false;

        _path = path;
        rapidjson::Value& v = doc[ "itemList" ];
        _itemDesignList.reserve( doc.Size() );
        for ( decltype( v.Size() ) i{}; i < v.Size(); i += 1 )
        {
            auto& _item = v[ i ];
            auto& lastItem = _itemDesignList.emplace_back
            (
                ItemDesignId{ _item[ "Id" ].GetUint() },
                ToUtf16( _item[ "Name" ].GetString() ),
                _item[ "Price" ].GetInt()
            );
        }

        _itemDesignMap.reserve( _itemDesignList.size() );
        for ( auto& design : _itemDesignList )
            _itemDesignMap.emplace( design._id, &design );

        return true;
    }

    /// <summary>
    /// 초기화 한다
    /// </summary>
    bool Initialize() override final
    {
        for ( auto& design : _itemDesignList )
        {
            if ( !design.Initialize() )
                return false;

            design._manager = this;
        }

        for ( auto& design : _itemDesignList )
        {
            _itemDesignMap.emplace( design._id, &design );
        }

        return true;
    }

    /// <summary>
    /// 기획 데이터를 획득한다
    /// </summary>
    const ItemDesign* GetItemDesign( ItemDesignId id ) const
    {
        auto iter = _itemDesignMap.find( id );
        if ( iter != _itemDesignMap.end() )
            return iter->second;

        return nullptr;
    }

    /*/// <summary>
    /// 기획 클래스 데이터를 획득한다.
    /// <summary>
    /// 기획 시트명을 반환한다
    /// </summary>
    const ItemDesignGroup& GetItemDesignGroup( ItemDesignId itemDesignId ) const
    {
        auto iter = _itemDesignGroupMap.find( itemDesignId );
        if ( iter != _itemDesignGroupMap.end() )
            return iter->second;

        static ItemDesignGroup s_itemDesignGroup;
        return s_itemDesignGroup;
    }*/

    /// <summary>
    /// 기획 시트명을 반환한다
    /// </summary>
    const StringView& GetSheetName() const override final { return _sheetName; }

    /// <summary>
    /// 경로 반환한다
    /// </summary>
    const String& GetPath() const { return _path; }


public:
    /// <summary>
    /// 맵 컨테이너
    /// </summary>
    ItemDesignMap _itemDesignMap;

    /// <summary>
    /// 리스트 컨테이너
    /// </summary>
    ItemDesignList _itemDesignList;

    /// <summary>
    /// 시트 명
    /// </summary>
    Field( _sheetName )
    StringView _sheetName{ L"Item" };

    /// <summary>
    /// 경로
    /// </summary>
    Field( _path )
    String _path;

    /*/// 아이템 정보 데이터 맵
    Field( _itemDesignGroupMap )
    ItemDesignGroupMap _itemDesignGroupMap;*/
};

EXPORT_END
