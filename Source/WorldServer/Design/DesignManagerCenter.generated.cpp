/// <summary>
/// 이 부분은 툴에 의해 자동 생성될 부분이다...( 나중에 분리 예정 )
/// </summary>

import Mala.Core.Types;
import Mala.Log;

import DesignManagerCenter;
import DesignTypes;

import ClassDesignManager;
import ItemDesignManager;
import NpcDesignManager;
import QuestDesignManager;
import QuestStepDesignManager;
import SkillDesignManager;

/// <summary>
/// 데이터를 로드한다.
/// </summary>
void DesignManagerCenter::Prepare()
{
    /// 자동화 예정
    GSkillDesignManager     = new SkillDesignManager();
    GClassDesignManager     = new ClassDesignManager();
    GNpcDesignManager       = new NpcDesignManager();
    GItemDesignManager      = new ItemDesignManager();
    GQuestDesignManager     = new QuestDesignManager();
    GQuestStepDesignManager = new QuestStepDesignManager();

    /// 자동화 예정
    _designManagers.emplace_back( GClassDesignManager );
    _designManagers.emplace_back( GItemDesignManager );
    _designManagers.emplace_back( GNpcDesignManager );
    _designManagers.emplace_back( GSkillDesignManager );

    /// 임시로 순서 조정
    _designManagers.emplace_back( GQuestStepDesignManager );
    _designManagers.emplace_back( GQuestDesignManager );
}
