//#include <future>;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Log;
import Mala.Reflection;

import DesignManagerCenter;
import DesignTypes;


/// <summary>
/// 데이터를 로드한다.
/// </summary>
bool DesignManagerCenter::Load( StringRef path )
{
    Prepare();

    for ( auto* designManager : _designManagers )
    {
        auto& typeInfo = designManager->GetTypeInfo();
        auto designPath = path + L"Design/" + String( designManager->GetSheetName() );
        designPath += L".json";

        if ( !designManager->Load( designPath ) )
        {
            ERROR_LOG( L"%ws Load Failed... Path[%ws]",
                typeInfo.GetNameW(),
                designPath.c_str() );
        }
        else
        {
            WARN_LOG( L"%ws Load Ok...", typeInfo.GetNameW() );
        }
    }

    return true;
}

import Mala.Container;

/// <summary>
/// 데이터를 로드한다.
/// std::async로 구현되어 있음, 라이브러리 스레드풀 내부에 스레드를 만들기 떄문에 유의
/// </summary>
bool DesignManagerCenter::LoadAsync( StringRef path )
{
    Prepare();

    Mala::Container::Vector< std::future< bool > > futures;
    futures.reserve( _designManagers.size() );

    for ( auto* designManager : _designManagers )
    {
        auto& typeInfo = designManager->GetTypeInfo();
        auto designPath = path + L"Design/" + String( designManager->GetSheetName() );
        designPath += L".json";

        futures.emplace_back( designManager->LoadAsync( designPath ) );
    }

    for ( auto& f : futures )
    {
        f.wait();
    }

    return true;
}

/// <summary>
/// 데이터를 로드한다.
/// </summary>
bool DesignManagerCenter::Initialize()
{
    for ( auto* designManager : _designManagers )
    {
        auto& typeInfo = designManager->GetTypeInfo();
        auto designPath = String{ L"Design/" } + String( designManager->GetSheetName() );
        designPath += L".json";
        if ( !designManager->Initialize() )
        {
            ERROR_LOG( L"%ws Initialize Failed... Path[%ws]",
                typeInfo.GetNameW(),
                designPath.c_str() );
        }
        else
        {
            WARN_LOG( L"%ws Initialize Ok...", typeInfo.GetNameW() );
        }
    }

    return true;
}
