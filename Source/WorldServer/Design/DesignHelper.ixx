export module DesignHelper;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Reflection;


EXPORT_BEGIN


enum class EOpCode
{
    /// Relation Operator
    Equal,
    NotEqual,
    Less,
    LessEqual,
    Greater,
    GreaterEqual,

    /// Logical Operator
    Not,
    And,
    Or,

    ///
    In,
    NotIn,
};

/// <summary>
///
/// </summary>
class IOpCode
{
public:
    GENERATE_CLASS_TYPE_INFO( IOpCode );

public:
    virtual bool Init() = 0;
    virtual bool Check( i64 value ) = 0;
};

/// <summary>
///
/// </summary>
class OpCodeEqual : public IOpCode
{
public:
    GENERATE_CLASS_TYPE_INFO( OpCodeEqual );

public:
    bool Init() final{}
    bool Check( i64 value ) final { return value == _value; }

private:
    i64 _value;
};

class OpCodeNot : public IOpCode
{
public:
    GENERATE_CLASS_TYPE_INFO( OpCodeNot );

public:
    bool Init() final{}
    bool Check( i64 value ) final { return value != _value; }

private:
    i64 _value;
};


EXPORT_END
