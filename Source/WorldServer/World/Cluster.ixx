export module Cluster;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Net.PacketHeader;
import Mala.Math;
import Mala.Threading.Lock;

import WorldObject;
import WorldObjectTypes;
import Consts;
import Rect;
import Grid;
import EWorldObjectType;
import WorldTypes;

using namespace Mala::Container;
using namespace Mala::Math;
using namespace Mala::Threading;

EXPORT_BEGIN

/// <summary>
/// 월드의 공간 분할을 위한 객체
/// </summary>
struct Cluster
{
public:
    /// <summary>
    /// 내부 타입 정의
    /// </summary>
    using ClusterIndex = Vector2Int;

    /// <summary>
    /// 초기화 한다
    /// </summary>
    bool Initialize( const ClusterIndex& index );

    /// <summary>
    /// 포함 여부를 반환한다
    /// </summary>
    bool Contains( WorldObjectId objectId );

    /// <summary>
    /// 액터를 반환한다
    /// </summary>
    WorldObjectPtr GetWorldObject( WorldObjectId objectId );

    /// <summary>
    /// 입장한다
    /// </summary>
    void Enter( WorldObjectRef object );

    /// <summary>
    /// 퇴장한다
    /// </summary>
    void Leave( WorldObjectId objectId );

    /// <summary> 액터 맵 </summary>
    WorldObjectMap _objects;

    /// <summary> Npc맵 </summary>
    NpcMap _npcs;

    /// <summary> Pc맵 </summary>
    PcMap _pcs;

    /// <summary> 인덱스 </summary>
    ClusterIndex _index;

    /// <summary> 클러스터 영역 </summary>
    Rect _rect;

    /// <summary> 이웃 클러스터 </summary>
    //Cluster* _neighbors[ 8 ];
};


EXPORT_END
