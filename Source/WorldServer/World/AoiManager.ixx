export module AoiManager;

import <Macro.h>;

import Mala.Core;
import Mala.Core.EnumHelper;
import Mala.Core.Format;
import Mala.Log.ConsoleLog;
import Mala.Container;
import Mala.Net.PacketDispatcher;
import Mala.Math;
import Mala.Threading;

import WorldObject;
import WorldObjectTypes;
import Consts;
import Character;
import Cluster;
import Npc;
import Pc;
import Rect;
import World;
import WorldHelper;
import WorldTypes;

import EWorldObjectType;

using namespace Mala;
using namespace Mala::Container;
using namespace Mala::Math;
using namespace Mala::Threading;


EXPORT_BEGIN

/// <summary>
/// 클러스터 종류
/// </summary>

enum class EClusterType
{
    Pc  = EnumFlag::ToValue< WorldObjectTypeList, Pc >(),
    Npc = EnumFlag::ToValue< WorldObjectTypeList, Npc >(),

	All = Pc | Npc,

	Max = LengthV< WorldObjectTypeList >(),
};

/// <summary>
/// AOI 클러스터
/// </summary>
struct AoiCluster
{
    /// <summary>
    /// 생성자
    /// </summary>
    AoiCluster() = default;

public:
    /// <summary>
    /// 액터 맵
    /// </summary>
    WorldObjectMap _objects[ (int)EClusterType::Max ];
    WorldObjectMap& _pcs{ _objects[ (int)EClusterType::Pc -1 ] };
    WorldObjectMap& _npcs{ _objects[ (int)EClusterType::Npc -1 ] };

    /// <summary>
    /// 영역 사각형
    /// </summary>
    Box _rect;
};

using AoiClusterList = StaticVector< AoiCluster*, 9 >;

/// <summary>
/// 시야 관리자
/// </summary>
class AoiManager
{
    friend class ViewComponent;

public:
    void Init();

    AoiCluster& GetAoiCluster( Index index );
    AoiCluster& GetAoiCluster( Position pos );

    void OnEnter( WorldObject* character );
    void OnEnter( Index to, WorldObject* character );
    void OnLeave( Index from, WorldObject* character );
    void OnMove( const Position& fromPos, const Position& toPos, WorldObjectRef character );
    AoiClusterList& GetAoiClusterList( Position pos );
    WorldObjectMaps2& GatherWorldObject( Position pos, EWorldObjectType actorType = EWorldObjectType::Max );
    WorldPtr GetWorldPtr();

private:
    /// <summary>
    /// AOI 클러스터 목록
    /// </summary>
    AoiCluster _aoiClusters[ Consts::Aoi::Capacity ];

    /// <summary>
    /// 소유 월드
    /// </summary>
    WorldWeakPtr _ownerWorldPtr{};
};

EXPORT_END
