import <Macro.h>;

import <array>;
import <memory>;
import Mala.Core.Types;
import Mala.Log.ConsoleLog;
import Mala.Container;
import Mala.Net.PacketDispatcher;
import Mala.Math;
import Mala.Math.Vector3;
import Mala.Threading.Lock;

import World;
import WorldHelper;
import ClusterManager;
import Channel;
import ThreadLocal;
import WorldObject;
import Npc;
import Pc;
import WorldObjectTypes;
import Character;
import Consts;
import Config;
import NearClusterView;
import NearClusterViewGrid;
import WorldTypes;


using namespace Mala::Container;
using namespace Mala::Math;
using namespace Mala::Threading;

// EXPORT_BEGIN

/// <summary>
/// 월드를 초기화 한다
/// </summary>
World::World( Channel* channel )
: _channel{ channel }
{
}

/// <summary>
/// 월드를 초기화 한다
/// </summary>
bool World::Initialize()
{
    i32 baseId   = 30000;
    i32 npcCount = Config::NpcCount;// / Global::ConcurrentThreadCount;
    i32 i = 0;

    for ( i32 z{}; z < 200; z += 1 )
    {
        for ( i32 x{}; x < 200; x += 1 )
        {
            Position position{ x * 4.0f * Multiple, 0.f, z * 4.0f * Multiple };
            u64 npcId = i + baseId;
            i += 1;

            Spawn< Npc >( position, WorldObjectId{ npcId } );
        }
    }

    INFO_LOG( L"Npc Init Ok... " );

    return true;
}

void World::SpawnWorldObject( WorldObjectRef objectRef )
{
    objectRef->Post( [ objectRef ]{ objectRef->EnterWorld(); } );
}

void World::SpawnWorldObject( WorldObjectPtr&& objectRef )
{
    objectRef->Post( [ objectRef ]{ objectRef->EnterWorld(); } );
}

bool World::DespawnWorldObject( WorldObjectRef object )
{
    return true;
}

// EXPORT_END
