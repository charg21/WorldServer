import <Macro.h>;
import <iostream>;

import Mala.Core.Types;
import Mala.Core.EnumHelper;
import Mala.Core.Format;
import Mala.Log.ConsoleLog;
import Mala.Container;
import Mala.Container.WaitFreeQueue;
import Mala.Net.PacketDispatcher;
import Mala.Math;
import Mala.Threading;

import WorldObject;
import WorldObjectTypes;
import AoiManager;
import Consts;
import Character;
import Npc;
import Pc;
import Cluster;
import ClusterGrid;
import Config;
import World;
import WorldHelper;
import WorldTypes;
import ThreadLocal;

import EWorldObjectType;

using namespace Mala::Core;
using namespace Mala::Container;
using namespace Mala::Math;
using namespace Mala::Threading;


void AoiManager::Init()
{
    for ( auto y = 0; y < Consts::Aoi::MaxRow; y += 1 )
    {
        for ( auto x = 0; x < Consts::Aoi::MaxCol; x += 1 )
        {
            auto clusterIndex = Index{ x, y };
            if ( !WorldHelper::ValidateAoiIndex( clusterIndex ) )
                continue;

            AoiCluster& cluster = GetAoiCluster( clusterIndex );
            cluster._rect = Box{
                (float)( clusterIndex._x * Consts::Aoi::Width ),
                (float)( clusterIndex._x + 1 ) * Consts::Aoi::Width,
                (float)( clusterIndex._y * Consts::Aoi::Height ),
                (float)( clusterIndex._y + 1 ) * Consts::Aoi::Height };
        }
    }
}

inline AoiCluster& AoiManager::GetAoiCluster( Index index )
{
    return _aoiClusters[ ( index._y * ( Consts::Aoi::MaxCol ) ) + index._x ];
}

inline AoiCluster& AoiManager::GetAoiCluster( Position pos )
{
    auto clusterIndex = WorldHelper::GetAoiIndex( pos );
    return GetAoiCluster( clusterIndex );
}

void AoiManager::OnEnter( WorldObject* character )
{
    auto clusterIndex = WorldHelper::GetAoiIndex( character->GetPos() );
    OnEnter( clusterIndex, character );
}

void AoiManager::OnEnter( Index to, WorldObject* character )
{
    auto& toCluster = GetAoiCluster( to );

    switch ( character->GetWorldObjectType() )
    {
    case EWorldObjectType::Pc:
        ASSERT_CRASH( toCluster._pcs.emplace( character->GetObjectId(), (Pc*)character ).second );
        return;

    case EWorldObjectType::Npc:
        ASSERT_CRASH( toCluster._npcs.emplace( character->GetObjectId(), (Npc*)character ).second ) ;
        return;
    }
}

void AoiManager::OnLeave( Index from, WorldObject* character )
{
    auto& fromCluster{ GetAoiCluster( from ) };
    switch ( character->GetWorldObjectType() )
    {
    case EWorldObjectType::Pc:
        ASSERT_CRASH( fromCluster._pcs.erase( character->GetObjectId() ) );
        return;
    case EWorldObjectType::Npc:
        ASSERT_CRASH( fromCluster._npcs.erase( character->GetObjectId() ) );
        return;
    }
}

void AoiManager::OnMove( const Position& fromPos, const Position& toPos, WorldObjectRef character )
{
    const auto fromIndex{ WorldHelper::GetAoiIndex( fromPos ) };
    const auto toIndex{ WorldHelper::GetAoiIndex( toPos ) };
    if ( fromIndex == toIndex )
        return;

    OnLeave( fromIndex, character.get() );
    OnEnter( toIndex, character.get() );
}

AoiClusterList& AoiManager::GetAoiClusterList( Position pos )
{
    static thread_local AoiClusterList clusterList;
    clusterList.clear();

    auto clusterIndex = WorldHelper::GetAoiIndex( pos );
    for ( const auto& policy : WorldHelper::ClusterAccessPolicy )
    {
        auto nearClusterIndex = clusterIndex + policy;
        if ( !WorldHelper::ValidateAoiIndex( nearClusterIndex ) )
            continue;

        clusterList.push_back( &GetAoiCluster( nearClusterIndex ) );
    }

    return clusterList;
}

WorldObjectMaps2& AoiManager::GatherWorldObject( Position pos, EWorldObjectType actorType )
{
    auto& gatherList = ViewComponent::GetGatherList();

    auto clusterIndex = WorldHelper::GetAoiIndex( pos );
    for ( const auto& policy : WorldHelper::ClusterAccessPolicy )
    {
        auto nearClusterIndex = clusterIndex + policy;
        if ( !WorldHelper::ValidateAoiIndex( nearClusterIndex ) )
            continue;

        auto& nearCluster = GetAoiCluster( nearClusterIndex );

        switch ( actorType )
        {
        case EWorldObjectType::Pc:
            gatherList.emplace_back( &nearCluster._pcs );
            break;

        case EWorldObjectType::Npc:
            gatherList.emplace_back( &nearCluster._npcs );
            break;

        case EWorldObjectType::Max:
            gatherList.emplace_back( &nearCluster._pcs );
            gatherList.emplace_back( &nearCluster._npcs );
            break;
        }

    }

    return gatherList;
}

inline WorldPtr AoiManager::GetWorldPtr()
{
    return _ownerWorldPtr.lock();
}
