export module ClusterJob;

import <Macro.h>;

import Mala.Core;
import Mala.Core.CoreGlobal;
import Mala.Container;
import Mala.Math;
import Mala.Threading;

import WorldObject;
import WorldObjectTypes;
import WorldTypes;
import Config;

using namespace Mala::Container;
using namespace Mala::Math;
using namespace Mala::Threading;

EXPORT_BEGIN

enum class EClusterChangeReason
{
    EnterWorld,
    LeaveWorld,
    Move,
};

template< std::invocable TExecuteJob >
auto MakeGlobalLambdaJob( TExecuteJob&& job )
{
    return xnew< GlobalLambdaJob< TExecuteJob > >(
        GThreadCount,
        std::forward< TExecuteJob >( job ) );
}

template< std::invocable TExecuteJob, std::invocable TFinishJob >
auto MakeGlobalLambdaJobEx( TExecuteJob&& execJob, TFinishJob&& finishJob )
{
    return xnew< GlobalLambdaJobEx< TExecuteJob, TFinishJob > >(
        GThreadCount,
        std::forward< TExecuteJob >( execJob ),
        std::forward< TFinishJob >( finishJob ) );
}

namespace ClusterEnterJob
{
    IGlobalJob* Make( WorldObjectPtr&& actorRef, EClusterChangeReason reason );
};

namespace ClusterLeaveJob
{
    IGlobalJob* Make( WorldObjectPtr&& actorRef );
};

namespace ClusterSwitchJob
{
    IGlobalJob* Make( ClusterIndex from, ClusterIndex to, WorldObjectPtr&& actorRef );
};

EXPORT_END
