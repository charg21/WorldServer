export module NearClusterView;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Container.StaticVector;

import Mala.Math;
import Rect;
import WorldObject;
import WorldObjectTypes;
import Cluster;
import Consts;
import WorldTypes;

using namespace Mala::Container;

EXPORT_BEGIN

/// <summary>
/// 기준 좌표에서, 접하는 클러스터 목록 캐싱
/// 클러스터 접근 순서를 강제하여 데드락을 방지한다
///
/// [ 1 ] [ 2 ] [ 3 ]
/// [ 4 ] [ 5 ] [ 6 ]
/// [ 7 ] [ 8 ] [ 9 ]
///
/// 클러스터 접근은 본인 기준, 좌상단, 우상단, 좌하단 우하단 순으로 접근한다.
/// 본인이 5번에 우하단 구석에 있는 경우 클러스터 접근시 5, 6, 8, 9 순서대로 접근을 한다.
/// 순환 대기 방지( 데드락 조건 )
/// </summary>
template< typename TCluster >
class NearClusterViewBase
    : public StaticVector< Cluster*, Consts::Cluster::MaxNearCount >
{
public:
    /// <summary>
    /// 초기화 한다
    /// </summary>
    bool Initialize( Index position )
    {
        return true;
    }

    /// <summary>
    /// 최대 수용량을 반환한다
    /// </summary>
    consteval size_t Capacity()
    {
        return Consts::Cluster::MaxNearCount;
    }

    /// <summary>
    /// 비교 연산자를 정의한다
    /// </summary>
    bool operator==( NearClusterViewBase< TCluster >& other )
    {
        for ( i32 n = 0; n < Consts::Cluster::MaxNearCount; ++n )
        {
            if ( other[ n ] != ( *this )[ n ] )
            {
                return false;
            }
        }

        return true;
    }
};

class NearClusterView : public NearClusterViewBase< Cluster >
{
};


EXPORT_END
