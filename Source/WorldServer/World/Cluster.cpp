import Mala.Core.Types;
import Mala.Core.TypeCast;
import Mala.Container;
import Mala.Container.StaticVector;
import Mala.Net.PacketHeader;
import Mala.Math;

import WorldObject;
import Pc;
import Npc;
import Consts;
import Cluster;
import Rect;
import World;
import WorldTypes;


/// <summary>
/// 초기화 한다
/// </summary>
bool Cluster::Initialize( const ClusterIndex& index )
{
    _index = index;

    _rect._left   = index._x * static_cast< int >( Consts::Cluster::Width );
    _rect._bottom = index._y * static_cast< int >( Consts::Cluster::Height );

    _rect._right = _rect._left   + Consts::Cluster::Width  - 1;
    _rect._top   = _rect._bottom + Consts::Cluster::Height - 1;

    _rect._right = std::min( _rect._right, static_cast< int >( Consts::World::Width  - 1 ) );
    _rect._top   = std::min( _rect._top,   static_cast< int >( Consts::World::Height - 1 ) );

    return true;
}

/// <summary>
/// 포함 여부를 반환한다
/// </summary>
bool Cluster::Contains( WorldObjectId actorId )
{
    return _objects.contains( actorId );
}

/// <summary>
/// 액터를 반환한다
/// </summary>
inline WorldObjectPtr Cluster::GetWorldObject( WorldObjectId actorId )
{
    auto findIter = _objects.find( actorId );
    if ( findIter == _objects.end() )
        return nullptr;

    return findIter->second->SharedFromWorldObject();
}

/// <summary>
/// 클러스에 퇴장한다
/// </summary>
void Cluster::Leave( WorldObjectId actorId )
{
    _objects.erase( actorId );
    _pcs.erase( actorId );
    _npcs.erase( actorId );
}

/// <summary>
/// 클러스에 입장한다
/// </summary>
void Cluster::Enter( WorldObjectRef object )
{
    _objects.emplace( object->GetObjectId(), object.get() );

    switch ( object->GetWorldObjectType() )
    {
    case EWorldObjectType::Pc:
        _pcs.emplace( object->GetObjectId(), (Pc*)( object.get() ) );

    case EWorldObjectType::Npc:
        _npcs.emplace( object->GetObjectId(), (Npc*)( object.get() ) );
    }

    object->SetClusterIndex( _index );
}
