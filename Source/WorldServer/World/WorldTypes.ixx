export module WorldTypes;

#include "../Macro.h"

import Mala.Math;

EXPORT_BEGIN

using ClusterIndex = Mala::Math::Vector2Int;
using Index        = Mala::Math::Vector2Int;

using WorldPosition = Mala::Math::Vector3;
using Position      = Mala::Math::Vector3;

/// <summary>
/// 월드 타입 정의
/// </summary>
USING_SHARED_PTR( World );

EXPORT_END
