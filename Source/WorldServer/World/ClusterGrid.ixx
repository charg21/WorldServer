export module ClusterGrid;

import <Macro.h>;

import <array>;
import <memory>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Math;

import Cluster;
import WorldObject;
import WorldObjectTypes;
import Character;
import Consts;
import Grid;


using namespace Mala::Container;

EXPORT_BEGIN

/// <summary>
/// 클러스터 배열
/// </summary>
class ClusterGrid
    : public Grid< Cluster, Consts::Cluster::MaxRow + 1, Consts::Cluster::MaxCol + 1 >
{
public:
    using Base         = Grid< Cluster, Consts::Cluster::MaxRow + 1, Consts::Cluster::MaxCol + 1 >;
    using Clusters     = typename Base::Array2dType;
    using ClusterIndex = typename Base::IndexType;

    /// <summary>
    /// 초기화 한다
    /// </summary>
    bool Initialize()
    {
        i32 y = 0;
        for ( auto& clusterList : Base::Get() )
        {
            i32 x = 0;
            for ( auto& cluster : clusterList )
            {
                if ( !cluster.Initialize( ClusterIndex{ x, y } ) )
                    return false;

                x += 1;
            }

            y += 1;
        }

        return true;
    }

    /// <summary>
    /// 특정 클러스에 입장한다
    /// </summary>
    /// <param name="actor"> 액터 </param>
    void Enter( const ClusterIndex& index, WorldObjectRef object )
    {
        Base::Get()[ index._y ][ index._x ].Enter( object );
    }

    /// <summary>
    /// 특정 클러스터에 퇴장한다
    /// </summary>
    void Leave( ClusterIndex index, WorldObjectId id )
    {
        Base::Get()[ index._y ][ index._x ].Leave( id );
    }

    /// <summary>
    /// 클러스터 목록을 반환한다
    /// </summary>
    Clusters& GetClusters()
    {
        return Base::Get();
    }

    /// <summary>
    /// 특정 인덱스의 클러스터를 반환한다
    /// </summary>
    Cluster& GetCluster( i32 y, i32 x )
    {
        return Base::Get()[ y ][ x ];
    }

    /// <summary>
    /// 특정 인덱스의 클러스터를 반환한다
    /// </summary>
    Cluster& GetCluster( ClusterIndex index )
    {
        return Base::Get()[ index._y ][ index._x ];
    }
};

EXPORT_END
