export module World;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Core.TypeCast;
import Mala.Core.CoreTLS;
import Mala.Container;
import Mala.Log.ConsoleLog;

import Mala.Math;
import Mala.Memory;
import Mala.Net.PacketDispatcher;
import Mala.Threading.Lock;
import Mala.Threading.JobExecutor;

import WorldObject;
import WorldObjectTypes;
import Consts;
import WorldGlobal;
import Mala.Threading.GlobalJobExecutor;
import Character;
import Npc;
import Channel;
import WorldTypes;
import ThreadLocal;
import NpcDesignPtr;
import DesignTypes;


using namespace Mala::Container;
using namespace Mala::Math;
using namespace Mala::Threading;

EXPORT_BEGIN

/// <summary>
/// 월드 공간
/// </summary>
class World
{
public:
    /// <summary>
    /// 생성자
    /// </summary>
    World( Channel* channel = nullptr );

    /// <summary>
    /// 월드를 초기화 한다
    /// </summary>
    bool Initialize();

    /// <summary>
    /// 액터를 스폰한다
    /// </summary>
    template< WorldObjectType TWorldObjectType >
    void Spawn( const Position& pos, WorldObjectId objectId );
    void SpawnWorldObject( WorldObjectRef object );
    void SpawnWorldObject( WorldObjectPtr&& object );

    /// <summary>
    /// 액터를 디스폰한다
    /// </summary>
    bool DespawnWorldObject( WorldObjectRef object );

    /// <summary>
    /// 채널 식별자
    /// </summary>
    int GetChannelId() { return _channel->_channelId; }

    /// <summary> 채널 </summary>
    Channel* _channel{};
};

EXPORT_END

template< WorldObjectType TWorldObjectType >
inline void World::Spawn( const Position& pos, WorldObjectId objectId )
{
    auto objectPtr{ MakeShared< TWorldObjectType >() };
    objectPtr->SetWorldObjectId( objectId );
    objectPtr->SetPos( pos );
    objectPtr->SetFromPos( pos );

    //if constexpr ( std::is_same_v< TWorldObjectType, Npc > )
    if ( auto npc = TypeCast< Npc >( objectPtr ) )
    {
        NpcDesignPtr npcDesign{ NpcDesignId{ 1 } };
        npc->_design = npcDesign;
        npc->_npcStat.LoadFromDesign();
    }

    if ( LThreadType == EThreadType::Worker )
        SpawnWorldObject( std::move( objectPtr ) );
    else
        LExecutor->Post( [ MOVE( objectPtr ) ]
        {
            GWorld->SpawnWorldObject( std::move( objectPtr ) );
         } );
}
