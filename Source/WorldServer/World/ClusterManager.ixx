export module ClusterManager;

import <Macro.h>;

import Mala.Core;
import Mala.Container;
import Mala.Log.ConsoleLog;
import Mala.Math;
import Mala.Memory;
import Mala.Threading;

import WorldObject;
import WorldObjectTypes;
import AoiManager;
import Consts;
import Cluster;
import ClusterGrid;
import Grid;
import Npc;
import Pc;
import World;
import ThreadLocal;
import WorldTypes;
import WorldHelper;

import EWorldObjectType;

using namespace Mala::Container;
using namespace Mala::Math;
using namespace Mala::Threading;

EXPORT_BEGIN

/// <summary>
/// 클러스터 관리자
/// </summary>
class ClusterManager
{
    friend class ViewComponent;

public:
    /// <summary>
    /// 초기화 한다
    /// </summary>
    bool Init();

    //void Enter( WorldObjectRef object );
    //void Enter( Index to, WorldObjectRef object );
    //void Leave( Index from, WorldObjectRef object );

    /// <summary>
    /// 특정 클러스터를 반환한다
    /// </summary>
    //Cluster* GetCluster( const ClusterIndex& index );

public:
    ///// <summary>
    ///// 클러스터 그리드
    ///// </summary>
    //ClusterGrid _clusterGrid;

    //inline static Atomic< int > _init;
};

EXPORT_END
