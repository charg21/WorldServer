import <Macro.h>;

import <array>;
import <memory>;
import Mala.Core.EnumHelper;
import Mala.Core.Types;
import Mala.Container;
import Mala.Math;
import Mala.Math.Vector3;

import WorldObject;
import WorldObjectTypes;
import AoiManager;
import Consts;

import ClusterManager;
import WorldTypes;
import WorldHelper;
import ThreadLocal;
import Rect;



using namespace Mala;
using namespace Mala::Math;
using namespace Mala::Container;

Position WorldHelper::GetNextPos( const Position& curPos, EDirection direction )
{
    Position toPos = curPos;

    switch ( direction )
    {
    case EDirection::Up:
        toPos += ( Vector3::Up * 40 );
        break;
    case EDirection::Down:
        toPos += ( Vector3::Down * 40 );
        break;
    case EDirection::Right:
        toPos += ( Vector3::Right * 40 );
        break;
    case EDirection::Left:
        toPos += ( Vector3::Left * 40 );
        break;
    default:
        size_t* invalidPtr{}; *invalidPtr = 02;
        break;
    }

    toPos._x = Mala::Math::Clamp( toPos._x, 0.f, (f32)( Consts::World::Width ) - 1.f );
    toPos._y = Mala::Math::Clamp( toPos._y, 0.f, 799.f );
    toPos._z = Mala::Math::Clamp( toPos._z, 0.f, (f32)( Consts::World::Width ) - 1.f );

    return toPos;
}

std::pair< Position, Direction > WorldHelper::GetNextPos(
    const Position& fromPos,
    const Position& toPos,
          f32       deltaTime,
          f32       moveSpeed )
{
    Position diff = toPos - fromPos;

    // 목표 위치까지의 거리 계산
    f32 distanceToTarget = diff.Size();

    // 목표까지 이동하려는 계산
    f32 moveAmount = moveSpeed * deltaTime;

    // 이동하는 동안 이동 양이 목표 위치까지 거리보다 크지 않도록 제한
    if ( moveAmount > distanceToTarget )
        moveAmount = distanceToTarget;

    // 이동할 거리와 방향을 고려한 이동 벡터
    Position dir = diff.GetNormalize();
    Position moveDir = dir * moveAmount;

    //  이동 벡터로 현재 위치를 업데이트
    Position nextPos = fromPos + moveDir;

    return { nextPos, Direction( dir._x, dir._z ) };
}

Index WorldHelper::GetClusterIndex( const Position& position )
{
    using namespace Consts::Cluster;

    const auto normalClusterZ = std::min( ( position._z / Width  ), (f32)( MaxRow ) );
    const auto normalClusterX = std::min( ( position._x / Height ), (f32)( MaxCol ) );

    return { normalClusterX, normalClusterZ };
}

Index WorldHelper::GetAoiIndex( Position pos )
{
    using namespace Consts::Aoi;

    const auto aoiClusterZ = std::min( ( pos._z / (f64)( Width ) ), (f64)( MaxRow ) );
    const auto aoiClusterX = std::min( ( pos._x / (f64)( Height ) ), (f64)( MaxCol ) );

    return { (i32)aoiClusterX, (i32)aoiClusterZ };
}

bool WorldHelper::ValidateAoiIndex( Index nearClusterIndex )
{
    if ( nearClusterIndex._x < 0 || nearClusterIndex._x >= Consts::Aoi::MaxCol )
        return false;

    if ( nearClusterIndex._y < 0 || nearClusterIndex._y >= Consts::Aoi::MaxRow )
        return false;

    return true;
}


struct CollisionHelper
{
public:
    static bool IsOverlapped( const Box& rect, const Mala::Math::Circle& circle )
    {
        f32 closestX = Clamp< float >( rect._left, circle._center._x, ( float )rect._right );
        f32 closestY = Clamp< float >( rect._top, circle._center._z, (float)rect._bottom );

        f32 distanceX = circle._center._x - closestX;
        f32 distanceY = circle._center._z - closestY;

        f32 distanceSquared = ( distanceX * distanceX ) + ( distanceY * distanceY );

        return distanceSquared < ( circle._radius * circle._radius );
    }
};

AoiClusters& WorldHelper::GatherAoiClusterList(
    const Position& pos,
          f32       radius )
{
    static thread_local AoiClusters clusterList;
    clusterList.Reset();

    Circle circle( pos, radius );
    auto& aoiClusterList = LAoiManager->GetAoiClusterList( pos );
    for ( AoiCluster* aoiCluster : aoiClusterList )
    {
        if ( CollisionHelper::IsOverlapped( aoiCluster->_rect, circle ) )
            clusterList._clusters.push_back( aoiCluster );
    }

    return clusterList;
}

WorldHelper::ClusterWorldObjectList& WorldHelper::GatherWorldObjectList(
    const Position&        fromPos,
          f32              radius,
          EWorldObjectType objectType )
{
    static thread_local ClusterWorldObjectList gatherList( Consts::World::DefaultGatherListCapacity );
    gatherList.clear();

    const auto& clusterList = GatherAoiClusterList( fromPos, radius );
    for ( auto* cluster : clusterList._clusters )
    {
        if ( EnumHelper< EWorldObjectType >::HasFlag( objectType, EWorldObjectType::Pc ) )
        {
            for( auto& [ _, pc ] : cluster->_pcs )
            {
                gatherList.push_back( pc );
            }
        }

        if ( EnumHelper< EWorldObjectType >::HasFlag( objectType, EWorldObjectType::Npc ) )
        {
            for( auto& [ _, npc ] : cluster->_npcs )
            {
                gatherList.push_back( npc );
            }
        }
    }

    return gatherList;
}
