export module NearClusterViewGrid;

import <Macro.h>;

import <memory>;

import Mala.Core.Crash;
import Mala.Core.Types;
import Mala.Container;
import Mala.Math;

import WorldObjectTypes;
import Cluster;
import ClusterGrid;
import NearClusterView;
import Rect;
import Consts;
import Grid;
import WorldTypes;

class NearClusterView;
using namespace Mala::Container;
using namespace Mala::Core;
using namespace Mala::Math;

EXPORT_BEGIN

/// <summary>
/// 주변 클러스터 캐싱 목록을 좌표로 매핑하여 접근하는 클래스
/// </summary>
class NearClusterViewGrid
    : public Grid< NearClusterView, Consts::World::Height, Consts::World::Width >
{
    inline static constexpr size_t Size{ sizeof( Cluster ) * Height * Width };

    using NearClusterViews = Grid::Array2dType;

public:
    /// <summary>
    /// 초기화 한다
    /// </summary>
    bool Initialize( ClusterGrid& ClusterGrid )
    {
        const std::pair< i32, i32 > clusterAccessPolicy[] =
        {
            { -1, -1 }, { -1, 0 }, { -1, 1  },
            {  0, -1 }, {  0, 0 }, {  0, 1  },
            {  1, -1 }, {  1, 0 }, {  1, 1  },
        };

        constexpr size_t halfFov = Consts::View::HalfFov;

        auto& views = Get();

        for( i32 z{}; z < Height; z += 1 )
        {
            for ( i32 x{}; x < Width; x += 1 )
            {
                NearClusterView& clusterView = views[ z ][ x ];
                if( !clusterView.Initialize( Index( x, z ) ) )
                    return false;

                const Rect viewRect{ x - halfFov, x + halfFov, z - halfFov, z + halfFov };

                for ( const auto& [ yOffset, xOffset ] : clusterAccessPolicy )
                {
                    i32 curZ = ( z / Consts::Cluster::Height ) + yOffset;
                    i32 curX = ( x / Consts::Cluster::Width  ) + xOffset;

                    if ( IsOutOfRangeForClusterIndex( curZ, curX ) )
                        continue;

                    Cluster& cluster = ClusterGrid.GetCluster( curZ, curX );

                    if ( Rect::IsOverlapped( viewRect, cluster._rect ) )
                    {
                        clusterView.push_back( &cluster );
                        Mala::Core::CrashIfFalse( clusterView.size() < ( Consts::Cluster::MaxNearCount + 1 ) );
                    }
                }
            }
        }

        return true;
    }

    /// <summary>
    /// y x가 클러스터 인덱스 범위를 넘어가는지 여부를 반환한다
    /// </summary>
    static bool IsOutOfRangeForClusterIndex( i32 z, i32 x )
    {
        if ( ( z < 0 ) || ( z > Consts::Cluster::MaxRow ) )
            return true;

        if ( ( x < 0 ) || ( x > Consts::Cluster::MaxCol ) )
            return true;

        return false;
    }

    /// <summary>
    /// 지정 좌표의 클러스터 캐싱 목록을 반환한다
    /// </summary>
    NearClusterView& GetNearClusterView( i32 z, i32 x )
    {
        return Get()[ z ][ x ];
    }

private:
    NearClusterViews _sectorViews;
};


EXPORT_END