import <Macro.h>;

import Mala.Core.Crash; ///
import Mala.Log.ConsoleLog;
import Mala.Core.Random; ///
import Mala.Core.Types;
import Mala.Core.TypeCast;
import Mala.Core.CoreGlobal;
import Mala.Core.Format;
import Mala.Container;
import Mala.Net.PacketDispatcher;
import Mala.Math;
import Mala.Threading;
import Mala.Memory;

import WorldObject;
import WorldObjectTypes;
import AoiManager;
import Consts;
import Character;
import ClusterManager;
import Config;
import NearClusterView;
import NearClusterViewGrid;
import World;
import ThreadLocal;
import Npc;
import Pc;
import WorldTypes;
import WorldHelper;

import PktVector3; ///

using namespace Mala::Core;
using namespace Mala::Container;
using namespace Mala::Math;
using namespace Mala::Threading;


/// <summary>
/// 월드를 초기화 한다
/// </summary>
bool ClusterManager::Init()
{
    //if ( !_clusterGrid.Initialize() )
    //{
    //    ERROR_LOG( L"ClusterGrid 초기화 실패..." );
    //    return false;
    //}

    //INFO_LOG( L"Npc Init Ok... %d [ %d ~ %d ] ", LThreadId, ( npcCountPerThread * ( LThreadId -1 ) ), ( npcCountPerThread * ( LThreadId - 1 ) ) + npcCountPerThread );

    return true;
}

//NearClusterView ClusterManager::GetNearClusterView( const Position& position )
//{
//    auto& nearClusterView =
//        ClusterManager::_clusterViewGrid.GetNearClusterView( position._z, position._x );
//
//    NearClusterView ncv;
//    for ( Cluster* nearCluster : nearClusterView )
//        ncv.push_back( LClusterManager->GetCluster( nearCluster->_index ) );
//
//    return ncv;
//}

///// <summary>
///// 특정 클러스터를 반환한다
///// </summary>
//Cluster* ClusterManager::GetCluster( const ClusterIndex& index )
//{
//    return &_clusterGrid.GetCluster( index._y, index._x );
//}
