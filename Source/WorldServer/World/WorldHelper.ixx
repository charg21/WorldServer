export module WorldHelper;

import <Macro.h>;

import Mala.Core.Types;
import Mala.Math;

import WorldObjectTypes;
import WorldTypes;
import EDirection;
import EWorldObjectType;

using namespace Mala::Math;
using namespace Mala::Container;

EXPORT_BEGIN

struct AoiCluster;

class AoiClusters
{
public:
    AoiClusters() : _clusters{ 18 }
    {
        _clusters.clear();
    }
	~AoiClusters() = default;

    void Reset()
    {
        _clusters.clear();
    }

public:
    Vector< AoiCluster* > _clusters;
};

struct WorldHelper
{
public:
    /// <summary>
    /// 액터 목록 타입 정의
    /// </summary>
    using ClusterWorldObjectList = Vector< WorldObject* >;

    inline static const Index ClusterAccessPolicy[ 9 ]
    {
        { -1, -1 }, { -1, 0 }, { -1, 1  },
        {  0, -1 }, {  0, 0 }, {  0, 1  },
        {  1, -1 }, {  1, 0 }, {  1, 1  },
    };

public:
	/// <summary>
	/// 다음 위치를 반환한다
	/// </summary>
	static Position GetNextPos( const Position& curPos, EDirection direction );
    static std::pair< Position, Direction > GetNextPos(
        const Position& fromPos,
        const Position& toPos,
              f32       deltaTime,
              f32       moveSpeed = 1.0f );

	static ClusterIndex GetClusterIndex( const Position& position );
    static Index GetAoiIndex( Position pos );
    static bool ValidateAoiIndex( Index index );
    static AoiClusters& GatherAoiClusterList( const Position& fromPos, f32 radius );
    static ClusterWorldObjectList& GatherWorldObjectList(
        const Position&        fromPos,
              f32              radius,
              EWorldObjectType objectType = EWorldObjectType::All );
};


EXPORT_END
