import <Macro.h>;

import Mala.Core;
import Mala.Core.CoreGlobal;
import Mala.Log.ConsoleLog;
import Mala.Core.Format;
import Mala.Container;
import Mala.Math;
import Mala.Threading;
import Mala.Memory;

import WorldObject;
import WorldObjectManager;
import WorldObjectTypes;
import AoiManager;
import Consts;
import Character;
import ClusterJob;
import ClusterManager;
import Config;
import World;
import ThreadLocal;
import WorldTypes;
import WorldHelper;

using namespace Mala::Core;
using namespace Mala::Container;
using namespace Mala::Math;
using namespace Mala::Threading;


IGlobalJob* ClusterEnterJob::Make(
    WorldObjectPtr&&     object,
    EClusterChangeReason reason )
{
    auto index = WorldHelper::GetClusterIndex( object->GetPos() );
    object->SetClusterIndex( index );

	return ::MakeGlobalLambdaJob( [ index, MOVE( object ), reason ]
	{
        LWorldObjectManager->Register( object->GetObjectId(), object );
		//LClusterManager->GetCluster( index )->Enter( object );
	} );
}

IGlobalJob* ClusterLeaveJob::Make( WorldObjectPtr&& object )
{
    return ::MakeGlobalLambdaJob( [ MOVE( object ) ]
    {
        //LClusterManager->GetCluster( object->GetClusterIndex() )->Leave( object->GetObjectId() );
        LWorldObjectManager->Unregister( object->GetObjectId() );
    } );
}

IGlobalJob* ClusterSwitchJob::Make(
    ClusterIndex     from,
    ClusterIndex     to,
    WorldObjectPtr&& object )
{
	return ::MakeGlobalLambdaJob( [ from, to, MOVE( object ) ]
    {
    	//LClusterManager->GetCluster( from )->Leave( object->GetObjectId() );
    	//LClusterManager->GetCluster( to )->Enter( object );
    } );
}
