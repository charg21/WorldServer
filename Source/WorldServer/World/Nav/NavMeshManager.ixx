
#include <format>;
#include "recast/RecastHelpers.h"
#include "recast/Detour/DetourNavMeshQuery.h"
export module NavMeshManager;

import <Macro.h>;
import <array>;
import <memory>;
import <iostream>;


import Mala.Core;
import Mala.Log.ConsoleLog;
import Mala.Container;
import Mala.Math;
import Mala.Text;

import WorldObject;
import WorldObjectTypes;
import Consts;
import Character;
import WorldHelper;
import WorldTypes;
import EWorldObjectType;
import WorldGlobal;

using namespace Mala;
using namespace Mala::Container;
using namespace Mala::Math;
using namespace Mala::Threading;


EXPORT_BEGIN

class NavMeshLoader
{
public:
    dtNavMesh* Load( const String& navMeshPath )
    {
        auto path{ Mala::Text::Utf16::ToUtf8( navMeshPath ) };
        return RecastHelpers::DeSerializedtNavMesh( path.c_str() );
    }
};


using dtNavMeshQueryPtr = std::unique_ptr< dtNavMeshQuery >;

/// <summary>
/// 네비 메시 관리자
/// </summary>
class NavMeshManager
{
public:
	~NavMeshManager()
	{
		if ( _navMesh )
			dtFreeNavMesh( _navMesh );
	}

    bool Load( const String& navMeshPath )
    {
        _navMesh = NavMeshLoader{}.Load( navMeshPath );
        return _navMesh;
    }

    dtNavMesh* GetNavMesh() const { return _navMesh; }
    static dtNavMeshQuery* GetNavMeshQuery()
    {
        if ( !_navMeshQuery )
            _navMeshQuery = std::make_unique< dtNavMeshQuery >();

        _navMeshQuery->init( GNavMeshManager->GetNavMesh(), 2048 );
        return _navMeshQuery.get();
    }

    void Test()
    {
        RecastHelpers::FVector3 result{ 1547, 1963, 200 };
        RecastHelpers::FVector3 start{ 1547, 1963, 200 };
        //RecastHelpers::FVector3 start{ 259, 270, 5 };
        RecastHelpers::FVector3 end{ 672, 654, 5 };
        std::vector< RecastHelpers::FVector3 > paths;
        auto* mesh{ GetNavMesh() };
        RecastHelpers::findStraightPath( GetNavMesh(), start, end, paths );
        RecastHelpers::findStraightPath2( GetNavMesh(), { -1005, 294, 0 }, { -761, 495, 0 } );

        dtQueryResult StringPullResult;
        //std::array< dtPolyRef, 100 > polys;
  //      dtNavMeshQuery{}.findStraightPath( &start.X, &end.X,
  //          (dtPolyRef*)&polys, 100, StringPullResult, DT_STRAIGHTPATH_AREA_CROSSINGS );

        dtNavMeshQuery query;
        query.init( GetNavMesh(), 65536 );

        dtPolyRef StartPoly = 0;
        dtQueryFilter filter{};
        auto start2 = RecastHelpers::Unreal2RecastPoint( end );
        auto end2 = RecastHelpers::Unreal2RecastPoint( RecastHelpers::FVector3{ 50, 50, 50 } );
        query.findNearestPoly( &start2.X, &end2.X, &filter, &StartPoly, nullptr );

        std::array< dtPolyRef, 100 > polys{};
        polys[ 0 ] = StartPoly;
        query.findStraightPath( &start.X, &end.X, (dtPolyRef*)&polys, 100, StringPullResult, DT_STRAIGHTPATH_AREA_CROSSINGS );

        if ( RecastHelpers::dtIsValidNavigationPoint( mesh, result ) )
        {
            std::cout << "유효한 네비게이션 포인트입니다.";
        }
        else
        {
            std::cout << "유효하지 안흔 네비게이션 포인트입니다.";
        }

        dtFreeNavMesh( mesh );
    }

private:
    dtNavMesh* _navMesh{};
    inline static dtNavMeshQueryPtr _navMeshQuery{};
};


EXPORT_END
